﻿using Mineros.Data.Models;
using DotNetNuke.Common;
using DotNetNuke.Entities.Portals;
using DotNetNuke.Entities.Users;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.FileSystem;
using DotNetNuke.Services.Log.EventLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Caching;
using Mineros.Model;

namespace Mineros.Data
{
    public class EasyDnnNewsData
    {
        private Cache categoriesCache = new Cache();
        private Cache newsCache = new Cache();
        private Cache documentsCache = new Cache();

        private string connection
        {
            get
            {
                //TODO: Cambiar conexión para pruebas
#if DEBUG
                return ConfigurationManager.ConnectionStrings["mnidbccq01"].ConnectionString;
#else
                return ConfigurationManager.ConnectionStrings["SiteSqlServer"].ConnectionString;
#endif
            }
        }

        public List<EasyDnnNewsCategories> GetCategoriesArgosReportes(string localeCode)
        {
            List<EasyDnnNewsCategories> categories = new List<EasyDnnNewsCategories>();
            try
            {
                categories = HttpContext.Current.Cache["Categories"] as List<EasyDnnNewsCategories>;

                //if (HttpContext.Current.Cache["Categories"] == null)
                if (categories == null)
                {
                    //string connection = ConfigurationManager.ConnectionStrings["SiteSqlServer"].ConnectionString;

                    using (SqlConnection con = new SqlConnection(connection))
                    {
                        con.Open();
                        using (SqlCommand command = new SqlCommand("SpGetCategoriesArgosReportes", con))
                        {
                            command.CommandType = CommandType.StoredProcedure;

                            SqlParameter paramLocaleCode = new SqlParameter
                            {
                                ParameterName = "@localeCode"
                            };

                            paramLocaleCode.Value = localeCode;
                            command.Parameters.Add(paramLocaleCode);


                            PortalSettings portalSettings = Globals.GetPortalSettings();

                            int portalid = portalSettings.PortalId;
                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    EasyDnnNewsCategories category = new EasyDnnNewsCategories
                                    {
                                        CategoryID = (int)reader["CategoryID"],
                                        PortalID = (int)reader["PortalID"],
                                        CategoryName = reader["CategoryName"].ToString(),
                                        Position = (int)reader["Position"],
                                        Level = (int)reader["Level"]
                                    };


                                    if (reader["ParentCategory"] != DBNull.Value)
                                    {
                                        category.ParentCategory = (int)reader["ParentCategory"];
                                    }

                                    if (reader["CategoryText"] != DBNull.Value)
                                    {
                                        category.Description = HttpUtility.HtmlDecode(reader["CategoryText"].ToString());
                                    }


                                    categories.Add(category);
                                }
                            }
                        }
                    }

                    HttpContext.Current.Cache.Insert("Categories", categories);
                }
                //else
                //{
                //    categories = HttpContext.Current.Cache["Categories"] as List<EasyDnnNewsCategories>;
                //}

                return categories;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<EasyDnnNewsCategories> GetCategories(string localeCode)
        {
            List<EasyDnnNewsCategories> categories = new List<EasyDnnNewsCategories>();
            try
            {
                categories = categoriesCache["Categories"] as List<EasyDnnNewsCategories>;


                if (categories == null || (categories != null && categories.Count < 1))
                {
                    categories = new List<EasyDnnNewsCategories>();
                    //string connection = ConfigurationManager.ConnectionStrings["SiteSqlServer"].ConnectionString;

                    using (SqlConnection con = new SqlConnection(connection))
                    {
                        con.Open();
                        using (SqlCommand command = new SqlCommand("SpGetCategoriesArgosReportes", con))
                        {
                            command.CommandType = CommandType.StoredProcedure;

                            SqlParameter paramLocaleCode = new SqlParameter
                            {
                                ParameterName = "@localeCode"
                            };

                            paramLocaleCode.Value = localeCode;
                            command.Parameters.Add(paramLocaleCode);

                            PortalSettings portalSettings = Globals.GetPortalSettings();
                            int portalid = portalSettings.PortalId;

                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    EasyDnnNewsCategories category = new EasyDnnNewsCategories
                                    {
                                        CategoryID = (int)reader["CategoryID"],
                                        PortalID = (int)reader["PortalID"],
                                        CategoryName = reader["CategoryName"].ToString(),
                                        Position = (int)reader["Position"],
                                        Level = (int)reader["Level"]
                                    };

                                    if (reader["ParentCategory"] != DBNull.Value)
                                    {
                                        category.ParentCategory = (int)reader["ParentCategory"];
                                    }

                                    if (reader["CategoryText"] != DBNull.Value)
                                    {
                                        category.Description = HttpUtility.HtmlDecode(reader["CategoryText"].ToString());
                                    }

                                    categories.Add(category);
                                }
                            }
                        }
                    }

                    // TODO: Cambiar caché por días
#if DEBUG
                    categoriesCache.Insert("Categories", categories, null, DateTime.Now.AddSeconds(30), Cache.NoSlidingExpiration);
#else
                    categoriesCache.Insert("Categories", categories, null, DateTime.Now.AddSeconds(30), Cache.NoSlidingExpiration);
#endif
                }
                //else
                //{
                //    categories = categoriesCache["Categories"] as List<EasyDnnNewsCategories>;
                //}

                return categories;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<EasyDnnNewsCategories> GetAllCategoriesNews(string localeCode)
        {
            List<EasyDnnNewsCategories> categories = new List<EasyDnnNewsCategories>();
            PortalSettings portalSettings = Globals.GetPortalSettings();
            int portalid = portalSettings.PortalId;

            try
            {
                categories = categoriesCache["Categories"] as List<EasyDnnNewsCategories>;

                if (categories == null || (categories != null && categories.Count < 1))
                {
                    categories = new List<EasyDnnNewsCategories>();
                    string connection = ConfigurationManager.ConnectionStrings["SiteSqlServer"].ConnectionString;

                    using (SqlConnection con = new SqlConnection(connection))
                    {
                        con.Open();
                        using (SqlCommand command = new SqlCommand("GetAllCategoriesNews", con))
                        {
                            command.CommandType = CommandType.StoredProcedure;

                            // TODO: descomentar y colocar el PortalID correcto
#if DEBUG
                            command.Parameters.Add(new SqlParameter("PortalID", portalid));
#else
                            command.Parameters.Add(new SqlParameter("PortalID", portalid));
#endif

                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    EasyDnnNewsCategories category = new EasyDnnNewsCategories
                                    {
                                        CategoryNameLocalize = reader["CategoryNameLocale"].ToString(),
                                        DescriptionLocalize = HttpUtility.HtmlDecode(reader["DescriptionLocalize"].ToString()),
                                        CategoryID = (int)reader["CategoryID"],
                                        PortalID = (int)reader["PortalID"],
                                        CategoryName = reader["CategoryName"].ToString(),
                                        Position = (int)reader["Position"],
                                        Level = (int)reader["Level"],
                                        LocaleCode = reader["LocaleCode"].ToString()
                                    };

                                    if (reader["ParentCategory"] != DBNull.Value)
                                    {
                                        category.ParentCategory = (int)reader["ParentCategory"];
                                    }

                                    if (reader["CategoryText"] != DBNull.Value)
                                    {
                                        category.Description = HttpUtility.HtmlDecode(reader["CategoryText"].ToString());
                                    }
                                    else
                                        category.Description = string.Empty;

                                    categories.Add(category);
                                }
                            }
                        }
                    }

                    // TODO: Cambiar caché por días
#if DEBUG
                    categoriesCache.Insert("Categories", categories, null, DateTime.Now.AddSeconds(30), Cache.NoSlidingExpiration);
#else
                    categoriesCache.Insert("Categories", categories, null, DateTime.Now.AddSeconds(30), Cache.NoSlidingExpiration);
#endif
                }
                //else
                //{
                //    categories = categoriesCache["Categories"] as List<EasyDnnNewsCategories>;
                //}
                return categories;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<EasyDnnNewsCategories> GetCategoriesByParent(string parentCategoryName, int? parentCategoryId, string localeCode, string orderByCategoryName = "asc")
        {
            try
            {
                List<EasyDnnNewsCategories> categories = new List<EasyDnnNewsCategories>();

                //string connection = ConfigurationManager.ConnectionStrings["SiteSqlServer"].ConnectionString;

                using (SqlConnection con = new SqlConnection(connection))
                {
                    con.Open();
                    using (SqlCommand command = new SqlCommand("SpGetCategoriesByParent", con))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        ///Paràmetro Categoria///
                        SqlParameter paramParentCategoryName = new SqlParameter
                        {
                            ParameterName = "@ParentCategoryName"
                        };
                        if (!string.IsNullOrEmpty(parentCategoryName))
                            paramParentCategoryName.Value = parentCategoryName;
                        else
                            paramParentCategoryName.Value = DBNull.Value;

                        paramParentCategoryName.IsNullable = true;

                        command.Parameters.Add(paramParentCategoryName);

                        SqlParameter paramLocaleCode = new SqlParameter
                        {
                            ParameterName = "@localeCode"
                        };

                        paramLocaleCode.Value = localeCode;
                        command.Parameters.Add(paramLocaleCode);

                        SqlParameter paramParentCategoryId = new SqlParameter
                        {
                            ParameterName = "@ParentCategoryId"
                        };
                        if (parentCategoryId != null)
                            paramParentCategoryId.Value = parentCategoryId;
                        else
                            paramParentCategoryId.Value = DBNull.Value;

                        paramParentCategoryId.IsNullable = true;

                        command.Parameters.Add(paramParentCategoryId);


                        SqlParameter paramOrderByCategoryName = new SqlParameter
                        {
                            ParameterName = "@OrderByCategoryName"
                        };
                        if (parentCategoryId != null)
                            paramOrderByCategoryName.Value = parentCategoryId;
                        else
                            paramOrderByCategoryName.Value = DBNull.Value;

                        paramOrderByCategoryName.IsNullable = true;

                        command.Parameters.Add(paramOrderByCategoryName);


                        PortalSettings portalSettings = Globals.GetPortalSettings();

                        int portalid = portalSettings.PortalId;
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                EasyDnnNewsCategories category = new EasyDnnNewsCategories
                                {
                                    CategoryID = (int)reader["CategoryID"],
                                    PortalID = (int)reader["PortalID"],
                                    CategoryName = reader["CategoryName"].ToString(),
                                    Position = (int)reader["Position"],
                                    Level = (int)reader["Level"]
                                };
                                if (reader["CategoryText"] != DBNull.Value)
                                {
                                    category.Description = HttpUtility.HtmlDecode(reader["CategoryText"].ToString());

                                }


                                if (reader["ParentCategory"] != DBNull.Value)
                                {
                                    category.ParentCategory = (int)reader["ParentCategory"];
                                }

                                categories.Add(category);
                            }
                        }


                    }
                }

                return categories;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<EasyDnnNewsApp> GetArticles(int? parentCategory, int? categoryId, int page, int pageSize, ref int countItems, string localeCode)
        {
            List<EasyDnnNewsApp> news = new List<EasyDnnNewsApp>();
            DateTime today = DateTime.Today;
            try
            {
                string connection = ConfigurationManager.ConnectionStrings["SiteSqlServer"].ConnectionString;
                using (SqlConnection con = new SqlConnection(connection))
                {
                    con.Open();
                    using (SqlCommand command = new SqlCommand("Sp_GetArticles", con))
                    {
                        command.CommandType = CommandType.StoredProcedure;


                        SqlParameter paramPage = new SqlParameter
                        {
                            ParameterName = "page",
                            Value = page
                        };
                        command.Parameters.Add(paramPage);


                        SqlParameter parampageSize = new SqlParameter
                        {
                            ParameterName = "pageSize",
                            Value = pageSize
                        };
                        command.Parameters.Add(parampageSize);


                        SqlParameter paramCountItems = new SqlParameter("countItems", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output,
                            Value = 0
                        };
                        command.Parameters.Add(paramCountItems);


                        SqlParameter paramDate = new SqlParameter
                        {
                            ParameterName = "nowDate",
                            Value = today
                        };
                        command.Parameters.Add(paramDate);
                        ///Paràmetro Categoria///
                        SqlParameter paramParentCategory = new SqlParameter
                        {
                            ParameterName = "parentCategory",
                            IsNullable = true
                        };

                        if (parentCategory != null)
                        {
                            paramParentCategory.Value = parentCategory;
                        }
                        else
                        {
                            paramParentCategory.Value = DBNull.Value;
                        }
                        command.Parameters.Add(paramParentCategory);

                        ///Parámetro title///
                        SqlParameter paramCategoryId = new SqlParameter
                        {
                            ParameterName = "categoryId",
                            IsNullable = true
                        };

                        if (categoryId != null)
                        {
                            paramCategoryId.Value = categoryId;
                        }
                        else
                        {
                            paramCategoryId.Value = DBNull.Value;
                        }
                        command.Parameters.Add(paramCategoryId);

                        PortalSettings portalSettings = Globals.GetPortalSettings();
                        int portalid = portalSettings.PortalId;

                        command.Parameters.Add(new SqlParameter("localeCode", localeCode));

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                EasyDnnNewsApp newInfo = new EasyDnnNewsApp
                                {
                                    ArticleID = (int)reader["ArticleID"],
                                    Title = reader["Title"].ToString()
                                };
                                if (reader["PortalID"] != DBNull.Value)
                                {
                                    newInfo.PortalID = (int)reader["PortalID"];
                                }
                                if (reader["ParentCategory"] != DBNull.Value)
                                {
                                    newInfo.ParentCategory = (int)reader["ParentCategory"];
                                }

                                if (reader["ModuleID"] != DBNull.Value)
                                {
                                    newInfo.ModuleID = (int)reader["ModuleID"];
                                }

                                if (reader["SubTitle"] != DBNull.Value)
                                {
                                    newInfo.SubTitle = reader["SubTitle"].ToString();
                                }

                                if (reader["Summary"] != DBNull.Value)
                                {
                                    newInfo.Summary = reader["Summary"].ToString();
                                }

                                if (reader["Article"] != DBNull.Value)
                                {
                                    newInfo.Article = HttpUtility.HtmlDecode(reader["Article"].ToString());
                                }

                                if (reader["ArticleImage"] != DBNull.Value)
                                {
                                    newInfo.ArticleImage = reader["ArticleImage"].ToString();
                                }
                                if (reader["DateAdded"] != DBNull.Value)
                                {
                                    newInfo.DateAdded = (DateTime)reader["DateAdded"];
                                }
                                TimeZoneInfo tz = TimeZoneInfo.FindSystemTimeZoneById("SA Pacific Standard Time");
                                if (reader["PublishDate"] != DBNull.Value)
                                {
                                    newInfo.PublishDate = TimeZoneInfo.ConvertTimeToUtc((DateTime)reader["PublishDate"], tz);
                                }

                                if (reader["CategoryID"] != DBNull.Value)
                                {
                                    newInfo.CategoryID = (int)reader["CategoryID"];
                                }

                                if (reader["CategoryName"] != DBNull.Value)
                                {
                                    newInfo.CategoryName = reader["CategoryName"].ToString();
                                }

                                if (reader["Featured"] != DBNull.Value)
                                {
                                    newInfo.Featured = (bool)reader["Featured"];
                                }

                                if (reader["HideDefaultLocale"] != DBNull.Value)
                                {
                                    newInfo.HideDefaultLocale = (bool)reader["HideDefaultLocale"];
                                }
                                if (reader["ExpireDate"] != DBNull.Value)
                                {
                                    newInfo.ExpireDate = (DateTime)reader["ExpireDate"];
                                }
                                news.Add(newInfo);
                            }
                        }

                        countItems = (int)command.Parameters["countItems"].Value;
                    }
                }
                if (localeCode == "es-ES")
                {
                    return news.Where(s => s.HideDefaultLocale == false).ToList();
                }
                else
                {
                    return news;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<EasyDnnNewsAppJobs> GetArticlesJobs(int? parentCategory, int? categoryId, int page, int pageSize, ref int countItems, string localeCode)
        {
            List<EasyDnnNewsAppJobs> news = new List<EasyDnnNewsAppJobs>();
            DateTime today = DateTime.Today;
            try
            {
                string connection = ConfigurationManager.ConnectionStrings["SiteSqlServer"].ConnectionString;
                using (SqlConnection con = new SqlConnection(connection))
                {
                    con.Open();
                    using (SqlCommand command = new SqlCommand("Sp_GetArticles", con))
                    {
                        command.CommandType = CommandType.StoredProcedure;


                        SqlParameter paramPage = new SqlParameter
                        {
                            ParameterName = "page",
                            Value = page
                        };
                        command.Parameters.Add(paramPage);
                        SqlParameter paramDate = new SqlParameter
                        {
                            ParameterName = "nowDate",
                            Value = today
                        };
                        command.Parameters.Add(paramDate);
                        SqlParameter parampageSize = new SqlParameter
                        {
                            ParameterName = "pageSize",
                            Value = pageSize
                        };
                        command.Parameters.Add(parampageSize);


                        SqlParameter paramCountItems = new SqlParameter("countItems", SqlDbType.Int)
                        {
                            Direction = ParameterDirection.Output,
                            Value = 0
                        };
                        command.Parameters.Add(paramCountItems);

                        ///Paràmetro Categoria///
                        SqlParameter paramParentCategory = new SqlParameter
                        {
                            ParameterName = "parentCategory",
                            IsNullable = true
                        };

                        if (parentCategory != null)
                        {
                            paramParentCategory.Value = parentCategory;
                        }
                        else
                        {
                            paramParentCategory.Value = DBNull.Value;
                        }
                        command.Parameters.Add(paramParentCategory);

                        ///Parámetro title///
                        SqlParameter paramCategoryId = new SqlParameter
                        {
                            ParameterName = "categoryId",
                            IsNullable = true
                        };

                        if (categoryId != null)
                        {
                            paramCategoryId.Value = categoryId;
                        }
                        else
                        {
                            paramCategoryId.Value = DBNull.Value;
                        }
                        command.Parameters.Add(paramCategoryId);

                        PortalSettings portalSettings = Globals.GetPortalSettings();
                        int portalid = portalSettings.PortalId;

                        command.Parameters.Add(new SqlParameter("localeCode", localeCode));

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                EasyDnnNewsAppJobs newInfo = new EasyDnnNewsAppJobs
                                {
                                    ArticleID = (int)reader["ArticleID"],
                                    Title = reader["Title"].ToString()
                                };
                                if (reader["PortalID"] != DBNull.Value)
                                {
                                    newInfo.PortalID = (int)reader["PortalID"];
                                }
                                if (reader["ParentCategory"] != DBNull.Value)
                                {
                                    newInfo.ParentCategory = (int)reader["ParentCategory"];
                                }

                                if (reader["ModuleID"] != DBNull.Value)
                                {
                                    newInfo.ModuleID = (int)reader["ModuleID"];
                                }

                                if (reader["SubTitle"] != DBNull.Value)
                                {
                                    newInfo.SubTitle = reader["SubTitle"].ToString();
                                }

                                if (reader["Summary"] != DBNull.Value)
                                {
                                    newInfo.Summary = reader["Summary"].ToString();
                                }

                                if (reader["Article"] != DBNull.Value)
                                {
                                    newInfo.Article = HttpUtility.HtmlDecode(reader["Article"].ToString());
                                }

                                if (reader["ArticleImage"] != DBNull.Value)
                                {
                                    newInfo.ArticleImage = reader["ArticleImage"].ToString();
                                }
                                if (reader["DateAdded"] != DBNull.Value)
                                {
                                    newInfo.DateAdded = (DateTime)reader["DateAdded"];
                                }

                                if (reader["PublishDate"] != DBNull.Value)
                                {
                                    newInfo.PublishDate = (DateTime)reader["PublishDate"];
                                }

                                if (reader["CategoryID"] != DBNull.Value)
                                {
                                    newInfo.CategoryID = (int)reader["CategoryID"];
                                }

                                if (reader["CategoryName"] != DBNull.Value)
                                {
                                    newInfo.CategoryName = reader["CategoryName"].ToString();
                                }

                                if (reader["Featured"] != DBNull.Value)
                                {
                                    newInfo.Featured = (bool)reader["Featured"];
                                }

                                if (reader["HideDefaultLocale"] != DBNull.Value)
                                {
                                    newInfo.HideDefaultLocale = (bool)reader["HideDefaultLocale"];
                                }
                                if (reader["CountryNews"] != DBNull.Value)
                                {
                                    newInfo.CountryNews = reader["CountryNews"].ToString();
                                }
                                if (reader["CityNews"] != DBNull.Value)
                                {
                                    newInfo.CityNews = reader["CityNews"].ToString();
                                }
                                if (reader["EnterpriseNews"] != DBNull.Value)
                                {
                                    newInfo.EnterpriseNews = reader["EnterpriseNews"].ToString();
                                }
                                if (reader["ProccessInterest"] != DBNull.Value)
                                {
                                    newInfo.ProccessInterest = reader["ProccessInterest"].ToString();
                                }
                                if (reader["Locations"] != DBNull.Value)
                                {
                                    newInfo.Locations = reader["Locations"].ToString();
                                }
                                if (reader["LevelPosition"] != DBNull.Value)
                                {
                                    newInfo.LevelPosition = reader["LevelPosition"].ToString();
                                }
                                if(reader["ExpireDate"] != DBNull.Value)
                                {
                                    newInfo.ExpireDate = (DateTime)reader["ExpireDate"];
                                }
                                news.Add(newInfo);
                            }
                        }

                        countItems = (int)command.Parameters["countItems"].Value;
                    }
                }


                if (localeCode == "es-ES")
                {
                    return news.Where(s => s.HideDefaultLocale == false && s.ExpireDate >= today).ToList();
                }
                else
                {
                    return news.Where(s => s.ExpireDate >= today).ToList();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<EasyDnnNewsApp> GetArticleNews()
        {
            PortalSettings portalSettings = Globals.GetPortalSettings();
            int portalid = portalSettings.PortalId;
            List<EasyDnnNewsApp> news = new List<EasyDnnNewsApp>();
            try
            {
                news = newsCache["News"] as List<EasyDnnNewsApp>;

                if (news == null)
                {
                    news = new List<EasyDnnNewsApp>();
                    //ConfigurationManager.ConnectionStrings["SiteSqlServer"].ConnectionString;
                    string connection = ConfigurationManager.ConnectionStrings["SiteSqlServer"].ConnectionString;
                    using (SqlConnection con = new SqlConnection(connection))
                    {
                        con.Open();
                        using (SqlCommand command = new SqlCommand("SP_GetAllNews", con))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            // TODO: descomentar y colocar el PortalID correcto
#if DEBUG
                            command.Parameters.Add(new SqlParameter("PortalID", portalid));
#else
                            command.Parameters.Add(new SqlParameter("PortalID", portalid));
#endif
                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    EasyDnnNewsApp newInfo = new EasyDnnNewsApp
                                    {
                                        ArticleID = (int)reader["ArticleID"],
                                        Title = reader["Title"].ToString()
                                    };
                                    if (reader["SubTitle"] != DBNull.Value)
                                    {
                                        newInfo.SubTitle = reader["SubTitle"].ToString();
                                    }


                                    if (reader["Summary"] != DBNull.Value)
                                    {
                                        newInfo.Summary = reader["Summary"].ToString();
                                    }
                                    if (reader["ModuleID"] != DBNull.Value)
                                    {
                                        newInfo.ModuleID = (int)reader["ModuleID"];
                                    }

                                    if (reader["Article"] != DBNull.Value)
                                    {
                                        newInfo.Article = HttpUtility.HtmlDecode(reader["Article"].ToString());
                                    }

                                    if (reader["ArticleImage"] != DBNull.Value)
                                    {
                                        newInfo.ArticleImage = reader["ArticleImage"].ToString();


                                    }
                                    if (reader["DateAdded"] != DBNull.Value)
                                    {
                                        newInfo.DateAdded = (DateTime)reader["DateAdded"];
                                    }

                                    if (reader["PublishDate"] != DBNull.Value)
                                    {
                                        newInfo.PublishDate = (DateTime)reader["PublishDate"];
                                    }

                                    if (reader["CategoryID"] != DBNull.Value)
                                    {
                                        newInfo.CategoryID = (int)reader["CategoryID"];
                                    }

                                    if (reader["CategoryName"] != DBNull.Value)
                                    {
                                        newInfo.CategoryName = reader["CategoryName"].ToString();
                                    }

                                    if (reader["Featured"] != DBNull.Value)
                                    {
                                        newInfo.Featured = (bool)reader["Featured"];
                                    }


                                    // English 
                                    newInfo.LangTitle = reader["LangTitle"].ToString() ?? string.Empty;
                                    newInfo.LangSubTitle = reader["LangSubTitle"].ToString() ?? string.Empty;
                                    newInfo.LangSummary = reader["LangSummary"].ToString() ?? string.Empty;
                                    newInfo.LangArticle = reader["LangArticle"].ToString() ?? string.Empty;
                                    newInfo.LangLocaleCode = reader["LangLocaleCode"].ToString() ?? string.Empty;
                                    newInfo.LangLocaleString = reader["LangLocaleString"].ToString() ?? string.Empty;

                                    news.Add(newInfo);
                                }
                            }

                            //countItems = (int)command.Parameters["countItems"].Value;
                        }
                    }

                    // TODO: Cambiar caché por días
#if DEBUG
                    newsCache.Insert("News", news, null, DateTime.Now.AddSeconds(30), Cache.NoSlidingExpiration);
#else
                    newsCache.Insert("News", news, null, DateTime.Now.AddSeconds(30), Cache.NoSlidingExpiration);
#endif
                }
                //else
                //    news = newsCache["News"] as List<EasyDnnNewsApp>;

                return news;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<EasyDnnNewsDocuments> GetEasyDnnNewsDocuments(int articleId, string localeCode)
        {
            List<EasyDnnNewsDocuments> list = new List<EasyDnnNewsDocuments>();

            string connection = ConfigurationManager.ConnectionStrings["SiteSqlServer"].ConnectionString;

            using (SqlConnection con = new SqlConnection(connection))
            {
                con.Open();
                using (SqlCommand command = new SqlCommand("Sp_GetEasyDnnNewsDocuments", con))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    ///Paràmetro Categoria///
                    SqlParameter paramArticleId = new SqlParameter
                    {
                        ParameterName = "@ArticleId",
                        Value = articleId
                    };
                    command.Parameters.Add(paramArticleId);
                    command.Parameters.Add(new SqlParameter("localeCode", localeCode));

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            EasyDnnNewsDocuments document = new EasyDnnNewsDocuments
                            {
                                DocEntryID = (int)reader["DocEntryID"],
                                UserID = (int)reader["UserID"],
                                DateUploaded = (DateTime)reader["DateUploaded"],
                                FilePath = reader["FilePath"].ToString()
                            };

                            if (reader["Title"] != DBNull.Value)
                                document.Title = reader["Title"].ToString();

                            if (reader["Description"] != DBNull.Value)
                                document.Description = reader["Description"].ToString();

                            if (reader["FileName"] != DBNull.Value)
                                document.FileName = reader["FileName"].ToString();

                            document.Visible = (bool)reader["Visible"];

                            if (reader["FileExtension"] != DBNull.Value)
                                document.FileExtension = reader["FileExtension"].ToString();

                            document.ModuleId = (int)reader["ModuleId"];
                            document.PortalId = (int)reader["PortalId"];
                            document.ArticleID = (int)reader["ArticleID"];

                            string pathOriginalExtension = HttpContext.Current.Server.MapPath(string.Format("/Portals/{0}/Temp/{1}/{2}{3}", document.PortalId, document.ArticleID, document.FileName, document.FileExtension));
                            string pathResource = HttpContext.Current.Server.MapPath(string.Format("{0}{1}", document.FilePath, document.FileExtension));

                            string fileName = string.Format("{0}{1}", document.FileName, document.FileExtension);

                            PortalSettings portalSettings = Globals.GetPortalSettings();
                            int portalid = portalSettings.PortalId;

                            string absolutePath = HttpContext.Current.Server.MapPath(document.FilePath);

                            document.UrlView = GetUrlView(portalid, document.ArticleID, absolutePath, document.FileName, document.FileExtension);
                            //SaveDocumentOriginalFormat(pathResource, document.ArticleID.ToString(), document.PortalId.ToString(), fileName);

                            list.Add(document);
                        }
                    }
                }
            }

            return list;

        }

        public List<EasyDnnNewsDocuments> GetALLEasyDnnNewsDocuments(int articleId, string localeCode, string tabName = null)
        {
            List<EasyDnnNewsDocuments> list = new List<EasyDnnNewsDocuments>();
            PortalSettings portalSettings = Globals.GetPortalSettings();
            int portalid = portalSettings.PortalId;

            list = documentsCache["documents"] as List<EasyDnnNewsDocuments>;

            if (list == null || (list != null && list.Count == 0))
            {
                list = new List<EasyDnnNewsDocuments>();
                string connection = ConfigurationManager.ConnectionStrings["SiteSqlServer"].ConnectionString;
                using (SqlConnection con = new SqlConnection(connection))
                {
                    con.Open();
                    using (SqlCommand command = new SqlCommand("Sp_GetALLEasyDnnNewsDocuments", con))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add(new SqlParameter("TabName", tabName));

                        // TODO: descomentar y colocar el PortalID correcto
#if DEBUG
                        command.Parameters.Add(new SqlParameter("PortalID", portalid));
#else
                        command.Parameters.Add(new SqlParameter("PortalID", portalid));
#endif
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                EasyDnnNewsDocuments document = new EasyDnnNewsDocuments
                                {
                                    DocEntryID = (int)reader["DocEntryID"],
                                    UserID = (int)reader["UserID"],
                                    DateUploaded = (DateTime)reader["DateUploaded"],
                                    FilePath = reader["FilePath"].ToString()
                                };

                                if (reader["Title"] != DBNull.Value)
                                    document.Title = reader["Title"].ToString();

                                if (reader["Description"] != DBNull.Value)
                                    document.Description = reader["Description"].ToString();

                                if (reader["ModuleID"] != DBNull.Value)
                                {
                                    document.ModuleId = (int)reader["ModuleID"];
                                }
                                else
                                {
                                    document.ModuleId = 450;
                                }



                                if (reader["FileName"] != DBNull.Value)
                                    document.FileName = reader["FileName"].ToString();

                                document.Visible = (bool)reader["Visible"];

                                if (reader["FileExtension"] != DBNull.Value)
                                    document.FileExtension = reader["FileExtension"].ToString();

                                if (reader["LocaleCode"] != DBNull.Value)
                                    document.LocaleCode = reader["LocaleCode"].ToString();

                                //document.ModuleId = (int)reader["ModuleId"];
                                document.PortalId = (int)reader["PortalId"];
                                document.ArticleID = (int)reader["ArticleID"];

                                string pathOriginalExtension = HttpContext.Current.Server.MapPath(string.Format("/Portals/{0}/Temp/{1}/{2}{3}", document.PortalId, document.ArticleID, document.FileName, document.FileExtension));
                                string pathResource = HttpContext.Current.Server.MapPath(string.Format("{0}{1}", document.FilePath, document.FileExtension));

                                string fileName = string.Format("{0}{1}", document.FileName, document.FileExtension);
                                string absolutePath = HttpContext.Current.Server.MapPath(document.FilePath);
                                document.UrlView = GetUrlView(portalid, document.ArticleID, absolutePath, document.FileName, document.FileExtension);
                                //SaveDocumentOriginalFormat(pathResource, document.ArticleID.ToString(), document.PortalId.ToString(), fileName);

                                list.Add(document);
                            }
                        }
                    }
                }
                // TODO: Cambiar caché por días
#if DEBUG
                documentsCache.Insert("documents", list, null, DateTime.Now.AddSeconds(30), Cache.NoSlidingExpiration);
#else
                documentsCache.Insert("documents", list, null, DateTime.Now.AddSeconds(30), Cache.NoSlidingExpiration);
#endif
            }
            //else
            //    list = documentsCache["documents"] as List<EasyDnnNewsDocuments>;

            var resultArticles = list.Where(x => x.ArticleID == articleId && x.LocaleCode.ToLower() == localeCode.ToLower()).ToList();
            return resultArticles;
            //return list.Where(x => x.ArticleID == articleId).ToList();
        }

        public List<EasyDnnFieldsValues> GetEasyDnnNewsFieldsValues(int? parentCategory, int? categoryId, string localeCode)
        {
            try
            {
                string connection = ConfigurationManager.ConnectionStrings["SiteSqlServer"].ConnectionString;
                List<EasyDnnFieldsValues> fieldsValues = new List<EasyDnnFieldsValues>();

                using (SqlConnection con = new SqlConnection(connection))
                {
                    con.Open();
                    using (SqlCommand command = new SqlCommand("Sp_GetEasyDnnFieldValues", con))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        ///Paràmetro Categoria///
                        SqlParameter paramParentCategory = new SqlParameter
                        {
                            ParameterName = "parentCategory",
                            IsNullable = true
                        };

                        if (parentCategory != null)
                        {
                            paramParentCategory.Value = parentCategory;
                        }
                        else
                        {
                            paramParentCategory.Value = DBNull.Value;
                        }
                        command.Parameters.Add(paramParentCategory);

                        ///Parámetro title///
                        SqlParameter paramCategoryId = new SqlParameter
                        {
                            ParameterName = "categoryId",
                            IsNullable = true
                        };

                        if (categoryId != null)
                        {
                            paramCategoryId.Value = categoryId;
                        }
                        else
                        {
                            paramCategoryId.Value = DBNull.Value;
                        }
                        command.Parameters.Add(paramCategoryId);

                        command.Parameters.Add(new SqlParameter("localeCode", localeCode));

                        PortalSettings portalSettings = Globals.GetPortalSettings();

                        int portalid = portalSettings.PortalId;
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                EasyDnnFieldsValues info = new EasyDnnFieldsValues
                                {
                                    CustomFieldID = (int)reader["CustomFieldID"],
                                    ArticleID = (int)reader["ArticleID"]
                                };

                                if (reader["Name"] != DBNull.Value)
                                {
                                    info.Name = reader["Name"].ToString();
                                }

                                if (reader["RText"] != DBNull.Value)
                                {
                                    info.RText = reader["RText"].ToString();
                                }

                                if (reader["Decimal"] != DBNull.Value)
                                {
                                    info.Decimal = (decimal)reader["Decimal"];
                                }
                                if (reader["Int"] != DBNull.Value)
                                {
                                    info.Int = (int)reader["Int"];
                                }

                                if (reader["Text"] != DBNull.Value)
                                {
                                    info.Text = reader["Text"].ToString();
                                }

                                if (reader["Bit"] != DBNull.Value)
                                {
                                    info.Bit = (bool)reader["Bit"];
                                }

                                if (reader["DateTime"] != DBNull.Value)
                                {
                                    info.DateTime = (DateTime)reader["DateTime"];
                                }

                                fieldsValues.Add(info);
                            }
                        }
                    }
                }

                return fieldsValues;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ClientDeceval> GetAccionesPpales(int? categoryId)
        {
            try
            {
                string connection = ConfigurationManager.ConnectionStrings["SiteSqlServer"].ConnectionString;
                List<ClientDeceval> fieldsValues = new List<ClientDeceval>();

                using (SqlConnection con = new SqlConnection(connection))
                {
                    con.Open();
                    using (SqlCommand command = new SqlCommand("Sp_GetAccionesPpales", con))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        ///Parámetro title///
                        SqlParameter paramCategoryId = new SqlParameter
                        {
                            ParameterName = "categoryId",
                            IsNullable = true
                        };

                        if (categoryId != null)
                        {
                            paramCategoryId.Value = categoryId;
                        }
                        else
                        {
                            paramCategoryId.Value = DBNull.Value;
                        }
                        command.Parameters.Add(paramCategoryId);

                        PortalSettings portalSettings = Globals.GetPortalSettings();

                        int portalid = portalSettings.PortalId;
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                ClientDeceval info = new ClientDeceval
                                {
                                    numberID = reader["NumberId"].ToString()
                                };

                                if (reader["TypeNit"] != DBNull.Value)
                                {
                                    info.typeNit = reader["TypeNit"].ToString();
                                }

                                if (reader["CodeDeceval"] != DBNull.Value)
                                {
                                    info.codeDeceval = reader["CodeDeceval"].ToString();
                                }

                                if (reader["nameClient"] != DBNull.Value)
                                {
                                    info.nameClient = reader["nameClient"].ToString();
                                }


                                fieldsValues.Add(info);
                            }
                        }
                    }
                }
                return fieldsValues;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteAccionesPpales(int categoryId)
        {
            try
            {
                string connection = ConfigurationManager.ConnectionStrings["SiteSqlServer"].ConnectionString;
                using (SqlConnection con = new SqlConnection(connection))
                {
                    con.Open();
                    using (SqlCommand command = new SqlCommand("Sp_DeleteClientDeceval", con))
                    {
                        command.CommandType = CommandType.StoredProcedure;



                        command.ExecuteNonQuery();
                    }
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ClientDeceval> InsertAccionesPpales(List<ClientDeceval> fieldsValues)
        {
            try
            {
                string connection = ConfigurationManager.ConnectionStrings["SiteSqlServer"].ConnectionString;
                using (SqlConnection con = new SqlConnection(connection))
                {
                    con.Open();
                    using (SqlCommand command = new SqlCommand("Sp_InsertClientDeceval", con))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add("@identificacion", DbType.Int64);
                        command.Parameters.Add("@tipoNit", DbType.String);
                        command.Parameters.Add("@nombreCliente", DbType.String);
                        command.Parameters.Add("@codeDeceval", DbType.Int64);

                        //command.Parameters.Add("@fechaCorte", DbType.Int32);

                        foreach (var acciones in fieldsValues)
                        {
                            command.Parameters[0].Value = acciones.numberID;
                            command.Parameters[1].Value = acciones.typeNit;
                            command.Parameters[2].Value = acciones.nameClient;
                            command.Parameters[3].Value = acciones.codeDeceval;
                            //command.Parameters[6].Value = acciones.FechaCorte;

                            command.ExecuteNonQuery();
                        }
                    }
                    con.Close();
                }
                return fieldsValues;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool SaveDocumentOriginalFormat(string pathDocumentResource, string articleID, string portalID, string fileName)
        {
            if (File.Exists(pathDocumentResource))
            {
                string articleDirectory = string.Format("/Temp/{0}", articleID);

                int _portalId = int.Parse(portalID);

                if (!FolderManager.Instance.FolderExists(_portalId, "Temp"))
                {
                    FolderManager.Instance.AddFolder(_portalId, "Temp");
                }

                if (!FolderManager.Instance.FolderExists(_portalId, articleDirectory))
                {
                    FolderManager.Instance.AddFolder(_portalId, articleDirectory);
                }

                IFolderInfo folderArticle = FolderManager.Instance.GetFolder(_portalId, articleDirectory);

                if (!FileManager.Instance.FileExists(folderArticle, fileName))
                {
                    try
                    {
                        using (FileStream fileStreamResource = new FileStream(pathDocumentResource, FileMode.Open))
                        {

                            FileManager.Instance.AddFile(folderArticle, fileName, fileStreamResource);
                        }
                        return true;
                    }
                    catch (Exception ex)
                    {
                        Exceptions.LogException(ex);
                        return false;
                    }
                }

                //if (!Directory.Exists(articleDirectory))
                //{
                //    Directory.CreateDirectory(articleDirectory);
                //}

                //if (!File.Exists(pathOriginalExtension))
                //{
                //    try
                //    {
                //        SendLog(pathDocumentResource);
                //        SendLog(pathOriginalExtension);

                //        using (FileStream fileStreamResource = new FileStream(pathDocumentResource, FileMode.Open))
                //        {
                //            using (Stream destination = File.Create(pathOriginalExtension))
                //            {
                //                for (int a = fileStreamResource.ReadByte(); a != -1; a = fileStreamResource.ReadByte())
                //                {
                //                    destination.WriteByte((byte)a);
                //                }
                //            }
                //        }

                //        return true;
                //    }
                //    catch (Exception ex)
                //    {
                //        Exceptions.LogException(ex);

                //        return false;
                //    }
                //}
            }
            return true;
        }

        public static string GetUrlView(int portalId, int articleId, string pathDocumentResource, string fileName, string fileExtension)
        {
            var fileManager = FileManager.Instance;
            var folderManager = FolderManager.Instance;
            IFolderInfo targetFolder = new FolderInfo();

            targetFolder = folderManager.GetFolder(portalId, $"Temp/{articleId}");
            if (targetFolder != null && fileManager.FileExists(targetFolder, fileName + fileExtension))
            {
                var document = fileManager.GetFile(targetFolder, fileName + fileExtension);
                return $"/Portals/{portalId}/{document.RelativePath}";
            }

            var originPath = pathDocumentResource;
            try
            {
                if (File.Exists(originPath))
                {
                    if (!folderManager.FolderExists(portalId, $"Temp/{articleId}"))
                        targetFolder = folderManager.AddFolder(portalId, $"Temp/{articleId}");
                    else
                        targetFolder = folderManager.GetFolder(portalId, $"Temp/{articleId}");

                    using (FileStream stream = File.Open(originPath, FileMode.Open))
                    {
                        var document = fileManager.AddFile(targetFolder, fileName + fileExtension, stream);
                        return $"/Portals/{portalId}/{document.RelativePath}";
                    }
                }
            }
            catch (Exception ex)
            {
                SendLog(ex.ToString());
            }

            return string.Empty;
        }

        public static void SendLog(string msn)
        {
            EventLogController eventLog = new EventLogController();
            LogInfo logInfo = new LogInfo();
            PortalSettings ps = PortalController.Instance.GetCurrentPortalSettings();
            UserInfo userInfo = UserController.Instance.GetCurrentUserInfo();
            logInfo.LogUserID = userInfo.UserID;
            logInfo.LogPortalID = ps.PortalId;
            logInfo.LogTypeKey = EventLogController.EventLogType.ADMIN_ALERT.ToString();
            logInfo.AddProperty("Mineros Inversionistas", msn);
            eventLog.AddLog(logInfo);
        }
    }
}
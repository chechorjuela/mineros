﻿using DotNetNuke.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Model.Commons
{
    [TableName("dbo.CertificateDeceval")]
    [PrimaryKey("CertificateDECEVALID", AutoIncrement = true)]
    public class CertificateDeceval
    {
        public int CertificateDECEVALID { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string FileExtension { get; set; }
        public string Identication { get; set; }
        public string CodeDeceval { get; set; }
        public DateTime CreateAt { get; set; }
        public DateTime? UpdateAt { get; set; }
    }
}

﻿using DotNetNuke.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Model.Commons
{
    [TableName("dbo.Certificates")]
    [PrimaryKey("CertificateID", AutoIncrement = true)]
    public class Certificate
    {
        public int CertificateID { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string FileExtension { get; set; }
        public string YearCertificate { get; set; }
        public string Enterprise { get; set; }
        public int CertiticateType { get; set; }
        public int CustomerID { get; set; }
        public DateTime CreateAt { get; set; }
        public DateTime? UpdateAt { get; set; }
    }
}

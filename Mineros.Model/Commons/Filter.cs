﻿using DotNetNuke.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Model.Commons
{
    public class Filter
    {
        public string @Operator { get; set; }
        public string Value { get; set; }
        public string Field { get; set; }
    }
    public class Paginator
    {
        public int Skip { get; set; }
        public int Take { get; set; }
    }
    public class Filters
    {
        [IgnoreColumn]
        public string Logic { get; set; }

        public List<Filter> Filter { get; set; }
        [IgnoreColumn]
        public Paginator Paginator { get; set; }
    }
}

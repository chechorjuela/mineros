﻿using DotNetNuke.ComponentModel.DataAnnotations;
using Mineros.Model.Commons;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Mineros.Model
{
    [Table("dbo.Providers")]
    [TableName("dbo.Providers")]
    [PrimaryKey("Id", AutoIncrement = true)]
    public partial class Providers
    {
        [Key]
        [StringLength(1)]
        public int Id { get; set; }
        public string NameEnterprise { get; set; }
        public string Nit { get; set; }
        public string SiteUrl { get; set; }
        public string ContactName { get; set; }
        public string ContactEmail { get; set; }
        public string CompanyEmail { get; set; }
        public string PhoneNumber { get; set; }
        public string FilePath { get; set; }
        public string Countries { get; set; }
        public string Brand { get; set; }
        public int Status { get; set; }
        public DateTime? CreateAt { get; set; }
        public DateTime? UpdateAt { get; set; }
        public DateTime? DeleteAt { get; set; }
       

    }
}

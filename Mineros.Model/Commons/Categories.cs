﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Model.Commons
{
    public class Categories
    {
        public int CategoryID { get; set; }
        public int PortalID { get; set; }
        public string CategoryName { get; set; }
        public int Position { get; set; }
        public int? ParentCategory { get; set; }
        public int Level { get; set; }

        public string Description { get; set; }


    }
}

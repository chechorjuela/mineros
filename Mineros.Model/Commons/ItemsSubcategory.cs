﻿using DotNetNuke.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Model.Commons
{
    [TableName("dbo.ItemsSubcategory")]
    [PrimaryKey("Id", AutoIncrement = true)]
    public class ItemsSubcategory
    {
        public int Id { get; set; }
        public int SubcategoryID { get; set; }
        public int ProviderId { get; set; }
    }
}

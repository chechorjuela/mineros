﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Model.Commons
{
    public class Customer
    {
        public int CustomerId { get; set; }
        public string NameCustomer { get; set; }
    }
}

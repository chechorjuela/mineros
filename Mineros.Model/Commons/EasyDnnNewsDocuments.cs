﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Data.Models
{
    [Serializable]
    public class EasyDnnNewsDocuments
    {
        public int ArticleID { get; set; }
        public int DocEntryID { get; set; }
        public int UserID { get; set; }
        public DateTime DateUploaded { get; set; }
        public string FilePath { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string FileName { get; set; }
        public bool Visible { get; set; }

        public string FileExtension { get; set; }

        public int? ModuleId { get; set; }
        public int PortalId { get; set; }
        public string LocaleCode { get; set; }

        public string UrlView { get; set; }
    }
}

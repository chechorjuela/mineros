﻿using DotNetNuke.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Model.Commons
{
    [TableName("dbo.Interest_Settings")]
    [PrimaryKey("Id", AutoIncrement = true)]
    public class InterestSettings
    {
        public int Id { get; set; }
        public string name_spanish { get; set; }
        public string name_english { get; set; }
        public int type_interest { get; set; }
    }
}

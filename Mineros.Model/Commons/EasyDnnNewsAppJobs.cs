﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Model
{
    public class EasyDnnNewsAppJobs
    {

        public int ArticleID { get; set; }
        public int PortalID { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string Summary { get; set; }
        public string Article { get; set; }
        public string ArticleImage { get; set; }
        public DateTime DateAdded { get; set; }
        public DateTime PublishDate { get; set; }
        public DateTime ExpireDate {get;set;}
        public string Category { get; set; }
        public int CategoryID { get; set; }
        public string CategoryName { get; set; }
        public bool Featured { get; set; }
        public bool HideDefaultLocale { get; set; }
        public int? ParentCategory { get; set; }
        public int? ModuleID { get; set; }
        public string CountryNews { get; set; }
        public string CityNews { get; set; }
        public string EnterpriseNews { get; set; }

        // English
        public string LangTitle { get; internal set; }
        public string LangSubTitle { get; internal set; }
        public string LangSummary { get; internal set; }
        public string LangArticle { get; internal set; }
        public string LangLocaleCode { get; internal set; }
        public string LangLocaleString { get; internal set; }

        // Interest 
       public string ProccessInterest { get; set; }
       public string LevelPosition { get; set; }
       public string Locations { get; set; }
    }
}

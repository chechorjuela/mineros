﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Model.Commons
{
    public class InfoRelevanteViewItem
    {
        public int ArticleID { get; set; }
        public int CategoryID { get; set; }
        public string Title { get; set; }
        public DateTime PublishDate { get; set; }
        public Articles Article { get; set; }
        public Documents Document { get; set; }
        public int? ModuleID { get; set; }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Model.Commons
{
    public class CertificateType
    {
        public int  CertificateID { get; set; }
        public string Name { get; set; }
        public string NameSelect { get; set; }
        
    }
}

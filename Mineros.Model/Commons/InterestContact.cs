﻿using DotNetNuke.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Model.Commons
{

    [TableName("dbo.InterestContact")]
    [PrimaryKey("Id", AutoIncrement = true)]
    public class InterestContact
    {
        public int Id { get; set; }
        public string name_contact { get; set; }
        public string number_id { get; set; }
        public string email { get; set; }
        public string number_phone { get; set; }
        public string city_current { get; set; }
        public string country_current { get; set; }
        public int? interest_position_first { get; set; }
        public int? interest_position_second { get; set; }
        public int? process_interest_first { get; set; }
        public int? process_interest_second { get; set; }
        public int? location_interest_first { get; set; }
        public int? location_interest_second { get; set; }
        public string application { get; set; }
        public string file_dir { get; set; }
        [IgnoreColumn]
        public ImageFile file { get; set; }
        public string locationApplicant { get; set; }
        public string processApplicant { get; set; }
        public string positionApplicant { get; set; }
    }

    public class ImageFile
    {
        public int lastModified { get; set; }

        public DateTime lastModifiedDate { get; set; }

        public string name { get; set; }

        public int size { get; set; }

        public string type { get; set; }
        public string webkitRelativePath { get; set; }
    }
}

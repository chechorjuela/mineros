﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Model.Commons
{
    public class ChartDataObject
    {
        public IList<IList<string>> chartData { get; set; }
    }
}

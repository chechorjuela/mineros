﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Model.Commons
{
    public class InfoRelevanteResult
    {
        public List<Categories> CategoriesItems { get; set; }
        public List<InfoRelevanteViewItem> Items { get; set; }
    }
}

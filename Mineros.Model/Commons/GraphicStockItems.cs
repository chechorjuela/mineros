﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Model.Commons
{
    public class GraphicStockItems
    {
        public int Id { get; set; }
        public DateTime Fecha { get; set; }
        public float PFCEMARGOS { get; set; }
        public float CEMARGOS { get; set; }
    }
}

﻿using DotNetNuke.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Data.Models
{
   
    [TableName("dbo.ClientDeceval")]
    [PrimaryKey("Id", AutoIncrement = true)]
    public class ClientDeceval
    {
        public int Id { get; set; }
        public string codeDeceval { get; set; }
        public string typeNit { get; set; }
        public string numberID { get; set; }
        public string nameClient { get; set; }
      
    }
}
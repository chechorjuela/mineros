﻿using DotNetNuke.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Model.Commons
{
    [TableName("dbo.Category")]
    [PrimaryKey("Id", AutoIncrement = true)]
    public class Category
    {
        public int Id { get; set; }
        public string English { get; set; }
        public string Spanish { get; set; }
        public int TypeActivityId { get; set; }
    }
}

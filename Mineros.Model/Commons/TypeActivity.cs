﻿using DotNetNuke.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Model.Commons
{
    [TableName("dbo.TypeActivity")]
    [PrimaryKey("Id", AutoIncrement = true)]
    public class TypeActivity
    {
        public int Id { get; set; }
        public string Spanish { get; set; }
        public string English { get; set; }
    }
}

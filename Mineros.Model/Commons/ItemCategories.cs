﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Model.Commons
{
    public class ItemCategories
    {
        public int Id { get; set; }
        public int SubcategoryId { get; set; }
        public int ProviderId { get; set; }
        public int CategoryId { get; set; }
        public int TypeActivityId { get; set; }
        public string SubcategorySpanish { get; set; }
        public string SubcategoryEpanish { get; set; }
        public string CategorySpanish { get; set; }
        public string CategoryEnglish { get; set; }
        public string TypeActivitySpanish { get; set; }
        public string TypeActivityEnglish { get; set; }

    }
}

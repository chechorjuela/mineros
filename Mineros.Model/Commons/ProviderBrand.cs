﻿using DotNetNuke.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Model.Commons
{
    [TableName("dbo.ProvidersBrands")]
    [PrimaryKey("Id", AutoIncrement = true)]
    public class ProviderBrand
    {
        public int Id { get; set; }
        public int BrandId { get; set; }
        public int ProvidersId { get; set; }
    }
}

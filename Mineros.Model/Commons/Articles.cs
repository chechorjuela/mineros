﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Model.Commons
{

    public class Articles
    {
        public int ArticleID { get; set; }
        public int PortalID { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string Summary { get; set; }
        public string Article { get; set; }
        public string ArticleImage { get; set; }
        public DateTime DateAdded { get; set; }
        public DateTime PublishDate { get; set; }
        public string Category { get; set; }
        public int CategoryID { get; set; }
        public string CategoryName { get; set; }
        public bool Featured { get; set; }

        // English
        public string LangTitle { get;  set; }
        public string LangSubTitle { get;  set; }
        public string LangSummary { get;  set; }
        public string LangArticle { get;  set; }
        public string LangLocaleCode { get;  set; }
        public string LangLocaleString { get;  set; }
    }
}

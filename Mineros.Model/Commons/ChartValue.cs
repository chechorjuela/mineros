﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Model.Commons
{
    public class ChartValue
    {
        public string name { get; set; }
        public string value { get; set; }
        public string timespan { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Mineros.Data.Models
{
    [Serializable]
    public class EasyDnnNewsCategories
    {
        public int CategoryID { get; set; }
        public int PortalID { get; set; }
        public string CategoryName { get; set; }
        public int Position { get; set; }
        public int? ParentCategory { get; set; }
        public int Level { get; set; }
        public string Description { get; set; }
        public string LocaleCode { get; set; }
        public string CategoryNameLocalize { get; set; }
        public string DescriptionLocalize { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Model.Commons
{
    public class Stock
    {
        public DateTime Fecha { get; set; }
        public float Actual { get; set; }
        public float Anterior { get; set; }
        public float Valor { get; set; }
        public string Accion { get; set; }
        public string Variacion { get; set; }
    }
}

﻿using System;

namespace Mineros.Data.Models
{
    [Serializable]
    public class EasyDnnFieldsValues
    {
        public int CustomFieldID { get; set; }
        public string Name { get; set; }
        public int ArticleID { get; set; }
        public string RText { get; set; }
        public decimal? Decimal { get; set; }
        public int? Int { get; set; }
        public string Text { get; set; }
        public bool? Bit { get; set; }
        public DateTime? DateTime { get; set; }


    }
}

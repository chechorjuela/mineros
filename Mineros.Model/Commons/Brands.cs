﻿using DotNetNuke.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Model.Commons
{
    [TableName("dbo.Brand")]
    [PrimaryKey("Id", AutoIncrement = true)]
    public class Brands
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}

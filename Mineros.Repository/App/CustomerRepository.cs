﻿using Mineros.Model.Commons;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mineros.Repository.App
{
    public class CustomerRepository : Repository<Customer>
    {
        public CustomerRepository(string connectionString) : base(connectionString) { }
    }
}

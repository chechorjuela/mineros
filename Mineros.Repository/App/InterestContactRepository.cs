﻿using Mineros.Model.Commons;
using System;
using System.Collections.Generic;
using System.Text;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Linq;

namespace Mineros.Repository.App
{
    public class InterestContactRepository : Repository<InterestContact>
    {
        public InterestContactRepository(string connectionString) : base(connectionString) { }

        public DataSourceResult GetInterestContactFilter(DataSourceRequest req)
        {
            using (var contex = this.Context)
            {
                var view = "SELECT * FROM dbo.InterestContact";

                var res = contex.ExecuteQuery<InterestContact>(System.Data.CommandType.Text, view);

                if (res != null && res.Count() > 0)
                {
                    return res.ToDataSourceResult(req);
                }
                else
                {
                    return new DataSourceResult();
                }
            }
        }


    }
}

﻿using Mineros.Model.Commons;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Mineros.Repository
{
    public class CertificateDecevalRepository : Repository<CertificateDeceval>
    {
        public CertificateDecevalRepository(string connectionString) : base(connectionString) { }
        
        public List<CertificateDeceval> GetCertificatesSearch(string nit, string customerId)
        {
            List<CertificateDeceval> returnCertificate = new List<CertificateDeceval>();
            using (var context = this.Context)
            {
                var certificates = context.ExecuteQuery<CertificateDeceval>(System.Data.CommandType.Text, "WHERE CodeDeceval = " + customerId);
                if (certificates != null && certificates.Count() > 0)
                {
                    returnCertificate =  certificates.ToList();
                    return returnCertificate;
                }
                return null;
            }
        }

        public CertificateDeceval GetCertidicateDecevalByNitAndCodeval(string nit, string codeDeceval)
        {
            CertificateDeceval returnCertificate = new CertificateDeceval();
            using (var context = this.Context)
            {
                var certificates = context.ExecuteQuery<CertificateDeceval>(System.Data.CommandType.Text, "WHERE Identication like '" + nit + "' AND CodeDeceval like " + codeDeceval );
                if (certificates != null && certificates.Count() > 0)
                {
                    returnCertificate = certificates.First();
                    return returnCertificate;
                }
                return null;
            }
        }

        public CertificateDeceval GetCertidicateDecevalByCodeval(string codeDeceval)
        {
            CertificateDeceval returnCertificate = new CertificateDeceval();
            using (var context = this.Context)
            {
                var certificates = context.ExecuteQuery<CertificateDeceval>(System.Data.CommandType.Text, "WHERE CodeDeceval like " + codeDeceval);
                if (certificates != null && certificates.Count() > 0)
                {
                    returnCertificate = certificates.First();
                    return returnCertificate;
                }
                return null;
            }
        }
    }

}

﻿using Kendo.Mvc.UI;
using Mineros.Model.Commons;
using System;
using System.Collections.Generic;
using System.Text;
using Kendo.Mvc.Extensions;
using System.Linq;

namespace Mineros.Repository.App
{
    public class InterestSettingsRepository : Repository<InterestSettings>
    {
        public InterestSettingsRepository(string connectionString) : base(connectionString) { }


        public DataSourceResult GetInterestSettingsFilter(DataSourceRequest req)
        {
            using (var contex = this.Context)
            {
                var view = "SELECT * FROM dbo.Interest_Settings WHERE Id!=0";

                var res = contex.ExecuteQuery<InterestSettings>(System.Data.CommandType.Text, view);

                if (res != null && res.Count() > 0)
                {
                    return res.ToDataSourceResult(req);
                }
                else
                {
                    return new DataSourceResult();
                }
            }
        }
    }

}

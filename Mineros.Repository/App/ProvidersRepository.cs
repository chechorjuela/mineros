﻿using Kendo.Mvc.UI;
using Mineros.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using DotNetNuke.Services.Mail;

namespace Mineros.Repository.App
{
    public class ProvidersRepository : Repository<Providers>
    {
        public ProvidersRepository(string connectionString) : base(connectionString) { }

        public List<Providers> getProviderByLastMonth()
        {
            using (var context = this.Context)
            {
                var result = context.ExecuteQuery<Providers>(System.Data.CommandType.StoredProcedure, "[dbo].[SP_GetProvidersByMonthly]").ToList();
                if (result != null && result.Count > 0)
                {
                    return result;
                }
                return new List<Providers>();
            }
        }
        public void sendEmail(string fromAddress, string subject, string htmlBody)
        {
            try
            {
                Mail.SendEmail(fromAddress, fromAddress, subject, htmlBody);
            }
            catch (Exception exp)
            {
                // exp.Message;
            }
        }
    }
}

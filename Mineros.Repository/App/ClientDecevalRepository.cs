﻿using Mineros.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mineros.Repository
{
    public class ClientDecevalRepository :  Repository<ClientDeceval>
    {
        public ClientDecevalRepository(string connectionString) : base(connectionString) { }

        public ClientDeceval GetCertidicateByNitEnterpriseAndDate(string nit,string codeDeceval)
        {
            ClientDeceval returnClientDeceval = new ClientDeceval();
            using (var context = this.Context)
            {
                var clientDeceval = context.ExecuteQuery<ClientDeceval>(System.Data.CommandType.Text, "WHERE NumberID like " + nit + " AND CodeDeceval like " + codeDeceval);
                if (clientDeceval != null && clientDeceval.Count() > 0)
                {
                    returnClientDeceval = clientDeceval.First();
                    return returnClientDeceval;
                }
                return null;
            }
        }
    }
}

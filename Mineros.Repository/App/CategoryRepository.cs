﻿using Mineros.Model.Commons;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mineros.Repository.App
{
    public class CategoryRepository:Repository<Category>
    {
        public CategoryRepository(string connectionString) : base(connectionString) { }
    }
}

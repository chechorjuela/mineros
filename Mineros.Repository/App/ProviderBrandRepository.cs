﻿using Mineros.Model.Commons;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mineros.Repository.App
{
    public class ProviderBrandRepository:Repository<ProviderBrand>
    {
        public ProviderBrandRepository(string connectionString) : base(connectionString) { }
    }
}

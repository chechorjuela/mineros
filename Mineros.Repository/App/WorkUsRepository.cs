﻿using DotNetNuke.Services.Mail;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net.Mail;
using System.Text;

namespace Mineros.Repository
{
    public class WorkUsRepository
    {
        public void sendEmail(string workUsContract, string attachment,string subject,string body,string country_job, string bodyto)
        {

            List<Attachment> listAttchach = new List<Attachment>();
            listAttchach.Add(new Attachment(attachment));
            var EmailSender = "";
            switch (country_job)
            {
                case "argentina":
                    EmailSender = ConfigurationManager.AppSettings["EmailSenderArgentina"].ToString();
                    break;
                case "nicaragua":
                    EmailSender = ConfigurationManager.AppSettings["EmailSenderNicaragua"].ToString();
                    break;
                case "colombia":
                default:
                    EmailSender = ConfigurationManager.AppSettings["EmailSenderColombia"].ToString();
                    break;
            }
          
            try
            {
                Mail.SendEmail(workUsContract, workUsContract,subject,bodyto);
                Mail.SendEmail(EmailSender, EmailSender, EmailSender, subject, body, listAttchach);
            }
            catch (Exception exp)
            {
               // exp.Message;
            }
        }
    }
}

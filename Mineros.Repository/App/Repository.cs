﻿using System;
using System.Collections.Generic;
using System.Text;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using DotNetNuke.Collections;
using DotNetNuke.Data;

namespace Mineros.Repository
{

    public class Repository<T> : IRepository<T> where T : class
    {
        public IDataContext Context
        {
            get
            {
                return DataContext.Instance(baseDatos);
            }
        }

        public Repository(string connectionName)
        {
            baseDatos = connectionName;
        }
        public string baseDatos { get; set; }

        public void Create(T t)
        {
            using (IDataContext ctx = DataContext.Instance(baseDatos))
            {
                var rep = ctx.GetRepository<T>();
                rep.Insert(t);
            }
        }

        public void Delete(int Id, int scopeValue)
        {
            var t = Get(Id, scopeValue);
            Delete(t);
        }

        public void Delete(T t)
        {
            try
            {
                using (IDataContext ctx = DataContext.Instance(baseDatos))
                {
                    var rep = ctx.GetRepository<T>();
                    rep.Delete(t);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        public void DeleteByConditions(string sqlCondition, params object[] args)
        {
            using (IDataContext ctx = DataContext.Instance(baseDatos))
            {
                var rep = ctx.GetRepository<T>();
                rep.Delete(sqlCondition, args);
            }
        }

        public IEnumerable<T> Get(int scopeValue)
        {
            IEnumerable<T> t;
            using (IDataContext ctx = DataContext.Instance(baseDatos))
            {
                var rep = ctx.GetRepository<T>();
                t = rep.Get(scopeValue);
            }
            return t;
        }
        public IEnumerable<T> GetList(string parametro, object Value)
        {
            IEnumerable<T> t;
            using (IDataContext ctx = DataContext.Instance(baseDatos))
            {
                var rep = ctx.GetRepository<T>();
                string sql = string.Format("WHERE {0}='{1}'", parametro, Value);
                t = rep.Find(sql).ToList();
            }
            return t;
        }

        public T Get(string parametro, object Value)
        {
            T t;
            using (IDataContext ctx = DataContext.Instance(baseDatos))
            {
                var rep = ctx.GetRepository<T>();
                string sql = string.Format("WHERE {0}='{1}'", parametro, Value);
                t = rep.Find(sql).FirstOrDefault();
            }
            return t;
        }
        public IEnumerable<T> GetByParameter(string parametro, object Value)
        {
            IEnumerable<T> t;
            using (IDataContext ctx = DataContext.Instance(baseDatos))
            {
                var rep = ctx.GetRepository<T>();
                string sql = string.Format("WHERE {0}='{1}'", parametro, Value);
                t = rep.Find(sql);
            }
            return t;
        }
        public T Get(int Id, int scopeValue)
        {
            T t;
            using (IDataContext ctx = DataContext.Instance(baseDatos))
            {
                var rep = ctx.GetRepository<T>();
                t = rep.GetById(Id, scopeValue);
            }
            return t;
        }
        public T GetById(int PrimaryKey)
        {
            T t;
            using (IDataContext ctx = DataContext.Instance(baseDatos))
            {
                var rep = ctx.GetRepository<T>();
                t = rep.GetById(PrimaryKey);
            }
            return t;
        }
        public T GetById(string PrimaryKey)
        {
            T t;
            using (IDataContext ctx = DataContext.Instance(baseDatos))
            {
                var rep = ctx.GetRepository<T>();
                t = rep.GetById(PrimaryKey);
            }
            return t;
        }
        public IEnumerable<T> Get()
        {
            IEnumerable<T> t;
            using (IDataContext ctx = DataContext.Instance(baseDatos))
            {
                var rep = ctx.GetRepository<T>();
                t = rep.Get();
            }
            return t;
        }
        public int Count(int scopeValue)
        {
            int count = 0;
            using (IDataContext ctx = DataContext.Instance(baseDatos))
            {
                var rep = ctx.GetRepository<T>();
                count = rep.Get(scopeValue).Count();
            }
            return count;
        }

        public void Update(T t)
        {
            using (IDataContext ctx = DataContext.Instance(baseDatos))
            {
                var rep = ctx.GetRepository<T>();
                rep.Update(t);
            }
        }
        public IEnumerable<T> GetPage(int pagina, int maxitems, int scopeValue)
        {
            IEnumerable<T> t;

            using (IDataContext ctx = DataContext.Instance(baseDatos))
            {
                var rep = ctx.GetRepository<T>();
                var page = rep.GetPage(scopeValue, pagina, maxitems);
                t = page.AsEnumerable();
            }
            return t;
        }
        public int CountPage(int scopeValue, int maxItems)
        {
            int result = 0;
            int aux = 0;
            for (int i = 0; i < Count(scopeValue); i++)
            {
                if (aux == maxItems)
                {
                    aux = 0;
                }
                if (aux == 0)
                {
                    result++;
                }
                aux++;
            }

            return result;
        }
        public IEnumerable<T> ExecuteProcedureEnumerable(string procedimiento, params object[] parametros)
        {
            IEnumerable<T> t;
            using (IDataContext ctx = DataContext.Instance(baseDatos))
            {
                t = ctx.ExecuteQuery<T>(CommandType.StoredProcedure, procedimiento, parametros);
            }
            return t;
        }
        public T ExcetuteEscalarProcedure(string nombre, params object[] parametros)
        {
            T t;
            using (IDataContext ctx = DataContext.Instance(baseDatos))
            {
                t = ctx.ExecuteScalar<T>(CommandType.StoredProcedure, nombre, parametros);
            }
            return t;
        }
        public int ExecuteEscalarProcedureInt(string nombre, params object[] parametros)
        {
            int t;
            using (IDataContext ctx = DataContext.Instance(baseDatos))
            {
                t = ctx.ExecuteScalar<int>(CommandType.StoredProcedure, nombre, parametros);
            }
            return t;
        }
        public T ExecuteProcedureSingular(string procedimiento, params object[] parametros)
        {
            T t;
            using (IDataContext ctx = DataContext.Instance(baseDatos))
            {
                t = ctx.ExecuteSingleOrDefault<T>(CommandType.StoredProcedure, procedimiento, parametros);
            }
            return t;
        }

        public List<T> ExecuteProcedurePlural(string procedimiento, params object[] parametros)
        {
            List<T> t;
            using (IDataContext ctx = DataContext.Instance(baseDatos))
            {
                t = ctx.ExecuteSingleOrDefault<List<T>>(CommandType.StoredProcedure, procedimiento, parametros);
            }
            return t;
        }

        public void ExecuteProcedure(string procedimiento, params object[] parametros)
        {
            using (IDataContext ctx = DataContext.Instance(baseDatos))
            {
                ctx.Execute(CommandType.StoredProcedure, procedimiento, parametros);
            }
        }
        public void ExecuteProcedure(string nombreConexion, string procedimiento, params object[] parametros)
        {
            using (IDataContext ctx = DataContext.Instance(nombreConexion))
            {
                ctx.Execute(CommandType.StoredProcedure, procedimiento, parametros);
            }
        }
    }
}

﻿using Mineros.Model.Commons;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mineros.Repository.App
{
    public class SubcategoryRepository:Repository<SubCategory>
    {
        public SubcategoryRepository(string connectionString) : base(connectionString) { }
    }
}

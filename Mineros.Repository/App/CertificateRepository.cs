﻿using Mineros.Model.Commons;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Mineros.Repository
{
    public class CertificateRepository : Repository<Certificate>
    {
        public CertificateRepository(string connectionString) : base(connectionString) { }

        public List<Certificate> GetCertificatesSearch(string nit, int customerId, int typeCertificateId)
        {
            List<Certificate> returnCertificate = new List<Certificate>();
            using (var context = this.Context)
            {
                var certificates = context.ExecuteQuery<Certificate>(System.Data.CommandType.Text, "WHERE Enterprise like '" + nit + "' and CertiticateType = " + typeCertificateId + " AND CustomerId = " + customerId);
                if (certificates != null && certificates.Count() > 0)
                {
                    returnCertificate = certificates.ToList();
                    return returnCertificate;
                }
                return null;
            }
        }

        public Certificate GetCertidicateByNitEnterpriseAndDate(string nit, int typeCertificateId, int customerId,string file_path, string year)
        {
            Certificate returnCertificate = new Certificate();
            using (var context = this.Context)
            {
                var certificates = context.ExecuteQuery<Certificate>(System.Data.CommandType.Text, "WHERE Enterprise like '" + nit + "' and CertiticateType = " + typeCertificateId + " AND CustomerId = " + customerId + " AND FileName like '" + file_path + "' AND YearCertificate = " + year);
                if (certificates != null && certificates.Count() > 0)
                {
                    returnCertificate = certificates.First();
                    return returnCertificate;
                }
                return null;
            }
        }
    }

}

﻿using Mineros.Model.Commons;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace Mineros.Repository.App
{
    public class BrandRepository:Repository<Brands>
    {
        public BrandRepository(string connectionString) : base(connectionString) { }

        public List<Brands> GetAllTagsByProviderId(int providerId)
        {
            using (var context = this.Context)
            {
                var result = context.ExecuteQuery<Brands>(System.Data.CommandType.StoredProcedure, "[dbo].[GetAllTagsByProviderId]",providerId).ToList();

                if (result != null && result.Count > 0)
                {
                    return result;
                }
                return new List<Brands>();
            }
        }

   
    }
}

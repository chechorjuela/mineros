﻿using Mineros.Model.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Repository.App
{
    public class ItemsSubcategoryRepository:Repository<ItemsSubcategory>
    {
        public ItemsSubcategoryRepository(string connectionString) : base(connectionString) { }

        public List<ItemCategories> getAllItemsByProviderId(int providerId)
        {
            using (var context = this.Context)
            {
                var result = context.ExecuteQuery<ItemCategories>(System.Data.CommandType.StoredProcedure, "[dbo].[SP_GetItemsCategoryByProviderId]", providerId).ToList();
                
                if (result != null && result.Count > 0)
                {
                    return result;
                }
                return new List<ItemCategories>();
            }
        }
    }
}

﻿using Mineros.Model.Commons;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mineros.Repository.App
{
    public class TypeActivityRepository:Repository<TypeActivity>
    {
        public TypeActivityRepository(string connectionString) : base(connectionString) { }
    }
}

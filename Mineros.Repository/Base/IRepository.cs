﻿using DotNetNuke.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mineros.Repository
{
    public interface IRepository<T>
    {

        IDataContext Context { get; }
        void Create(T t);
        void Delete(int Id, int scopeValue);
        void Delete(T t);
        void DeleteByConditions(string sqlCondition, params object[] args);
        IEnumerable<T> Get(int scopeValue);
        IEnumerable<T> Get();
        T Get(string paremeter, object Value);
        T GetById(int PrimaryKey);
        T GetById(string PrimaryKey);
        T Get(int Id, int scopeValue);
        int Count(int scopeValue);
        void Update(T t);
        IEnumerable<T> GetPage(int pagina, int maxItems, int scopeValue);
        int CountPage(int scopeValue, int maxItems);
        IEnumerable<T> ExecuteProcedureEnumerable(string procedure, params object[] parameter);
        T ExcetuteEscalarProcedure(string nombre, params object[] parameter);
        int ExecuteEscalarProcedureInt(string nombre, params object[] parameter);
        T ExecuteProcedureSingular(string procedure, params object[] parameter);
        void ExecuteProcedure(string procedure, params object[] parameter);
        IEnumerable<T> GetByParameter(string parameter, object Value);
        void ExecuteProcedure(string nameConnection, string procedure, params object[] parameter);
    }
}

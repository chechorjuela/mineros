﻿using AutoMapper;
using DotNetNuke.Web.Api;
using Mineros.Data.Models;
using Mineros.Model.Commons;
using System.Collections.Generic;
using System.Web.Http;

namespace Mineros.API
{
    public class RouterMapper : IServiceRouteMapper
    {
        public void RegisterRoutes(IMapRoute mapRouteManager)
        {
            Mapper.Initialize(cfg => {
                cfg.CreateMap<EasyDnnNewsDocuments, EasyDnnNewsDocumentsDTO>().ReverseMap();
            });

            mapRouteManager.MapHttpRoute("services", "default", "{controller}/{action}", new[] { "Mineros.API" });
            GlobalConfiguration.Configuration.Formatters
                .JsonFormatter.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
        }
    }
}
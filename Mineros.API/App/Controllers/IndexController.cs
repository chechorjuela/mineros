﻿using Mineros.Domain.Contracts.Common;
using Mineros.Domain.Services;
using Mineros.Model.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Mineros.API
{
    [AllowAnonymous]
    public class IndexController : APIBase
    {
        private RequestUrlService responseService = new RequestUrlService();
        
        [HttpGet]
        public ResponseContract<string> Index()
        {
            ResponseContract<string> response = new ResponseContract<string>();

            try
            {
                response.Data = "success";
            }
            catch (Exception exp)
            {
                response.Header.Message = exp.Message;
                response.Header.Code = HttpCodes.InternalServerError;
            }

            return response;
        }

        [HttpGet]
        public ResponseContract<List<Stock>> GetRequestBvcByNemo(string urlName)
        {
            ResponseContract<List<Stock>> response = new ResponseContract<List<Stock>>();

            try
            {
                //response.Data = this._requestUrlBvcService.ConsultRequestBvc(urlName);
                response.Data = this.responseService.getAccionByName(urlName);
            }
            catch (Exception exp)
            {
                response.Header.Message = exp.Message;
            }
            return response;
        }
        [AllowAnonymous]
        [HttpGet]
        public ResponseContract<List<Stock>> FileGetStock()
        {

            ResponseContract<List<Stock>> response = new ResponseContract<List<Stock>>();
            try
            {
                response.Data = this.responseService.GetFileStock();
            }
            catch (Exception exp)
            {
                response.Header.Message = exp.Message;
            }

            return response;
        }

        [AllowAnonymous]
        [HttpGet]
        public ResponseContract<List<ChartValue>> FileGetStockHome()
        {
            ResponseContract<List<ChartValue>> response = new ResponseContract<List<ChartValue>>();
            try
            {
                response.Data = this.responseService.FileGetStockHome();
            }
            catch (Exception exp)
            {
                response.Header.Message = exp.Message;
            }

            return response;
            
        }

        [AllowAnonymous]
        [HttpGet]
        public ResponseContract<List<GraphicStock>> GetTotalGraphics(string name)
        {

            ResponseContract<List<GraphicStock>> response = new ResponseContract<List<GraphicStock>>();
            try
            {
                response.Data = this.responseService.GetTotalGraphics(name);
            }
            catch(Exception exp)
            {
                response.Header.Message = exp.Message;
            }

            return response;
        }

        [AllowAnonymous]
        [HttpGet]
        public ResponseContract<string> GetTrm()
        {

            ResponseContract<string> response = new ResponseContract<string>();
            try
            {
                response.Data = this.responseService.getTrmBankrep().Replace("\"","");
            }
            catch (Exception exp)
           {
                response.Header.Message = exp.Message;
            }

            return response;
        }
    }
}

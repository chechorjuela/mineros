﻿using DotNetNuke.Entities.Modules;
using DotNetNuke.Services.Search.Entities;
using DotNetNuke.Web.Api;
using Kendo.Mvc.UI;
using Mineros.Domain;
using Mineros.Domain.Contracts;
using Mineros.Domain.Contracts.Common;
using Mineros.Domain.Contracts.Request;
using Mineros.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Mineros.API
{
    [AllowAnonymous]
    public class ProviderController : APIBase
    {
        private ProviderDomain _providerDomain;


        private CategoryDomain _categoryDomain;

        private SubcategoryDomain _subCategoryDomain;

        private TypeActivityDomain _typeActivityDomain;

        public ProviderController()
        {
            this._providerDomain = new ProviderDomain();
            this._categoryDomain = new CategoryDomain();
            this._subCategoryDomain = new SubcategoryDomain();
            this._typeActivityDomain = new TypeActivityDomain();
        }
        /***
         * The services about providers 
         * The Crud
        ***/
        [HttpGet]
        [DnnAuthorize(StaticRoles = "Consulta Proveedores, Administrators,Admin Proveedores")]
        public async Task<ResponseContract<DataSourceResult>> getAllProviders([System.Web.Http.ModelBinding.ModelBinder(typeof(DataSourceRequestModelBinder))] DataSourceRequest requestMessage)
        {
            ResponseContract<DataSourceResult> response = new ResponseContract<DataSourceResult>();
            try
            {
                response.Data = await this._providerDomain.getAllProviders(requestMessage);
            }
            catch (Exception exp)
            {
                response.Header.Message = exp.Message;
                response.Header.Code = HttpCodes.InternalServerError;
            }
            return response;
        }
        [HttpGet]
        public ResponseContract<Providers> findProviderById()
        {
            ResponseContract<Providers> response = new ResponseContract<Providers>();
            try
            {

            }
            catch (Exception exp)
            {
                response.Header.Message = exp.Message;
                response.Header.Code = HttpCodes.InternalServerError;
            }
            return response;
        }
        [HttpGet]
        [DnnAuthorize(StaticRoles = "Administrators,Admin Proveedores")]
        public async Task<ResponseContract<bool>> deleteProviderById(int id)
        { 

            ResponseContract<bool> response = new ResponseContract<bool>();
            try
            {
                response.Data = await this._providerDomain.DeleteProvider(id);
            }
            catch (Exception exp)
            {
                response.Header.Message = exp.Message;
                response.Header.Code = HttpCodes.InternalServerError;
            }
            return response;
        }
        [HttpPost]
        public async Task<ResponseContract<ProviderContract>> createProvider(RequestProviderContract RequestProvider)
        {
            ResponseContract<ProviderContract> response = new ResponseContract<ProviderContract>();
            try
            {
                bool existProvider = await this._providerDomain.getProviderById(RequestProvider.nit);
                if (!existProvider)
                {
                    response.Data = await this._providerDomain.CreateProvider(RequestProvider);
                }
                else
                {
                    response.Header.Message = "provider_exist";
                }
            }
            catch (Exception exp)
            {
                response.Header.Message = exp.Message;
                response.Header.Code = HttpCodes.InternalServerError;
            }
            return response;
        }
        [HttpPost]
        public async  Task<ResponseContract<ProviderContract>> updateProvider(RequestProviderContract RequestProvider)
        {
            ResponseContract<ProviderContract> response = new ResponseContract<ProviderContract>();
            try
            {
                response.Data = await this._providerDomain.UpdateProvider(RequestProvider);
            }
            catch (Exception exp)
            {
                response.Header.Message = exp.Message;
                response.Header.Code = HttpCodes.InternalServerError;
            }
            return response;
        }

     
        [HttpGet]
        public ResponseContract<bool> deleteItem()
        {
            ResponseContract<bool> response = new ResponseContract<bool>();
            try
            {

            }
            catch (Exception exp)
            {
                response.Header.Message = exp.Message;
                response.Header.Code = HttpCodes.InternalServerError;
            }
            return response;
        }
       
        /***
            End Items
            Start services Category
         ***/
        [HttpGet]
        public async Task<ResponseContract<List<CategoryContract>>> getAllCategory()
        {
            ResponseContract<List<CategoryContract>> response = new ResponseContract<List<CategoryContract>>();
            try
            {
                response.Data = await this._categoryDomain.getAllCategories();
            }
            catch (Exception exp)
            {
                response.Header.Message = exp.Message;
                response.Header.Code = HttpCodes.InternalServerError;
            }
            return response;
        }
        [HttpGet]
        public async Task<ResponseContract<DataSourceResult>> getAllCategoryFilter([System.Web.Http.ModelBinding.ModelBinder(typeof(DataSourceRequestModelBinder))] DataSourceRequest requestMessage)
        {
            ResponseContract<DataSourceResult> response = new ResponseContract<DataSourceResult>();
            try
            {
                response.Data = await this._categoryDomain.getAllCategoriesFilter(requestMessage);
            }
            catch (Exception exp)
            {
                response.Header.Message = exp.Message;
                response.Header.Code = HttpCodes.InternalServerError;
            }
            return response;
        }
        [HttpGet]
        public ResponseContract<CategoryContract> findCategoryById()
        {
            ResponseContract<CategoryContract> response = new ResponseContract<CategoryContract>();
            try
            {

            }
            catch (Exception exp)
            {
                response.Header.Message = exp.Message;
                response.Header.Code = HttpCodes.InternalServerError;
            }
            return response;
        }
        [HttpPost]
        [DnnAuthorize(StaticRoles = "Administrators,Admin Proveedores")]
        public async Task<ResponseContract<bool>> CreateCategory(RequestCategoryContract categoryContract)
        {
            ResponseContract<bool> response = new ResponseContract<bool>();
            try
            {
                response.Data = await this._categoryDomain.saveCreateCategory(categoryContract);
            }
            catch (Exception exp)
            {
                response.Header.Message = exp.Message;
                response.Header.Code = HttpCodes.InternalServerError;
            }
            return response;
        }
        [HttpGet]
        [DnnAuthorize(StaticRoles = "Administrators,Admin Proveedores")]
        public ResponseContract<List<CategoryContract>> categoryByTypeActivityId()
        {
            ResponseContract<List<CategoryContract>> response = new ResponseContract<List<CategoryContract>>();
            try
            {

            }
            catch (Exception exp)
            {
                response.Header.Message = exp.Message;
                response.Header.Code = HttpCodes.InternalServerError;
            }
            return response;
        }
        [HttpPost]
        [DnnAuthorize(StaticRoles = "Administrators,Admin Proveedores")]
        public async Task<ResponseContract<CategoryContract>> UpdateCategory(RequestCategoryContract categoryContract)
        {
            ResponseContract<CategoryContract> response = new ResponseContract<CategoryContract>();
            try
            {
                response.Data = await this._categoryDomain.updateCategory(categoryContract);

            }
            catch (Exception exp)
            {
                response.Header.Message = exp.Message;
                response.Header.Code = HttpCodes.InternalServerError;
            }
            return response;
        }
        [HttpGet]
        [DnnAuthorize(StaticRoles = "Administrators,Admin Proveedores")]
        public async Task<ResponseContract<bool>> DeleteCategory(int id)
        {
            ResponseContract<bool> response = new ResponseContract<bool>();
            try
            {
                response.Data = await this._categoryDomain.deleteCategory(id);
            }
            catch (Exception exp)
            {
                response.Header.Message = exp.Message;
                response.Header.Code = HttpCodes.InternalServerError;
            }
            return response;
        }
        /**
         * SubCategories services
         * */
        [HttpGet]
        public async Task<ResponseContract<DataSourceResult>> getAllSubcategory([System.Web.Http.ModelBinding.ModelBinder(typeof(DataSourceRequestModelBinder))] DataSourceRequest requestMessage)
        {
            ResponseContract<DataSourceResult> response = new ResponseContract<DataSourceResult>();
            try
            {
                response.Data = await this._subCategoryDomain.getAllSubcategoryAll(requestMessage);
            }
            catch (Exception exp)
            {
                response.Header.Message = exp.Message;
                response.Header.Code = HttpCodes.InternalServerError;

            }
            return response;
        }
        [HttpGet]
        public ResponseContract<SubCategoryContract> findSubcategoryById()
        {

            ResponseContract<SubCategoryContract> response = new ResponseContract<SubCategoryContract>();
            try
            {

            }
            catch (Exception exp)
            {
                response.Header.Message = exp.Message;
                response.Header.Code = HttpCodes.InternalServerError;
            }
            return response;
        }
        [HttpGet]
        public ResponseContract<List<SubCategoryContract>> findSubcategoryByCategoryId()
        {

            ResponseContract<List<SubCategoryContract>> response = new ResponseContract<List<SubCategoryContract>>();
            try
            {

            }
            catch (Exception exp)
            {
                response.Header.Message = exp.Message;
                response.Header.Code = HttpCodes.InternalServerError;
            }
            return response;
        }
        [HttpPost]
        [DnnAuthorize(StaticRoles = "Administrators,Admin Proveedores")]
        public async Task<ResponseContract<bool>> createSubcategory(RequestSubcategoryContract subCategory)
        {
            ResponseContract<bool> response = new ResponseContract<bool>();
            try
            {
                response.Data = await this._subCategoryDomain.storeSubcategory(subCategory);
            }
            catch (Exception exp)
            {
                response.Header.Message = exp.Message;
                response.Header.Code = HttpCodes.InternalServerError;
            }
            return response;
        }
        [HttpPost]
        [DnnAuthorize(StaticRoles = "Administrators,Admin Proveedores")]
        public async  Task<ResponseContract<bool>> updateSubcategory(RequestSubcategoryContract subCategory)
        {
            ResponseContract<bool> response = new ResponseContract<bool>();
            try
            {
                response.Data = await this._subCategoryDomain.updateCategory(subCategory);
            }
            catch (Exception exp)
            {
                response.Header.Message = exp.Message;
                response.Header.Code = HttpCodes.InternalServerError;
            }
            return response;
        }
        [HttpGet]
        [DnnAuthorize(StaticRoles = "Administrators,Admin Proveedores")]
        public async Task<ResponseContract<bool>> deleteSubCategory(int id)
        {
            ResponseContract<bool> response = new ResponseContract<bool>();
            try
            {
                response.Data = await this._subCategoryDomain.deleteSubcategory(id);
            }
            catch (Exception exp)
            {
                response.Header.Message = exp.Message;
                response.Header.Code = HttpCodes.InternalServerError;
            }
            return response;
        }
        /***
         * 
         * TypeActivity Service
         * 
         * ***/
        [HttpGet]
        public async Task<ResponseContract<List<TypeActivityContract>>> getAllActivity()
        {
            ResponseContract<List<TypeActivityContract>> response = new ResponseContract<List<TypeActivityContract>>();
            try
            {
                response.Data = await this._typeActivityDomain.getAllTypeActivity();
            }catch(Exception exp)
            {

                response.Header.Message = exp.Message;
                response.Header.Code = HttpCodes.InternalServerError;
            }
            return response;
        }
        [HttpGet]
        public ResponseContract<TypeActivityContract> getActivityById()
        {
            ResponseContract<TypeActivityContract> response = new ResponseContract<TypeActivityContract>();
            try
            {
                
            }
            catch (Exception exp)
            {

                response.Header.Message = exp.Message;
                response.Header.Code = HttpCodes.InternalServerError;
            }
            return response;
        }
        [HttpGet]
        public async Task<ResponseContract<bool>> monthlyReport()
        {
            ResponseContract<bool> response = new ResponseContract<bool>();
            try
            {
                response.Data = await this._providerDomain.getMontlyReport();
            }
            catch (Exception exp)
            {
                response.Data = false;
                response.Header.Message = exp.Message;
            }
            return response;
        }

    }
}

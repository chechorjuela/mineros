﻿using DotNetNuke.ComponentModel.DataAnnotations;
using DotNetNuke.Entities.Portals;
using DotNetNuke.Entities.Users;
using DotNetNuke.Services.Log.EventLog;
using DotNetNuke.Web.Api;
using Mineros.Data;
using Mineros.Data.Models;
using Mineros.Model.Commons;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Caching;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace Mineros.API
{
    public class TraceController : APIBase
    {
        MemoryCache memCache = MemoryCache.Default;

        [AllowAnonymous]
        [HttpGet]
        [Cacheable(CacheTimeOut = 60000)]
        public string FileGetContents(string nemo)
        {
            return GetResponse(nemo);
        }

        public string GetResponse(string nemo)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(("http://bvc.com.co/mercados/GraficosServlet?home=no&tipo=ACCION&mercInd=RV&nemo=") + nemo);

                request.Method = "GET";
                String test = String.Empty;
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    Stream dataStream = response.GetResponseStream();
                    StreamReader reader = new StreamReader(dataStream);
                    test = reader.ReadToEnd();
                    reader.Close();
                    dataStream.Close();
                }
                return test;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string GetVariacion(float Actual, float Anterior)
        {
            var mayor = (Actual > Anterior) ? Actual : Anterior;
            var menor = (Actual < Anterior) ? Actual : Anterior;

            var res = (1 - (menor / mayor)) * 100;

            if (Actual < Anterior)
                res = -res;

            return res.ToString("0.00");
        }

        [AllowAnonymous]
        [HttpGet]
        [Cacheable(CacheTimeOut = 120000)]
        public List<Stock> FileGetStock()
        {
            List<Stock> stocks = new List<Stock>();
            stocks = memCache.Get("FileGetStockData") as List<Stock>;
            if (stocks != null)
                return stocks;
            else
                stocks = new List<Stock>();

            try
            {
                CultureInfo ci = new CultureInfo("en-US");
                Thread.CurrentThread.CurrentCulture = ci;
                Thread.CurrentThread.CurrentUICulture = ci;


                string CEMARGOS = GetResponse("CEMARGOS");
                var splitCEMARGOS = CEMARGOS.Split('\n');
                var cm = splitCEMARGOS[0].Split(',');
                stocks.Add(
                            new Stock()
                            {
                                Accion = "CEMARGOS",
                                Actual = float.Parse(cm[1].ToString()),
                                Anterior = float.Parse(cm[3].ToString()),
                                Fecha = Convert.ToDateTime(cm[0].ToString()),
                                Valor = float.Parse(cm[2].ToString()),
                                Variacion = GetVariacion(float.Parse(cm[1].ToString()), float.Parse(cm[3].ToString()))
                            });

                string PFCEMARGOS = GetResponse("PFCEMARGOS");
                var splitPFCEMARGOS = PFCEMARGOS.Split('\n');
                var pfcm = splitPFCEMARGOS[0].Split(',');
                stocks.Add(
                            new Stock()
                            {
                                Accion = "PFCEMARGOS",
                                Actual = float.Parse(pfcm[1].ToString()),
                                Anterior = float.Parse(pfcm[3].ToString()),
                                Fecha = Convert.ToDateTime(pfcm[0].ToString()),
                                Valor = float.Parse(pfcm[2].ToString()),
                                Variacion = GetVariacion(float.Parse(pfcm[1].ToString()), float.Parse(pfcm[3].ToString()))
                            });

                CacheItemPolicy policy = new CacheItemPolicy { AbsoluteExpiration = DateTimeOffset.UtcNow.AddMinutes(5) };
                memCache.Add("FileGetStockData", stocks, policy);

                memCache.Add("FileGetStockDataContingency", stocks, new CacheItemPolicy { AbsoluteExpiration = ObjectCache.InfiniteAbsoluteExpiration });
                return stocks;
            }
            catch (Exception ex)
            {
                if (memCache.Get("FileGetStockDataContingency") != null)
                    return memCache.Get("FileGetStockDataContingency") as List<Stock>;

                return new List<Stock>();
            }
        }

        [AllowAnonymous]
        [HttpGet]
        [Cacheable(CacheTimeOut = 120000)]
        public List<GraphicStockItems> FileGetStockHome()
        {
            List<GraphicStockItems> totalStock = new List<GraphicStockItems>();
            CultureInfo ci = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = ci;
            Thread.CurrentThread.CurrentUICulture = ci;
            totalStock = memCache.Get("FileGetStockHomeData") as List<GraphicStockItems>;
            if (totalStock != null)
                return totalStock;
            else
                totalStock = new List<GraphicStockItems>();

            try
            {
                List<Stock> stocks = new List<Stock>();
                string CEMARGOS = GetResponse("CEMARGOS");
                var splitCEMARGOS = CEMARGOS.Split('\n');
                var accionCEMARGOS = from e in splitCEMARGOS
                                     where e.Split(',').Count() > 1
                                     select
                                       new Stock()
                                       {
                                           Accion = "CEMARGOS",
                                           Actual = float.Parse(e.Split(',')[1].ToString()),
                                           Anterior = float.Parse(e.Split(',')[3].ToString()),
                                           Fecha = Convert.ToDateTime(e.Split(',')[0].ToString()),
                                           Valor = float.Parse(e.Split(',')[2].ToString())
                                       };

                stocks.AddRange(accionCEMARGOS);
                string PFCEMARGOS = GetResponse("PFCEMARGOS");
                var splitPFCEMARGOS = PFCEMARGOS.Split('\n');
                var accionPFCEMARGOS = from e in splitPFCEMARGOS
                                       where e.Split(',').Count() > 1
                                       select
                                         new Stock()
                                         {
                                             Accion = "PFCEMARGOS",
                                             Actual = float.Parse(e.Split(',')[1].ToString()),
                                             Anterior = float.Parse(e.Split(',')[3].ToString()),
                                             Fecha = Convert.ToDateTime(e.Split(',')[0].ToString()),
                                             Valor = float.Parse(e.Split(',')[2].ToString())
                                         };

                stocks.AddRange(accionPFCEMARGOS);

                int auxCont = 1;

                var dates = stocks.Distinct().OrderBy(x => x.Fecha).Select(x => new GraphicStockItems { Fecha = x.Fecha, Id = auxCont++ });

                var stocksDesc = stocks.OrderBy(x => x.Fecha);

                float auxCEMARGOS = 0;

                float auxPFCEMARGOS = 0;

                foreach (var item in dates)
                {
                    var Argos = stocks.Where(x => x.Fecha == item.Fecha);
                    if (Argos.Count() > 0)
                    {
                        GraphicStockItems ga = new GraphicStockItems() { Fecha = item.Fecha };

                        foreach (var i in Argos)
                        {
                            if (i.Accion == "CEMARGOS")
                            {
                                ga.CEMARGOS = i.Actual == 0 ? auxCEMARGOS : i.Actual;
                                auxCEMARGOS = i.Actual == 0 ? auxCEMARGOS : i.Actual;
                                ga.PFCEMARGOS = ga.PFCEMARGOS == 0 ? auxPFCEMARGOS : ga.PFCEMARGOS;
                            }

                            if (i.Accion == "PFCEMARGOS")
                            {
                                ga.PFCEMARGOS = i.Actual == 0 ? auxPFCEMARGOS : i.Actual;
                                auxPFCEMARGOS = i.Actual == 0 ? auxPFCEMARGOS : i.Actual;
                                ga.CEMARGOS = ga.CEMARGOS == 0 ? auxCEMARGOS : ga.CEMARGOS;
                            }
                        }
                        totalStock.Add(ga);
                    }
                }

                CacheItemPolicy policy = new CacheItemPolicy { AbsoluteExpiration = DateTimeOffset.UtcNow.AddMinutes(5) };
                memCache.Add("FileGetStockHomeData", totalStock, policy);

                memCache.Add("FileGetStockHomeDataContingency", totalStock, new CacheItemPolicy { AbsoluteExpiration = ObjectCache.InfiniteAbsoluteExpiration });
                return totalStock;
            }
            catch (Exception ex)
            {
                if (memCache.Get("FileGetStockHomeDataContingency") != null)
                    return memCache.Get("FileGetStockHomeDataContingency") as List<GraphicStockItems>;

                return new List<GraphicStockItems>();
            }

        }

        [AllowAnonymous]
        [HttpGet]
        [Cacheable(CacheTimeOut = 120000)]
        public InfoRelevanteResult GetInfoRelevante(string cultureCountry, string category)
        {
            InfoRelevanteResult result = new InfoRelevanteResult();
            int countItems = 0;

            EasyDnnNewsData easyDnnNewsData = new EasyDnnNewsData();
            List<InfoRelevanteViewItem> Items = new List<InfoRelevanteViewItem>();
            try
            {
                //var Categories = easyDnnNewsData.GetCategoriesArgosReportes(CultureCountry);
                //var Categories = easyDnnNewsData.GetCategories(cultureCountry);
                var Categories = easyDnnNewsData.GetAllCategoriesNews(cultureCountry);
                var parentCategory = Categories.FirstOrDefault(s => s.CategoryName.ToLower() == category.ToLower());
                if (parentCategory != null)
                {
                    var Articles = easyDnnNewsData.GetArticles(parentCategory.CategoryID, null, 0, 0, ref countItems, cultureCountry).OrderByDescending(s => s.PublishDate);

                    foreach (var article in Articles)
                    {
                        var documents = easyDnnNewsData.GetALLEasyDnnNewsDocuments(article.ArticleID, cultureCountry);
                        Articles articleView = ConvertArticleToArticleView(article);

                        var document = documents.FirstOrDefault(x => x.ArticleID == article.ArticleID && x.LocaleCode.ToLower() == cultureCountry.ToLower());

                        Documents documentView = ConvertDocumentToDocumentView(document);

                        InfoRelevanteViewItem item = new InfoRelevanteViewItem()
                        {
                            ArticleID = article.ArticleID,
                            CategoryID = article.CategoryID,
                            Title = article.Title,
                            PublishDate = article.PublishDate,
                            Article = articleView,
                            Document = documentView,
                            ModuleID = article.ModuleID
                        };
                        Items.Add(item);
                    }

                    var categoriesItems = (from ca in Articles.Select(s => s.CategoryID).Distinct() select new EasyDnnNewsCategories() { CategoryID = ca, CategoryName = Categories.First(s => s.CategoryID == ca).CategoryName }).Distinct().OrderByDescending(s => s.CategoryName).ToList();

                    List<Categories> categoriesList = new List<Categories>();
                    foreach (var item in categoriesItems)
                    {
                        Categories ca = ConvertCategoryToCategoryView(item);
                        categoriesList.Add(ca);
                    }
                    result.CategoriesItems = categoriesList;
                    result.Items = Items;
                }
                return result;
            }
            catch (Exception ex)
            {
                SendLog("Error Service GetInfoRelevante " + ex.ToString());
                return new InfoRelevanteResult();
            }
        }

        private void SendLog(string msn)
        {
            EventLogController eventLog = new EventLogController();
            LogInfo logInfo = new LogInfo();
            PortalSettings ps = PortalController.Instance.GetCurrentPortalSettings();
            UserInfo userInfo = UserController.Instance.GetCurrentUserInfo();
            logInfo.LogUserID = userInfo.UserID;
            logInfo.LogPortalID = ps.PortalId;
            logInfo.LogTypeKey = EventLogController.EventLogType.ADMIN_ALERT.ToString();
            logInfo.AddProperty("Mineros Inversionistas", msn);
            eventLog.AddLog(logInfo);
        }

        private Articles ConvertArticleToArticleView(EasyDnnNewsApp article)
        {
            Articles articleView = new Articles();
            articleView.Article = article.Article;
            articleView.ArticleID = article.ArticleID;
            articleView.ArticleImage = article.ArticleImage;
            articleView.Category = article.Category;
            articleView.CategoryID = article.CategoryID;
            articleView.CategoryName = article.CategoryName;
            articleView.DateAdded = article.DateAdded;
            articleView.Featured = article.Featured;
            articleView.LangArticle = article.LangArticle;
            articleView.LangLocaleCode = article.LangLocaleCode;
            articleView.LangLocaleString = article.LangLocaleString;
            articleView.LangSubTitle = article.LangSubTitle;
            articleView.LangSummary = article.LangSummary;
            articleView.LangTitle = article.LangTitle;
            articleView.PortalID = article.PortalID;
            articleView.PublishDate = article.PublishDate;
            articleView.SubTitle = article.SubTitle;
            articleView.Summary = article.Summary;
            articleView.Title = article.Title;
            return articleView;
        }
        private Documents ConvertDocumentToDocumentView(EasyDnnNewsDocuments document)
        {
            Documents documentView = new Documents();
            if (document != null)
            {
                documentView.ArticleID = document.ArticleID;
                documentView.DateUploaded = document.DateUploaded;
                documentView.Description = document.Description;
                documentView.DocEntryID = document.DocEntryID;
                documentView.FileExtension = document.FileExtension;
                documentView.FileName = document.FileName;
                documentView.FilePath = document.FilePath;
                documentView.LocaleCode = document.LocaleCode;
                documentView.ModuleId = document.ModuleId;
                documentView.PortalId = document.PortalId;
                documentView.Title = document.Title;
                documentView.UrlView = document.UrlView;
                documentView.UserID = document.UserID;
                documentView.Visible = document.Visible;
            }
            return documentView;
        }
        private Categories ConvertCategoryToCategoryView(EasyDnnNewsCategories item)
        {
            Categories ca = new Categories();
            ca.CategoryID = item.CategoryID;
            ca.CategoryName = item.CategoryName;
            ca.Description = item.Description;
            ca.Level = item.Level;
            ca.ParentCategory = item.ParentCategory;
            ca.PortalID = item.PortalID;
            ca.Position = item.Position;
            return ca;
        }
    }

}
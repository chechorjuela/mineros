﻿using DotNetNuke.Entities.Modules;
using DotNetNuke.Services.FileSystem;
using DotNetNuke.Services.Exceptions;
using Mineros.Data.Models;
using Mineros.Domain.App;
using Mineros.Domain.Contracts;
using Mineros.Domain.Contracts.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Newtonsoft.Json;
using Mineros.Model;
using Mineros.Model.Commons;

namespace Mineros.API
{
    public class WorkusController : APIBase
    {
        private EasyDnnNewsDomain easyNewsDomain = new EasyDnnNewsDomain();
        private WorkUsDomain workusDomain = new WorkUsDomain();
        private InterestContactDomain _interestContactDomain = new InterestContactDomain();

        [HttpGet]
        [AllowAnonymous]
        public ResponseContract<List<EasyDnnNewsAppJobs>> getListWorks(int page, int limit)
        {
            ResponseContract<List<EasyDnnNewsAppJobs>> responseContract = new ResponseContract<List<EasyDnnNewsAppJobs>>();

            try
            {
                responseContract.Data = easyNewsDomain.getEasyNewsWorks(page, limit);
            }
            catch (Exception exp)
            {
                responseContract.Header.Message = exp.Message;
                responseContract.Header.Code = HttpCodes.InternalServerError;
            }
            return responseContract;
        }

        [HttpGet]
        [AllowAnonymous]
        public ResponseContract<int> getCountWorksJob()
        {
            int page = 0;
            int limit = 0;
            ResponseContract<int> responseContract = new ResponseContract<int>();
            try
            {
                responseContract.Data = easyNewsDomain.getEasyNewsWorks(page, limit).Count();
            }
            catch (Exception exp)
            {
                responseContract.Header.Message = exp.Message;
                responseContract.Header.Code = HttpCodes.InternalServerError;
            }
            return responseContract;

        }
        [HttpPost]
        [AllowAnonymous]
        public ResponseContract<InterestContact> SendFormEmail()
        {
            FileManager fileManager = new FileManager();
            FolderManager folderManager = new FolderManager();
            PortalModuleBase oPortalMB = new PortalModuleBase();

            WorkUsContactContract workUsContactContract = new WorkUsContactContract();
            ResponseContract<InterestContact> response = new ResponseContract<InterestContact>();
            try
            {
                var httpRequest = HttpContext.Current.Request;
                int key = 0;

                foreach (var formData in httpRequest.Form.AllKeys)
                {
                    switch (key)
                    {
                        case 0:
                            workUsContactContract.name = httpRequest.Form[key];
                            break;
                        case 1:
                            workUsContactContract.phone = httpRequest.Form[key];
                            break;
                        case 2:
                            workUsContactContract.city = httpRequest.Form[key];
                            break;
                        case 3:
                            workUsContactContract.email = httpRequest.Form[key];
                            break;
                        case 4:
                            workUsContactContract.title_job = httpRequest.Form[key];
                            break;
                        case 5:
                            workUsContactContract.country_job = httpRequest.Form[key];
                            break;
                        case 6:
                            workUsContactContract.city_job = httpRequest.Form[key];
                            break;
                        case 7:
                            workUsContactContract.enterprise_job = httpRequest.Form[key];
                            break;
                        case 8:
                            workUsContactContract.summary_job = httpRequest.Form[key];
                            break;

                    }
                    key++;
                }
               
                var pic = HttpContext.Current.Request.Files[0];
                var folders = folderManager.GetFolders(oPortalMB.PortalId);
                string nameFolderDocument = "TrabajeConNosotros";
                string nameFolder = "FileDocument/" + nameFolderDocument;
                IFolderInfo folderDocumentPut = folders.FirstOrDefault(f => f.FolderName == nameFolderDocument);
                if (folderDocumentPut == null)
                {
                    folderDocumentPut = folderManager.AddFolder(oPortalMB.PortalId, nameFolder);
                }
                IFileInfo fileInfo = fileManager.AddFile(folderDocumentPut, pic.FileName, pic.InputStream);
                workUsContactContract.file = fileInfo.PhysicalPath;
                InterestContact interestContact = new InterestContact();
                foreach (string fields in httpRequest.Form)
                {
                    switch (fields)
                    {
                        case "number_id":
                            interestContact.number_id = httpRequest.Form[fields];
                            break;
                        case "name":
                            interestContact.name_contact = httpRequest.Form[fields];
                            break;
                        case "email":
                            interestContact.email = httpRequest.Form[fields];
                            break;
                        case "phone":
                            interestContact.number_phone = httpRequest.Form[fields];
                            break;
                        case "current_city":
                            interestContact.city_current = httpRequest.Form[fields];
                            break;
                        case "current_country":
                            interestContact.country_current = httpRequest.Form[fields];
                            break;
                        case "title_job":
                            interestContact.application = httpRequest.Form[fields];
                            break;
                        case "locationApplicant":
                            interestContact.locationApplicant = httpRequest.Form[fields];
                            break;
                        case "processApplicant":
                            interestContact.processApplicant = httpRequest.Form[fields];
                            break;
                        case "positionApplicant":
                            interestContact.positionApplicant = httpRequest.Form[fields];
                            break;

                    }
                }
                string Path_file = fileInfo.PhysicalPath;
                interestContact.file_dir = "/Portals/" + oPortalMB.PortalId.ToString() + "/" + fileInfo.RelativePath;
                response.Data = this._interestContactDomain.storeInterestContact(interestContact, Path_file, false);
                workusDomain.sendEmailContactWork(workUsContactContract);
                ModuleLoadException exp = new ModuleLoadException(JsonConvert.SerializeObject(workUsContactContract).ToString());
                Exceptions.LogException(exp);
            }
            catch (Exception exp)
            {
                response.Header.Message = exp.Message;
                response.Header.Code = HttpCodes.InternalServerError;
            }
            return response;
        }
    }
}

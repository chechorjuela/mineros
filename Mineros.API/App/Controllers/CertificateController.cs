﻿using DotNetNuke.Entities.Modules;
using DotNetNuke.Services.FileSystem;
using DotNetNuke.Web.Api;
using Mineros.Data.Models;
using Mineros.Domain;
using Mineros.Domain.Contracts;
using Mineros.Domain.Contracts.Common;
using Mineros.Model.Commons;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Mineros.API
{
    public class CertificateController : APIBase
    {
        private CertificateTypeDomain _certificateTypeDom;

        private CertificateDomain _certiticateDomain;

        private CustomerDomain _customerDoamin;

        private CertificateDecevalDomain _certificateDecevalDomain;

        private ClienteDecevalDomain _clienteDecevalDomain;

        public CertificateController()
        {
            this._certificateTypeDom = new CertificateTypeDomain();
            this._certiticateDomain = new CertificateDomain();
            this._customerDoamin = new CustomerDomain();
            this._certificateDecevalDomain = new CertificateDecevalDomain();
            this._clienteDecevalDomain = new ClienteDecevalDomain();
        }

        [HttpGet]
        [AllowAnonymous]
        public ResponseContract<List<CertificateType>> GetCertificateType()
        {

            ResponseContract<List<CertificateType>> responseContract = new ResponseContract<List<CertificateType>>();
            try
            {
                responseContract.Data = this._certificateTypeDom.GetCertificateAll();
            }
            catch (Exception exp)
            {
                responseContract.Header.Message = exp.Message;
                responseContract.Header.Code = HttpCodes.InternalServerError;
            }


            return responseContract;
        }
        [HttpGet]
        [AllowAnonymous]
        public ResponseContract<List<Customer>> GetCustomers()
        {

            ResponseContract<List<Customer>> responseContract = new ResponseContract<List<Customer>>();
            try
            {
                responseContract.Data = this._customerDoamin.GetCustomers();
            }
            catch (Exception exp)
            {

            }

            return responseContract;
        }

        [HttpPost]
        [DnnAuthorize(StaticRoles = "Administrators")]
        public ResponseContract<List<Certificate>> uploadFilesCertificate()
        {

            ResponseContract<List<Certificate>> response = new ResponseContract<List<Certificate>>();
            //context.Response.ContentType = "text/plain";
            try
            {
                var httpRequest = HttpContext.Current.Request;

                FileManager fileManager = new FileManager();
                FolderManager folderManager = new FolderManager();
                PortalModuleBase oPortalMB = new PortalModuleBase();
                var folders = folderManager.GetFolders(oPortalMB.PortalId);
                IFolderInfo folderDocuments = folders.FirstOrDefault(s => s.FolderName == "FileDocument");
                //IFolderInfo ifolderDocuments = folders.FirstOrDefault(s => s.FolderName == "FileDocument");

                if (folderDocuments == null)
                {
                    folderManager.AddFolder(oPortalMB.PortalId, "FileDocument");
                    folders = folderManager.GetFolders(oPortalMB.PortalId);
                    folderDocuments = folders.FirstOrDefault(s => s.FolderName == "FileDocument");
                }
                string year_selected = httpRequest["year"];

                List<Certificate> listCertificate = new List<Certificate>();
                foreach (string file in httpRequest.Files)
                {
                    Certificate certificate = new Certificate();
                    var postedFile = httpRequest.Files[file];
                    int nit_file = postedFile.FileName.IndexOf("_");
                    string nameFolderDocument = postedFile.FileName.Substring(0, nit_file);
                    string nameFolder = "FileDocument/" + nameFolderDocument;
                    IFolderInfo folderDocumentPut = folders.FirstOrDefault(f => f.FolderName == nameFolderDocument);
                    if (folderDocumentPut == null)
                    {
                        folderDocumentPut = folderManager.AddFolder(oPortalMB.PortalId, nameFolder);
                    }
                    //var filePath = HttpContext.Current.Server.MapPath("~/" + postedFile.FileName);
                    IFileInfo fileInfo = fileManager.AddFile(folderDocumentPut, postedFile.FileName, postedFile.InputStream);
                    if (fileInfo != null)
                    {
                        certificate = this._certiticateDomain.GetCertificateByNitEnterpriseAndYear(nameFolderDocument, Int32.Parse(httpRequest["typecertificate"]), Int32.Parse(httpRequest["customer"]), fileInfo.FileName, year_selected);
                        if (certificate != null)
                        {
                            certificate.FileName = fileInfo.FileName;
                            certificate.FileExtension = fileInfo.Extension;
                            certificate.Enterprise = nameFolderDocument;
                            certificate.FilePath = "/Portals/" + oPortalMB.PortalId.ToString() + "/" + fileInfo.RelativePath;
                            certificate.YearCertificate = year_selected;
                            certificate.UpdateAt = DateTime.Now;
                            certificate.CustomerID = Int32.Parse(httpRequest["customer"]);
                            certificate.CertiticateType = Int32.Parse(httpRequest["typecertificate"]);
                            this._certiticateDomain.updateCertificate(certificate);
                        }
                        else
                        {
                            certificate = new Certificate();
                            certificate.FileName = fileInfo.FileName;
                            certificate.FileExtension = fileInfo.Extension;
                            certificate.Enterprise = nameFolderDocument;
                            certificate.FilePath = "/Portals/" + oPortalMB.PortalId.ToString() + "/" + fileInfo.RelativePath;
                            certificate.CreateAt = DateTime.Now;
                            certificate.YearCertificate = year_selected;
                            certificate.CustomerID = Int32.Parse(httpRequest["customer"]);
                            certificate.CertiticateType = Int32.Parse(httpRequest["typecertificate"]);
                            this._certiticateDomain.storeCertificate(certificate);
                        }

                    }
                    listCertificate.Add(certificate);
                }
                if (listCertificate.Count > 0)
                {
                    response.Data = listCertificate.ToList();

                }
            }
            catch (Exception exp)
            {
                response.Header.Message = exp.Message;
                response.Header.Code = HttpCodes.InternalServerError;
            }


            return response;
        }
        [HttpPost]
        [DnnAuthorize(StaticRoles = "Administrators")]
        public ResponseContract<List<CertificateDeceval>> uploadFilesCertificateDeceval()
        {
            ResponseContract<List<CertificateDeceval>> response = new ResponseContract<List<CertificateDeceval>>();

            try
            {
                //context.Response.ContentType = "text/plain";
                var httpRequest = HttpContext.Current.Request;

                FileManager fileManager = new FileManager();
                FolderManager folderManager = new FolderManager();
                PortalModuleBase oPortalMB = new PortalModuleBase();
                var folders = folderManager.GetFolders(oPortalMB.PortalId);
                IFolderInfo folderDocuments = folders.FirstOrDefault(s => s.FolderName == "FileDocument");
                //IFolderInfo ifolderDocuments = folders.FirstOrDefault(s => s.FolderName == "FileDocument");

                if (folderDocuments == null)
                {
                    folderManager.AddFolder(oPortalMB.PortalId, "FileDocument");
                    folders = folderManager.GetFolders(oPortalMB.PortalId);
                    folderDocuments = folders.FirstOrDefault(s => s.FolderName == "FileDocument");
                }

                List<CertificateDeceval> listCertificate = new List<CertificateDeceval>();

                string nameFolder = "FileDocument/Accionistas/";

                IFolderInfo folderDocumentPut = folders.FirstOrDefault(f => f.FolderName == "Accionistas");
                if (folderDocumentPut == null)
                {
                    folderDocumentPut = folderManager.AddFolder(oPortalMB.PortalId, nameFolder);
                }

                foreach (string file in httpRequest.Files)
                {
                    CertificateDeceval certificate = new CertificateDeceval();
                    var postedFile = httpRequest.Files[file];
                    int codeDecevalIndex = postedFile.FileName.IndexOf(".");
                    string codeDeceval = postedFile.FileName.Substring(0, codeDecevalIndex);
                    //string nameFolder = "FileDocument/" + httpRequest["nit"];
                  
                    //var filePath = HttpContext.Current.Server.MapPath("~/" + postedFile.FileName);
                    bool fileExist = fileManager.FileExists(folderDocumentPut, postedFile.FileName);
                    if (fileExist)
                    {
                        IFileInfo fileFind = fileManager.GetFile(folderDocumentPut, postedFile.FileName);
                        fileManager.DeleteFile(fileFind);
                    }
                    IFileInfo fileInfo = fileManager.AddFile(folderDocumentPut, postedFile.FileName, postedFile.InputStream);
                    if (fileInfo != null)
                    {
                        certificate = this._certificateDecevalDomain.GetCertificateByCodedeceval(codeDeceval.ToString());
                        if (certificate != null)
                        {
                            certificate.FileName = fileInfo.FileName;
                            certificate.FileExtension = fileInfo.Extension;
                            certificate.Identication = null;
                            certificate.FilePath = "/Portals/" + oPortalMB.PortalId.ToString() + "/" + fileInfo.RelativePath;
                            certificate.UpdateAt = DateTime.Now;
                            certificate.CodeDeceval = codeDeceval.ToString();

                            this._certificateDecevalDomain.updateCertificatedeceval(certificate);

                        }
                        else
                        {
                            certificate = new CertificateDeceval();
                            certificate.FileName = fileInfo.FileName;
                            certificate.FileExtension = fileInfo.Extension;
                            certificate.Identication = null;
                            certificate.FilePath = "/Portals/" + oPortalMB.PortalId.ToString() + "/" + fileInfo.RelativePath;
                            certificate.CreateAt = DateTime.Now;
                            certificate.CodeDeceval = codeDeceval.ToString();

                            this._certificateDecevalDomain.storeCertificate(certificate);
                        }

                    }
                    listCertificate.Add(certificate);
                }
                if (listCertificate.Count > 0)
                {
                    response.Data = listCertificate.ToList();
                }
            }
            catch (Exception exp)
            {
                response.Header.Message = exp.Message;
                response.Header.Code = HttpCodes.InternalServerError;
            }
           
            return response;
        }

        [HttpPost]
        [AllowAnonymous]
        public ResponseContract<List<Certificate>> GetCerticates(RequestCertificates requestCertificate)
        {
            ResponseContract<List<Certificate>> response = new ResponseContract<List<Certificate>>();

            try
            {
                response.Data = this._certiticateDomain.GetCertificatesSearch(requestCertificate);
            }
            catch (Exception exp)
            {
                response.Header.Message = exp.Message;
                response.Header.Code = HttpCodes.InternalServerError;
            }

            return response;
        }

        [HttpPost]
        [AllowAnonymous]
        public ResponseContract<List<CertificateDeceval>> GetCerticatesDeceval(RequestCertificateDeceval requestCertificate)
        {
            ResponseContract<List<CertificateDeceval>> response = new ResponseContract<List<CertificateDeceval>>();

            try
            {
                response.Data = this._certificateDecevalDomain.GetCertificatesSearch(requestCertificate);
            }
            catch (Exception exp)
            {
                response.Header.Message = exp.Message;
                response.Header.Code = HttpCodes.InternalServerError;
            }

            return response;
        }
        [HttpGet]
        [AllowAnonymous]
        public ResponseContract<List<ClientDeceval>> GetClientDeceval()
        {
            ResponseContract<List<ClientDeceval>> response = new ResponseContract<List<ClientDeceval>>();

            try
            {
                response.Data = this._clienteDecevalDomain.GetClientDeceval();
            }
            catch (Exception exp)
            {
                response.Header.Message = exp.Message;
                response.Header.Code = HttpCodes.InternalServerError;
            }

            return response;
        }

        [HttpGet]
        [AllowAnonymous]
        public ResponseContract<ClientDecevalFile> GetClientDecevalByCodeAndNit(string nit_request,string codeDeceval_request)
        {
            ResponseContract<ClientDecevalFile> response = new ResponseContract<ClientDecevalFile>();
           
            try
            {
                FolderManager folderManager = new FolderManager();
                PortalModuleBase oPortalMB = new PortalModuleBase();
                var folders = folderManager.GetFolders(oPortalMB.PortalId);

                ClientDecevalRequest requesClientDeceval = new ClientDecevalRequest { nit = nit_request,codeDeceval = codeDeceval_request };

                ClientDeceval clientDeceval = this._clienteDecevalDomain.GetClientDecevalByCodeAndNit(requesClientDeceval);

                FileManager fileManager = new FileManager();
                IFolderInfo folderDocumentPut = folders.FirstOrDefault(f => f.FolderName == "Accionistas");
                ClientDecevalFile clientDecevalFile = new ClientDecevalFile { 
                    codeDeceval = clientDeceval.codeDeceval,
                    Id = clientDeceval.Id,
                    typeNit =clientDeceval.typeNit,
                    numberID =clientDeceval.numberID, 
                    nameClient = clientDeceval.nameClient,
                    fileExist = File.Exists(folderDocumentPut.PhysicalPath+"/"+codeDeceval_request + ".pdf")
                };

                response.Data = clientDecevalFile;

            }
            catch (Exception exp)
            {
                response.Header.Message = exp.Message;
                response.Header.Code = HttpCodes.InternalServerError;
            }

            return response;
        }
    }
}

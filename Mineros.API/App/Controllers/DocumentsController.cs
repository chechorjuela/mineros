﻿using AutoMapper;
using Mineros.Data;
using Mineros.Data.Models;
using Mineros.Domain.Contracts.Common;
using Mineros.Model.Commons;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Mineros.API
{
    public class DocumentsController : APIBase
    {

        public List<EasyDnnNewsApp> ListNews { get; set; }
        public List<EasyDnnNewsDocuments> ListDocuments { get; set; }

        public List<EasyDnnNewsCategories> Categories { get; set; }

        private string CultureCountry
        {
            get
            {
                CultureInfo culture2 = CultureInfo.CurrentCulture;
                //return culture2.Parent.ToString() ?? "es";
                return culture2.Name ?? "es-ES";
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public ResponseContract<List<List<EasyDnnNewsDocumentsDTO>>> GetDocumentsAsamblea()
        {
            ResponseContract<List<List<EasyDnnNewsDocumentsDTO>>> response = new ResponseContract<List<List<EasyDnnNewsDocumentsDTO>>>();
            try
            {


                EasyDnnNewsData easyDnnNewsData = new EasyDnnNewsData();
                List<List<EasyDnnNewsDocumentsDTO>> list = new List<List<EasyDnnNewsDocumentsDTO>>();

                ListNews = easyDnnNewsData.GetArticleNews();

                Categories = easyDnnNewsData.GetAllCategoriesNews(CultureCountry);

                string setting_category = "Informes Financieros Consolidados";

                var parentCategoryReportes = Categories.FirstOrDefault(s => s.CategoryName.ToLower() == setting_category.ToString().ToLower());
                if (parentCategoryReportes != null)
                {

                    var articles = ListNews.Where(x => x.CategoryID == parentCategoryReportes.CategoryID).ToList();

                    foreach (var art in articles)
                    {
                        List<EasyDnnNewsDocuments> list_item = easyDnnNewsData.GetALLEasyDnnNewsDocuments(art.ArticleID, CultureCountry);
                        //List<EasyDnnNewsDocumentsDTO> = Mapper.Map<List<EasyDnnNewsDocuments>>(list_item);
                        var result_list = Mapper.DynamicMap<List<EasyDnnNewsDocumentsDTO>>(list_item);

                        list.Add(result_list);
                    }
                    response.Data = list;
                }
            }
            catch (Exception exp)
            {
                response.Header.Message = exp.Message;
                response.Header.Code = HttpCodes.InternalServerError;
            }
            return response;
        }

        [HttpGet]
        [AllowAnonymous]
        public ResponseContract<List<NewsFoundationDataDTO>> GetDocumentFoundation(string filter)
        {
            ResponseContract<List<NewsFoundationDataDTO>> response = new ResponseContract<List<NewsFoundationDataDTO>>();
            EasyDnnNewsData easyDnnNewsData = new EasyDnnNewsData();

            List<NewsFoundationDataDTO> list_newsFoundation = new List<NewsFoundationDataDTO>();
            try
            {

                Categories = easyDnnNewsData.GetAllCategoriesNews(CultureCountry);

                var parentCategoryReportes = Categories.FirstOrDefault(s => s.CategoryName.ToLower() == filter.ToString().ToLower());
                if (parentCategoryReportes != null)
                {
                    ListNews = easyDnnNewsData.GetArticleNews();

                    var reportsYears = Categories.Where(s => s.ParentCategory == parentCategoryReportes.CategoryID).OrderByDescending(s => s.CategoryName).ToList();
                    foreach (var item in reportsYears)
                    {
                        var countDocs = 0;
                        //int countItems = 0;
                        var artList = ListNews.Where(x => x.CategoryID == item.CategoryID).OrderByDescending(s => s.PublishDate).ToList();
                        //var artList = easyDnnNewsData.GetArticles(null, trimI.CategoryID, 0, 0, ref countItems, cultureCountry).OrderByDescending(s => s.PublishDate).ToList();
                        if (artList != null && artList.Count > 0)
                        {
                            
                            foreach (var lastArt in artList)
                            {
                                var documents = easyDnnNewsData.GetALLEasyDnnNewsDocuments(lastArt.ArticleID, CultureCountry);

                                if (documents != null && documents.Count > 0)
                                {
                                    foreach (var docs in documents)
                                    {
                                        //NewsFoundationDataDTO newFoundation = new NewsFoundationDataDTO();
                                        var newFoundation =  Mapper.DynamicMap<NewsFoundationDataDTO>(docs);
                                        newFoundation.CategoryId = item.CategoryID;
                                        newFoundation.CategoryName = item.CategoryName;
                                        list_newsFoundation.Add(newFoundation);
                                    }

                                }
                            }

                        }
                        var reps = artList.Count;
                        countDocs += reps;
                    }
                    response.Data = list_newsFoundation;
                }
            }
            catch (Exception exp)
            {
                response.Header.Message = exp.Message;
                response.Header.Code = HttpCodes.InternalServerError;
            }
            return response;
        }

        public List<EasyDnnNewsCategories> GetSubCategories(int categoryId)
        {
            return Categories.Where(s => s.ParentCategory == categoryId).ToList();
        }
    }
}

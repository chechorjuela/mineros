﻿using DotNetNuke.Entities.Modules;
using DotNetNuke.Services.FileSystem;
using DotNetNuke.Web.Api;
using Mineros.Domain.App;
using Mineros.Domain.Contracts;
using Mineros.Domain.Contracts.Common;
using Mineros.Model.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Kendo.Mvc.UI;
using Newtonsoft.Json;
using System.Net.Http;

namespace Mineros.API
{
    
    public class InterestContactController : APIBase
    {
        private InterestSettingsDomain _interestSettingsDomain;
        private InterestContactDomain _interestContactDomain;
        public InterestContactController()
        {
            this._interestSettingsDomain = new InterestSettingsDomain();
            this._interestContactDomain = new InterestContactDomain();
        }
        [HttpGet]
        [AllowAnonymous]
        public ResponseContract<List<InterestSettings>> Index()
        {
            ResponseContract<List<InterestSettings>> response = new ResponseContract<List<InterestSettings>>();

            try
            {
                response.Data = this._interestSettingsDomain.listInterestSettings();
            }
            catch (Exception exp)
            {
                response.Header.Message = exp.Message;
                response.Header.Code = HttpCodes.InternalServerError;
            }

            return response;
        }

        [HttpPost]
        [AllowAnonymous]
        public ResponseContract<InterestContact> SendFormInterest()
        {
            ResponseContract<InterestContact> response = new ResponseContract<InterestContact>();
            InterestContact interestContact = new InterestContact();
            FileManager fileManager = new FileManager();
            FolderManager folderManager = new FolderManager();
            PortalModuleBase oPortalMB = new PortalModuleBase();
            try
            {
                var httpRequest = HttpContext.Current.Request;
                foreach (string fields in httpRequest.Form)
                {
                    switch (fields)
                    {
                        case "nit":
                            interestContact.number_id = httpRequest.Form[fields];
                            break;
                        case "namefull":
                            interestContact.name_contact = httpRequest.Form[fields];
                            break;
                        case "email":
                            interestContact.email = httpRequest.Form[fields];
                            break;
                        case "phone":
                            interestContact.number_phone = httpRequest.Form[fields];
                            break;
                        case "city":
                            interestContact.city_current = httpRequest.Form[fields];

                            break;
                        case "country":
                            interestContact.country_current = httpRequest.Form[fields];
                            break;
                        case "interest_first":
                            if (httpRequest.Form[fields] != "")
                            {
                                interestContact.interest_position_first = int.Parse(httpRequest.Form[fields]);
                            }
                            else
                            {
                                interestContact.interest_position_first = null;
                            }
                            break;
                        case "interest_second":
                            if(httpRequest.Form[fields] != "")
                            {
                                interestContact.interest_position_second = int.Parse(httpRequest.Form[fields]);
                            }
                            else
                            {
                                interestContact.interest_position_second = null;
                            }
                            break;
                        case "proccess_first":
                            if (httpRequest.Form[fields]!="")
                            {
                                interestContact.process_interest_first = int.Parse(httpRequest.Form[fields]);

                            }
                            else
                            {
                                interestContact.process_interest_first = null;

                            }
                            break;
                        case "proccess_second":
                            if(httpRequest.Form[fields] != ""){
                            interestContact.process_interest_second = int.Parse(httpRequest.Form[fields]);

                            }
                            else
                            {
                                interestContact.process_interest_second = null;

                            }
                            break;
                        case "location_first":
                            if (httpRequest.Form[fields] != "")
                            {
                                interestContact.location_interest_first = int.Parse(httpRequest.Form[fields]);
                            }
                            else
                            {
                                interestContact.location_interest_first = null;
                            }
                            break;
                        case "location_second":
                            if(httpRequest.Form[fields] != "" ){
                                interestContact.location_interest_second = int.Parse(httpRequest.Form[fields]) ;
                            }
                            else
                            {
                                interestContact.location_interest_second =  null;
                            }
                            break;
                        case "application":
                            interestContact.application = httpRequest.Form[fields];
                            break;
                    }

                }
                var folders = folderManager.GetFolders(oPortalMB.PortalId);
                if (!folderManager.FolderExists(oPortalMB.PortalId, "FileDocument"))
                {
                    folderManager.AddFolder(oPortalMB.PortalId, "FileDocument");
                }
                folders = folderManager.GetFolders(oPortalMB.PortalId);
                string Path_file = "";

                foreach (string file in httpRequest.Files)
                {
                    var postFile = httpRequest.Files[file];
                    if (postFile.FileName != "")
                    {
                        string nameFolder = "FileDocument/InterestContact/" + interestContact.number_id;
                        IFolderInfo folderDocumentPut = folders.FirstOrDefault(f => f.FolderName == interestContact.number_id);
                        if (folderDocumentPut == null)
                        {
                            folderDocumentPut = folderManager.AddFolder(oPortalMB.PortalId, nameFolder);
                        }
                        int lastIndexPoint = postFile.FileName.LastIndexOf(".") + 1;
                        int lenghtString = postFile.FileName.Length - lastIndexPoint;
                        string name_file = interestContact.number_id + "_"+interestContact.name_contact.Replace(" ", "_") + "."+ postFile.FileName.Substring(lastIndexPoint, lenghtString);
                        IFileInfo fileInfo = fileManager.AddFile(folderDocumentPut, name_file, postFile.InputStream);
                        Path_file = fileInfo.PhysicalPath;
                        interestContact.file_dir = "/Portals/" + oPortalMB.PortalId.ToString() + "/" + fileInfo.RelativePath;
                    }

                }
                 response.Data = this._interestContactDomain.storeInterestContact(interestContact, Path_file);

            }
            catch (Exception exp)
            {
                response.Header.Message = exp.Message;
            }

            return response;
        }

        [HttpGet]
        [DnnAuthorize]
        public ResponseContract<DataSourceResult> getInterestContact([System.Web.Http.ModelBinding.ModelBinder(typeof(DataSourceRequestModelBinder))]DataSourceRequest requestMessage)
        {
            ResponseContract<DataSourceResult> response = new ResponseContract<DataSourceResult>();
            try
            {
                //DataSourceRequest request = JsonConvert.DeserializeObject<DataSourceRequest>(
                //    requestMessage.RequestUri.ParseQueryString().GetKey(0)
                //);

                response.Data = this._interestContactDomain.getInterestContactFilter(requestMessage);
            }
            catch (Exception exp)
            {
                response.Header.Message = exp.Message;
            }
            return response;
        }

        [HttpGet]
        [DnnAuthorize]
        public ResponseContract<InterestContact> getInterestContactById(int id)
        {
            ResponseContract<InterestContact> response = new ResponseContract<InterestContact>();
            try
            {
                response.Data = this._interestContactDomain.getInterestContactsByID(id);
            }
            catch (Exception exp)
            {
                response.Header.Message = exp.Message;
            }
            return response;
        }
        [HttpPost]
        [DnnAuthorize]
        public ResponseContract<InterestContact> updateInterestContact()
        {
            ResponseContract<InterestContact> response = new ResponseContract<InterestContact>();
            InterestContact interestContact = new InterestContact();
            FileManager fileManager = new FileManager();
            FolderManager folderManager = new FolderManager();
            PortalModuleBase oPortalMB = new PortalModuleBase();
            try
            {
                var httpRequest = HttpContext.Current.Request;
                foreach (string fields in httpRequest.Form)
                {
                    switch (fields)
                    {
                        case "id":
                            interestContact.Id = int.Parse(httpRequest.Form[fields]);
                            break;
                        case "number_id":
                            interestContact.number_id = httpRequest.Form[fields];
                            break;
                        case "name_contact":
                            interestContact.name_contact = httpRequest.Form[fields];
                            break;
                        case "email":
                            interestContact.email = httpRequest.Form[fields];
                            break;
                        case "number_phone":
                            interestContact.number_phone = httpRequest.Form[fields];
                            break;
                        case "city_current":
                            interestContact.city_current = httpRequest.Form[fields];

                            break;
                        case "country_current":
                            interestContact.country_current = httpRequest.Form[fields];
                            break;
                        case "interest_position_first":
                            if (httpRequest.Form[fields] != "" && httpRequest.Form[fields] != "null")
                            {
                                interestContact.interest_position_first = int.Parse(httpRequest.Form[fields]);
                            }
                            else
                            {
                                interestContact.interest_position_first = null;
                            }
                                break;
                        case "interest_position_second":
                            if (httpRequest.Form[fields] != "" && httpRequest.Form[fields] != "null")
                            {
                                interestContact.interest_position_second = int.Parse(httpRequest.Form[fields]);
                            }
                            else
                            {
                                interestContact.interest_position_second = null;
                            }
                            break;
                        case "process_interest_first":
                            if (httpRequest.Form[fields] != "" && httpRequest.Form[fields] != "null")
                            {
                                interestContact.process_interest_first = int.Parse(httpRequest.Form[fields]);
                            }
                            else
                            {
                                interestContact.process_interest_first = null;

                            }
                            break;
                        case "process_interest_second":
                            if (httpRequest.Form[fields] != "" && httpRequest.Form[fields] != "null")
                            {
                                interestContact.process_interest_second = int.Parse(httpRequest.Form[fields]);

                            }
                            else
                            {
                                interestContact.process_interest_second = null;

                            }
                            break;
                        case "location_interest_first":
                            if (httpRequest.Form[fields] != "" && httpRequest.Form[fields] != "null")
                            {
                                interestContact.location_interest_first = int.Parse(httpRequest.Form[fields]);
                            }
                            else
                            {
                                interestContact.location_interest_first = null;
                            }
                            break;
                        case "location_interest_second":
                            if (httpRequest.Form[fields] != "" && httpRequest.Form[fields] != "null")
                            {
                                interestContact.location_interest_second = int.Parse(httpRequest.Form[fields]);
                            }
                            else
                            {
                                interestContact.location_interest_second = null;
                            }

                            break;
                        case "application":
                            interestContact.application = httpRequest.Form[fields];
                            break;
                    }
                    
                }
                var folders = folderManager.GetFolders(oPortalMB.PortalId);
                if (!folderManager.FolderExists(oPortalMB.PortalId, "FileDocument"))
                {
                    folderManager.AddFolder(oPortalMB.PortalId, "FileDocument");
                }
                interestContact.file_dir = this._interestContactDomain.getInterestContactsByID(interestContact.Id).file_dir;
                folders = folderManager.GetFolders(oPortalMB.PortalId);
                foreach (string file in httpRequest.Files)
                {
                    var postFile = httpRequest.Files[file];
                    if (postFile.FileName != "")
                    {
                        
                        string nameFolder = "FileDocument/InterestContact/" + interestContact.number_id;
                        IFolderInfo folderDocumentPut = folders.FirstOrDefault(f => f.FolderName == interestContact.number_id);
                        if (folderDocumentPut == null)
                        {
                            folderDocumentPut = folderManager.AddFolder(oPortalMB.PortalId, nameFolder);
                        }
                        InterestContact interestContactFile = this._interestContactDomain.getInterestContactsByID(interestContact.Id);
                        if (interestContactFile.file_dir !=null)
                        {
                            int lengh_last = interestContactFile.file_dir.LastIndexOf("/");

                            string name_file_delete = interestContactFile.file_dir.Substring(lengh_last, interestContactFile.file_dir.Length - lengh_last).Replace("/", "").ToString();
                            IFileInfo fileExist = fileManager.GetFile(folderDocumentPut, name_file_delete);
                            fileManager.DeleteFile(fileExist);
                        }
                       
                        IFileInfo fileInfo = fileManager.AddFile(folderDocumentPut, postFile.FileName, postFile.InputStream);

                        interestContact.file_dir = "/Portals/" + oPortalMB.PortalId.ToString() + "/" + fileInfo.RelativePath;
                    }

                }
                response.Data = this._interestContactDomain.updateInterestContact(interestContact);
            }
            catch (Exception exp)
            {
                response.Header.Message = exp.Message;
            }
            return response;
        }
        [HttpGet]
        [DnnAuthorize]
        public ResponseContract<bool> deleteInterestContactById(int id)
        {
            ResponseContract<bool> response = new ResponseContract<bool>();
            try
            {   
                response.Data = this._interestContactDomain.deleteInterestContactById(id);
            }
            catch (Exception exp)
            {

            }
            return response;
        }

        [HttpGet]
        [DnnAuthorize]
        public ResponseContract<string> downloadZip([System.Web.Http.ModelBinding.ModelBinder(typeof(DataSourceRequestModelBinder))]DataSourceRequest requestMessage)
        {
            ResponseContract<string> response = new ResponseContract<string>();
            try
            {
            
                response.Data = this._interestContactDomain.createFileDownloadZip(requestMessage);
            }
            catch (Exception exp)
            {
                response.Header.Message = exp.Message;
            }
            return response;
        }
        [HttpPost]
        [DnnAuthorize]
        public ResponseContract<InterestSettings> storeSettings(InterestSettings interestSave)
        {
            ResponseContract<InterestSettings> response = new ResponseContract<InterestSettings>();
            try
            {
                response.Data = this._interestSettingsDomain.saveInterest(interestSave);
            }
            catch (Exception exp)
            {
                response.Header.Message = exp.Message;
            }
            return response;
        }
        [HttpGet]
        [DnnAuthorize]
        public ResponseContract<bool> delelteSettings(int id)
        {
            ResponseContract<bool> response = new ResponseContract<bool>();
            try
            {
                response.Data = this._interestSettingsDomain.deleteInterest(id);
            }
            catch (Exception exp)
            {
                response.Header.Message = exp.Message;
            }
            return response;
        }
        [HttpPost]
        [DnnAuthorize]
        public ResponseContract<InterestSettings> updateSettings(InterestSettings interestSave)
        {
            ResponseContract<InterestSettings> response = new ResponseContract<InterestSettings>();
            try
            {
                response.Data = this._interestSettingsDomain.updateInterest(interestSave);
            }
            catch (Exception exp)
            {
                response.Header.Message = exp.Message;
            }
            return response;
        }
        [HttpGet]
        [AllowAnonymous]
        public ResponseContract<DataSourceResult> listSettingsFilter([System.Web.Http.ModelBinding.ModelBinder(typeof(DataSourceRequestModelBinder))]DataSourceRequest requestMessage)
        {
            ResponseContract<DataSourceResult> response = new ResponseContract<DataSourceResult>();
            try
            {
                //DataSourceRequest request = JsonConvert.DeserializeObject<DataSourceRequest>(
                //    requestMessage.RequestUri.ParseQueryString().GetKey(0)
                //);
                response.Data = this._interestSettingsDomain.getInterestSettingsFilter(requestMessage);
            }
            catch (Exception exp)
            {
                response.Header.Message = exp.Message;
            }
            return response;
        }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DotNetNuke.Web.Api;
using Mineros.API.App.Config;

namespace Mineros.API
{
    public class APIBase : DnnApiController
    {
        public APIBase()
        {
            AutomapperConfig.CreateMaps();

        }
        public int GetPortalIdFromRequest()
        {
            if (Request.Headers.Contains("PortalId"))
            {
                IEnumerable<string> headerValues = Request.Headers.GetValues("PortalId");
                return Convert.ToInt32(headerValues.FirstOrDefault());
            }
            return -1;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.API.App.Config
{
    public class AutomapperConfig
    {
        public static void CreateMaps()
        {
            Mineros.Domain.Config.AutomapperConfig.CreateMaps();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Users;
using DotNetNuke.Security.Roles;
using DotNetNuke.Services.Scheduling;
using Mineros.Domain.Contracts;
using Mineros.Repository.App;

namespace Mineros.Domain
{
    public class ScheduleMonthly : SchedulerClient
    {
        private ProvidersRepository _providersRepsotory;
        public ScheduleMonthly(ScheduleHistoryItem oItem)
         : base()
        {
            BaseDomain baseDomian = new BaseDomain();
            this._providersRepsotory = new ProvidersRepository(baseDomian.ConnectionName);
            this.ScheduleHistoryItem = oItem;
        }

        public override void DoWork()
        {
            try
            {
                //Perform required items for logging
                this.Progressing();
                PortalModuleBase oPortalMB = new PortalModuleBase();
                List<ProviderContract> listProviders = Mapper.DynamicMap<List<ProviderContract>>(this._providersRepsotory.getProviderByLastMonth().Where((p) => p.DeleteAt == null));
                var usersRole = RoleController.Instance.GetUsersByRole(0, "Consulta Proveedores");
                foreach (var user in usersRole)
                {
                    if (user.Email != null || user.Email != "")
                    {
                        this.sendMessageToUser(listProviders, user);
                    }
                }
                //Your code goes here
                //To log note
                //this.ScheduleHistoryItem.AddLogNote("note")
                //Show success
                this.ScheduleHistoryItem.Succeeded = true;
            }
            catch (Exception ex)
            {
                this.ScheduleHistoryItem.Succeeded = false;
                this.Errored(ref ex);
                DotNetNuke.Services.Exceptions.Exceptions.LogException(ex);
            }
        }
        public void sendMessageToUser(List<ProviderContract> providers, UserInfo user)
        {
            StringBuilder sb = new StringBuilder();
            StringBuilder sp_send = new StringBuilder();
            var subject = "Reporte Provedores";
            sb.AppendLine(string.Format("Señor(a) {0}.", user.FirstName));
            sp_send.AppendLine(string.Format("<p>Hola {0}!</p>", user.FirstName));
            sb.AppendLine(string.Format("<br><br>Listado de proveedores que se registraron o actualizaron su información en el sistema durante el mes anterior:", providers.Count().ToString()));
            if (providers.Count() > 0)
            {
                string Table = "<TABLE><THEAD>" +
                    "<TR>" +
                    "<TH>NIT</TH>" +
                    "<TH>Nombre Proveedor</TH>" +
                    "<TH>Nombre Asesor</TH>" +
                    "<TH>Email Asesor</TH>" +
                    "<TH>Email Proveedor</TH>" +
                    "<TH>Telefono</TH>" +
                    "<TH>Sitio</TH>" +
                    "</TR></THEAD><TBODY>";
                foreach (ProviderContract provider in providers)
                {
                    Table += "<TR>" +
                        "<TD>" + provider.Nit + "</TD>" +
                        "<TD>" + provider.NameEnterprise + "</TD>" +
                        "<TD>" + provider.ContactName + "</TD>" +
                        "<TD>" + provider.ContactEmail + "</TD>" +
                        "<TD>" + provider.CompanyEmail + "</TD>" +
                        "<TD>" + provider.PhoneNumber + "</TD>" +
                        "<TD>" + provider.SiteUrl + "</TD>" +
                        "</TR>";
                }
                Table += "</TBODY></TABLE>";
                sb.AppendLine(Table);
            }
            this._providersRepsotory.sendEmail(user.Email, subject.ToString(), sb.ToString());
        }
    }
}

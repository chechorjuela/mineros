﻿using ICSharpCode.SharpZipLib.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Domain
{
    public class Utils
    {
        public string compressFile(string dirFileZip, List<FileInfo> fileinfo,string folder_path,int portal_id)
        {
            DateTime current = DateTime.Now;
            string folder_file = "";
            try
            {
                if (fileinfo.Equals(0))
                {
                    return "error";
                }

                string zipfName = "Request" + current.Date.Day.ToString() +
                    current.Date.Month.ToString() + current.Date.Year.ToString() +
                    current.TimeOfDay.Duration().Hours.ToString() +
                    current.TimeOfDay.Duration().Minutes.ToString() + current.TimeOfDay.Duration().Seconds.ToString();

                folder_file = dirFileZip + "\\" + zipfName + ".zip";
                using (ZipOutputStream s = new ZipOutputStream(File.Create(folder_file)))
                {
                    s.SetLevel(9); // 0-9, 9 being the highest level of compression

                    byte[] buffer = new byte[4096];

                    foreach(FileInfo file in fileinfo)
                    {
                        
                        string path_file = folder_path + file.ToString().Substring(1, file.ToString().Length - 1).Replace("/", @"\").Replace("Portals\\" + portal_id + "\\", "");
                        ZipEntry entry = new ZipEntry(Path.GetFileName(path_file));

                        entry.DateTime = DateTime.Now;
                        s.PutNextEntry(entry);
                        using (FileStream fs = File.OpenRead(path_file))
                        {
                            int sourceBytes;
                            do
                            {
                                sourceBytes = fs.Read(buffer, 0, buffer.Length);
                                s.Write(buffer, 0, sourceBytes);
                            }
                            while (sourceBytes > 0);
                        }
                    }
                    s.Finish();
                    s.Close();
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
            return folder_file;

        }
    }
}

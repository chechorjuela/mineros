﻿using AutoMapper;
using Mineros.Domain.Contracts;
using Mineros.Model;
using Mineros.Model.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Domain.Config
{
    public class AutomapperConfig
    {
        public static void CreateMaps()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Providers, ProviderContract>().ReverseMap();
                cfg.CreateMap<Brands, BrandContract>().ReverseMap();
                cfg.CreateMap<Category, CategoryContract>().ReverseMap();
                cfg.CreateMap<SubCategory, SubCategoryContract>().ReverseMap();
                cfg.CreateMap<TypeActivity,TypeActivityContract>().ReverseMap();
                cfg.CreateMap<ItemCategories, ItemCategoryContract>().ReverseMap();
            });
        }
    }
}

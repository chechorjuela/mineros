﻿using Mineros.Domain.Services;
using Mineros.Model.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Domain.Interfaces
{
    public interface IRequestUrlBvcService
    {
        string ConsultRequestBvc(string name);
        List<Stock> GetFileStock();
        string GetVariacion(float current, float previous);
        List<ChartValue> FileGetStockHome();
    }
}

﻿using DotNetNuke.Entities.Modules;
using DotNetNuke.Services.FileSystem;
using Mineros.Domain.Interfaces;
using Mineros.Model.Commons;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Caching;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Xml;

namespace Mineros.Domain.Services
{
    public class RequestUrlService : IRequestUrlBvcService
    {
        MemoryCache memCache = MemoryCache.Default;

        public string ConsultRequestBvc(string name)
        {
            try
            {
                ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(delegate { return true; });

                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(("http://bvc.com.co/mercados/GraficosServlet?home=no&tipo=ACCION&mercInd=RV&nemo=") + name);

                request.Method = "GET";
                String responseConsult = String.Empty;
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    Stream dataStream = response.GetResponseStream();
                    StreamReader reader = new StreamReader(dataStream);
                    responseConsult = reader.ReadToEnd();
                    reader.Close();
                    dataStream.Close();
                }
                return responseConsult;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }


        public List<Stock> getAccionByName(string name)
        {
            List<Stock> stocksGetByName = new List<Stock>();

            stocksGetByName = memCache.Get("FileGetStockByNameData") as List<Stock>;
            if (stocksGetByName != null)
                return stocksGetByName;
            else
                stocksGetByName = new List<Stock>();

            CultureInfo ci = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = ci;
            Thread.CurrentThread.CurrentUICulture = ci;


            string MINEROS = ConsultRequestBvc(name.ToUpper());
            var splitMineros = MINEROS.Split('\n');
            var cm = splitMineros[0].Split(',');
            stocksGetByName.Add(
                        new Stock()
                        {
                            Accion = "MINEROS",
                            Actual = float.Parse(cm[1].ToString()),
                            Anterior = float.Parse(cm[3].ToString()),
                            Fecha = Convert.ToDateTime(cm[0].ToString()),
                            Valor = float.Parse(cm[2].ToString()),
                            Variacion = GetVariacion(float.Parse(cm[1].ToString()), float.Parse(cm[3].ToString()))
                        });

            CacheItemPolicy policy = new CacheItemPolicy { AbsoluteExpiration = DateTimeOffset.UtcNow.AddMinutes(5) };
            memCache.Add("FileGetStockByNameData", stocksGetByName, policy);

            memCache.Add("FileGetStockDataByNameContingency", stocksGetByName, new CacheItemPolicy { AbsoluteExpiration = ObjectCache.InfiniteAbsoluteExpiration });

            return stocksGetByName;

        }
        public List<Stock> GetFileStock()
        {


            List<Stock> stocks = new List<Stock>();
            stocks = memCache.Get("FileGetStockFilesData") as List<Stock>;
            if (stocks != null)
                return stocks;
            else
                stocks = new List<Stock>();

            CultureInfo ci = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = ci;
            Thread.CurrentThread.CurrentUICulture = ci;


            string CEMARGOS = ConsultRequestBvc("MINEROS");
            var splitCEMARGOS = CEMARGOS.Split('\n');
            var cm = splitCEMARGOS[0].Split(',');
            stocks.Add(
                        new Stock()
                        {
                            Accion = "MINEROS",
                            Actual = float.Parse(cm[1].ToString()),
                            Anterior = float.Parse(cm[3].ToString()),
                            Fecha = Convert.ToDateTime(cm[0].ToString()),
                            Valor = float.Parse(cm[2].ToString()),
                            Variacion = GetVariacion(float.Parse(cm[1].ToString()), float.Parse(cm[3].ToString()))
                        });

            string PFCEMARGOS = ConsultRequestBvc("MINEROS");
            var splitPFCEMARGOS = PFCEMARGOS.Split('\n');
            var pfcm = splitPFCEMARGOS[0].Split(',');
            stocks.Add(
                        new Stock()
                        {
                            Accion = "MINEROS",
                            Actual = float.Parse(pfcm[1].ToString()),
                            Anterior = float.Parse(pfcm[3].ToString()),
                            Fecha = Convert.ToDateTime(pfcm[0].ToString()),
                            Valor = float.Parse(pfcm[2].ToString()),
                            Variacion = GetVariacion(float.Parse(pfcm[1].ToString()), float.Parse(pfcm[3].ToString()))
                        });

            CacheItemPolicy policy = new CacheItemPolicy { AbsoluteExpiration = DateTimeOffset.UtcNow.AddMinutes(5) };
            memCache.Add("FileGetStockFilesData", stocks, policy);

            memCache.Add("FileGetStockDataFilesContingency", stocks, new CacheItemPolicy { AbsoluteExpiration = ObjectCache.InfiniteAbsoluteExpiration });

            return stocks;

        }

        public List<ChartValue> FileGetStockHome()
        {

            List<ChartValue> charValur = new List<ChartValue>();

            DateTime now = DateTime.Now;
            var unixTimestamp = (DateTime.UtcNow.Subtract(now)).TotalSeconds;
            String timeStamp = unixTimestamp.ToString();
            JavaScriptSerializer json_serializer = new JavaScriptSerializer();

            string urlGold = "https://www.apmex.com/SpotPrice/GetHistoricalChart?metalname=gold&_=" + timeStamp;
            List<GraphicStock> totalStock = new List<GraphicStock>();

            String responseConsult = getRequestUrl(urlGold);
            ChartDataObject objectSerialize = JsonConvert.DeserializeObject<ChartDataObject>(responseConsult);
            IList<string> lastChart = (IList<string>)objectSerialize.chartData.Last();
            ChartValue charobj = new ChartValue() { name = "Gold", value = lastChart[1], timespan = lastChart[0] };
            charValur.Add(charobj);

            string urlSilver = "https://www.apmex.com/spotprice/GetHistoricalChart?metalname=Silver&metalId=2&_=" + timeStamp;
            responseConsult = getRequestUrl(urlSilver);
            objectSerialize = JsonConvert.DeserializeObject<ChartDataObject>(responseConsult);
            lastChart = (IList<string>)objectSerialize.chartData.Last();
            charobj = new ChartValue() { name = "Silver", value = lastChart[1], timespan = lastChart[0] };
            charValur.Add(charobj);

            return charValur;

        }

        public List<GraphicStock> GetTotalGraphics(string name)
        {
            CultureInfo ci = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = ci;
            Thread.CurrentThread.CurrentUICulture = ci;
            List<GraphicStock> totalStock = new List<GraphicStock>();
            List<Stock> stocks = new List<Stock>();
            totalStock = memCache.Get("FileGetStockHomeData") as List<GraphicStock>;
            if (totalStock != null)
                return totalStock;
            else
                totalStock = new List<GraphicStock>();


            string MINEROS_ACCIONS = ConsultRequestBvc(name);

            var splitMineros = MINEROS_ACCIONS.Split('\n');
            if (splitMineros.Count() > 1)
            {
                return generateStock(splitMineros);
            }
            else
            {
                FileManager fileManager = new FileManager();
                FolderManager folderManager = new FolderManager();
                PortalModuleBase oPortalMB = new PortalModuleBase();
                var folders = folderManager.GetFolders(oPortalMB.PortalId);
                IFolderInfo folderDocumentPut = folders.FirstOrDefault(f => f.FolderName == "");
                string[] line = File.ReadAllLines(folderDocumentPut.PhysicalPath + "/graphics/graphics.txt");
                return generateStock(line);
            }
             
            
        }

        public List<GraphicStock> generateStock(string[] splitMineros)
        {
            List<GraphicStock> totalStock = new List<GraphicStock>();
            List<Stock> stocks = new List<Stock>();
            totalStock = memCache.Get("FileGetStockHomeData") as List<GraphicStock>;
            if (totalStock != null)
                return totalStock;
            else
                totalStock = new List<GraphicStock>();

            var accionMineros = from e in splitMineros
                                    where e.Split(',').Count() > 1
                                    select
                                      new Stock()
                                      {
                                          Accion = "MINEROS",
                                          Actual = float.Parse(e.Split(',')[1].ToString()),
                                          Anterior = float.Parse(e.Split(',')[3].ToString()),
                                          Fecha = Convert.ToDateTime(e.Split(',')[0].ToString()),
                                          Valor = float.Parse(e.Split(',')[2].ToString())
                                      };

                stocks.AddRange(accionMineros);

                int auxCont = 1;

                var dates = stocks.Distinct().OrderBy(x => x.Fecha).Select(x => new GraphicStock { Fecha = x.Fecha, Id = auxCont++ });

                var stocksDesc = stocks.OrderBy(x => x.Fecha);

                float auxMineros = 0;

                foreach (var item in dates)
                {
                    var Mineros = stocks.Where(x => x.Fecha == item.Fecha);
                    if (Mineros.Count() > 0)
                    {
                        GraphicStock ga = new GraphicStock() { Fecha = item.Fecha };

                        foreach (var i in Mineros)
                        {
                            if (i.Accion == "MINEROS")
                            {
                                ga.Action = i.Actual == 0 ? auxMineros : i.Actual;
                                auxMineros = i.Actual == 0 ? auxMineros : i.Actual;
                            }
                        }
                        totalStock.Add(ga);
                    }
                }
                
                CacheItemPolicy policy = new CacheItemPolicy { AbsoluteExpiration = DateTimeOffset.UtcNow.AddMinutes(5) };
                memCache.Add("FileGetStockHomeData", totalStock, policy);
                memCache.Add("FileGetStockHomeDataContingency", totalStock, new CacheItemPolicy { AbsoluteExpiration = ObjectCache.InfiniteAbsoluteExpiration });
                
                return totalStock;
        }
        public string GetVariacion(float current, float previous)
        {
            var mayor = (current > previous) ? current : previous;
            var menor = (current < previous) ? current : previous;

            var res = (1 - (menor / mayor)) * 100;

            if (current < previous)
                res = -res;

            return res.ToString("0.00");
        }
        
        public String getRequestUrl(string urlRequest)
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(urlRequest);

            request.Method = "GET";
            String responseConsult = String.Empty;
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                responseConsult = reader.ReadToEnd();
                reader.Close();
                dataStream.Close();
            }

            return responseConsult;
        }

        public string getTrmBankrep()
        {
            string TRM = getRequestUrl("http://app.docm.co/prod/Dmservices/Utilities.svc/GetTRM");

            return TRM;
        }
    }

}
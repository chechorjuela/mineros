﻿using Kendo.Mvc.UI;
using Mineros.Domain.Contracts;
using Mineros.Model.Commons;
using Mineros.Repository.App;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Domain.App
{
    public class InterestSettingsDomain : BaseDomain
    {
        private InterestSettingsRepository _interestSettingsRepository;

        public InterestSettingsDomain()
        {
            this._interestSettingsRepository = new InterestSettingsRepository(this.ConnectionName);
        }

        public List<InterestSettings> listInterestSettings()
        {
            return this._interestSettingsRepository.Get().Where(x=>x.Id!=0).ToList();
        }

        public InterestSettings saveInterest(InterestSettings intereseSetting)
        {
            this._interestSettingsRepository.Create(intereseSetting);
            return intereseSetting;
        }

        public bool deleteInterest(int id)
        {
            this._interestSettingsRepository.Delete(this._interestSettingsRepository.GetById(id));
            return true;
        }

        public InterestSettings updateInterest(InterestSettings interestSetting)
        {
            this._interestSettingsRepository.Update(interestSetting);
            return this._interestSettingsRepository.GetById(interestSetting.Id);
        }

        public DataSourceResult getInterestSettingsFilter(DataSourceRequest request)
        {
            return this._interestSettingsRepository.GetInterestSettingsFilter(request);

        }
    }
}

﻿using Mineros.Domain.Contracts;
using Mineros.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Domain.App
{
    public class WorkUsDomain
    {
        private WorkUsRepository WorkUsRepository;

        public WorkUsDomain()
        {
            this.WorkUsRepository = new WorkUsRepository();
        }

        public void sendEmailContactWork(WorkUsContactContract workUsContract)
        {

            StringBuilder sb = new StringBuilder();
            StringBuilder sp_send = new StringBuilder();

            var subject = "Aplicacion de oferta de trabajo";

            sb.AppendLine(string.Format("El Señor(a) {0} ha aplicado la oferta de trabajo {1}, Para la sede de {2}-{3}.",
                workUsContract.name.ToString(), workUsContract.title_job.ToString(), workUsContract.country_job, workUsContract.city_job));
            sb.AppendLine(string.Format("<br><br>Se puede comunicar al telefono: {0}. o con el correo suministrado: {1}.",
                workUsContract.phone.ToString(), workUsContract.email.ToString()));

            sp_send.AppendLine(string.Format("<p>Hola {0}!</p>",
                workUsContract.name.ToString()));
            sp_send.AppendLine(string.Format("<p>Hemos recibido tu hoja de vida. Esta será almacenada en nuestra base de datos y será tenida en cuenta al momento de tener una vacancte que se ajuste a tu perfil"));
            sp_send.AppendLine(string.Format("<p>Muchas gracias!</p>"));

            this.WorkUsRepository.sendEmail(workUsContract.email, workUsContract.file, subject.ToString(), sb.ToString(),workUsContract.country_job.ToLower(), sp_send.ToString());
        }

    }
}

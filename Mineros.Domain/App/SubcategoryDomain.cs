﻿using AutoMapper;
using Mineros.Domain.Contracts;
using Mineros.Repository.App;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Mineros.Model.Commons;
using Mineros.Domain.Contracts.Request;

namespace Mineros.Domain
{
    public class SubcategoryDomain:BaseDomain
    {
        private SubcategoryRepository subcategoryRepository;
        private CategoryRepository categoryRepository;
        private TypeActivityRepository typeActivityRepository;
        public SubcategoryDomain()
        {
            this.subcategoryRepository = new SubcategoryRepository(this.ConnectionName);
            this.categoryRepository = new CategoryRepository(this.ConnectionName);
            this.typeActivityRepository = new TypeActivityRepository(this.ConnectionName);
        }

        public async Task<List<SubCategoryContract>> GetAllSubCategory()
        {
            var response = new List<SubCategoryContract>();
            List<SubCategoryContract> listSubcategory = Mapper.DynamicMap<List<SubCategoryContract>>(this.subcategoryRepository.Get().ToList());
            response = listSubcategory;
            return response;
        }

        public async Task<List<SubCategoryContract>> getSubcategoryByCategory(int categoryId)
        {
            var response = new List<SubCategoryContract>();
            List<SubCategoryContract> listSubcategories = Mapper.DynamicMap<List<SubCategoryContract>>(this.subcategoryRepository.Get().Where(s=>s.CategoryId== categoryId).ToList());
          
            response = listSubcategories;
            return response;
        }
        public async Task<DataSourceResult> getAllSubcategoryAll(DataSourceRequest request)
        {
            List<SubCategoryContract> listSubcategory = Mapper.DynamicMap<List<SubCategoryContract>>(this.subcategoryRepository.Get().ToList());
            
            if (listSubcategory != null)
            {
                List<SubCategoryResponseContract> response = new List<SubCategoryResponseContract>();
                
                foreach (SubCategoryContract subcategories in listSubcategory)
                {
                    SubCategoryResponseContract subcategory = new SubCategoryResponseContract();
                    subcategory.SubcategoryId = subcategories.Id;
                    subcategory.SubcategoryEnglish = subcategories.English;
                    subcategory.SubcategorySpanish = subcategories.Spanish;
                    subcategory.CategoryId = subcategories.CategoryId;
                    Category category = this.categoryRepository.GetById(subcategory.CategoryId);
                    subcategory.CategoryEnglish = category.English;
                    subcategory.CategorySpanish = category.Spanish;
                    subcategory.TypeActivityId = category.TypeActivityId;
                    TypeActivity typeActivity = this.typeActivityRepository.GetById(subcategory.TypeActivityId);
                    subcategory.TypeactivitySpanish = typeActivity.Spanish;
                    subcategory.TypeactivityEnglish = typeActivity.English;

                    response.Add(subcategory);
                }
                return response.ToDataSourceResult(request);
            }

            return new DataSourceResult();
        }

        public async Task<bool> storeSubcategory(RequestSubcategoryContract subcategory)
        {
            try
            {
                SubCategory subCategory = new SubCategory
                {
                    Spanish = subcategory.Spanish,
                    English = subcategory.English,
                    CategoryId = subcategory.CategoryId
                };
                this.subcategoryRepository.Create(subCategory);
                return true;
            }
            catch (Exception exp)
            {
                return false;
            }
        }
        public async Task<bool> updateCategory(RequestSubcategoryContract subcategory)
        {
            try
            {
                SubCategory subCategory = new SubCategory
                {
                    Id = subcategory.Id,
                    Spanish = subcategory.Spanish,
                    English = subcategory.English,
                    CategoryId = subcategory.CategoryId
                };
                this.subcategoryRepository.Update(subCategory);
                return true;
            }
            catch (Exception exp)
            {
                return false;
            }
        }
        public async Task<bool> deleteSubcategory(int id)
        {
            try
            {
                this.subcategoryRepository.Delete(this.subcategoryRepository.GetById(id));
                return true;
            }
            catch (Exception exp)
            {
                return false;
            }
        }
    }
}

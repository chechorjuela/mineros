﻿using AutoMapper;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Users;
using DotNetNuke.Security.Roles;
using DotNetNuke.Services.FileSystem;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Mineros.Domain.Contracts;
using Mineros.Model;
using Mineros.Model.Commons;
using Mineros.Repository.App;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace Mineros.Domain
{
    public class ProviderDomain : BaseDomain
    {
        private ProvidersRepository _providersRepsotory;
        private BrandRepository _brandRepository;
        private ItemsSubcategoryRepository _itemSubcategoryRepository;
        private ProviderBrandRepository _providersBrandRepository;
        private CategoryRepository _categoryRepository;

        public ProviderDomain()
        {
            this._providersRepsotory = new ProvidersRepository(this.ConnectionName);
            this._brandRepository = new BrandRepository(this.ConnectionName);
            this._itemSubcategoryRepository = new ItemsSubcategoryRepository(this.ConnectionName);
            this._providersBrandRepository = new ProviderBrandRepository(this.ConnectionName);
            this._categoryRepository = new CategoryRepository(this.ConnectionName);
        }
        public async Task<bool> getProviderById(string nit)
        {
            bool existProvider = false;
            var provider = this._providersRepsotory.GetByParameter("Nit", nit);
            if (provider != null && provider.Count() > 0)
            {
                existProvider = true;
            }
            return existProvider;
        }
        public async Task<DataSourceResult> getAllProviders(DataSourceRequest request)
        {
            var response = new List<ProviderContract>();
            List<ProviderContract> listProviders = Mapper.DynamicMap<List<ProviderContract>>(this._providersRepsotory.Get().ToList().Where((l) => l.DeleteAt == null));
            foreach (ProviderContract listProvider in listProviders)
            {
                List<BrandContract> brands = Mapper.DynamicMap<List<BrandContract>>(this._brandRepository.GetAllTagsByProviderId(listProvider.Id));
                List<ItemCategoryContract> items= Mapper.DynamicMap<List<ItemCategoryContract>>(this._itemSubcategoryRepository.getAllItemsByProviderId(listProvider.Id));
                listProvider.Brands = brands;
                listProvider.BrandsString = string.Join(",", brands.Select(b=>b.Name));
                listProvider.ItemCategory = items;
                listProvider.Categories = string.Join(",",items.Select(i=>i.CategorySpanish).Distinct());
                listProvider.SubCategories = string.Join(",", items.Select(i => i.SubcategorySpanish).Distinct());
            }
            if (listProviders!=null)
            {
                return listProviders.ToDataSourceResult(request);
            }
            
            return new DataSourceResult();
        }

        public async Task<ProviderContract> CreateProvider(RequestProviderContract provider)
        {
            ProviderContract providerContract = new ProviderContract();
            List<BrandContract> listBrandContract = new List<BrandContract>();
            string folderDir = "";
            var jsonCountries = JsonConvert.SerializeObject(provider.countries);
            if (provider.fileBrochure!=null) {
                folderDir = await this.createFileBoucher(provider.fileBrochure.FileStringBase64, provider.nit, provider.fileBrochure.TypeFileBase64);

            }
            Providers providerSave = new Providers
            {
                NameEnterprise = provider.nameEnterprise,
                Nit = provider.nit,
                SiteUrl = provider.siteUrl,
                ContactName = provider.contactName,
                PhoneNumber = provider.phoneNumber,
                ContactEmail = provider.contactEmail,
                CompanyEmail = provider.contactEmail,
                Countries = jsonCountries.ToString(),
                FilePath = folderDir,
                CreateAt =  DateTime.Now,
                UpdateAt = DateTime.Now
            };
            this._providersRepsotory.Create(providerSave);

            providerContract = Mapper.DynamicMap<ProviderContract>(this._providersRepsotory.GetByParameter("Nit", providerSave.Nit).FirstOrDefault());
           
            if (provider.subcategoryProduct!=null)
            {
                foreach (CategoryList categoryprd in provider.subcategoryProduct)
                {
                    ItemsSubcategory itemSubcategory = new ItemsSubcategory
                    {
                        SubcategoryID = categoryprd.id,
                        ProviderId = providerSave.Id
                    };
                    this._itemSubcategoryRepository.Create(itemSubcategory);
                }
            }
            if ( provider.subcategoryService!=null)
            {
                foreach (CategoryList categorysrv in provider.subcategoryService)
                {
                    ItemsSubcategory itemSubcategory = new ItemsSubcategory
                    {
                        SubcategoryID = categorysrv.id,
                        ProviderId = providerSave.Id
                    };
                    this._itemSubcategoryRepository.Create(itemSubcategory);
                }
            }
            if (provider.subcategoryProduct != null || provider.subcategoryService != null)
            {
                List<ItemCategories> itemCategory = this._itemSubcategoryRepository.getAllItemsByProviderId(providerSave.Id);
                providerContract.ItemCategory =  Mapper.DynamicMap<List<ItemCategoryContract>>(itemCategory);
            }
            foreach (var brand in provider.brands)
            {
                Brands brandFind = new Brands();
                var getBrandFind = this._brandRepository.GetByParameter("Name", brand);
                if (getBrandFind != null && getBrandFind.Count()>0)
                {
                    brandFind = getBrandFind.First();
                }
                else
                {
                    Brands brand_save = new Brands
                    {
                        Name = brand
                    };
                    this._brandRepository.Create(brand_save);
                    brandFind = this._brandRepository.GetByParameter("Name", brand_save.Name).First();
                }
             
                ProviderBrand providerBrand = new ProviderBrand
                {
                    ProvidersId = providerContract.Id,
                    BrandId = brandFind.Id,
                };
                this._providersBrandRepository.Create(providerBrand);
                BrandContract brandContract = Mapper.DynamicMap<BrandContract>(brandFind);
                listBrandContract.Add(brandContract);
            }

            providerContract.Brands = listBrandContract;
            this.sendMessageCreateProvider(providerContract);
            this.sendmessageToEnterprise(providerContract);

            return providerContract;
        }

        public async Task<ProviderContract> UpdateProvider(RequestProviderContract requestProvider)
        {

            ProviderContract providerContract = new ProviderContract();
          
            string folderDir = "";
            var jsonCountries = JsonConvert.SerializeObject(requestProvider.countries);
            List<BrandContract> listBrandContract = new List<BrandContract>();
            Providers providerBefore = new Providers();
            if (requestProvider.codeChek)
            {
                providerBefore = this._providersRepsotory.GetById(requestProvider.id);
                if (providerBefore.Nit != requestProvider.nit)
                {
                    providerBefore = null;
                }
            }
            else
            {
                providerBefore = this._providersRepsotory.GetById(requestProvider.id);

            }
            if (providerBefore!=null)
            {
                if (requestProvider.fileBrochure != null)
                {
                    folderDir = await this.createFileBoucher(requestProvider.fileBrochure.FileStringBase64, requestProvider.nit, requestProvider.fileBrochure.TypeFileBase64);
                }
                else
                {
                    folderDir = providerBefore.FilePath;
                }

                Providers providerSave = new Providers
                {
                    Id = requestProvider.id,
                    NameEnterprise = requestProvider.nameEnterprise,
                    Nit = requestProvider.nit,
                    SiteUrl = requestProvider.siteUrl,
                    ContactName = requestProvider.contactName,
                    PhoneNumber = requestProvider.phoneNumber,
                    ContactEmail = requestProvider.contactEmail,
                    CompanyEmail = requestProvider.contactEmail,
                    Countries = jsonCountries.ToString(),
                    FilePath = folderDir,
                    CreateAt = providerBefore.CreateAt,
                    UpdateAt = DateTime.Now
                };

                this._providersRepsotory.Update(providerSave);
                providerContract = Mapper.DynamicMap<ProviderContract>(this._providersRepsotory.GetById(providerSave.Id));
                this._providersBrandRepository.DeleteByConditions("WHERE [ProvidersId] = @0", providerSave.Id);
                foreach (var brand in requestProvider.brands)
                {
                    Brands brandFind = new Brands();
                    var getBrandFind = this._brandRepository.GetByParameter("Name", brand);
                    if (getBrandFind != null && getBrandFind.Count() > 0)
                    {
                        brandFind = getBrandFind.First();
                    }
                    else
                    {
                        Brands brand_save = new Brands
                        {
                            Name = brand
                        };
                        this._brandRepository.Create(brand_save);
                        brandFind = this._brandRepository.GetByParameter("Name", brand_save.Name).First();
                    }

                    ProviderBrand providerBrand = new ProviderBrand
                    {
                        ProvidersId = providerContract.Id,
                        BrandId = brandFind.Id,
                    };
                    this._providersBrandRepository.Create(providerBrand);
                    BrandContract brandContract = Mapper.DynamicMap<BrandContract>(brandFind);
                    listBrandContract.Add(brandContract);
                }
                providerContract.Brands = listBrandContract;
                this._itemSubcategoryRepository.DeleteByConditions("WHERE [ProviderId] = @0", providerSave.Id);
                if (requestProvider.subcategoryProduct != null)
                {
                    foreach (CategoryList categoryprd in requestProvider.subcategoryProduct)
                    {
                        ItemsSubcategory itemSubcategory = new ItemsSubcategory
                        {
                            SubcategoryID = categoryprd.id,
                            ProviderId = providerSave.Id
                        };
                        this._itemSubcategoryRepository.Create(itemSubcategory);
                    }
                }
                if (requestProvider.subcategoryService != null)
                {
                    foreach (CategoryList categorysrv in requestProvider.subcategoryService)
                    {
                        ItemsSubcategory itemSubcategory = new ItemsSubcategory
                        {
                            SubcategoryID = categorysrv.id,
                            ProviderId = providerSave.Id
                        };
                        this._itemSubcategoryRepository.Create(itemSubcategory);
                    }
                }
                if (requestProvider.subcategoryProduct != null || requestProvider.subcategoryService != null)
                {
                    List<ItemCategories> itemCategory = this._itemSubcategoryRepository.getAllItemsByProviderId(providerSave.Id);
                    providerContract.ItemCategory = Mapper.DynamicMap<List<ItemCategoryContract>>(itemCategory);
                }
            }
            this.sendmessageUpdate(providerContract);
            return providerContract;
        }
        public async Task<bool> DeleteProvider(int id)
        {
            Providers provider = this._providersRepsotory.GetById(id);
            if (provider!=null)
            {
                provider.DeleteAt = DateTime.Now;
                this._providersRepsotory.Update(provider);
                return true;
            }
            return false;
        }

        public async Task<string> createFileBoucher(string stringFileBase64,string nit,string typebase64)
        {
            string filePath = "";
            FileManager fileManager = new FileManager();
            FolderManager folderManager = new FolderManager();
            PortalModuleBase oPortalMB = new PortalModuleBase();
            var folders = folderManager.GetFolders(oPortalMB.PortalId);
            
            byte[] imageBytes = Convert.FromBase64String(stringFileBase64.Replace("data:" + typebase64 + ";base64,", ""));

            IFolderInfo folderDocuments = folders.FirstOrDefault(s => s.FolderName == "FileDocument");

            if (folderDocuments == null)
            {
                folderManager.AddFolder(oPortalMB.PortalId, "FileDocument");
                folders = folderManager.GetFolders(oPortalMB.PortalId);
                folderDocuments = folders.FirstOrDefault(s => s.FolderName == "FileDocument");
            }

            string nameFolderBrochures = "FileDocument/Brochures/";

            IFolderInfo folderDocumentPut = folders.FirstOrDefault(f => f.FolderName == "Brochures");

            if (folderDocumentPut == null)
            {
                folderDocumentPut = folderManager.AddFolder(oPortalMB.PortalId, nameFolderBrochures);
            }
            string nameFolder =  nameFolderBrochures + nit;

            folderDocumentPut = folders.FirstOrDefault(f => f.FolderPath == nameFolder+"/");
            if (folderDocumentPut == null)
            {
                folderDocumentPut = folderManager.AddFolder(oPortalMB.PortalId, nameFolder);
            }
            string ext = "";
            switch (typebase64.ToLower())
            {
                case "application/pdf":
                    ext = ".pdf";
                    break;
            }
            string nameFile = "Brochure_" + nit + ext;
            File.WriteAllBytes(@folderDocumentPut.PhysicalPath.Replace(@"\", "/")+ nameFile, imageBytes);

            return folderDocumentPut.FolderPath + nameFile;
        }
        public async Task<bool> getMontlyReport()
        {
            bool response = false;
            try
            {
                PortalModuleBase oPortalMB = new PortalModuleBase();
                List<ProviderContract> listProviders = Mapper.DynamicMap<List<ProviderContract>>(this._providersRepsotory.getProviderByLastMonth().Where((p)=>p.DeleteAt==null));
                var usersRole = RoleController.Instance.GetUsersByRole(oPortalMB.PortalId,"Consulta");
                foreach (var user in usersRole)
                {
                    if (user.Email!= null || user.Email!="")
                    {
                        this.sendMessageToUser(listProviders, user);
                    }
                }
                response = true;
            }
            catch (Exception exp)
            {

            }
            return response;
        }
        public void sendMessageCreateProvider(ProviderContract provider)
        {
            StringBuilder sb = new StringBuilder();
            StringBuilder sp_send = new StringBuilder();
            PortalModuleBase oPortalMB = new PortalModuleBase();

            var subject = "Registro Provedor";

            sp_send.AppendLine(string.Format("{0}", subject));

            sb.AppendLine(string.Format("Señor(a) {0}.", provider.ContactName));


            sb.AppendLine(string.Format("<br>Los datos fueron ingresados a nuestra base de datos exitosamente. Para actualizar la información lo puedes hacer. Ingresando el siguiente código <strong>{0}</strong> al momento de registrar nuevamente el formulario. Gracias!", provider.Id));
            this._providersRepsotory.sendEmail(provider.ContactEmail, sp_send.ToString(), sb.ToString());

            var usersRole = RoleController.Instance.GetUsersByRole(oPortalMB.PortalId, "Asesor");
            foreach (var user in usersRole)
            {
                if (user.Email != null || user.Email != "")
                {
                    this.sendMessageToUserRole(provider, user);
                }
            }
        }
        public void sendmessageToEnterprise(ProviderContract provider)
        {
            StringBuilder sb = new StringBuilder();
            StringBuilder sp_send = new StringBuilder();
            var subject = "Registro";
            sp_send.AppendLine(string.Format("{0}", subject));
            sb.AppendLine(string.Format("Señor(es) {0}.", provider.NameEnterprise));
            sb.AppendLine(string.Format("<br>Los datos de la compañía fue registrada en nuestra página. Con el siguiente código (<strong>{0}</strong>) se puede actualizar la información sumunistrada. Gracias!", provider.Id));
            this._providersRepsotory.sendEmail(provider.CompanyEmail, sp_send.ToString(), sb.ToString());

            StringBuilder sbp = new StringBuilder();
            StringBuilder sbp_send = new StringBuilder();
            sbp.AppendLine(string.Format("<br>Los datos de la compañía <strong>{0}</strong> fue registrada en la página. Con el nit <strong>{1}</strong>. Gracias!", provider.NameEnterprise,provider.Nit));
            this._providersRepsotory.sendEmail(ConfigurationManager.AppSettings["EmailProveedor"].ToString(), sbp_send.ToString(), sbp.ToString());
        }

        public void sendmessageUpdate(ProviderContract provider)
        {
            StringBuilder sb = new StringBuilder();
            StringBuilder sp_send = new StringBuilder();
            var subject = "Registro de proveedor";
            sp_send.AppendLine(string.Format("{0}", subject));
            sb.AppendLine(string.Format("Señor(es) {0}.", provider.NameEnterprise));
            sb.AppendLine(string.Format("<br>Los datos de la compañía ha sido actualizada. Con el siguiente código (<strong>{0}</strong>) se puede actualizar la información sumunistrada. Gracias!", provider.Id));
            this._providersRepsotory.sendEmail(provider.ContactEmail, sp_send.ToString(), sb.ToString());

        }

        public void sendMessageToUserRole(ProviderContract provider, UserInfo user)
        {
            StringBuilder sb = new StringBuilder();
            StringBuilder sp_send = new StringBuilder();
            var subject = "Proveedor Registrado";
            sp_send.AppendLine(string.Format("{0}", subject));
            sb.AppendLine(string.Format("Señor(a) {0}.", user.FirstName));
            sb.AppendLine(string.Format("<br><br>Se ha registrado un nuevo proveedor, con el siguiente nit {0}, con el nombre {1}",  provider.Nit, provider.NameEnterprise));
            this._providersRepsotory.sendEmail(user.Email, sp_send.ToString(), sb.ToString());
        }
        public void sendMessageToUser(List<ProviderContract> providers,UserInfo user)
        {
            StringBuilder sb = new StringBuilder();
            StringBuilder sp_send = new StringBuilder();
            
            var subject = "Reporte Provedores";
            sp_send.AppendLine(string.Format("{0}", subject));

            sb.AppendLine(string.Format("Señor(a) {0}.",user.FirstName));

            

            sb.AppendLine(string.Format("<br><br>En este mes se han actualizados y registrados {0}. Este es el listado de los proveedores en este último mes:", providers.Count().ToString()));
            if (providers.Count()>0)
            {
                string Table = "<TABLE><THEAD>" +
                    "<TR>" +
                    "<TH>NIT</TH>" +
                    "<TH>Nombre Proveedor</TH>" +
                    "<TH>Nombre Asesor</TH>" +
                    "<TH>Email Asesor</TH>" +
                    "<TH>Email Proveedor</TH>" +
                    "<TH>Telefono</TH>" +
                    "<TH>Sitio</TH>" +
                    "</TR></THEAD><TBODY>";
                
                foreach (ProviderContract provider in providers)
                {
                    Table += "<TR>" +
                        "<TD>" + provider.Nit + "</TD>" +
                        "<TD>" + provider.NameEnterprise + "</TD>" +
                        "<TD>" + provider.ContactName + "</TD>" +
                        "<TD>" + provider.ContactEmail + "</TD>" +
                        "<TD>" + provider.CompanyEmail + "</TD>" +
                        "<TD>" + provider.PhoneNumber + "</TD>" +
                        "<TD>" + provider.SiteUrl + "</TD>" +
                        "</TR>";
                }
                Table += "</TBODY></TABLE>";
                sb.AppendLine(Table);

            }
            this._providersRepsotory.sendEmail(user.Email,sp_send.ToString(), sb.ToString());
        }

    }
}

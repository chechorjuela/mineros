﻿using AutoMapper;
using Mineros.Domain.Contracts;
using Mineros.Repository.App;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Domain
{
    public class TypeActivityDomain:BaseDomain
    {
        private TypeActivityRepository _typeActivityRepository;
        public TypeActivityDomain()
        {
            this._typeActivityRepository = new TypeActivityRepository(this.ConnectionName);
        }
        public async Task<List<TypeActivityContract>> getAllTypeActivity()
        {

            var response = new List<TypeActivityContract>();
            List<TypeActivityContract> listTypeActivity = Mapper.DynamicMap<List<TypeActivityContract>>(this._typeActivityRepository.Get().ToList());

            response = listTypeActivity;
            return response;
        }
        public async Task<TypeActivityContract> getAllTypeActivity(int id)
        {
            var response = new TypeActivityContract();
            TypeActivityContract listTypeActivity = Mapper.DynamicMap<TypeActivityContract>(this._typeActivityRepository.Get().Where((t)=>t.Id==id).First());
            response = listTypeActivity;
            return response;
        }
    }
}

﻿using Mineros.Data.Models;
using Mineros.Domain.Contracts;
using Mineros.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Domain
{
    public class ClienteDecevalDomain : BaseDomain
    {

        private ClientDecevalRepository _clientDecevalRepository;
        public ClienteDecevalDomain()
        {
            this._clientDecevalRepository = new ClientDecevalRepository(this.ConnectionName);
        }

        public List<ClientDeceval> GetClientDeceval()
        {
            return this._clientDecevalRepository.Get().ToList();
        }

        public ClientDeceval GetClientDecevalByCodeAndNit(ClientDecevalRequest requestClientDeceval)
        {
            ClientDeceval clientDeceval = new ClientDeceval();

            if (requestClientDeceval != null)
            {
                clientDeceval = this._clientDecevalRepository.GetCertidicateByNitEnterpriseAndDate(requestClientDeceval.nit, requestClientDeceval.codeDeceval);
            }

            return clientDeceval;
        }
    }
}

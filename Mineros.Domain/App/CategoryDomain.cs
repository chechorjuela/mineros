﻿using AutoMapper;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Mineros.Domain.Contracts;
using Mineros.Domain.Contracts.Common;
using Mineros.Domain.Contracts.Request;
using Mineros.Model.Commons;
using Mineros.Repository.App;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Domain
{
    public class CategoryDomain:BaseDomain
    {

        private CategoryRepository categoryRepository;
        private SubcategoryDomain subcategoryDomain;
        private TypeActivityDomain typeActivity;

        public CategoryDomain()
        {
            this.categoryRepository = new CategoryRepository(this.ConnectionName);
            this.subcategoryDomain = new SubcategoryDomain();
            this.typeActivity = new TypeActivityDomain();
        }

        public async Task<List<CategoryContract>> getAllCategories()
        {
            var response = new List<CategoryContract>();
            List<CategoryContract> listCategories = Mapper.DynamicMap<List<CategoryContract>>(this.categoryRepository.Get().ToList());
            foreach (CategoryContract listCategory in listCategories)
            {
                listCategory.subCategories = await this.subcategoryDomain.getSubcategoryByCategory(listCategory.Id);
                listCategory.TypeActivities = await this.typeActivity.getAllTypeActivity(listCategory.TypeActivityId);
            }
            response = listCategories;
            return response;
        }
        public async Task<DataSourceResult> getAllCategoriesFilter(DataSourceRequest request)
        {
            var response = new List<CategoryContract>();
            List<CategoryContract> listCategories = Mapper.DynamicMap<List<CategoryContract>>(this.categoryRepository.Get().ToList());
            if (listCategories.Count()>0)
            {
                foreach (CategoryContract listCategory in listCategories)
                {
                    listCategory.subCategories = await this.subcategoryDomain.getSubcategoryByCategory(listCategory.Id);
                    listCategory.TypeActivities = await this.typeActivity.getAllTypeActivity(listCategory.TypeActivityId);
                }
                return listCategories.ToDataSourceResult(request);
            }
            return new DataSourceResult();
        }
        public async Task<bool> saveCreateCategory(RequestCategoryContract categoryContract)
        {
            try
            {
                Category category = new Category
                {
                    TypeActivityId = categoryContract.TypeActivityId,
                    Spanish = categoryContract.Spanish,
                    English = categoryContract.English
                };
                this.categoryRepository.Create(category);
            }
            catch(Exception exp)
            {
                return false;
            }
            return true;
        }
        public async Task<CategoryContract> updateCategory(RequestCategoryContract categoryContract)
        {
            try
            {
                Category category = new Category
                {
                    Id=categoryContract.Id,
                    TypeActivityId = categoryContract.TypeActivityId,
                    Spanish = categoryContract.Spanish,
                    English = categoryContract.English
                };
                this.categoryRepository.Update(category);
                CategoryContract categorResponse = Mapper.DynamicMap<CategoryContract>(this.categoryRepository.GetById(category.Id));
                return categorResponse;

            }
            catch (Exception exp)
            {
                return null;
            }
        }
        public async Task<bool> deleteCategory(int id)
        {
            try
            {
                Category category = this.categoryRepository.GetById(id);
                this.categoryRepository.Delete(category);
                return true;
            }
            catch (Exception exp)
            {
                return false;
            }

        }
    }
}

﻿using Mineros.Model.Commons;
using Mineros.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Domain
{
    public class CertificateTypeDomain : BaseDomain
    {
        private CertificateTypeRepository _certificateTypeRepository;
        public CertificateTypeDomain(){
            this._certificateTypeRepository = new CertificateTypeRepository(this.ConnectionName);
        }

        public List<CertificateType> GetCertificateAll()
        {
            return this._certificateTypeRepository.Get().ToList();
        }

    }
}

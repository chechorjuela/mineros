﻿using Mineros.Domain.Contracts;
using Mineros.Model.Commons;
using Mineros.Repository.App;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Domain
{
    public class ProviderItemDomain:BaseDomain
    {
        private ItemsSubcategoryRepository _itemSubcategoryRepository;
        public ProviderItemDomain()
        {
            this._itemSubcategoryRepository = new ItemsSubcategoryRepository(this.ConnectionName);
        }
    
    }
}

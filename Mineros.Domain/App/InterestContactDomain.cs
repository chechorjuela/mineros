﻿using DotNetNuke.Entities.Modules;
using DotNetNuke.Services.FileSystem;
using Kendo.Mvc.UI;
using Mineros.Domain.Contracts;
using Mineros.Model.Commons;
using Mineros.Repository;
using Mineros.Repository.App;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Domain.App
{
    public class InterestContactDomain : BaseDomain
    {
        private InterestContactRepository _interestContactRepository;
        private WorkUsRepository _workUsRepository;
        private Utils _utils;
        public InterestContactDomain()
        {
            this._utils = new Utils();
            this._interestContactRepository = new InterestContactRepository(this.ConnectionName);
            this._workUsRepository = new WorkUsRepository();
        }

        public InterestContact storeInterestContact(InterestContact interestContact,string file_path="",bool sendEmail = true)
        {
            StringBuilder sb = new StringBuilder();
            StringBuilder sp_send = new StringBuilder();
            if (sendEmail)
            {
                var subject = "información";

                sb.AppendLine(string.Format("El Señor(a) {0} ha Enviado datos.",
                    interestContact.name_contact.ToString()));
                sb.AppendLine(string.Format("<br><br>Se puede comunicar al telefono: {0}. o con el correo suministrado: {1}.",
                    interestContact.number_phone.ToString(), interestContact.email.ToString()));

                sp_send.AppendLine(string.Format("<p>Hola {0}!</p>",
                    interestContact.name_contact.ToString()));
                sp_send.AppendLine(string.Format("<p>Hemos recibido tus datos. Esta será almacenada en nuestra base de datos y será tenida en cuenta al momento de tener una vacancte que se ajuste a tu intereses."));
                sp_send.AppendLine(string.Format("<p>¡Muchas gracias Por enviar tu información!</p>"));
                this._workUsRepository.sendEmail(interestContact.email, file_path, subject.ToString(), sb.ToString(), "default", sp_send.ToString());
            }
            
            this._interestContactRepository.Create(interestContact);

            return interestContact;
        }

   

        public InterestContact getInterestContactsByID(int id)
        {
            return this._interestContactRepository.GetById(id);
        }

        public DataSourceResult getInterestContactFilter(DataSourceRequest request)
        {
            return this._interestContactRepository.GetInterestContactFilter(request);

        }
        public bool deleteInterestContactById(int id)
        {
            bool boolDelete = false;
            try
            {
                this._interestContactRepository.Delete(this._interestContactRepository.GetById(id));
                boolDelete = true;
            }
            catch (Exception exp)
            {
               
            }
            return boolDelete;
        }
        public InterestContact updateInterestContact(InterestContact interestContact)
        {
            InterestContact itemUpdated = new InterestContact();
            try
            {
                itemUpdated = interestContact;
                this._interestContactRepository.Update(interestContact);
            }
            catch (Exception exp) { 
            }
            return itemUpdated;
        }

        public string createFileDownloadZip(DataSourceRequest datarequest)
        {
            datarequest.Page = 1;
            datarequest.PageSize = 1000000000;
            FolderManager folderManager = new FolderManager();

            DataSourceResult sourceResult = this._interestContactRepository.GetInterestContactFilter(datarequest);
            List<InterestContact> interest_file = new List<InterestContact>();
            
            foreach (InterestContact result in sourceResult.Data)
            {
               interest_file.Add(new InterestContact() { 
                   Id=result.Id, 
                   name_contact= result.name_contact,
                   email=result.email, 
                   number_phone=result.number_phone,
                   city_current=result.city_current,
                   country_current=result.country_current,
                   interest_position_first= result.interest_position_first,
                   process_interest_first=result.interest_position_first,
                   location_interest_first=result.location_interest_first,
                   file_dir = result.file_dir});
            }

            PortalModuleBase oPortalMB = new PortalModuleBase();
            var folders = folderManager.GetFolders(oPortalMB.PortalId);
            IFolderInfo folderDocuments = folders.FirstOrDefault(s => s.FolderName == "ZipTemp");
            IFolderInfo path_dir_file = folders.FirstOrDefault(s => s.FolderName == "");
            if (folderDocuments == null)
            {
                folderManager.AddFolder(oPortalMB.PortalId, "ZipTemp");
                folders = folderManager.GetFolders(oPortalMB.PortalId);
                folderDocuments = folders.FirstOrDefault(s => s.FolderName == "ZipTemp");
            }

            List<System.IO.FileInfo> fileInfo = new List<System.IO.FileInfo>();
            foreach (InterestContact interest in interest_file)
            {
                if (interest.file_dir!=null)
                {
                    System.IO.FileInfo file = new System.IO.FileInfo(interest.file_dir);
                    fileInfo.Add(file);
                }
               
            }
             
            string path_zip_disk = this._utils.compressFile(folderDocuments.PhysicalPath, fileInfo, path_dir_file.PhysicalPath, oPortalMB.PortalId);
            int index_site = path_zip_disk.IndexOf("Portals");
            string path_zip = path_zip_disk.Substring(index_site, path_zip_disk.Length - index_site).Replace(@"\", "/");
            return path_zip;
        }
    }
}

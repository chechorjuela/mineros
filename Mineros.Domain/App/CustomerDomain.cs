﻿using Mineros.Model.Commons;
using Mineros.Repository.App;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Domain
{
    public class CustomerDomain : BaseDomain
    {
        private CustomerRepository _customerRepository;

        public CustomerDomain()
        {
            this._customerRepository = new CustomerRepository(this.ConnectionName);
        }

        public List<Customer> GetCustomers()
        {
            return this._customerRepository.Get().ToList();
        }
    }
}

﻿using Mineros.Domain.Contracts;
using Mineros.Model.Commons;
using Mineros.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Domain
{
    public class CertificateDecevalDomain : BaseDomain
    {
        private CertificateDecevalRepository _certificateDecevalRepository;
        public CertificateDecevalDomain()
        {
            this._certificateDecevalRepository = new CertificateDecevalRepository(this.ConnectionName);
        }

        public CertificateDeceval storeCertificate(CertificateDeceval certificate)
        {
            this._certificateDecevalRepository.Create(certificate);

            return certificate;
        }

        public List<CertificateDeceval> GetCertificatesSearch(RequestCertificateDeceval requestCertificates)
        {
            List<CertificateDeceval> certificates = this._certificateDecevalRepository.GetCertificatesSearch(requestCertificates.identification,requestCertificates.codeDeceval);

            return certificates;

        }

        public CertificateDeceval GetCertificateByNitCodedeceval(string nit,string codeDeceval)
        {
            CertificateDeceval certificate = this._certificateDecevalRepository.GetCertidicateDecevalByNitAndCodeval(nit, codeDeceval);

            return certificate;
        }

        public CertificateDeceval GetCertificateByCodedeceval(string codeDeceval)
        {
            CertificateDeceval certificate = this._certificateDecevalRepository.GetCertidicateDecevalByCodeval(codeDeceval);

            return certificate;
        }

        public void updateCertificatedeceval(CertificateDeceval certificate)
        {
            this._certificateDecevalRepository.Update(certificate);
        }

    }
}

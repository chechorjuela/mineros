﻿using Mineros.Domain.Contracts;
using Mineros.Model.Commons;
using Mineros.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Domain
{
    public class CertificateDomain : BaseDomain
    {
        private CertificateRepository _certificateRepository;
        public CertificateDomain()
        {
            this._certificateRepository = new CertificateRepository(this.ConnectionName);
        }

        public Certificate storeCertificate(Certificate certificate)
        {
            this._certificateRepository.Create(certificate);

            return certificate;
        }

        public List<Certificate> GetCertificatesSearch(RequestCertificates requestCertificates)
        {
            List<Certificate> certificates = this._certificateRepository.GetCertificatesSearch(requestCertificates.nit,requestCertificates.custumerID, requestCertificates.typeCertificate);

            return certificates;

        }

        public Certificate GetCertificateByNitEnterpriseAndYear(string nit, int typeCertificateId, int enterprise,string path_file,string year)
        {
            Certificate certificate = this._certificateRepository.GetCertidicateByNitEnterpriseAndDate(nit, typeCertificateId, enterprise, path_file, year);

            return certificate;
        }

        public void updateCertificate(Certificate certificate)
        {
            this._certificateRepository.Update(certificate);
        }

    }
}

﻿using Mineros.Data;
using Mineros.Data.Models;
using Mineros.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Domain.App
{
    public class EasyDnnNewsDomain
    {

        public string search_category = "Ofertas laborales";
        private string CultureCountry
        {
            get
            {
                CultureInfo culture2 = CultureInfo.CurrentCulture;
                return culture2.Name ?? "es-ES";
            }
        }
        public List<EasyDnnNewsAppJobs> getEasyNewsWorks(int page, int limit)
        {

            List<EasyDnnNewsAppJobs> Articles = new List<EasyDnnNewsAppJobs>();

            EasyDnnNewsData easyDnnNewsData = new EasyDnnNewsData();
            int countItems = 2;
            var Categories = easyDnnNewsData.GetAllCategoriesNews(CultureCountry);
            //lnkurl.PostBackUrl = ResolveUrl(string.Format("~/DesktopModules/EasyDNNNews/DocumentDownload.ashx?portalid={0}&moduleid={1}&articleid={2}&documentid={3}", PortalId, document.ModuleId, document.ArticleID, document.DocEntryID));

            var parentCategory = Categories.FirstOrDefault(s => s.CategoryName.ToLower() == search_category.ToString().ToLower());

            if (parentCategory != null)
            {
                Articles = easyDnnNewsData.GetArticlesJobs(null, parentCategory.CategoryID, page, limit, ref countItems, CultureCountry).OrderByDescending((a) => a.PublishDate).ToList();

            }

            return Articles;
        }
    }
}

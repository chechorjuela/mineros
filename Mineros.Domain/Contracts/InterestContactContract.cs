﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Domain.Contracts
{
    public class InterestContactContract
    {
        public string nameFull { get; set; }
        public string nit { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string city { get; set; }
        public string country { get; set; }
        public int interest_first { get; set; }
        public int? interest_second { get; set; }
        public int proccess_first { get; set; }
        public int? process_second { get; set; }
        public int location_first { get; set; }
        public int? location_second { get; set; }
        public string dir_file { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace Mineros.Domain.Contracts
{
    [DataContract()]
    public class ItemCategoryContract
    {
        [DataMember()]
        public int Id { get; set; }
        [DataMember()]
        public int SubcategoryId { get; set; }
        [DataMember()]
        public int ProviderId { get; set; }
        [DataMember()]
        public int CategoryId { get; set; }
        [DataMember()]
        public int TypeActivityId { get; set; }
        [DataMember()]
        public string SubcategorySpanish { get; set; }
        [DataMember()]
        public string SubcategoryEnglish { get; set; }
        [DataMember()]
        public string CategorySpanish { get; set; }
        [DataMember()] 
        public string CategoryEnglish { get; set; }
        [DataMember()]
        public string TypeActivitySpanish { get; set; }
        [DataMember()]
        public string TypeActivityEnglish { get; set; }

    }
}

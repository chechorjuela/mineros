﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Domain.Contracts
{
    public class RequestCertificates
    {
        public string nit { get; set; }
        public int custumerID { get; set; }
        public int typeCertificate { get; set; }
    }
}

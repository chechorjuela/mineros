﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Domain.Contracts.Request
{
    public class RequestCategoryContract
    {
        public int Id { get; set; }
        public string Spanish { get; set; }
        public string English { get; set; }
        public int TypeActivityId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Domain.Contracts
{
    public class SubCategoryContract
    {
        public int Id { get; set; }
        public string Spanish { get; set; }
        public string English { get; set; }
        public int CategoryId { get; set; }
        public CategoryContract category { get; set; }
    }

    public class SubCategoryResponseContract
    {
        public int SubcategoryId { get; set; }
         public string SubcategorySpanish { get; set; }
        public string SubcategoryEnglish { get; set; }
        public int CategoryId { get; set; }
        public string CategorySpanish { get; set; }
        public string CategoryEnglish { get; set; }
        public int TypeActivityId { get; set; }
        public string TypeactivitySpanish { get; set; }
        public string TypeactivityEnglish { get; set; }
    }
}

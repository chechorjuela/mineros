﻿using Mineros.Model.Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Domain.Contracts
{
    [DataContract()]
    public class ProviderContract
    {
        [DataMember()]
        public int Id { get; set; }
        [DataMember()]
        public string Nit { get; set; }
        [DataMember()]
        public string NameEnterprise { get; set; }
        [DataMember()]
        public string SiteUrl { get; set; }
        [DataMember()]
        public string ContactName { get; set; }
        [DataMember()]
        public string ContactEmail { get; set; }
        [DataMember()]
        public string CompanyEmail { get; set; }
        [DataMember()]
        public string PhoneNumber { get; set; }
        [DataMember()]
        public string FilePath { get; set; }
        [DataMember()]
        public string Countries { get; set; }
        [DataMember()]
        public int Status { get; set; }
        [DataMember()]
        public DateTime CreateAt { get; set; }
        [DataMember()]
        public DateTime? UpdateAt { get; set; }
        [DataMember()]
        public DateTime? DeleteAt { get; set; }
        [DataMember()]
        public List<BrandContract> Brands { get; set; }
        [DataMember()]
        public List<ItemCategoryContract> ItemCategory { get; set; }
        [DataMember()]
        public string Categories { get; set; }
        [DataMember()]
        public string SubCategories { get; set; }
        [DataMember()]
        public string BrandsString { get; set; }
        public ProviderContract()
        {

        }
    }
}

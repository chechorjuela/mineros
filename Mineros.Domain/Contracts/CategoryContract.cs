﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Domain.Contracts
{
    public class CategoryContract
    {
        public int Id { get; set; }
        public string Spanish { get; set; }
        public string English { get; set; }
        public string Status { get; set; }
        public int TypeActivityId { get; set; }
        public TypeActivityContract TypeActivities {get;set;}
        public List<SubCategoryContract> subCategories { get; set; }
    }
}

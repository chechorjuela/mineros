﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Domain.Contracts
{
    public class ClientDecevalRequest
    {
        public string codeDeceval { get; set; }
        public string nit { get; set; }
    }
}

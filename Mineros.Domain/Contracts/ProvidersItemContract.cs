﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Domain.Contracts
{
    public class ProvidersItemContract
    {
        public int Id { get; set; }
        public SubCategoryContract ServiceId{ get; set; }
        public ProviderContract ProvidersId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Domain.Contracts
{
    public class InterestSettingsContract
    {
        public int id { get; set; }
        public string name_spanish { get; set; }
        public string name_english { get; set; }
        public int type_interest { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Domain.Contracts
{
    public class ClientDecevalFile
    {
        public int Id { get; set; }
        public string codeDeceval { get; set; }
        public string typeNit { get; set; }
        public string numberID { get; set; }
        public string nameClient { get; set; }
        public bool fileExist { get; set; }
    }
}

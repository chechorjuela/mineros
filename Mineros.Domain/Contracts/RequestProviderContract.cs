﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Domain.Contracts
{
    public class RequestProviderContract
    {
        public int id { get; set; }
        public string nameEnterprise { get; set; }
        public string nit { get; set; }
        public string siteUrl { get; set; }
        public string contactName { get; set; }
        public string phoneNumber { get; set; }
        public string contactEmail { get; set; }
        public string companyEmail { get; set; }
        public string[] countries { get; set; }
        public List<CategoryList> subcategoryProduct { get; set; }
        public List<CategoryList> subcategoryService { get; set; }
        public string[] brands { get; set; }
        public FileBrochure fileBrochure { get; set; }
        public bool codeChek { get; set; }
    }

    public class FileBrochure
    {
        public string FileStringBase64 { get; set; }
        public string FileName { get; set; }
        public string SizeImage { get; set; }
        public string TypeFileBase64 { get; set; }
    }

    public class CategoryList
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}

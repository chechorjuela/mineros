﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Domain.Contracts
{
    public class WorkUsContactContract
    {
        public string name { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string city { get; set; }
        public int easyNewId { get; set; }
        public string file { get; set; }
        public string fileExtension { get; set; }
        public string fileName { get; set; }
        public string title_job { get; set; }
        public string country_job { get; set; }
        public string city_job { get; set; }
        public string enterprise_job { get; set; }
        public string summary_job { get; set; }
    }
}

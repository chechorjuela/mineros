﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Domain.Contracts
{
    public class RequestCertificateDeceval
    {
        public string identification { get; set; }
        public string codeDeceval { get; set; }
    }
}

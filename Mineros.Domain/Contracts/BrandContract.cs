﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mineros.Domain.Contracts
{
    public class BrandContract
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}

<%@ Control Language="c#" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>
<%@ Register TagPrefix="dnn" TagName="USER" Src="~/Admin/Skins/user.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGIN" Src="~/Admin/Skins/Login.ascx" %>
<%@ Register TagPrefix="dnn" TagName="STYLES" Src="~/Admin/Skins/Styles.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LINKTOMOBILE" Src="~/Admin/Skins/LinkToMobileSite.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LANGUAGE" Src="~/Admin/Skins/Language.ascx" %>
<%@ Register TagPrefix="dnn" TagName="SEARCH" Src="~/Admin/Skins/Search.ascx" %>
<%@ Register TagPrefix="dnn" TagName="META" Src="~/Admin/Skins/Meta.ascx" %>
<%@ Register TagPrefix="dnn" TagName="jQuery" Src="~/Admin/Skins/jQuery.ascx" %>

<dnn:META ID="META1" runat="server" Name="viewport" Content="width=device-width,initial-scale=1, maximum-scale=1" />
<dnn:META runat="server" Name="description" Content="Mineros" />

<!-- Css Includes -->
<dnn:DnnCssInclude runat="server" ID="cssBootstrap" FilePath="css/bootstrap.min.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssFonts" FilePath="css/fonts.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssclassFonts" FilePath="css/class-fonts.css" PathNameAlias="SkinPath" />

<dnn:DnnCssInclude runat="server" ID="cssColors" FilePath="css/colors.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssStyles" FilePath="css/styles.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssStylesNosotrosDireccion" FilePath="css/styles-nosotros-direccion.css" PathNameAlias="SkinPath" />

<!-- JS Includes -->
<dnn:DnnJsInclude runat="server" ID="jsPopper" FilePath="js/popper.min.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsBootstrap" FilePath="js/bootstrap.min.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsCustom" FilePath="js/custom.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsCustomNosotrosDireccion" FilePath="js/app-NosotrosDireccion.js" PathNameAlias="SkinPath" />
<body>
    <section id="baner" class="section-baner">
      <div id="ContentPane" runat="server"></div>
    </section>
    <section id="direccionamiento" class="section-direccionamiento">
      <div id="direccionamientoRunatServe" runat="server"></div>
    </section>
    <section id="valores" class="section-valores bg-alto" data-aos="fade-down">
        <div class="container-fluid" >
            <div id="valoresTitleRunatServe" runat="server"></div>
            <div class="row content-col-tag">
                <div class="col-12 col-md-6 col-lg-4 content-tag">
                    <div id="Tag1RunatServe" runat="server"></div>
                </div>
                <div class="col-12 col-md-6 col-lg-4 content-tag">
                    <div id="Tag2RunatServe" runat="server"></div>
                </div>
                <div class="col-12 col-md-6 col-lg-4 content-tag">
                    <div id="Tag3RunatServe" runat="server"></div>
                </div>
                <div class="col-12 col-md-6 col-lg-4 content-tag">
                    <div id="Tag4RunatServe" runat="server"></div>
                </div>
                <div class="col-12 col-md-6 col-lg-4 content-tag">
                    <div id="Tag5RunatServe" runat="server"></div>
                </div>
            </div>
        </div>
    </section>
    <section id="estructura" class="section-estructura">
      <div class="container-fluid">
        <div id="estructuraRunatServe" runat="server"></div>
      </div>
    </section>
</body>


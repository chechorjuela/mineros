<%@ Control Language="c#" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>
<%@ Register TagPrefix="dnn" TagName="USER" Src="~/Admin/Skins/user.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGIN" Src="~/Admin/Skins/Login.ascx" %>
<%@ Register TagPrefix="dnn" TagName="STYLES" Src="~/Admin/Skins/Styles.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LINKTOMOBILE" Src="~/Admin/Skins/LinkToMobileSite.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LANGUAGE" Src="~/Admin/Skins/Language.ascx" %>
<%@ Register TagPrefix="dnn" TagName="SEARCH" Src="~/Admin/Skins/Search.ascx" %>
<%@ Register TagPrefix="dnn" TagName="META" Src="~/Admin/Skins/Meta.ascx" %>
<%@ Register TagPrefix="dnn" TagName="jQuery" Src="~/Admin/Skins/jQuery.ascx" %>
<%@ Register TagPrefix="dnn" TagName="MENU" Src="~/DesktopModules/DDRMenu/Menu.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGO" Src="~/Admin/Skins/Logo.ascx" %>
<dnn:META ID="META1" runat="server" Name="viewport" Content="width=device-width,initial-scale=1, maximum-scale=1" />
<dnn:META runat="server" Name="description" Content="Mineros" />
<!-- Css Includes -->
<dnn:DnnCssInclude runat="server" ID="cssBootstrap" FilePath="css/bootstrap.min.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssFonts" FilePath="css/fonts.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssclassFonts" FilePath="css/class-fonts.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssAos" FilePath="css/aos.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssLightSilder" FilePath="css/lightSilder.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssColors" FilePath="css/colors.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssHover" FilePath="css/hover-min.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssStyles" FilePath="css/styles.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssStylesResponsabilidadSocial" FilePath="css/style-ResponsabilidadSocial.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssSidebar" FilePath="css/sidebar.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssFontawesome" FilePath="css/fonts/fontawesome/css/all.css" PathNameAlias="SkinPath" />
<!-- JS Includes -->
<dnn:DnnJsInclude runat="server" ID="jsPopper" FilePath="js/popper.min.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsBootstrap" FilePath="js/bootstrap.min.js" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" FilePath="~/Resources/Libraries/kendo-ui-angular/css/kendo.common-material.min.css" />
<dnn:DnnCssInclude runat="server" FilePath="~/Resources/Libraries/kendo-ui-angular/css/kendo.material.min.css" />
<dnn:DnnCssInclude runat="server" ID="cssBanner" FilePath="css/style-banner.css" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" FilePath="~/Resources/Libraries/AngujarJS/01_04_05/angular.min.js" Priority="10" />
<dnn:DnnJsInclude runat="server" FilePath="~/Resources/Libraries/AngujarJS/01_04_05/angular-sanitize.min.js" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
<dnn:DnnJsInclude runat="server" FilePath="https://code.angularjs.org/1.6.9/angular-animate.min.js" Priority="10" />
<dnn:DnnJsInclude runat="server" ID="jsAos" FilePath="js/aos.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsEcharts" FilePath="libs/incubator-echarts-4.2.1/js/echarts.min.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsLighSlider" FilePath="js/lightslider.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsCustom" FilePath="js/custom.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsMain" FilePath="js/main.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" FilePath="~/DesktopModules/Mineros/scripts/app.js" Priority="10" />
<dnn:DnnJsInclude runat="server" ID="jsFiliales" FilePath="js/filiales_slider.js" PathNameAlias="SkinPath" />
<div id="hed-top-green" class="bg-mineral-green d-flex">
<!--     <ul id="bar-lang" class="nav justify-content-around nav-respon" data-aos="fade-left">
        <li class="nav-item">
            <a class="nav-link librefranklin-thin text-white" href="/en-us" data-toggle="modal" data-target="#inconstruction">English</a>
            <span></span>
        </li>
        <li class="nav-item">
            <a class="nav-link librefranklin-thin text-white active" href="/es-es">Español</a>
            <span></span>
        </li>
    </ul> -->
        <div id="bar-lang" class="nav justify-content-around" data-aos="fade-left">
            <dnn:LANGUAGE runat="server" ID="dnnLANGUAGE" ShowLinks="True" ShowMenu="False" ItemTemplate=''/>
        </div>    
    <ul id="hed-top-band" class="nav justify-content-end" data-aos="fade-left">
        <li class="nav-item">
            <a class="nav-link" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/argentina" target="_blank">
                <span class="box-img-loc hvr-icon-bob"><img src="/Portals/0/skins/mineros/images/2x/argentina.png" srcset="/Portals/0/skins/mineros/images/svg/argentina.svg" alt="Bandera de Argentina" class="hvr-icon"></span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/Chile" target="_blank">
                <span class="box-img-loc hvr-icon-bob"><img src="/Portals/0/ban-4-1.png" class="hvr-icon" alt="Bandera de Chile"></span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/Colombia" target="_blank">
                <span class="box-img-loc hvr-icon-bob"><img src="/Portals/0/skins/mineros/images/2x/colombia.png" srcset="/Portals/0/skins/mineros/images/svg/colombia.svg" alt="Bandera de Colombia" class="hvr-icon"></span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="http://hemco.com.ni/" target="_blank">
                <span class="box-img-loc hvr-icon-bob"><img src="/Portals/0/skins/mineros/images/2x/nicaragua.png" srcset="/Portals/0/skins/mineros/images/svg/nicaragua.svg" alt="Bandera de Nicaragua" class="hvr-icon"></span>
            </a>
        </li>
    </ul>
</div>
<header id="masthead" class="site-header js" role="banner" data-aos="fade-down">
    <div class="navigation-top bg-menu-trans">
        <div class="wrap d-flex  justify-content-between">
            <div id="logo-header" class="logo-header text-center position-relative">
                <dnn:LOGO runat="server" ID="dnnLOGO" />
                <div class="d-flex justify-content-center align-items-center line-logo">
                    <span class="d-flex"></span>
                </div>
            </div>
            <div class="d-flex box-menu">
                <dnn:MENU ID="MENU" MenuStyle="Menus/MainMenu" runat="server" NodeSelector="*" />
            </div>
        </div>
        <div class="d-flex mr-auto flex-wrap">
            <a class="img-margin" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/argentina" target="_blank">
                <span class="box-img-loc hvr-icon-bob">
                    <img src="/Portals/0/skins/mineros/images/Flag_of_Argentina.png" srcset="/Portals/0/skins/mineros/images/Flag_of_Argentina.svg" alt="Bandera De Argentina" class="icon-flag hvr-icon">
                </span>
            </a>
            <a class="img-margin" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/Chile" target="_blank">
                <span class="box-img-loc hvr-icon-bob">
                    <img src="/Portals/0/skins/mineros/images/2x/ban-4-1.png" srcset="/Portals/0/skins/mineros/images/SVG/ban-4-1.svg" alt="Bandera de Chile" class="icon-flag hvr-icon">
                </span>
            </a>
            <a class="img-margin" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/Colombia" target="_blank">
                <span class="box-img-loc hvr-icon-bob">
                    <img src="/Portals/0/skins/mineros/images/Flag-of-colombia.png" srcset="/Portals/0/skins/mineros/images/flag_of_colombia.svg" alt="Bandera de Colombia" class="icon-flag hvr-icon">
                </span>
            </a>
            <a class="img-margin" href="http://hemco.com.ni/" target="_blank">
                <span class="box-img-loc hvr-icon-bob">
                    <img src="/Portals/0/skins/mineros/images/Flag_of_Nicaragua.png" srcset="/Portals/0/skins/mineros/images/Flag_of_Nicaragua.svg" alt="Bandera de nicaragua" class="icon-flag hvr-icon">
                </span>
            </a>
            <div class="box-icon-rrss w-100 d-flex align-items-center">
                <a class="link-rrss text-white aos-init aos-animate" href="https://www.facebook.com/MinerosSA" data-aos="flip-down" target="_blank"><span><i class="text-white fab fa-facebook-f"></i></span></a>
                <a class="link-rrss text-white aos-init aos-animate" href="https://twitter.com/mineros_sa" data-aos="flip-down" target="_blank"><span><i class="text-white fab fa-twitter"></i></span></a>
                <a class="link-rrss text-white aos-init aos-animate" href="https://www.youtube.com/user/MINEROSSA" data-aos="flip-down" target="_blank"><span><i class="text-white fab fa-youtube"></i></span></a>
                <a class="link-rrss text-white aos-init aos-animate" href="http://instagram.com/mineros_sa" data-aos="flip-down" target="_blank"><span><i class="text-white fab fa-instagram"></i></span></a>
                <a class="link-rrss text-white aos-init aos-animate" href="https://co.linkedin.com/company/mineros-s.a." data-aos="flip-down" target="_blank"><span><i class="text-white fab fa-linkedin-in"></i></span></a>
            </div>
        </div>
    </div>
</header>
<style>
    html,
body {
    overflow-x: hidden;
}

/*---------------------------------------------------------
        section navigation
        ---------------------------------------------------------*/

.section-navigation .breadcrumb-item a {
    text-decoration: none;
    font-size: 1rem;
    line-height: 19.2px;
    letter-spacing: 0rem;
}

.section-navigation .breadcrumb-item.active {
    text-decoration: none;
    font-size: 1rem;
    line-height: 19.2px;
    letter-spacing: 0rem;
}

@media screen and (max-width: 575px) {
    .section-navigation .breadcrumb {
        flex-wrap: nowrap;
        align-items: center;
    }

    .section-navigation .breadcrumb-item.active {
        display: flex;
        align-items: center;
    }
}

.section-baner .col {
    min-height: 355px;
}

li.list-group-item.rounded-0 {
    border: 0;
}

/* Structure modification */
@media (min-width: 576px) {
    .container.content_AS {
        max-width: 540px;
    }
}

@media (min-width: 768px) {
    .container.content_AS {
        max-width: 720px;
    }
}

@media (min-width: 992px) {
    .container.content_AS {
        max-width: 960px;
    }

    .container.content_AS>.row {
        flex-wrap: nowrap;
        max-width: 100%;
        width: 100%;
    }
}

@media (min-width: 1200px) {
    .container.content_AS {
        max-width: 1150px;
    }

    .content_AS .content_S {
        flex-basis: 860px;
        padding-left: 0;
        padding-right: 25px;
        width: 100%;
        max-width: 860px;
    }

    .content_AS .content_A {
        flex-basis: 275px;
    }
}

.content_S .content_info {
    margin-bottom: 0;
    padding: 0;
    border: 0;
    z-index: 9;
}

.container.content_AS>.row {
    max-width: 100%;
    width: 100%;
}

li.list-group-item.rounded-0 {
    padding-left: 0;
}

.content_S .card-body {
    padding-bottom: 10px;
    padding-top: 30px;
    padding-right: 42px;
}

.content_AS .content_S .content_info {
    padding-top: 52px;
}

.content_AS .content_S .content_info.text_2 {
    padding-bottom: 45px;
    margin-bottom: 62px;
}

.content_AS .content_S .content_info.text_g .text {
    margin-bottom: 15px;
}

.content_AS .content_S .content_info .descarga .icon {
    width: 42px;
    height: 31px;
}

.content_AS .content_S .content_info.border {
    padding: 15px 60px 15px 32px;
    margin-bottom: 30px;
}

/*----------------- ASIDE -------------------------------*/
.content_A .aside .indicadores .indicadores-ico {
    min-width: 48px;
    width: 56px;
    height: 42px;
}

.content_A .aside .indicadores .indicadores-ico img {
    width: 37.385px;
    height: 27px;
}

.content_A .aside .indicadores .h4.title {
    border-bottom-width: 2px;
    border-top: 0;
    border-left: 0;
    border-right: 0;
    font-size: 1.125rem;
    line-height: 2.25rem;
    letter-spacing: 0rem;
    padding-left: 18px;
}

.content_A .aside .indicadores .card {
    margin-top: 32px;
}

.content_A .aside .indicadores .h4.card-title {
    font-size: 1.136875rem;
    line-height: 1.136875rem;
    letter-spacing: 0rem;
    margin-bottom: 2px;
}

.content_A .aside .indicadores .card-header .card-text {
    font-size: 2rem;
    line-height: 2.4rem;
    letter-spacing: 0rem;
}

.content_A .aside .indicadores .card-header .moneda {
    font-size: 1.0625rem;
    line-height: 1.275rem;
    letter-spacing: 0rem;
}

.content_A .aside .indicadores .card-header::after {
    content: "";
    position: absolute;
    border: 0.5px solid #fff;
    max-width: 192px;
    width: 192px;
    left: 20px;
    right: 0;
    bottom: 0;
}

.content_A .aside .indicadores .card-text {
    font-size: 0.875rem;
    line-height: 0.82rem;
    letter-spacing: 0rem;
}

.content_A .aside .indicadores .card-text span {
    font-size: 0.875rem;
    line-height: 1.05rem;
    letter-spacing: 0rem;
}

.content_A .aside .indicadores .card-body {
    padding-top: 15px;
}

.content_A .indicadores .card span.span-simbolo {
    font-size: 8.49rem;
    top: -10px;
    line-height: 10rem;
    right: -35px;
    font-family: "Franklin Gothic Demi";
    color: #fff;
    opacity: .10;
}

[class^="hvr-"] {
    margin: inherit;
    padding: inherit;
    cursor: pointer;
    /*background: #e1e1e1;*/
    text-decoration: none;
    color: #666;
    -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
}

.hvr-icon-wobble-horizontal {
    display: inline-block;
    vertical-align: middle;
    -webkit-transform: perspective(1px) translateZ(0);
    transform: perspective(1px) translateZ(0);
    box-shadow: 0 0 1px rgba(0, 0, 0, 0);
    -webkit-transition-duration: 0.3s;
    transition-duration: 0.3s;
}

.hvr-icon-wobble-horizontal .hvr-icon {
    -webkit-transform: translateZ(0);
    transform: translateZ(0);
}

.aside .tarjetas .trimestre {
    padding-top: 52px;
}

.aside .tarjetas .trimestre .card-header {
    padding-left: 29px;
    padding-top: 22px;
    padding-bottom: 8px;
}

.aside .tarjetas .trimestre .h4 {
    font-size: 1.875rem;
    line-height: 2.25rem;
    letter-spacing: 0rem;
}

.aside .tarjetas .trimestre .card-body {
    padding-top: 20px;
    padding-left: 31px;
    min-height: 220px;
}

.aside .tarjetas .trimestre .card-body .card-text {
    font-size: 1.11625rem;
    line-height: 1.34rem;
    letter-spacing: 0rem;
}

.aside .tarjetas .trimestre .card-body .image-two img {
    max-width: 100%;
    position: absolute;
    right: -65px;
    bottom: -33px;
}

.aside .tarjetas .boletines {
    padding-top: 66px;
}

.aside .tarjetas .boletines .card-body {
    background-image: url(/Portals/0/skins/mineros/images/boletin-img.jpg);
    background-repeat: no-repeat;
    background-size: cover;
    min-height: 356px;
    padding-top: 48px;
    padding-right: 14px;
    position: relative;
}

.aside .tarjetas .boletines .card-body::after {
    content: "";
    position: absolute;
    background-color: #22a571;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    opacity: .6;
}

.aside .tarjetas .boletines .card-body p.card-text {
    font-size: 1.125rem;
    line-height: 1.35rem;
    letter-spacing: 0rem;
    max-width: 146px;
    z-index: 9;
    position: relative;
    padding-bottom: 6px;
}

.aside .tarjetas .boletines .card-body .card-title {
    font-size: 1.875rem;
    line-height: 1.9rem;
    letter-spacing: 0rem;
    max-width: 143px;
    z-index: 9;
    position: relative;
}

.aside .tarjetas .boletines .card-footer {
    padding-top: 6px;
    padding-bottom: 5px;
}

.aside .tarjetas .boletines .card-footer .btn {
    font-size: 1.125rem;
    line-height: 1.35rem;
    letter-spacing: 0rem;
}

.aside .tarjetas .boletines .card-footer .btn span {
    margin-left: 25px;
}

@media (max-width: 991.8px) {
    .aside .tarjetas .trimestre .card-body {
        min-height: 282px;
    }

    .aside .tarjetas .boletines {
    padding-bottom: 52px;
    padding-top: 0;
}
}

@media (max-width: 575.8px) {
    .content_AS .content_S {
        padding-left: 0;
        padding-right: 0;
    }

    .content_AS .content_A {
        padding-left: 0;
        padding-right: 0;
    }

    .content_AS {
        padding-left: 10%;
        padding-right: 10%;
    }
    .content_AS .content_A {
         max-width: 80%;
         margin: auto;
    }

}


</style>
<section class="new_banner">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-md-6 col-lg-4 col_left bg-fern d-flex justify-content-end align-items-center">
                <img src="/portals/0/Images/NosotrosQuienesSomos/adorno-baner.png" srcset="/portals/0/Images/NosotrosQuienesSomos/adorno-baner.svg" class="adorno" alt="Image de adorno">
                <div id="panneCaja" class="caja bg-white position-relative" data-aos="fade-up" runat="server">
                </div>
                <!--./caja-->
            </div>
            <!--./col_left-->
            <div class="col-12 col-md-6 col-lg-8 col_right">
                <div id="panneBannerImage" runat="server"></div>
                <!--<img src="https://mineros.com.co/Portals/0/skins/mineros/images/2x/portada-inversionista-junta.jpg" alt="">-->
            </div>
            <!--./col_right-->
            <div class="line-color"></div>
        </div>
        <!--./row-->
    </div>
    <!--./container-fluid-->
</section><!-- .new-banner -->
<section class="section-navigation bg-pumice aos-init" data-aos="fade-left">
    <div class="container">
        <div id="panneBreadCrumb" runat="server"></div>
    </div>
    <!--.container-->
</section>
<!--.navigation-->
<div class="separador" style="height: 94px;"></div>
<section class="content">
    <div class="container content_AS">
        <div class="row">
            <div class="col-12 col-lg-8 content_S">
                <div id="Section1" runat="server"></div>
            </div>
            <!--./content_S-->
            <div class="col-12 col-lg-4 content_A px-sm-0 pr-lg-0">
                <div class="aside">
                    <div class="row m-0 indicadores">
                        <div class="col-12 px-0 mb-3">
                            <div id="panneTitle" runat="server"></div>
                        </div>
                        <div class="col-12 col-sm-6 col-lg-12 px-lg-0">
                            <div id="panneCard1" runat="server"></div>
                        </div>
                        <!--.col-12-->
                        <div class="col-12 col-sm-6 col-lg-12 px-lg-0">
                            <div id="panneCard2" runat="server"></div>
                        </div>
                        <!--.col-12-->
                        <div class="col-12 col-sm-6 col-lg-12 px-lg-0">
                            <div id="panneCard3" runat="server"></div>
                        </div>
                        <!--.col-12-->
                        <div class="col-12 col-sm-6 col-lg-12 px-lg-0">
                            <div id="panneCard4" runat="server"></div>
                        </div>
                        <!--.col-12-->
                        <div class="col-12 col-sm-12 col-lg-12 px-lg-0">
                            <div id="panneCard5" runat="server"></div>
                        </div>
                        <!--.col-12-->
                        <div class="col-12 col-sm-6 col-lg-12 px-lg-0">
                            <div id="panneCard6" runat="server"></div>
                        </div>
                        <!--.col-12-->
                    </div>
                    <!--.indicadores-->
                    <div class="row m-0 tarjetas">
                        <div class="col-12 col-sm-6 col-lg-12 px-lg-0 trimestre">
                            <div id="ResultadosFinancieros" class="card rounded-0 border-0" data-aos="fade-right" runat="server"></div>
                            <!--.card-->
                        </div>
                        <!--.col-12-->
                        <div class="col-12 col-sm-6 col-lg-12 px-lg-0 boletines">
                            <div id="BoletinesDePrensa" class="card rounded-0 border-0" data-aos="fade-right" runat="server"></div>
                            <!--.card-->
                        </div>
                        <!--.col-12-->
                    </div>
                    <!--.tarjetas-->
                </div>
            </div>
            <!--/.content_A-->
        </div>
    </div>
    <!--./container-->
</section>
<!--./content-->
<!--<div class="content_AS">
        <div class="content_S">
        </div>
        <div class="content_A">
            <div id="Aside" runat="server"></div>
        </div>
    </div>-->
<div id="contentPane" runat="server"></div>
<!-- Modal -->
<div class="modal fade page_construct" id="inconstruction" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="inconstructionLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="inconstructionLabel"><span>Pop up para</span> sección en inglés</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="card-text mb-0">En construcción....</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary px-4 rounded-0" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
    <footer class="bg-mineral-green" data-aos="fade-down">
        <div id="panneFooter" runat="server"></div>
    </footer>
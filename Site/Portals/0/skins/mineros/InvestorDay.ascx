<%@ Control Language="c#" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>
<%@ Register TagPrefix="dnn" TagName="USER" Src="~/Admin/Skins/user.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGIN" Src="~/Admin/Skins/Login.ascx" %>
<%@ Register TagPrefix="dnn" TagName="STYLES" Src="~/Admin/Skins/Styles.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LINKTOMOBILE" Src="~/Admin/Skins/LinkToMobileSite.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LANGUAGE" Src="~/Admin/Skins/Language.ascx" %>
<%@ Register TagPrefix="dnn" TagName="SEARCH" Src="~/Admin/Skins/Search.ascx" %>
<%@ Register TagPrefix="dnn" TagName="META" Src="~/Admin/Skins/Meta.ascx" %>
<%@ Register TagPrefix="dnn" TagName="jQuery" Src="~/Admin/Skins/jQuery.ascx" %>
<%@ Register TagPrefix="dnn" TagName="MENU" Src="~/DesktopModules/DDRMenu/Menu.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGO" Src="~/Admin/Skins/Logo.ascx" %>
<dnn:META ID="META1" runat="server" Name="viewport" Content="width=device-width,initial-scale=1, maximum-scale=1" />
<dnn:META runat="server" Name="description" Content="Mineros" />
<!-- Css Includes -->
<dnn:DnnCssInclude runat="server" ID="cssBootstrap" FilePath="css/bootstrap.min.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssInternaInforFinanciera" FilePath="css/informacion-financiera.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssAos" FilePath="css/aos.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssHover" FilePath="css/hover-min.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssFonts" FilePath="css/fonts.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssColors" FilePath="css/colors.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssLighSlider" FilePath="css/lightSilder.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssClassFonts" FilePath="css/class-fonts.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssStyles" FilePath="css/styles.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssBanner" FilePath="css/style-banner.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssSidebar" FilePath="css/sidebar.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssFontawesome" FilePath="css/fonts/fontawesome/css/all.css" PathNameAlias="SkinPath" />
<!-- JS Includes -->
<dnn:DnnJsInclude runat="server" ID="jsPopper" FilePath="js/popper.min.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsBootstrap" FilePath="js/bootstrap.min.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsAos" FilePath="js/aos.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsEcharts" FilePath="libs/incubator-echarts-4.2.1/js/echarts.min.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsCustom" FilePath="js/custom.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" FilePath="~/DesktopModules/Mineros/scripts/app.js" Priority="10" />
<dnn:DnnJsInclude runat="server" FilePath="~/DesktopModules/Mineros/scripts/echarts.common.min.js" Priority="10" />
<dnn:DnnCssInclude runat="server" FilePath="~/Resources/Libraries/kendo-ui-angular/css/kendo.common-material.min.css" />
<dnn:DnnCssInclude runat="server" FilePath="~/Resources/Libraries/kendo-ui-angular/css/kendo.material.min.css" />
<dnn:DnnJsInclude runat="server" ID="jsSlider" FilePath="js/lightslider.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" FilePath="~/Resources/Libraries/AngujarJS/01_04_05/angular.min.js" Priority="10" />
<dnn:DnnJsInclude runat="server" FilePath="~/Resources/Libraries/AngujarJS/01_04_05/angular-sanitize.min.js" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
<dnn:DnnJsInclude runat="server" FilePath="https://code.angularjs.org/1.6.9/angular-animate.min.js" Priority="10" />
<dnn:DnnJsInclude runat="server" ID="jsFiliales" FilePath="js/filiales_slider.js" PathNameAlias="SkinPath" />

<style>
.edsSizeShifter.NewsOne .rs-pagination li p{
    display:none;
}
</style>
<body class="informacion-financiera">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5ZKGB6Z" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <div id="hed-top-green" class="bg-mineral-green d-flex">
<!--         <ul id="bar-lang" class="nav justify-content-around" data-aos="fade-left">
            <li class="nav-item">
                <a class="nav-link librefranklin-thin text-white" href="/en-us" data-toggle="modal" data-target="#inconstruction">English</a>
                <span></span>
            </li>
            <li class="nav-item">
                <a class="nav-link librefranklin-thin text-white active" href="/es-es">Español</a>
                <span></span>
            </li>
        </ul> -->
        <div id="bar-lang" class="nav justify-content-around" data-aos="fade-left">
            <dnn:LANGUAGE runat="server" ID="dnnLANGUAGE" ShowLinks="True" ShowMenu="False" ItemTemplate=''/>
        </div>           
        <ul id="hed-top-band" class="nav justify-content-end" data-aos="fade-left">
            <li class="nav-item">
                <a class="nav-link" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/argentina" target="_blank">
                    <span class="box-img-loc hvr-icon-bob"><img src="/Portals/0/skins/mineros/images/2x/argentina.png" srcset="/Portals/0/skins/mineros/images/svg/argentina.svg" alt="Bandera de Argentina" class="hvr-icon"></span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/Chile" target="_blank">
                    <span class="box-img-loc hvr-icon-bob"><img src="/Portals/0/ban-4-1.png" class="hvr-icon" alt="Bandera de Chile"></span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/Colombia" target="_blank">
                    <span class="box-img-loc hvr-icon-bob"><img src="/Portals/0/skins/mineros/images/2x/colombia.png" srcset="/Portals/0/skins/mineros/images/svg/colombia.svg" alt="Bandera de Colombia" class="hvr-icon"></span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="http://hemco.com.ni/" target="_blank">
                    <span class="box-img-loc hvr-icon-bob"><img src="/Portals/0/skins/mineros/images/2x/nicaragua.png" srcset="/Portals/0/skins/mineros/images/svg/nicaragua.svg" alt="Bandera de Nicaragua" class="hvr-icon"></span>
                </a>
            </li>
        </ul>
    </div>
    <header id="masthead" class="site-header js" role="banner" data-aos="fade-down">
        <div class="navigation-top bg-menu-trans">
            <div class="wrap d-flex  justify-content-between">
                <div id="logo-header" class="logo-header text-center position-relative">
                    <dnn:LOGO runat="server" ID="dnnLOGO" />
                    <div class="d-flex justify-content-center align-items-center line-logo">
                        <span class="d-flex"></span>
                    </div>
                </div>
                <div class="d-flex box-menu">
                    <dnn:MENU ID="MENU" MenuStyle="Menus/MainMenu" runat="server" NodeSelector="*" />
                </div>
            </div>
            <div class="d-flex mr-auto flex-wrap">
                <a class="img-margin" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/argentina" target="_blank">
                    <span class="box-img-loc hvr-icon-bob">
                        <img src="/Portals/0/skins/mineros/images/Flag_of_Argentina.png" srcset="/Portals/0/skins/mineros/images/Flag_of_Argentina.svg" alt="Bandera De Argentina" class="icon-flag hvr-icon">
                    </span>
                </a>
                <a class="img-margin" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/Chile" target="_blank">
                    <span class="box-img-loc hvr-icon-bob">
                        <img src="/Portals/0/skins/mineros/images/2x/ban-4-1.png" srcset="/Portals/0/skins/mineros/images/SVG/ban-4-1.svg" alt="Bandera de Chile" class="icon-flag hvr-icon">
                    </span>
                </a>
                <a class="img-margin" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/Colombia" target="_blank">
                    <span class="box-img-loc hvr-icon-bob">
                        <img src="/Portals/0/skins/mineros/images/Flag-of-colombia.png" srcset="/Portals/0/skins/mineros/images/flag_of_colombia.svg" alt="Bandera de Colombia" class="icon-flag hvr-icon">
                    </span>
                </a>
                <a class="img-margin" href="http://hemco.com.ni/" target="_blank">
                    <span class="box-img-loc hvr-icon-bob">
                        <img src="/Portals/0/skins/mineros/images/Flag_of_Nicaragua.png" srcset="/Portals/0/skins/mineros/images/Flag_of_Nicaragua.svg" alt="Bandera de nicaragua" class="icon-flag hvr-icon">
                    </span>
                </a>
                <div class="box-icon-rrss w-100 d-flex align-items-center">
                    <a class="link-rrss text-white aos-init aos-animate" href="https://www.facebook.com/MinerosSA" data-aos="flip-down" target="_blank"><span><i class="text-white fab fa-facebook-f"></i></span></a>
                    <a class="link-rrss text-white aos-init aos-animate" href="https://twitter.com/mineros_sa" data-aos="flip-down" target="_blank"><span><i class="text-white fab fa-twitter"></i></span></a>
                    <a class="link-rrss text-white aos-init aos-animate" href="https://www.youtube.com/user/MINEROSSA" data-aos="flip-down" target="_blank"><span><i class="text-white fab fa-youtube"></i></span></a>
                    <a class="link-rrss text-white aos-init aos-animate" href="http://instagram.com/mineros_sa" data-aos="flip-down" target="_blank"><span><i class="text-white fab fa-instagram"></i></span></a>
                    <a class="link-rrss text-white aos-init aos-animate" href="https://co.linkedin.com/company/mineros-s.a." data-aos="flip-down" target="_blank"><span><i class="text-white fab fa-linkedin-in"></i></span></a>
                </div>
            </div>
        </div>
    </header>
    <section class="new_banner">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-md-6 col-lg-4 col_left bg-fern d-flex justify-content-end align-items-center">
                <img src="/portals/0/Images/NosotrosQuienesSomos/adorno-baner.png" srcset="/portals/0/Images/NosotrosQuienesSomos/adorno-baner.svg" class="adorno" alt="Image de adorno">
                <div id="panneCaja" class="caja bg-white position-relative" data-aos="fade-up" runat="server">
                </div><!--./caja-->
            </div><!--./col_left-->
            <div class="col-12 col-md-6 col-lg-8 col_right">
                <div id="panneBannerImage" runat="server"></div>
                <!--<img src="https://mineros.com.co/Portals/0/skins/mineros/images/2x/portada-inversionista-junta.jpg" alt="">-->
            </div><!--./col_right-->
            <div class="line-color"></div>
        </div><!--./row-->
    </div><!--./container-fluid-->
</section><!-- .new-banner --> 
    
    <section class="section-navigation bg-pumice" data-aos="fade-left">
        <div class="container">
            <div id="panneBreadCrumb" runat="server"></div>
        </div>
        <!--.container-->
    </section>
    <!--.navigation-->
    <section class="section-informacion" id="informacion">
        <div class="container px-sm-0">
            <div class="row m-0">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 column-content px-md-0">
                    <div class="row m-0 dividendos">
                        <div id="titleInformacion" class="col-12 px-0" runat="server"></div>
                        <!--.col-12-->
                        <div class="col-12 paragraph-content">
                            <div id="informacionParrafo1" class="paragraph color-scorpion librefranklin-regular" runat="server"></div>
                            <div id="informacionParrafo2" class="paragraph color-scorpion librefranklin-regular" runat="server"></div>
                        </div>
                        <!--.col-12-->
                    </div>
                    <div style="padding-bottom:0px!important;" class="row m-0 participacion">
                        <div id="participacionTitle" class="col-12 title px-0 d-flex" runat="server"></div>
                        <!--.title-->
                        <!--.col-12-->
                    </div>
                    <div style="margin-bottom:30px!important;" class="DnnModule DnnModule-EasyDNNnews DnnModule-402">
                        <div class="DNNContainer_noTitle">
                            <div id="dnn_ctr402_ContentPane">
                                <div id="dnn_ctr402_ModuleContent" class="DNNModuleContent ModEasyDNNnewsC">
                                    <div class="eds_news_module_402 news eds_subCollection_news eds_news_Vision eds_template_List_Article_List_Box_Documents eds_templateGroup_listArticleInBox eds_styleSwitchCriteria_module-402">
                                        <div id="dnn_ctr402_ViewEasyDNNNewsMain_ctl00_pnlListArticles">
                                            <ul class="list-group pl-0 ml-0">
                                                <div id="descargaPresentacion" class="edn_402_article_list_wrapper" runat="server"></div>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div style="margin-left:-15px!important;" id="info-pp" class="col-12 section-info-pp" data-aos="fade-down">
                            <div id="panneGoberns" class="col-12" runat="server"></div>
                        </div>
                    </div>
                    <div style="padding-bottom:30px!important;padding-top:60px!important;" class="row m-0 participacion">
                    <div id="galeriaTitle" class="col-12 title px-0 d-flex" runat="server"></div>
                    <div id="dnn_participacionPane" class="col-12 px-0 m-negativo aos-init aos-animate" data-aos="fade-up-right">
                </div>
                    <!--.title-->
                    <!--.col-12-->
                </div>
                </div>
                <!--.column-content-->
                <!--.aside-->
                <div class="col-12" style="margin-top:0px;margin-left:-15px!important;">
                    <div id="ContentPane" runat="server"></div>
                </div>
            </div>
            <!--.row-->
        </div>
        <!--.container-->
    </section>
    <!--.section-informacion-->
<!-- Modal -->
<div class="modal fade page_construct" id="inconstruction" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="inconstructionLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="inconstructionLabel"><span>Pop up para</span> sección en inglés</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="card-text mb-0">En construcción....</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary px-4 rounded-0" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
    <style>
    /*info-pp*/
    .section-info-pp .title {
        font-size: 1.875rem;
        line-height: 2.25rem;
        letter-spacing: 0rem;
        padding: 28px;
        max-width: 270px;
        margin: 0;
        padding-bottom: 44px;
        padding-top: 22px;
        position: relative;
    }

    @media (max-width: 500px) {
        .section-info-pp .title {
            font-size: 1.5625rem;
            line-height: 2.25rem;
            letter-spacing: 0rem;
        }
    }

    .section-info-pp .title:after {
        content: "";
        width: 56px;
        position: absolute;
        right: -28px;
        height: 100%;
        top: 0;
        opacity: 0.77;
        z-index: 1;
    }

    .bg-DCAA00,
    .section-info-pp .title:after,
    .section-baner .content-title:after,
    .section-baner .content-title .title:after {
        background-color: #daa900;
    }

    @media (max-width: 991px) {
        .section-info-pp .title:after {
            display: none;
        }
    }

    .section-info-pp .text {
        font-size: 1.25rem;
        line-height: 1.5rem;
        letter-spacing: 0rem;
        max-width: 915px;
        padding: 15px;
        transform: translateY(-28px);
        margin-left: 10px;
        margin-bottom: calc(40px - 28px);
        z-index: 2;
        position: relative;
    }

    @media (min-width: 768px) {
        .section-info-pp .content-tag .tag {
            width: calc(50% - 11px);
        }
    }

    @media (min-width: 992px) {
        .section-info-pp .text {
            margin-left: 60px;
            padding: 52px 25px;
            padding-right: 20px;
        }
    }

    @media screen and (min-width: 1500px) {
        .section-baner .col {
            min-height: 40vh !important;
        }
    }

    .section-info-pp .content-tag .tag {
        min-height: 81px;
        padding: 5px 22px;
        box-shadow: 0 0 5px 0 #afafaf;
        margin-bottom: 22px;
        width: 100%;
    }

    .section-info-pp .content-tag .tag .icon {
        width: 28px;
        height: 28px;
        margin-right: 13px;
    }

    .section-info-pp .content-tag .tag .text-tag {
        max-width: calc(100% - 28px - 13px - 1px);
        font-size: 1rem;
        line-height: 1rem;
        letter-spacing: 0rem;
    }

    .section-info-pp .content-descargra .title-d {
        min-height: 59px;
        z-index: 1;
        position: relative;
    }

    .section-info-pp .content-descargra .title-d .content-icon {
        width: 61px;
    }

    .section-info-pp .content-descargra .title-d .content-icon .icon {
        width: 32px;
    }

    .section-info-pp .content-descargra .title-d .content-text {
        padding: 5px 30px;
        font-size: 1.25rem;
        line-height: 1.5rem;
        letter-spacing: 0rem;
    }

    .section-info-pp .content-descargra .content-link {
        padding-top: 34px;
        padding-left: 20px;
        padding-right: 10px;
        margin-bottom: 35px;
        transform: translateY(-30px);
        z-index: 0;
    }

    .section-info-pp .content-descargra .content-link .link {
        min-height: 58px;
        border-top: 0;
        border-left: 0;
        border-right: 0;
        padding: 5px;
        padding-left: 7px;
        padding-right: 23px;
        cursor: pointer;
        text-decoration: none;
    }

    .section-info-pp .content-descargra .content-link .link:last-child {
        border-bottom: 0;
    }

    .section-info-pp .content-descargra .content-link .link .icon-1 {
        width: 24px;
        margin-right: 20px;
    }

    .section-info-pp .content-descargra .content-link .link .text-link {
        font-size: 1.125rem;
        line-height: 1.3125rem;
        letter-spacing: 0rem;
        text-decoration: none;
        width: calc(100% - 24px - 20px - 16px - 1px);
    }

    .section-info-pp .content-descargra .content-link .link .icon-2 {
        width: 16px;
    }

    #dnn_panneGoberns .btn-descargar-politicas {
        font-size: 1.25rem;
        line-height: 1.5rem;
        letter-spacing: 0rem;
        min-height: 61px;
        min-width: 350px;
        padding: 5px 30px;
        text-decoration: none;
        transition: 0.5s;
    }

    #dnn_panneGoberns .btn-descargar-politicas .icon {
        margin-right: 20px;
        width: 24px;
        height: 24px;
    }

    @media (max-width: 500px) {
        #dnn_panneGoberns .btn-descargar-politicas {
            min-width: 250px;
        }
    }

    .section-baner .bg-img {
        background-size: cover;
    }
    </style>
    <footer class="bg-mineral-green" data-aos="fade-down">
        <div id="panneFooter" runat="server"></div>
    </footer>
</body>
<script>
    jQuery(document).ready( function () {
        /*jQuery('.collapse-conatiner').attr('data-aos','fade-right');
        jQuery('.panel.panel-default.collapse-conatiner').addClass('aos-init aos-animate');*/
    });
</script>
<%@ Control Language="c#" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>
<%@ Register TagPrefix="dnn" TagName="USER" Src="~/Admin/Skins/user.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGIN" Src="~/Admin/Skins/Login.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGO" Src="~/Admin/Skins/Logo.ascx" %>
<%@ Register TagPrefix="dnn" TagName="STYLES" Src="~/Admin/Skins/Styles.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LINKTOMOBILE" Src="~/Admin/Skins/LinkToMobileSite.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LANGUAGE" Src="~/Admin/Skins/Language.ascx" %>
<%@ Register TagPrefix="dnn" TagName="META" Src="~/Admin/Skins/Meta.ascx" %>
<%@ Register TagPrefix="dnn" TagName="jQuery" Src="~/Admin/Skins/jQuery.ascx" %>
<%@ Register TagPrefix="dnn" TagName="MENU" Src="~/DesktopModules/DDRMenu/Menu.ascx" %>
<dnn:META ID="META1" runat="server" Name="viewport" Content="width=device-width,initial-scale=1, maximum-scale=1" />
<dnn:META runat="server" Name="description" Content="Mineros" />
<!-- Css Includes -->
<dnn:DnnCssInclude runat="server" ID="cssBootstrap" FilePath="css/bootstrap.min.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssHover" FilePath="css/hover-min.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssAos" FilePath="css/aos.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssFonts" FilePath="css/fonts.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssColors" FilePath="css/colors.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssClassFonts" FilePath="css/class-fonts.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssStyles" FilePath="css/styles.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssStylesHome" FilePath="css/style-home.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssFontawesome" FilePath="css/fonts/fontawesome/css/all.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssCertificaciones" FilePath="css/Certificaciones.css" PathNameAlias="SkinPath" />

<dnn:DnnJsInclude runat="server" FilePath="~/DesktopModules/Mineros/scripts/app.js" Priority="10" />
<dnn:DnnJsInclude runat="server" FilePath="~/DesktopModules/Mineros/scripts/echarts.common.min.js" Priority="10" />
<dnn:DnnCssInclude runat="server" FilePath="~/Resources/Libraries/kendo-ui-angular/css/kendo.common-material.min.css" />
<dnn:DnnCssInclude runat="server" FilePath="~/Resources/Libraries/kendo-ui-angular/css/kendo.material.min.css" />
<dnn:DnnJsInclude runat="server" FilePath="~/Resources/Libraries/AngujarJS/01_04_05/angular.min.js" Priority="10" />
<dnn:DnnJsInclude runat="server" FilePath="~/Resources/Libraries/AngujarJS/01_04_05/angular-sanitize.min.js" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
<dnn:DnnJsInclude runat="server" FilePath="https://code.angularjs.org/1.6.9/angular-animate.min.js" Priority="10" />
    <div id="hed-top-green" class="bg-mineral-green d-flex">
        <ul id="bar-lang" class="nav justify-content-around" data-aos="fade-left">
            <li class="nav-item">
                <a class="nav-link librefranklin-thin text-white" href="/en">English</a>
                <span></span>
            </li>
            <li class="nav-item">
                <a class="nav-link librefranklin-thin text-white active" href="/es">Español</a>
                <span></span>
            </li>
        </ul>
        <ul id="hed-top-band" class="nav justify-content-end" data-aos="fade-left">
            <li class="nav-item">
                <a class="nav-link" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/argentina" target="_blank">
                    <span class="box-img-loc hvr-icon-bob"><img src="/Portals/0/skins/mineros/images/2x/argentina.png" srcset="/Portals/0/skins/mineros/images/svg/argentina.svg" class="hvr-icon"></span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/Colombia" target="_blank">
                    <span class="box-img-loc hvr-icon-bob"><img src="/Portals/0/skins/mineros/images/2x/colombia.png" srcset="/Portals/0/skins/mineros/images/svg/colombia.svg" class="hvr-icon"></span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="http://hemco.com.ni/" target="_blank">
                    <span class="box-img-loc hvr-icon-bob"><img src="/Portals/0/skins/mineros/images/2x/nicaragua.png" srcset="/Portals/0/skins/mineros/images/svg/nicaragua.svg" class="hvr-icon"></span>
                </a>
            </li>
        </ul>
    </div>
<header id="masthead" class="site-header js" role="banner" data-aos="fade-down">
    <div class="navigation-top bg-menu-trans">
        <div class="wrap d-flex  justify-content-between">
            <div id="logo-header" class="logo-header text-center position-relative">
                <dnn:LOGO runat="server" ID="dnnLOGO" />
                <div class="d-flex justify-content-center align-items-center line-logo">
                    <span class="d-flex"></span>
                </div>
            </div>
            <div class="d-flex box-menu">
                <dnn:MENU ID="MENU" MenuStyle="Menus/MainMenu" runat="server" NodeSelector="*" />
            </div>
        </div>
        <div class="d-flex mr-auto flex-wrap">
            <a class="img-margin" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/argentina" target="_blank">
                <span class="box-img-loc hvr-icon-bob">
                    <img src="/Portals/0/skins/mineros/images/Flag_of_Argentina.png" srcset="/Portals/0/skins/mineros/images/Flag_of_Argentina.svg" alt="Bandera De Argentina" class="icon-flag hvr-icon">
                </span>
            </a>
            <a class="img-margin" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/Chile" target="_blank">
                <span class="box-img-loc hvr-icon-bob">
                    <img src="/Portals/0/skins/mineros/images/2x/ban-4-1.png" srcset="/Portals/0/skins/mineros/images/SVG/ban-4-1.svg" alt="Bandera de Chile" class="icon-flag hvr-icon">
                </span>
            </a>
            <a class="img-margin" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/Colombia" target="_blank">
                <span class="box-img-loc hvr-icon-bob">
                    <img src="/Portals/0/skins/mineros/images/Flag-of-colombia.png" srcset="/Portals/0/skins/mineros/images/flag_of_colombia.svg" alt="Bandera de Colombia" class="icon-flag hvr-icon">
                </span>
            </a>
            <a class="img-margin" href="http://hemco.com.ni/" target="_blank">
                <span class="box-img-loc hvr-icon-bob">
                    <img src="/Portals/0/skins/mineros/images/Flag_of_Nicaragua.png" srcset="/Portals/0/skins/mineros/images/Flag_of_Nicaragua.svg" alt="Bandera de nicaragua" class="icon-flag hvr-icon">
                </span>
            </a>
            <div class="box-icon-rrss w-100 d-flex align-items-center">
                <a class="link-rrss text-white aos-init aos-animate" href="https://www.facebook.com/MinerosSA" data-aos="flip-down" target="_blank"><span><i class="text-white fab fa-facebook-f"></i></span></a>
                <a class="link-rrss text-white aos-init aos-animate" href="https://twitter.com/mineros_sa" data-aos="flip-down" target="_blank"><span><i class="text-white fab fa-twitter"></i></span></a>
                <a class="link-rrss text-white aos-init aos-animate" href="https://www.youtube.com/user/MINEROSSA" data-aos="flip-down" target="_blank"><span><i class="text-white fab fa-youtube"></i></span></a>
                <a class="link-rrss text-white aos-init aos-animate" href="http://instagram.com/mineros_sa" data-aos="flip-down" target="_blank"><span><i class="text-white fab fa-instagram"></i></span></a>
                <a class="link-rrss text-white aos-init aos-animate" href="https://co.linkedin.com/company/mineros-s.a." data-aos="flip-down" target="_blank"><span><i class="text-white fab fa-linkedin-in"></i></span></a>
            </div>
        </div>
    </div>
</header>
<style>
    html,
    body {
        overflow-x: hidden;
    }

    /*---------------------------------------------------------
        section navigation
        ---------------------------------------------------------*/

    .section-navigation .breadcrumb-item a {
        text-decoration: none;
        font-size: 1rem;
        line-height: 19.2px;
        letter-spacing: 0rem;
    }

    .section-navigation .breadcrumb-item.active {
        text-decoration: none;
        font-size: 1rem;
        line-height: 19.2px;
        letter-spacing: 0rem;
    }

    @media screen and (max-width: 575px) {
        .section-navigation .breadcrumb {
            flex-wrap: nowrap;
            align-items: center;
        }

        .section-navigation .breadcrumb-item.active {
            display: flex;
            align-items: center;
        }
    }

    .section-baner .col {
        min-height: 355px;
    }

    li.list-group-item.rounded-0 {
        border: 0;
    }

    /* Structure modification */
    @media (min-width: 576px) {
        .container.content_AS {
            max-width: 540px;
        }
    }

    @media (min-width: 768px) {
        .container.content_AS {
            max-width: 720px;
        }
    }

    @media (min-width: 992px) {
        .container.content_AS {
            max-width: 960px;
        }

            .container.content_AS > .row {
                flex-wrap: nowrap;
                max-width: 100%;
                width: 100%;
            }
    }

    @media (min-width: 1200px) {
        .container.content_AS {
            max-width: 1150px;
        }

        .content_AS .content_S {
            flex-basis: 860px;
            padding-left: 0;
            padding-right: 25px;
            width: 100%;
            max-width: 860px;
        }

        .content_AS .content_A {
            flex-basis: 275px;
        }
    }

    .content_S .content_info {
        margin-bottom: 0;
        padding: 0;
        border: 0;
        z-index: 9;
    }

    .container.content_AS > .row {
        max-width: 100%;
        width: 100%;
    }

    li.list-group-item.rounded-0 {
        padding-left: 0;
    }

    .content_S .card-body {
        padding-bottom: 10px;
        padding-top: 30px;
        padding-right: 42px;
    }

    .content_AS .content_S .content_info {
        padding-top: 52px;
    }

        .content_AS .content_S .content_info.text_2 {
            padding-bottom: 45px;
            margin-bottom: 62px;
        }

        .content_AS .content_S .content_info.text_g .text {
            margin-bottom: 15px;
        }

        .content_AS .content_S .content_info .descarga .icon {
            width: 42px;
            height: 31px;
        }

        .content_AS .content_S .content_info.border {
            padding: 15px 60px 15px 32px;
            margin-bottom: 30px;
        }

    /*----------------- ASIDE -------------------------------*/
    .content_A .aside .indicadores .indicadores-ico {
        min-width: 48px;
        width: 56px;
        height: 42px;
    }

        .content_A .aside .indicadores .indicadores-ico img {
            width: 37.385px;
            height: 27px;
        }

    .content_A .aside .indicadores .h4.title {
        border-bottom-width: 2px;
        border-top: 0;
        border-left: 0;
        border-right: 0;
        font-size: 1.125rem;
        line-height: 2.25rem;
        letter-spacing: 0rem;
        padding-left: 18px;
    }

    .content_A .aside .indicadores .card {
        margin-top: 32px;
    }

    .content_A .aside .indicadores .h4.card-title {
        font-size: 1.136875rem;
        line-height: 1.136875rem;
        letter-spacing: 0rem;
        margin-bottom: 2px;
    }

    .content_A .aside .indicadores .card-header .card-text {
        font-size: 2rem;
        line-height: 2.4rem;
        letter-spacing: 0rem;
    }

    .content_A .aside .indicadores .card-header .moneda {
        font-size: 1.0625rem;
        line-height: 1.275rem;
        letter-spacing: 0rem;
    }

    .content_A .aside .indicadores .card-header::after {
        content: "";
        position: absolute;
        border: 0.5px solid #fff;
        max-width: 192px;
        width: 192px;
        left: 20px;
        right: 0;
        bottom: 0;
    }

    .content_A .aside .indicadores .card-text {
        font-size: 0.875rem;
        line-height: 0.82rem;
        letter-spacing: 0rem;
    }

        .content_A .aside .indicadores .card-text span {
            font-size: 0.875rem;
            line-height: 1.05rem;
            letter-spacing: 0rem;
        }

    .content_A .aside .indicadores .card-body {
        padding-top: 15px;
    }

    .content_A .indicadores .card span.span-simbolo {
        font-size: 8.49rem;
        top: -10px;
        line-height: 10rem;
        right: -35px;
        font-family: "Franklin Gothic Demi";
        color: #fff;
        opacity: .10;
    }

    [class^="hvr-"] {
        margin: inherit;
        padding: inherit;
        cursor: pointer;
        /*background: #e1e1e1;*/
        text-decoration: none;
        color: #666;
        -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
    }

    .hvr-icon-wobble-horizontal {
        display: inline-block;
        vertical-align: middle;
        -webkit-transform: perspective(1px) translateZ(0);
        transform: perspective(1px) translateZ(0);
        box-shadow: 0 0 1px rgba(0, 0, 0, 0);
        -webkit-transition-duration: 0.3s;
        transition-duration: 0.3s;
    }

        .hvr-icon-wobble-horizontal .hvr-icon {
            -webkit-transform: translateZ(0);
            transform: translateZ(0);
        }

    .aside .tarjetas .trimestre {
        padding-top: 52px;
    }

        .aside .tarjetas .trimestre .card-header {
            padding-left: 29px;
            padding-top: 22px;
            padding-bottom: 8px;
        }

        .aside .tarjetas .trimestre .h4 {
            font-size: 1.875rem;
            line-height: 2.25rem;
            letter-spacing: 0rem;
        }

        .aside .tarjetas .trimestre .card-body {
            padding-top: 20px;
            padding-left: 31px;
            min-height: 220px;
        }

            .aside .tarjetas .trimestre .card-body .card-text {
                font-size: 1.11625rem;
                line-height: 1.34rem;
                letter-spacing: 0rem;
            }

            .aside .tarjetas .trimestre .card-body .image-two img {
                max-width: 100%;
                position: absolute;
                right: -65px;
                bottom: -33px;
            }

    .aside .tarjetas .boletines {
        padding-top: 66px;
    }

        .aside .tarjetas .boletines .card-body {
            background-image: url(/Portals/0/skins/mineros/images/boletin-img.jpg);
            background-repeat: no-repeat;
            background-size: cover;
            min-height: 356px;
            padding-top: 48px;
            padding-right: 14px;
            position: relative;
        }

            .aside .tarjetas .boletines .card-body::after {
                content: "";
                position: absolute;
                background-color: #22a571;
                width: 100%;
                height: 100%;
                top: 0;
                left: 0;
                opacity: .6;
            }

            .aside .tarjetas .boletines .card-body p.card-text {
                font-size: 1.125rem;
                line-height: 1.35rem;
                letter-spacing: 0rem;
                max-width: 146px;
                z-index: 9;
                position: relative;
                padding-bottom: 6px;
            }

            .aside .tarjetas .boletines .card-body .card-title {
                font-size: 1.875rem;
                line-height: 1.9rem;
                letter-spacing: 0rem;
                max-width: 143px;
                z-index: 9;
                position: relative;
            }

        .aside .tarjetas .boletines .card-footer {
            padding-top: 6px;
            padding-bottom: 5px;
        }

            .aside .tarjetas .boletines .card-footer .btn {
                font-size: 1.125rem;
                line-height: 1.35rem;
                letter-spacing: 0rem;
            }

                .aside .tarjetas .boletines .card-footer .btn span {
                    margin-left: 25px;
                }

    @media (max-width: 991.8px) {
        .aside .tarjetas .trimestre .card-body {
            min-height: 282px;
        }

        .aside .tarjetas .boletines {
            padding-bottom: 52px;
            padding-top: 0;
        }
    }

    @media (max-width: 575.8px) {
        .content_AS .content_S {
            padding-left: 0;
            padding-right: 0;
        }

        .content_AS .content_A {
            padding-left: 0;
            padding-right: 0;
        }

        .content_AS {
            padding-left: 10%;
            padding-right: 10%;
        }

            .content_AS .content_A {
                max-width: 80%;
                margin: auto;
            }
    }

    #SliderResponsive {
        margin: 0 auto;
        padding-top: 15px;
    }

    .presentacion .box-icon {
        min-height: 59px;
        min-width: 62px;
    }

    .presentacion .h4 {
        min-height: 59px;
        padding: 14px 18px;
        font-size: 1.25rem;
        line-height: 1.5rem;
        letter-spacing: 0rem;
    }

    .section-admin .presentacion .title {
        max-width: 368px;
        width: 100%;
        z-index: 9;
    }

    .column-content .presentacion .title {
        max-width: 368px;
        width: 100%;
        z-index: 9;
    }

    .presentacion, .section-informacion .presentacion .Normal {
        position: relative;
        z-index: 1;
        display: flex;
    }

    .presentacion {
        padding-top: 30px;
        padding-left: 30px;
    }


        .presentacion .Normal {
            position: relative;
            z-index: 1;
            display: flex;
        }
</style>
<section class="overflowX-hidden">


    <main>
        
        <section class="page section-admin">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9 column-content px-md-0">
                        <div class="row m-0 presentacion" style="margin-left:65px !important;">
                            <div class="col-12">
                                  <div class="col-12 title px-0 d-flex aos-init aos-animate" data-aos="fade-down">
                                        <div id="panneIconTitle" runat="server"></div>
                                  </div>
                                  <div class="col-12 px-0">
                                    <!--.collapse-conatiner-->
                                    <!--.collapse-conatiner-->
                                  </div>
                                    <!--.title-->
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="col-12">
                       <div id="ContentPane" runat="server"></div>
                    </div>
                </div>
                   
            </div>
        </section>
    </main>
    <footer class="bg-mineral-green" data-aos="fade-down">
        <dnn:MENU ID="MenuFooter" MenuStyle="Menus/FooterMenu" runat="server" NodeSelector="*" />
    </footer>
    
</section>
<!-- JS Includes -->
<dnn:DnnCssInclude runat="server" FilePath="~/Resources/Libraries/kendo-ui-angular/css/kendo.common-material.min.css" />
<dnn:DnnCssInclude runat="server" FilePath="~/Resources/Libraries/kendo-ui-angular/css/kendo.material.min.css" />
<dnn:DnnJsInclude runat="server" ID="jsPopper" FilePath="js/popper.min.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsBootstrap" FilePath="js/bootstrap.min.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsAos" FilePath="js/aos.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsEcharts" FilePath="libs/incubator-echarts-4.2.1/js/echarts.min.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsMain" FilePath="js/main.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsAppHomee" FilePath="js/app-home.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" FilePath="~/Resources/Libraries/kendo-ui-angular/js/kendo.all.min.js" Priority="10" />
<dnn:DnnJsInclude runat="server" FilePath="~/Resources/Libraries/kendo-ui-angular/js/kendo.aspnetmvc.min.js" Priority="10" />
<dnn:DnnJsInclude runat="server" FilePath="~/Resources/Libraries/angularAutocomplete/multiple-select.min.js" Priority="10" />

<%@ Control Language="c#" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>
<%@ Register TagPrefix="dnn" TagName="USER" Src="~/Admin/Skins/user.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGIN" Src="~/Admin/Skins/Login.ascx" %>
<%@ Register TagPrefix="dnn" TagName="STYLES" Src="~/Admin/Skins/Styles.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LINKTOMOBILE" Src="~/Admin/Skins/LinkToMobileSite.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LANGUAGE" Src="~/Admin/Skins/Language.ascx" %>
<%@ Register TagPrefix="dnn" TagName="SEARCH" Src="~/Admin/Skins/Search.ascx" %>
<%@ Register TagPrefix="dnn" TagName="META" Src="~/Admin/Skins/Meta.ascx" %>
<%@ Register TagPrefix="dnn" TagName="jQuery" Src="~/Admin/Skins/jQuery.ascx" %>
<%@ Register TagPrefix="dnn" TagName="MENU" Src="~/DesktopModules/DDRMenu/Menu.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGO" Src="~/Admin/Skins/Logo.ascx" %>
<dnn:META ID="META1" runat="server" Name="viewport" Content="width=device-width,initial-scale=1, maximum-scale=1" />
<dnn:META runat="server" Name="description" Content="Mineros" />
<!-- Css Includes -->
<dnn:DnnCssInclude runat="server" ID="cssBootstrap" FilePath="css/bootstrap.min.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssAos" FilePath="css/aos.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssHover" FilePath="css/hover-min.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssLighSlider" FilePath="css/lightSilder.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssFonts" FilePath="css/fonts.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssClassFonts" FilePath="css/class-fonts.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssColors" FilePath="css/colors.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssStyles" FilePath="css/styles.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssStyleResponsabilidadSocial" FilePath="css/style-ResponsabilidadSocial.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssSidebar" FilePath="css/sidebar.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssBanner" FilePath="css/style-banner.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssFontawesome" FilePath="css/fonts/fontawesome/css/all.css" PathNameAlias="SkinPath" />
<!-- JS Includes -->
<dnn:DnnJsInclude runat="server" ID="jsPopper" FilePath="js/popper.min.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsBootstrap" FilePath="js/bootstrap.min.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsSlider" FilePath="js/lightslider.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsAos" FilePath="js/aos.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsCustom" FilePath="js/custom.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsFiliales" FilePath="js/filiales_slider.js" PathNameAlias="SkinPath" />

<body>
    <div id="hed-top-green" class="bg-mineral-green d-flex">
<!--         <ul id="bar-lang" class="nav justify-content-around" data-aos="fade-left">
            <li class="nav-item">
                <a class="nav-link librefranklin-thin text-white" href="/en-us" data-toggle="modal" data-target="#inconstruction">English</a>
                <span></span>
            </li>
            <li class="nav-item">
                <a class="nav-link librefranklin-thin text-white active" href="/es-es">Español</a>
                <span></span>
            </li>
        </ul> -->
        <div id="bar-lang" class="nav justify-content-around" data-aos="fade-left">
            <dnn:LANGUAGE runat="server" ID="dnnLANGUAGE" ShowLinks="True" ShowMenu="False" ItemTemplate=''/>
        </div>        
        <ul id="hed-top-band" class="nav justify-content-end" data-aos="fade-left">
            <li class="nav-item">
                <a class="nav-link" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/argentina" target="_blank">
                    <span class="box-img-loc hvr-icon-bob"><img src="/Portals/0/skins/mineros/images/2x/argentina.png" srcset="/Portals/0/skins/mineros/images/svg/argentina.svg" alt="Bandera de Argentina" class="hvr-icon"></span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/Chile" target="_blank">
                    <span class="box-img-loc hvr-icon-bob"><img src="/Portals/0/ban-4-1.png"  class="hvr-icon" alt="Bandera de Chile"></span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/Colombia" target="_blank">
                    <span class="box-img-loc hvr-icon-bob"><img src="/Portals/0/skins/mineros/images/2x/colombia.png" srcset="/Portals/0/skins/mineros/images/svg/colombia.svg" alt="Bandera de Colombia" class="hvr-icon"></span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="http://hemco.com.ni/" target="_blank">
                    <span class="box-img-loc hvr-icon-bob"><img src="/Portals/0/skins/mineros/images/2x/nicaragua.png" srcset="/Portals/0/skins/mineros/images/svg/nicaragua.svg" alt="Bandera de Nicaragua" class="hvr-icon"></span>
                </a>
            </li>
        </ul>
    </div>
    <header id="masthead" class="site-header js" role="banner" data-aos="fade-down">
        <div class="navigation-top bg-menu-trans">
            <div class="wrap d-flex  justify-content-between">
                <div id="logo-header" class="logo-header text-center position-relative">
                    <dnn:LOGO runat="server" ID="dnnLOGO" />
                    <div class="d-flex justify-content-center align-items-center line-logo">
                        <span class="d-flex"></span>
                    </div>
                </div>
                <div class="d-flex box-menu">
                    <dnn:MENU ID="MENU" MenuStyle="Menus/MainMenu" runat="server" NodeSelector="*" />
                </div>
            </div>
            <div class="d-flex mr-auto flex-wrap">
                <a class="img-margin" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/argentina" target="_blank">
                    <span class="box-img-loc hvr-icon-bob">
                        <img src="/Portals/0/skins/mineros/images/Flag_of_Argentina.png" srcset="/Portals/0/skins/mineros/images/Flag_of_Argentina.svg" alt="Bandera De Argentina" class="icon-flag hvr-icon">
                    </span>
                </a>
                <a class="img-margin" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/Chile" target="_blank">
                    <span class="box-img-loc hvr-icon-bob">
                        <img src="/Portals/0/skins/mineros/images/2x/ban-4-1.png" srcset="/Portals/0/skins/mineros/images/SVG/ban-4-1.svg" alt="Bandera de Chile" class="icon-flag hvr-icon">
                    </span>
                </a>
                <a class="img-margin" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/Colombia" target="_blank">
                    <span class="box-img-loc hvr-icon-bob">
                        <img src="/Portals/0/skins/mineros/images/Flag-of-colombia.png" srcset="/Portals/0/skins/mineros/images/flag_of_colombia.svg" alt="Bandera de Colombia" class="icon-flag hvr-icon">
                    </span>
                </a>
                <a class="img-margin" href="http://hemco.com.ni/" target="_blank">
                    <span class="box-img-loc hvr-icon-bob">
                        <img src="/Portals/0/skins/mineros/images/Flag_of_Nicaragua.png" srcset="/Portals/0/skins/mineros/images/Flag_of_Nicaragua.svg" alt="Bandera de nicaragua" class="icon-flag hvr-icon">
                    </span>
                </a>
                <div class="box-icon-rrss w-100 d-flex align-items-center">
                    <a class="link-rrss text-white aos-init aos-animate" href="https://www.facebook.com/MinerosSA" data-aos="flip-down" target="_blank"><span><i class="text-white fab fa-facebook-f"></i></span></a>
                    <a class="link-rrss text-white aos-init aos-animate" href="https://twitter.com/mineros_sa" data-aos="flip-down" target="_blank"><span><i class="text-white fab fa-twitter"></i></span></a>
                    <a class="link-rrss text-white aos-init aos-animate" href="https://www.youtube.com/user/MINEROSSA" data-aos="flip-down" target="_blank"><span><i class="text-white fab fa-youtube"></i></span></a>
                    <a class="link-rrss text-white aos-init aos-animate" href="http://instagram.com/mineros_sa" data-aos="flip-down" target="_blank"><span><i class="text-white fab fa-instagram"></i></span></a>
                    <a class="link-rrss text-white aos-init aos-animate" href="https://co.linkedin.com/company/mineros-s.a." data-aos="flip-down" target="_blank"><span><i class="text-white fab fa-linkedin-in"></i></span></a>
                </div>
            </div>
        </div>
    </header>
    <style>
    html,
    body {
        overflow-x: hidden;
        font-size: 16px;
    }

    .bg-5DB65F {
        background-color: #5DB65F;
    }

    .bg-DCAA00,
    .section-info .header:after,
    .section-baner .content-title:after,
    .section-baner .content-title .title:after {
        background-color: #daa900;
        background-color: var(--main-color-corn);
    }

    .bg-C1C3C2 {
        background-color: #c1c3c2;
        background-color: var(--main-color-pumice);
    }

   

    .container.content_AS {
        width: 100%;
        padding-right: 15px;
        padding-left: 15px;
        margin-right: auto;
        margin-left: auto;
    }

    @media (min-width: 576px) {
        .container.content_AS {
            max-width: 540px;
        }
    }

    @media (min-width: 768px) {
        .container.content_AS {
            max-width: 720px;
        }
    }

    @media (min-width: 992px) {
        .container.content_AS {
            max-width: 960px;
        }
    }

    @media (min-width: 1200px) {
        .row.tarjetas {
            max-width: 270px;
        }

        .container.content_AS {
            max-width: 1140px;
        }
    }

    @media screen and (min-width: 1500px) {
        .section-baner .col {
            min-height: 40vh;
        }
    }

    @media screen and (max-width: 991.8px) {
        .content_AS .content_S {
            padding-left: 15px;
            padding-right: 15px;
        }

        .content_AS .content_A {
            padding-left: 15px;
            padding-right: 15px;
        }

        .aside .tarjetas .boletines {
            padding-top: 0;
        }

        .section-baner .content-title {
            min-height: 205px;
        }
    }

    @media screen and (max-width: 575.8px) {
        .content_AS .content_S {
            padding-left: 10%;
            padding-right: 10%;
        }

        .content_AS .content_A {
            padding-left: 10%;
            padding-right: 10%;
        }

        .aside .tarjetas .boletines {
            padding-top: 0;
        }

        .section-baner .col.bg-img {
            min-height: auto;
            padding: 0;
            padding-top: 56.25%;
        }
    }

    @media screen and (max-width: 375px) {
        #section-6 .tittle-section-6 #text-section-6 {
            font-size: 1rem;
        }
    }

    .section-baner .content-title .title {
        margin-bottom: 0;
    }

    section.content {
        padding-bottom: 95px;
    }

    #section-6 .section-6-tittle-box .tittle-section-6 {
        max-width: 350px;
        width: 100%;
    }

    .breadcrumb {
        align-items: center;
        margin-bottom: 0;
        width: 100%;
        margin: 0;
        padding-left: 0;
        padding-right: 0;
    }

    .content_AS>row {
        width: 100%;
    }

    picture img {
        left: 0;
    }
    </style>
        <section class="new_banner">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-md-6 col-lg-4 col_left bg-fern d-flex justify-content-end align-items-center">
                <img src="/portals/0/Images/NosotrosQuienesSomos/adorno-baner.png" srcset="/portals/0/Images/NosotrosQuienesSomos/adorno-baner.svg" class="adorno" alt="Image de adorno">
                <div id="panneCaja" class="caja bg-white position-relative" data-aos="fade-up" runat="server">
                </div><!--./caja-->
            </div><!--./col_left-->
            <div class="col-12 col-md-6 col-lg-8 col_right">
                <div id="panneBannerImage" runat="server"></div>
                <!--<img src="https://mineros.com.co/Portals/0/skins/mineros/images/2x/portada-inversionista-junta.jpg" alt="">-->
            </div><!--./col_right-->
            <div class="line-color"></div>
        </div><!--./row-->
    </div><!--./container-fluid-->
</section><!-- .new-banner --> 
    
    
    <section class="section-navigation bg-pumice" data-aos="fade-left">
        <div class="container">
            <div id="panneBreadCrumb" runat="server"></div>
        </div>
        <!--.container-->
    </section><!-- /.section-navigation-->
    <div class="separador" style="height: 94px;" data-aos="fade-right"></div>
    <section id="section3-content_AS" class="section3-content_AS">
        <div class="container content_AS">
            <div class="row w-100">
                <div class="col-12 col-lg-8 col-md-12 column__content content_S">
                    <div id="panneSection1" class="" runat="server"></div>
                </div>
                <div class="aside col-12 col-lg-4 col-md-12 content_A mt-0">
                    <div class="row m-0 tarjetas">
                        <div class="col-12 col-sm-6 col-lg-12 pr-0 trimestre pt-0">
                            <div id="ResultadosFinancieros" class="card rounded-0 border-0" data-aos="fade-right" runat="server"></div>
                            <!--.card-->
                        </div>
                        <!--.col-12-->
                        <div class="col-12 col-sm-6 col-lg-12 pr-0 boletines">
                            <div id="dnn_BoletinesDePrensa" class="card rounded-0 border-0" data-aos="fade-right" runat="server"></div>
                            <!--.card-->
                        </div>
                        <!--.col-12-->
                    </div>
                    <!--.tarjetas-->
                </div>
                <!--/.aside-->
            </div>
            <!--/.row-->
        </div>
        <!--./container-->
    </section>
    <div id="ContentPane" runat="server"></div>
    <!-- Modal -->
<div class="modal fade page_construct" id="inconstruction" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="inconstructionLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="inconstructionLabel"><span>Pop up para</span> sección en inglés</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="card-text mb-0">En construcción....</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary px-4 rounded-0" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
    <footer class="bg-mineral-green" data-aos="fade-down">
        <div id="panneFooter" runat="server"></div>
    </footer>
</body>

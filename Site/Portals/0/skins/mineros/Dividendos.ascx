<%@ Control Language="c#" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>
<%@ Register TagPrefix="dnn" TagName="USER" Src="~/Admin/Skins/user.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGIN" Src="~/Admin/Skins/Login.ascx" %>
<%@ Register TagPrefix="dnn" TagName="STYLES" Src="~/Admin/Skins/Styles.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LINKTOMOBILE" Src="~/Admin/Skins/LinkToMobileSite.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LANGUAGE" Src="~/Admin/Skins/Language.ascx" %>
<%@ Register TagPrefix="dnn" TagName="SEARCH" Src="~/Admin/Skins/Search.ascx" %>
<%@ Register TagPrefix="dnn" TagName="META" Src="~/Admin/Skins/Meta.ascx" %>
<%@ Register TagPrefix="dnn" TagName="jQuery" Src="~/Admin/Skins/jQuery.ascx" %>
<%@ Register TagPrefix="dnn" TagName="MENU" Src="~/DesktopModules/DDRMenu/Menu.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGO" Src="~/Admin/Skins/Logo.ascx" %>
<dnn:META ID="META1" runat="server" Name="viewport" Content="width=device-width,initial-scale=1, maximum-scale=1" />
<dnn:META runat="server" Name="description" Content="Mineros" />
<!-- Css Includes -->
<dnn:DnnCssInclude runat="server" ID="cssBootstrap" FilePath="css/bootstrap.min.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssInternaBoletines" FilePath="css/InternaBoletines.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssFonts" FilePath="css/fonts.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssclassFonts" FilePath="css/class-fonts.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssLighSlider" FilePath="css/lightSilder.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssColors" FilePath="css/colors.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssStyles" FilePath="css/styles.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssAos" FilePath="css/aos.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssHover" FilePath="css/hover-min.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssGobierno" FilePath="css/style-nosotros-gobierno.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssInfo" FilePath="css/informacion-financiera.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssBanner" FilePath="css/style-banner.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssFontawesome" FilePath="css/fonts/fontawesome/css/all.css" PathNameAlias="SkinPath" />

<body class="nosotros_gobierno">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5ZKGB6Z" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <div id="hed-top-green" class="bg-mineral-green d-flex">
        <!--         <ul id="bar-lang" class="nav justify-content-around" data-aos="fade-left">
            <li class="nav-item">
                <a class="nav-link librefranklin-thin text-white" href="/en-us" data-toggle="modal" data-target="#inconstruction">English</a>
                <span></span>
            </li>
            <li class="nav-item">
                <a class="nav-link librefranklin-thin text-white active" href="/es-es">Español</a>
                <span></span>
            </li>
        </ul> -->
        <div id="bar-lang" class="nav justify-content-around" data-aos="fade-left">
            <dnn:LANGUAGE runat="server" ID="dnnLANGUAGE" ShowLinks="True" ShowMenu="False" ItemTemplate='' />
        </div>
        <ul id="hed-top-band" class="nav justify-content-end" data-aos="fade-left">
            <li class="nav-item">
                <a class="nav-link" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/argentina" target="_blank">
                    <span class="box-img-loc hvr-icon-bob"><img src="/Portals/0/skins/mineros/images/2x/argentina.png" srcset="/Portals/0/skins/mineros/images/svg/argentina.svg" alt="Bandera de Argentina" class="hvr-icon"></span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/Chile" target="_blank">
                    <span class="box-img-loc hvr-icon-bob"><img src="/Portals/0/ban-4-1.png" class="hvr-icon" alt="Bandera de Chile"></span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/Colombia" target="_blank">
                    <span class="box-img-loc hvr-icon-bob"><img src="/Portals/0/skins/mineros/images/2x/colombia.png" srcset="/Portals/0/skins/mineros/images/svg/colombia.svg" alt="Bandera de Colombia" class="hvr-icon"></span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="http://hemco.com.ni/" target="_blank">
                    <span class="box-img-loc hvr-icon-bob"><img src="/Portals/0/skins/mineros/images/2x/nicaragua.png" srcset="/Portals/0/skins/mineros/images/svg/nicaragua.svg" alt="Bandera de Nicaragua" class="hvr-icon"></span>
                </a>
            </li>
        </ul>
    </div>
    <!--./bg-mineral-green-->
    <header id="masthead" class="site-header js" role="banner" data-aos="fade-down">
        <div class="navigation-top bg-menu-trans">
            <div class="wrap d-flex  justify-content-between">
                <div id="logo-header" class="logo-header text-center position-relative">
                    <dnn:LOGO runat="server" ID="dnnLOGO" />
                    <div class="d-flex justify-content-center align-items-center line-logo">
                        <span class="d-flex"></span>
                    </div>
                </div>
                <div class="d-flex box-menu">
                    <dnn:MENU ID="MENU" MenuStyle="Menus/MainMenu" runat="server" NodeSelector="*" />
                </div>
            </div>
            <div class="d-flex mr-auto flex-wrap">
                <a class="img-margin" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/argentina" target="_blank">
                    <span class="box-img-loc hvr-icon-bob">
                        <img src="/Portals/0/skins/mineros/images/Flag_of_Argentina.png" srcset="/Portals/0/skins/mineros/images/Flag_of_Argentina.svg" alt="Bandera De Argentina" class="icon-flag hvr-icon">
                    </span>
                </a>
                <a class="img-margin" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/Chile" target="_blank">
                    <span class="box-img-loc hvr-icon-bob">
                        <img src="/Portals/0/skins/mineros/images/2x/ban-4-1.png" srcset="/Portals/0/skins/mineros/images/SVG/ban-4-1.svg" alt="Bandera de Chile" class="icon-flag hvr-icon">
                    </span>
                </a>
                <a class="img-margin" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/Colombia" target="_blank">
                    <span class="box-img-loc hvr-icon-bob">
                        <img src="/Portals/0/skins/mineros/images/Flag-of-colombia.png" srcset="/Portals/0/skins/mineros/images/flag_of_colombia.svg" alt="Bandera de Colombia" class="icon-flag hvr-icon">
                    </span>
                </a>
                <a class="img-margin" href="http://hemco.com.ni/" target="_blank">
                    <span class="box-img-loc hvr-icon-bob">
                        <img src="/Portals/0/skins/mineros/images/Flag_of_Nicaragua.png" srcset="/Portals/0/skins/mineros/images/Flag_of_Nicaragua.svg" alt="Bandera de nicaragua" class="icon-flag hvr-icon">
                    </span>
                </a>
                <div class="box-icon-rrss w-100 d-flex align-items-center">
                    <a class="link-rrss text-white aos-init aos-animate" href="https://www.facebook.com/MinerosSA" data-aos="flip-down" target="_blank"><span><i class="text-white fab fa-facebook-f"></i></span></a>
                    <a class="link-rrss text-white aos-init aos-animate" href="https://twitter.com/mineros_sa" data-aos="flip-down" target="_blank"><span><i class="text-white fab fa-twitter"></i></span></a>
                    <a class="link-rrss text-white aos-init aos-animate" href="https://www.youtube.com/user/MINEROSSA" data-aos="flip-down" target="_blank"><span><i class="text-white fab fa-youtube"></i></span></a>
                    <a class="link-rrss text-white aos-init aos-animate" href="http://instagram.com/mineros_sa" data-aos="flip-down" target="_blank"><span><i class="text-white fab fa-instagram"></i></span></a>
                    <a class="link-rrss text-white aos-init aos-animate" href="https://co.linkedin.com/company/mineros-s.a." data-aos="flip-down" target="_blank"><span><i class="text-white fab fa-linkedin-in"></i></span></a>
                </div>
            </div>
        </div>
    </header>
    <!--./site-header-->
    <style>
        body aside.aside .tarjetas .boletines .card-body {
            min-height: 335px;
            padding-top: 48px !important;
            padding-right: 14px !important;
            position: relative;
        }
        .section-info-pp .content-descargra .title-d .content-icon .icon,
        .section-politicas .content-info-politicas .title-d .content-icon .icon {
            height: 32px;
        }
        .section-info-pp .content-tag .tag .icon { height: 28px; }
        .section-politicas .content-info-politicas .btn-descargar-politicas .icon {
            height: 24px;
        }
        .section-etica .content-utilizar .item .icon { height: 50px; }
        aside#aside { max-width: 100%;width: 100%; }
    @media screen and (min-width: 1500px) {
        .section-baner .col {
            min-height: 40vh;
        }
    }

    @media screen and (max-width: 991.8px) {
        .section-baner .content-title {
            min-height: 205px;
        }
        .section-baner .bg-img  {
            min-height: 355px;
            padding: 0;
        }
    }

    @media screen and (max-width: 767.8px) {
        .section-baner .bg-img {
            min-height: auto;
            padding-top: 56.22%;
        }
    }
    #dnn_panneLink1 .card,
    #dnn_panneTitleDescargab .card {
            transform: translateY(-30px);    
    }
#dnn_panneTitleDescargab .card-body {
    padding-top: 30px !important;
    border: 1px solid rgb(210, 210, 210);
    padding-left: 20px !important;
}

#dnn_panneTitleDescargab .card-body .list-group-item {
    border: 0;
    padding-left: 0 !important;
    margin-left: 0 !important;
}
.btn-descargar-politicas img.icon {
width: 32px;
    height: 32px;
}
#dnn_panneLink1 .card-body {
    padding-top: 30px !important;
    padding-left: 0 !important;
}
#dnn_panneLink1 .eds_news_Ozone ul {
    margin-left: 0 !important
}
#dnn_panneLink1 .eds_news_Ozone .list-group-item {
    border: 0;
    padding-left: 7px;
}
    </style>
    <section class="new_banner">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-md-6 col-lg-4 col_left bg-fern d-flex justify-content-end align-items-center">
                    <img src="/portals/0/Images/NosotrosQuienesSomos/adorno-baner.png" srcset="/portals/0/Images/NosotrosQuienesSomos/adorno-baner.svg" class="adorno" alt="Image de adorno">
                    <div id="panneCaja" class="caja bg-white position-relative" data-aos="fade-up" runat="server">
                    </div>
                    <!--./caja-->
                </div>
                <!--./col_left-->
                <div class="col-12 col-md-6 col-lg-8 col_right">
                    <div id="panneBannerImage" runat="server"></div>
                    <!--<img src="https://mineros.com.co/Portals/0/skins/mineros/images/2x/portada-inversionista-junta.jpg" alt="">-->
                </div>
                <!--./col_right-->
                <div class="line-color"></div>
            </div>
            <!--./row-->
        </div>
        <!--./container-fluid-->
    </section><!-- .new-banner -->
    <section class="section-navigation bg-pumice" data-aos="fade-left">
        <div class="container">
            <div id="panneBreadCrumb1" runat="server"></div>
        </div>
        <!--.container-->
    </section><!-- /.section-navigation-->
    <div class="container content-section-aside">
        <div class="row">
            <div class="col-12 col-lg-9">
                <section id="info-pp" class="section-info-pp" data-aos="fade-down">
                    <div id="panneTitle" runat="server"></div>
                    <div id="panneText1" runat="server"></div>
                    <div id="panneText2" runat="server"></div>
                    <div id="panneText3" runat="server"></div>
                    <div class="content-descargra mt-3" data-aos="fade-down">
                        <div id="panneTitleDescarga" runat="server"></div>
                        <div class="">
                            <div id="panneLink1" runat="server"></div>
                            <div id="panneLink2" runat="server"></div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="col-12 col-lg-3 pr-sm-0">
                <aside id="aside" class="aside">
                    <!--.indicadores-->
                    <div class="row m-0 tarjetas">
                        <div class="col-12 col-sm-6 col-lg-12 pr-0 trimestre">
                            <div id="panneInfoFinanciera" runat="server"></div>
                        </div>
                        <!--.col-12-->
                        <div class="col-12 col-sm-6 col-lg-12 pr-0 boletines">
                            <div id="panneBoletin" runat="server"></div>
                        </div>
                        <!--.col-12-->
                    </div>
                    <!--.tarjetas-->
                    <div class="row m-0 sucribir" style="display: none;">
                        <div class="col-12 col-sm-9 col-lg-12 m-auto pr-0">
                            <div class="card rounded-0 bg-jungle-green" data-aos="fade-up-right">
                                <div class="card-body rounded-0 text-center">
                                    <div data-icon="a" class="icon" style="color: #fff;"></div>
                                    <h3 class="h4 mb-0 text-center text-white librefranklin-bold" data-aos="fade-up-left">Suscríbase aquí</h3>
                                    <p class="h4 mb-0 card-text text-white text-center librefranklin-regular" data-aos="fade-up-right">para recibir información corporativa de interés en su correo electrónico.</p>
                                    <div class="card-footer border-0 rounded-0 bg-transparent">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1" class="sr-only" data-aos="fade-right">Email address</label>
                                            <input type="email" class="form-control rounded-0" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Inscríbete" data-aos="fade-right">
                                        </div>
                                        <button type="submit" class="btn btn-link text-decoration-none librefranklin-bold text-white d-none">Submit</button>
                                    </div>
                                    <!--.card-footer-->
                                </div>
                                <!--.card-body-->
                            </div>
                            <!--.card-->
                        </div>
                        <!--.col-12-->
                    </div>
                    <!--.sucribir-->
                </aside>
            </div>
        </div>
    </div>
    <div id="contentPane" runat="server"></div>
    <!-- Modal -->
    <div class="modal fade page_construct" id="inconstruction" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="inconstructionLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="inconstructionLabel"><span>Pop up para</span> sección en inglés</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="card-text mb-0">En construcción....</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary px-4 rounded-0" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <footer class="bg-mineral-green" data-aos="fade-down">
        <div id="panneFooter" runat="server"></div>
    </footer>
    <!-- JS Includes -->
    <dnn:DnnJsInclude runat="server" ID="jsPopper" FilePath="js/popper.min.js" PathNameAlias="SkinPath" />
    <dnn:DnnJsInclude runat="server" ID="jsSlider" FilePath="js/lightslider.js" PathNameAlias="SkinPath" />
    <dnn:DnnJsInclude runat="server" ID="jsBootstrap" FilePath="js/bootstrap.min.js" PathNameAlias="SkinPath" />
    <dnn:DnnJsInclude runat="server" ID="jsAos" FilePath="js/aos.js" PathNameAlias="SkinPath" />
    <dnn:DnnJsInclude runat="server" ID="jsCustom" FilePath="js/custom.js" PathNameAlias="SkinPath" />
    <dnn:DnnJsInclude runat="server" ID="jsFiliales" FilePath="js/filiales_slider.js" PathNameAlias="SkinPath" />
    <dnn:DnnJsInclude runat="server" FilePath="~/Resources/Libraries/kendo-ui-angular/js/kendo.all.min.js" Priority="10" />
    <dnn:DnnJsInclude runat="server" FilePath="~/Resources/Libraries/kendo-ui-angular/js/kendo.aspnetmvc.min.js" Priority="10" />
    <dnn:DnnJsInclude runat="server" FilePath="~/Resources/Libraries/angularAutocomplete/multiple-select.min.js" Priority="10" />

</body>
<%@ Control Language="c#" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>
<%@ Register TagPrefix="dnn" TagName="USER" Src="~/Admin/Skins/user.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGIN" Src="~/Admin/Skins/Login.ascx" %>
<%@ Register TagPrefix="dnn" TagName="STYLES" Src="~/Admin/Skins/Styles.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LINKTOMOBILE" Src="~/Admin/Skins/LinkToMobileSite.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LANGUAGE" Src="~/Admin/Skins/Language.ascx" %>
<%@ Register TagPrefix="dnn" TagName="SEARCH" Src="~/Admin/Skins/Search.ascx" %>
<%@ Register TagPrefix="dnn" TagName="META" Src="~/Admin/Skins/Meta.ascx" %>
<%@ Register TagPrefix="dnn" TagName="jQuery" Src="~/Admin/Skins/jQuery.ascx" %>
<%@ Register TagPrefix="dnn" TagName="MENU" Src="~/DesktopModules/DDRMenu/Menu.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGO" Src="~/Admin/Skins/Logo.ascx" %>



<dnn:DnnCssInclude ID="CssBootstrap" runat="server" FilePath="css/bootstrap.css" PathNameAlias="SkinPath" />

<dnn:DnnJsInclude ID="JssPopper" runat="server" FilePath="js/popper.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude ID="JssBootstrap" runat="server" FilePath="js/bootstrap.js" PathNameAlias="SkinPath" />


<dnn:META ID="META1" runat="server" Name="viewport" Content="width=device-width,initial-scale=1, maximum-scale=1" />
<dnn:META runat="server" Name="description" Content="TicketCar" />
<section>
    <div class="container pt-5">
        <div class="row">
            <div class="col-12">
                <div id="titulo" runat="server"></div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container mt-5">
        <div class="row">
            <div class="col-12">
                <div id="titulo2" runat="server"></div>
         
            </div>
            <div class="col-6">
                <div id="column1" runat="server"></div>
                
            </div>
            <div class="col-6">
                <div id="column2" runat="server"></div>
                
            </div>           
        </div>
    </div>
</section>


<div id="ContentPane" runat="server" style=""></div>
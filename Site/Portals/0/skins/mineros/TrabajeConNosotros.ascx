<%@ Control Language="c#" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>
<%@ Register TagPrefix="dnn" TagName="USER" Src="~/Admin/Skins/user.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGIN" Src="~/Admin/Skins/Login.ascx" %>
<%@ Register TagPrefix="dnn" TagName="STYLES" Src="~/Admin/Skins/Styles.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LINKTOMOBILE" Src="~/Admin/Skins/LinkToMobileSite.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LANGUAGE" Src="~/Admin/Skins/Language.ascx" %>
<%@ Register TagPrefix="dnn" TagName="SEARCH" Src="~/Admin/Skins/Search.ascx" %>
<%@ Register TagPrefix="dnn" TagName="META" Src="~/Admin/Skins/Meta.ascx" %>
<%@ Register TagPrefix="dnn" TagName="jQuery" Src="~/Admin/Skins/jQuery.ascx" %>
<%@ Register TagPrefix="dnn" TagName="MENU" Src="~/DesktopModules/DDRMenu/Menu.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGO" Src="~/Admin/Skins/Logo.ascx" %>
<dnn:META ID="META1" runat="server" Name="viewport" Content="width=device-width,initial-scale=1, maximum-scale=1" />
<dnn:META runat="server" Name="description" Content="Mineros" />
<!-- Css Includes -->
<dnn:DnnCssInclude runat="server" ID="cssBootstrap" FilePath="css/bootstrap.min.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssAos" FilePath="css/aos.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssHover" FilePath="css/hover-min.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssFonts" FilePath="css/fonts.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssColors" FilePath="css/colors.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssClassFonts" FilePath="css/class-fonts.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssTrabaja" FilePath="css/style-trabaja-nos.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssSidebar" FilePath="css/sidebar.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssStyles" FilePath="css/styles.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssBanner" FilePath="css/style-banner.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssLighSlider" FilePath="css/lightSilder.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssFontawesome" FilePath="css/fonts/fontawesome/css/all.css" PathNameAlias="SkinPath" />
<!-- JS Includes -->
<dnn:DnnJsInclude runat="server" ID="jsPopper" FilePath="js/popper.min.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsBootstrap" FilePath="js/bootstrap.min.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsAos" FilePath="js/aos.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsEcharts" FilePath="libs/incubator-echarts-4.2.1/js/echarts.min.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsMain" FilePath="js/main.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" FilePath="~/DesktopModules/Mineros/scripts/app.js" Priority="10" />
<dnn:DnnJsInclude runat="server" FilePath="~/DesktopModules/Mineros/scripts/echarts.common.min.js" Priority="10" />
<dnn:DnnCssInclude runat="server" FilePath="~/Resources/Libraries/kendo-ui-angular/css/kendo.common-material.min.css" />
<dnn:DnnCssInclude runat="server" FilePath="~/Resources/Libraries/kendo-ui-angular/css/kendo.material.min.css" />
<dnn:DnnJsInclude runat="server" FilePath="~/Resources/Libraries/AngujarJS/01_04_05/angular.min.js" Priority="10" />
<dnn:DnnJsInclude runat="server" FilePath="~/Resources/Libraries/AngujarJS/01_04_05/angular-sanitize.min.js" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
<dnn:DnnJsInclude runat="server" FilePath="https://code.angularjs.org/1.6.9/angular-animate.min.js" Priority="10" />
<dnn:DnnJsInclude runat="server" ID="jsSlider" FilePath="js/lightslider.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsCustom" FilePath="js/custom.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsFiliales" FilePath="js/filiales_slider.js" PathNameAlias="SkinPath" />
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5ZKGB6Z" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="hed-top-green" class="bg-mineral-green d-flex">
<!--     <ul id="bar-lang" class="nav justify-content-around" data-aos="fade-left">
        <li class="nav-item">
            <a class="nav-link librefranklin-thin text-white" href="/en-us" data-toggle="modal" data-target="#inconstruction">English</a>
            <span></span>
        </li>
        <li class="nav-item">
            <a class="nav-link librefranklin-thin text-white active" href="/es-es">Español</a>
            <span></span>
        </li>
    </ul> -->
        <div id="bar-lang" class="nav justify-content-around" data-aos="fade-left">
            <dnn:LANGUAGE runat="server" ID="dnnLANGUAGE" ShowLinks="True" ShowMenu="False" ItemTemplate=''/>
        </div>    
    <ul id="hed-top-band" class="nav justify-content-end" data-aos="fade-left">
        <li class="nav-item">
            <a class="nav-link" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/argentina" target="_blank">
                <span class="box-img-loc hvr-icon-bob"><img src="/Portals/0/skins/mineros/images/2x/argentina.png" srcset="/Portals/0/skins/mineros/images/svg/argentina.svg" alt="Bandera de Argentina" class="hvr-icon"></span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/Chile" target="_blank">
                <span class="box-img-loc hvr-icon-bob"><img src="/Portals/0/ban-4-1.png" class="hvr-icon" alt="Bandera de Chile"></span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/Colombia" target="_blank">
                <span class="box-img-loc hvr-icon-bob"><img src="/Portals/0/skins/mineros/images/2x/colombia.png" srcset="/Portals/0/skins/mineros/images/svg/colombia.svg" alt="Bandera de Colombia" class="hvr-icon"></span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="http://hemco.com.ni/" target="_blank">
                <span class="box-img-loc hvr-icon-bob"><img src="/Portals/0/skins/mineros/images/2x/nicaragua.png" srcset="/Portals/0/skins/mineros/images/svg/nicaragua.svg" alt="Bandera de Nicaragua" class="hvr-icon"></span>
            </a>
        </li>
    </ul>
</div><!-- /#hed-top-green-->
<header id="masthead" class="site-header js" role="banner" data-aos="fade-down">
    <div class="navigation-top bg-menu-trans">
        <div class="wrap d-flex  justify-content-between">
            <div id="logo-header" class="logo-header text-center position-relative">
                <dnn:LOGO runat="server" ID="dnnLOGO" />
                <div class="d-flex justify-content-center align-items-center line-logo">
                    <span class="d-flex"></span>
                </div>
            </div>
            <div class="d-flex box-menu">
                <dnn:MENU ID="MENU" MenuStyle="Menus/MainMenu" runat="server" NodeSelector="*" />
            </div>
        </div>
        <div class="d-flex mr-auto flex-wrap">
            <a class="img-margin" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/argentina" target="_blank">
                <span class="box-img-loc hvr-icon-bob">
                    <img src="/Portals/0/skins/mineros/images/Flag_of_Argentina.png" srcset="/Portals/0/skins/mineros/images/Flag_of_Argentina.svg" alt="Bandera De Argentina" class="icon-flag hvr-icon">
                </span>
            </a>
            <a class="img-margin" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/Chile" target="_blank">
                <span class="box-img-loc hvr-icon-bob">
                    <img src="/Portals/0/skins/mineros/images/2x/ban-4-1.png" srcset="/Portals/0/skins/mineros/images/SVG/ban-4-1.svg" alt="Bandera de Chile" class="icon-flag hvr-icon">
                </span>
            </a>
            <a class="img-margin" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/Colombia" target="_blank">
                <span class="box-img-loc hvr-icon-bob">
                    <img src="/Portals/0/skins/mineros/images/Flag-of-colombia.png" srcset="/Portals/0/skins/mineros/images/flag_of_colombia.svg" alt="Bandera de Colombia" class="icon-flag hvr-icon">
                </span>
            </a>
            <a class="img-margin" href="http://hemco.com.ni/" target="_blank">
                <span class="box-img-loc hvr-icon-bob">
                    <img src="/Portals/0/skins/mineros/images/Flag_of_Nicaragua.png" srcset="/Portals/0/skins/mineros/images/Flag_of_Nicaragua.svg" alt="Bandera de nicaragua" class="icon-flag hvr-icon">
                </span>
            </a>
                <div class="box-icon-rrss w-100 d-flex align-items-center">
                    <a class="link-rrss text-white aos-init aos-animate" href="https://www.facebook.com/MinerosSA" data-aos="flip-down" target="_blank"><span><i class="text-white fab fa-facebook-f"></i></span></a>
                    <a class="link-rrss text-white aos-init aos-animate" href="https://twitter.com/mineros_sa" data-aos="flip-down" target="_blank"><span><i class="text-white fab fa-twitter"></i></span></a>
                    <a class="link-rrss text-white aos-init aos-animate" href="https://www.youtube.com/user/MINEROSSA" data-aos="flip-down" target="_blank"><span><i class="text-white fab fa-youtube"></i></span></a>
                    <a class="link-rrss text-white aos-init aos-animate" href="http://instagram.com/mineros_sa" data-aos="flip-down" target="_blank"><span><i class="text-white fab fa-instagram"></i></span></a>
                    <a class="link-rrss text-white aos-init aos-animate" href="https://co.linkedin.com/company/mineros-s.a." data-aos="flip-down" target="_blank"><span><i class="text-white fab fa-linkedin-in"></i></span></a>
                </div>
        </div>
    </div>
</header>
<style>
    html,
body {
    overflow-x: hidden;
    font-size: 16px;
}

.bg-5DB65F {
    background-color: #5DB65F;
}

.bg-DCAA00,
.section-info .header:after,
.section-baner .content-title:after,
.section-baner .content-title .title:after {
    background-color: #daa900;
    background-color: var(--main-color-corn);
}

.bg-C1C3C2 {
    background-color: #c1c3c2;
    background-color: var(--main-color-pumice);
}

/*baner*/
.section-baner {
    position: relative;
}

.section-baner .col {
    min-height: 355px;
}

.section-baner .bg-img {
    background-position: center top;
    background-size: cover;
}

.section-baner .content-title {
    position: relative;
}

@media screen and (min-width: 1500px) {
    .section-baner .col {
        min-height: 40vh;
    }
}

@media (max-width: 991px) {
    .section-baner .content-title {
        min-height: 0;
        padding: 20px 15px;
    }
}

.section-baner .content-title .adorno {
    position: absolute;
    bottom: 0;
    left: 0;
    max-width: 100%;
    width: 235px;
}

.section-baner .content-title .title {
    font-size: 1.25rem;
    line-height: 1.5rem;
    letter-spacing: 0rem;
    position: relative;
    margin-bottom: 100px;
}

.section-baner .content-title .title span {
    font-size: 2.1875rem;
    line-height: 2.625rem;
    letter-spacing: 0rem;
}

.section-baner .content-title .title span:first-child {
    font-size: 1.25rem;
    line-height: 1;
}

.section-baner .content-title .title:after {
    content: "";
    position: absolute;
    left: 0;
    bottom: -16px;
    height: 2px;
    max-width: 100%;
    width: 90px;
}

.section-baner .content-title:after {
    content: "";
    width: 56px;
    position: absolute;
    right: -28px;
    height: 100%;
    top: 0;
    opacity: 0.77;
    z-index: 1;
}

@media (max-width: 991px) {
    .section-baner .content-title:after {
        display: none;
    }
}

.section-baner .line-color {
    width: 100%;
    height: 4px;
    display: block;
    background: #27a570;
    background: -moz-linear-gradient(left, #27a570 0%, #27a570 32%, #98d78a 32%, #98d78a 66%, #dca900 66%, #dcaa00 100%);
    background: -webkit-gradient(left top, right top, color-stop(0%, #27a570), color-stop(32%, #27a570), color-stop(32%, #98d78a), color-stop(66%, #98d78a), color-stop(66%, #dca900), color-stop(100%, #dcaa00));
    background: -webkit-linear-gradient(left, #27a570 0%, #27a570 32%, #98d78a 32%, #98d78a 66%, #dca900 66%, #dcaa00 100%);
    background: -o-linear-gradient(left, #27a570 0%, #27a570 32%, #98d78a 32%, #98d78a 66%, #dca900 66%, #dcaa00 100%);
    background: -ms-linear-gradient(left, #27a570 0%, #27a570 32%, #98d78a 32%, #98d78a 66%, #dca900 66%, #dcaa00 100%);
    background: linear-gradient(to right, #27a570 0%, #27a570 32%, #98d78a 32%, #98d78a 66%, #dca900 66%, #dcaa00 100%);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#27a570", endColorstr="#dcaa00", GradientType=1);
}

@media screen and (min-width: 1400px) {
    #dnn_ContentPane .banner.banner_2 {
        min-height: 40vh;
    }
}


.container.content_AS {
    width: 100%;
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
}

@media (min-width: 576px) {
    .container.content_AS {
        max-width: 540px;
    }
}

@media (min-width: 768px) {
    .container.content_AS {
        max-width: 720px;
    }
}

@media (min-width: 992px) {
    .container.content_AS {
        max-width: 960px;
    }
}

@media (min-width: 1200px) {
    .row.tarjetas {
        max-width: 270px;
    }

    .container.content_AS {
        max-width: 1140px;
    }
}

@media screen and (max-width: 991.8px) {
    .content_AS .content_S {
        padding-left: 15px;
        padding-right: 15px;
    }

    .content_AS .content_A {
        padding-left: 15px;
        padding-right: 15px;
    }

    .aside .tarjetas .boletines {
        padding-top: 0;
    }

    .section-baner .content-title {
        min-height: 205px;
    }

    .section-informacion .column-content .consolidados .col {
        flex-basis: 100%;
    }
}

@media screen and (max-width: 575.8px) {
    .content_AS .content_S {
        padding-left: 10%;
        padding-right: 10%;
    }

    .content_AS .content_A {
        padding-left: 10%;
        padding-right: 10%;
    }

    .aside .tarjetas .boletines {
        padding-top: 0;
    }

    .section-baner .col.bg-img {
        min-height: auto;
        padding: 0;
        padding-top: 56.25%;
    }
}

#section-6 {
    padding-top: 0 !important;
}

@media screen and (max-width: 375px) {
    #section-6 .tittle-section-6 #text-section-6 {
        font-size: 1rem;
    }
}

.section-baner .content-title .title {
    margin-bottom: 0;
}

section.content {
    padding-bottom: 95px;
}

#section-6 .section-6-tittle-box .tittle-section-6 {
    max-width: 350px;
    width: 100%;
}
</style>
    <section class="new_banner">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-md-6 col-lg-4 col_left bg-fern d-flex justify-content-end align-items-center">
                    <img src="/portals/0/Images/NosotrosQuienesSomos/adorno-baner.png" srcset="/portals/0/Images/NosotrosQuienesSomos/adorno-baner.svg" class="adorno" alt="Image de adorno">
                    <div id="panneCaja" class="caja bg-white position-relative" data-aos="fade-up" runat="server">
                    </div>
                    <!--./caja-->
                </div>
                <!--./col_left-->
                <div class="col-12 col-md-6 col-lg-8 col_right">
                    <div id="panneBannerImage" runat="server"></div>
                    <!--<img src="https://mineros.com.co/Portals/0/skins/mineros/images/2x/portada-inversionista-junta.jpg" alt="">-->
                </div>
                <!--./col_right-->
                <div class="line-color"></div>
            </div>
            <!--./row-->
        </div>
        <!--./container-fluid-->
    </section><!-- .new-banner -->

<section class="section-navigation bg-pumice" data-aos="fade-left">
    <div class="container">
        <div id="panneBreadCrumb">
            <div id="panneBreadCrumb" runat="server"></div>
        </div>
    </div>
    <!--.container-->
</section>
<!--/.section-navigation-->
<section class="section-informacion" id="informacion">
    <div class="container px-sm-0">
        <div class="row m-0">
            <div class="col-12 col-sm-12 col-md-12 col-lg-9 column-content px-md-0">
                <!--.participacion-->
                <div class="row m-0 presentacion">
                    <div class="col-12 title px-0 d-flex" data-aos="fade-down">
                        <div id="panneIconTitle" runat="server"></div>
                    </div>
                    <div class="col-12 px-0">
                        <!--.collapse-conatiner-->
                        <!--.collapse-conatiner-->
                    </div>
                    <!--.title-->
                </div>
                <!--.presentacion-->
                <div class="row m-0 consolidados">
                    <!--.title-->
                    <!-- Primera Card -->
                    <div class="col px-0 d-flex flex-column mb-4">
                        <div id="panneList" runat="server"></div>
                    </div>
                    <!-- Fin Primera Card -->
                    <!--.col-12-->
                </div>
                <div class="row m-0 dividendos mb-5 pb-5">
                    <div class="col-12 px-0">
                        <div id="panneTitleDividendos" runat="server"></div>
                        <!--.title-->
                    </div>
                    <!--.col-12-->
                    <div class="col-12 paragraph-content">
                        <div id="panneText" runat="server"></div>
                    </div>
                    <!--.col-12-->
                </div>
                <!--.row-->
                <div class="row m-0 participacion">
                    <!-- <div class="col-12 title px-0 d-flex">
                            <div class="box-icon bg-jungle-green d-flex justify-content-center align-items-center">
                                <img src="/Portals/0/skins/mineros/images/icon-char.png" srcset="/Portals/0/skins/mineros/images/icon-char.svg" align="Icon Chart" class="img-fluid" width="42" height="42">
                            </div>
                            <h3 class="h4 librefranklin-bold text-white bg-corn mb-0 w-100">Participación accionaria</h3>
                        </div> -->
                    <!--.title-->
                    <div class="col-12 px-0 m-negativo">
                    </div>
                    <div class="col-12 px-0 d-flex">
                    </div>
                    <!--.col-12-->
                </div>
                <!--.consolidados-->
                <div class="row m-0 relacion">
                    <div class="col-12 px-0">
                        <div class="card bg-jungle-green border-0 rounded-0">
                            <!--.card-body-->
                        </div>
                        <!--.card-->
                    </div>
                </div>
                <!--.relacion-->
            </div>
            <!--.column-content-->
            <!-- ASIDE -->
            <div class=" col-12 col-lg-3 col-md-12 content_A mt-0">
                <div class="aside mt-0">
                    <div class="row m-0 tarjetas">
                        <div class="col-12 col-sm-6 col-lg-12 pr-0 trimestre pt-0">
                            <div id="panneResultados" runat="server"></div>
                            <!--.card-->
                        </div>
                        <!--.col-12-->
                        <div class="col-12 col-sm-6 col-lg-12 pr-0 boletines">
                            <div id="panneBoletines" runat="server"></div>
                            <!--.card-->
                        </div>
                        <!--.col-12-->
                    </div>
                    <!--.tarjetas-->
                </div>
            </div>
            <!--.aside-->
        </div>
        <!--.row-->
    </div>
    <!--.container-->
</section>
<div id="ContentPane" runat="server"></div>

<!-- Modal -->
<div class="modal fade page_construct" id="inconstruction" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="inconstructionLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="inconstructionLabel"><span>Pop up para</span> sección en inglés</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="card-text mb-0">En construcción....</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary px-4 rounded-0" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<script>
if (document.querySelectorAll('.section-informacion .column-content .consolidados .card')) {
    var search = document.querySelectorAll('.section-informacion .column-content .consolidados .card');
    search.forEach(function(elemen, i) {
        //console.log(i);
        //Get primary letter
        var firstLetter = elemen.querySelector('.title a').innerText.charAt(0);
        //console.log( letra )
        //Modify value 
        elemen.querySelector('.fuente-grande').innerText = firstLetter;
        //console.log(elemen.querySelector('.fuente-grande').innerText);
    });
}
</script>
<!-- Footer -->
    <footer class="bg-mineral-green" data-aos="fade-down">
        <div id="panneFooter" runat="server"></div>
    </footer>
<!-- JS Includes -->
    <dnn:DnnCssInclude runat="server" FilePath="~/Resources/Libraries/kendo-ui-angular/css/kendo.common-material.min.css" />
<dnn:DnnCssInclude runat="server" FilePath="~/Resources/Libraries/kendo-ui-angular/css/kendo.material.min.css" />
    <dnn:DnnJsInclude runat="server" ID="DnnJsInclude1" FilePath="js/popper.min.js" PathNameAlias="SkinPath" />
    <dnn:DnnJsInclude runat="server" ID="DnnJsInclude2" FilePath="js/bootstrap.min.js" PathNameAlias="SkinPath" />
    <dnn:DnnJsInclude runat="server" ID="DnnJsInclude3" FilePath="js/aos.js" PathNameAlias="SkinPath" />
    <dnn:DnnJsInclude runat="server" ID="DnnJsInclude4" FilePath="libs/incubator-echarts-4.2.1/js/echarts.min.js" PathNameAlias="SkinPath" />
    <dnn:DnnJsInclude runat="server" ID="DnnJsInclude5" FilePath="js/main.js" PathNameAlias="SkinPath" />
    <dnn:DnnJsInclude runat="server" ID="jsAppHomee" FilePath="js/app-home.js" PathNameAlias="SkinPath" />
    <dnn:DnnJsInclude runat="server" FilePath="~/Resources/Libraries/kendo-ui-angular/js/kendo.all.min.js" Priority="10" />
<dnn:DnnJsInclude runat="server" FilePath="~/Resources/Libraries/kendo-ui-angular/js/kendo.aspnetmvc.min.js" Priority="10" />
<dnn:DnnJsInclude runat="server" FilePath="~/Resources/Libraries/angularAutocomplete/multiple-select.min.js" Priority="10" />

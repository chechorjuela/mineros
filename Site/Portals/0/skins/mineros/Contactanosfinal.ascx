<%@ Control Language="c#" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>
<%@ Register TagPrefix="dnn" TagName="USER" Src="~/Admin/Skins/user.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGIN" Src="~/Admin/Skins/Login.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGO" Src="~/Admin/Skins/Logo.ascx" %>
<%@ Register TagPrefix="dnn" TagName="STYLES" Src="~/Admin/Skins/Styles.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LINKTOMOBILE" Src="~/Admin/Skins/LinkToMobileSite.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LANGUAGE" Src="~/Admin/Skins/Language.ascx" %>
<%@ Register TagPrefix="dnn" TagName="SEARCH" Src="~/Admin/Skins/Search.ascx" %>
<%@ Register TagPrefix="dnn" TagName="META" Src="~/Admin/Skins/Meta.ascx" %>
<%@ Register TagPrefix="dnn" TagName="jQuery" Src="~/Admin/Skins/jQuery.ascx" %>
<%@ Register TagPrefix="dnn" TagName="MENU" Src="~/DesktopModules/DDRMenu/Menu.ascx" %>
<dnn:META ID="META1" runat="server" Name="viewport" Content="width=device-width,initial-scale=1, maximum-scale=1" />
<dnn:META runat="server" Name="description" Content="Mineros" />
<!-- Css Includes -->
<dnn:DnnCssInclude runat="server" ID="cssBootstrap" FilePath="css/bootstrap.min.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssHover" FilePath="css/hover-min.css" PathNameAlias="SkinPath" />
<%--<dnn:DnnCssInclude runat="server" ID="cssAos" FilePath="css/aos.css" PathNameAlias="SkinPath" />--%>
<dnn:DnnCssInclude runat="server" ID="cssFonts" FilePath="css/fonts.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssColors" FilePath="css/colors.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssClassFonts" FilePath="css/class-fonts.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssStyles" FilePath="css/styles.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssFontawesome" FilePath="css/fonts/fontawesome/css/all.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssContactanos" FilePath="css/contactanos.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssStylesHome" FilePath="css/style-home.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="DnnCssInclude8" FilePath="css/fonts/fontawesome/css/all.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssLighSlider" FilePath="css/lightSilder.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssBanner" FilePath="css/style-banner.css" PathNameAlias="SkinPath" />

<dnn:DnnJsInclude runat="server" FilePath="~/DesktopModules/Mineros/scripts/app.js" Priority="10" />
<dnn:DnnJsInclude runat="server" FilePath="~/DesktopModules/Mineros/scripts/echarts.common.min.js" Priority="10" />
<dnn:DnnJsInclude runat="server" ID="DnnJsInclude1" FilePath="js/lightslider.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" FilePath="~/Resources/Libraries/AngujarJS/01_04_05/angular.min.js" Priority="10" />
<dnn:DnnJsInclude runat="server" FilePath="~/Resources/Libraries/AngujarJS/01_04_05/angular-sanitize.min.js" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
<dnn:DnnJsInclude runat="server" FilePath="https://code.angularjs.org/1.6.9/angular-animate.min.js" Priority="10" />
<dnn:DnnJsInclude runat="server" ID="jsSlider" FilePath="js/lightslider.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsCustom" FilePath="js/custom.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsInputsForm" FilePath="js/inputsForm.js" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" FilePath="~/Resources/Libraries/kendo-ui-angular/css/kendo.common-material.min.css" />
<dnn:DnnCssInclude runat="server" FilePath="~/Resources/Libraries/kendo-ui-angular/css/kendo.material.min.css" />
<style type="text/css">
    .section-contact-col-right .form-frame{
        /*padding-bottom: 40px !important;*/
    }
    .dnnCheckbox{
        display: none !important;
    }
    .dnnFormItem input[type="text"], .dnnFormItem input[type="password"], .dnnFormItem input[type="email"], .dnnFormItem input[type="tel"], .dnnFormItem select, .dnnFormItem textarea {
        width: 45%;
        max-width:inherit !important;
    }
    .dnnForm.FeedbackForm .dnnFormItem INPUT, .dnnForm.FeedbackForm .dnnFormItem TEXTAREA, .dnnForm.FeedbackForm .dnnFormItem SELECT {
        width: 100% !important;
    }
    .dnnForm fieldset {
        clear: none;
        position: relative;
        margin-bottom: 0px !important;
        text-align: left;
    }
    .form-group .form-group SELECT, .form-group INPUT, .form-group TEXTAREA{
        width: 100% !important;
    }
    .section-contact-col-right .form-frame
    {
        padding-bottom: 64px !important;
    }
    .buttonSendForm{
        /*display: inline-block;*/
        text-decoration: none !important;
        background-color: rgb(90, 182, 84);
        background-color: var(--main-color-fern);
        font-family: 'LibreFranklin', sans-serif;
        font-size: 1rem;
        line-height: 1.3125rem;
        margin: auto;
        padding: 15px 70px;
        cursor: pointer;
        height: 52px;
        font-size: 1.20rem;
        border-radius: 10px;
    }
    .buttonSendForm:hover{
        background-color: rgb(218, 169, 0);
        background-color: var(--main-color-corn);
    }
    .textInformation{
            display: inline-block;
            text-decoration: none !important;
            font-family: 'LibreFranklin', sans-serif;
            font-size: 1rem;
            line-height: 1.3125rem;
            margin: auto;
            padding: 15px 70px;

        }
   /*
    .dnnPrimaryAction{
        display: inline-block !important;
        text-decoration: none !important;
        font-family: 'LibreFranklin', sans-serif !important;
        font-size: 1rem !important;
        line-height: 1.3125rem !important;
            margin: auto !important;
        padding: 15px 70px !important;
        cursor: pointer !important;
    }
*/
    .dnnPrimaryAction, .dnnFormItem input[type="submit"], a.dnnPrimaryAction {
        text-decoration: none !important;
        background: rgb(34, 165, 113);
        background: var(--main-color-jungle-green);
        font-family: 'LibreFranklin', sans-serif;
        font-size: 1rem;
        line-height: 1.3125rem;
        margin: auto;
        padding: 15px 70px;
        cursor: pointer;

        height: 52px;
        font-size: 1.20rem;
        border-radius: 10px;
        border-color: transparent !important;
        -webkit-box-shadow: inherit;
        box-shadow: inherit !important;
        color: #fff !important;
        text-shadow: inherit !important;
    }
    .dnnPrimaryAction:hover, .dnnFormItem input[type="submit"]:hover, a.dnnPrimaryAction:hover {
        background: rgb(218, 169, 0);
        background: var(--main-color-corn);
    }
    .dnnFormMessage.dnnFormError, .dnnFormMessage.dnnFormValidationSummary {
        background-color: transparent !important;
        border-color: transparent !important;
    }
    .dnnFormMessage {
        display: block;
        padding: 17px 18px;
        margin-bottom: 18px;
        border: 1px solid transparent !important;
        background: transparent !important;
        -webkit-border-radius: 3px;
        border-radius: 3px;
        max-width: 980px;
    }
    .check{}
    .check::before{
        border: #adb5bd solid 1px;
    }
    .uncheck{}
    .uncheck::before{
        border: red solid 1px;
    }

</style>
<body class="page-contactanos">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5ZKGB6Z" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <div id="hed-top-green" class="bg-mineral-green d-flex">
<!--         <ul id="bar-lang" class="nav justify-content-around" data-aos="fade-left">
            <li class="nav-item">
                <a class="nav-link librefranklin-thin text-white" href="/en-us" data-toggle="modal" data-target="#inconstruction">English</a>
                <span></span>
            </li>
            <li class="nav-item">
                <a class="nav-link librefranklin-thin text-white active" href="/es-es">Español</a>
                <span></span>
            </li>
        </ul> -->
        <div id="bar-lang" class="nav justify-content-around" data-aos="fade-left">
            <dnn:LANGUAGE runat="server" ID="dnnLANGUAGE" ShowLinks="True" ShowMenu="False" ItemTemplate=''/>
        </div>
        <ul id="hed-top-band" class="nav justify-content-end" data-aos="fade-left">
            <li class="nav-item">
                <a class="nav-link" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/argentina" target="_blank">
                    <span class="box-img-loc hvr-icon-bob"><img src="/Portals/0/skins/mineros/images/2x/argentina.png" srcset="/Portals/0/skins/mineros/images/svg/argentina.svg" alt="Bandera de Argentina" class="hvr-icon"></span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/Chile" target="_blank">
                    <span class="box-img-loc hvr-icon-bob"><img src="/Portals/0/ban-4-1.png" class="hvr-icon" alt="Bandera de Chile"></span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/Colombia" target="_blank">
                    <span class="box-img-loc hvr-icon-bob"><img src="/Portals/0/skins/mineros/images/2x/colombia.png" srcset="/Portals/0/skins/mineros/images/svg/colombia.svg" alt="Bandera de Colombia" class="hvr-icon"></span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="http://hemco.com.ni/" target="_blank">
                    <span class="box-img-loc hvr-icon-bob"><img src="/Portals/0/skins/mineros/images/2x/nicaragua.png" srcset="/Portals/0/skins/mineros/images/svg/nicaragua.svg" alt="Bandera de Nicaragua" class="hvr-icon"></span>
                </a>
            </li>
        </ul>
    </div>
    <header id="masthead" class="site-header js" role="banner" data-aos="fade-down">
        <div class="navigation-top bg-menu-trans">
            <div class="wrap d-flex  justify-content-between">
                <div id="logo-header" class="logo-header text-center position-relative">
                    <dnn:LOGO runat="server" ID="dnnLOGO" />
                    <div class="d-flex justify-content-center align-items-center line-logo">
                        <span class="d-flex"></span>
                    </div>
                </div>
                <div class="d-flex box-menu">
                    <dnn:MENU ID="MENU" MenuStyle="Menus/MainMenu" runat="server" NodeSelector="*" />
                </div>
            </div>
            <div class="d-flex mr-auto flex-wrap">
                <a class="img-margin" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/argentina" target="_blank">
                    <span class="box-img-loc hvr-icon-bob">
                        <img src="/Portals/0/skins/mineros/images/Flag_of_Argentina.png" srcset="/Portals/0/skins/mineros/images/Flag_of_Argentina.svg" alt="Bandera De Argentina" class="icon-flag hvr-icon">
                    </span>
                </a>
                <a class="img-margin" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/Chile" target="_blank">
                    <span class="box-img-loc hvr-icon-bob">
                        <img src="/Portals/0/skins/mineros/images/2x/ban-4-1.png" srcset="/Portals/0/skins/mineros/images/SVG/ban-4-1.svg" alt="Bandera de Chile" class="icon-flag hvr-icon">
                    </span>
                </a>
                <a class="img-margin" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/Colombia" target="_blank">
                    <span class="box-img-loc hvr-icon-bob">
                        <img src="/Portals/0/skins/mineros/images/Flag-of-colombia.png" srcset="/Portals/0/skins/mineros/images/flag_of_colombia.svg" alt="Bandera de Colombia" class="icon-flag hvr-icon">
                    </span>
                </a>
                <a class="img-margin" href="http://hemco.com.ni/" target="_blank">
                    <span class="box-img-loc hvr-icon-bob">
                        <img src="/Portals/0/skins/mineros/images/Flag_of_Nicaragua.png" srcset="/Portals/0/skins/mineros/images/Flag_of_Nicaragua.svg" alt="Bandera de nicaragua" class="icon-flag hvr-icon">
                    </span>
                </a>
                <div class="box-icon-rrss w-100 d-flex align-items-center">
                    <a class="link-rrss text-white aos-init aos-animate" href="https://www.facebook.com/MinerosSA" data-aos="flip-down" target="_blank"><span><i class="text-white fab fa-facebook-f"></i></span></a>
                    <a class="link-rrss text-white aos-init aos-animate" href="https://twitter.com/mineros_sa" data-aos="flip-down" target="_blank"><span><i class="text-white fab fa-twitter"></i></span></a>
                    <a class="link-rrss text-white aos-init aos-animate" href="https://www.youtube.com/user/MINEROSSA" data-aos="flip-down" target="_blank"><span><i class="text-white fab fa-youtube"></i></span></a>
                    <a class="link-rrss text-white aos-init aos-animate" href="http://instagram.com/mineros_sa" data-aos="flip-down" target="_blank"><span><i class="text-white fab fa-instagram"></i></span></a>
                    <a class="link-rrss text-white aos-init aos-animate" href="https://co.linkedin.com/company/mineros-s.a." data-aos="flip-down" target="_blank"><span><i class="text-white fab fa-linkedin-in"></i></span></a>
                </div>
            </div>
        </div>
    </header>
    <div id="ContentPane" runat="server"></div>

    <section class="new_banner">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-md-6 col-lg-4 col_left bg-fern d-flex justify-content-end align-items-center">
                    <img src="/portals/0/Images/NosotrosQuienesSomos/adorno-baner.png" srcset="/portals/0/Images/NosotrosQuienesSomos/adorno-baner.svg" class="adorno" alt="Image de adorno">
                    <div id="panneCaja" class="caja bg-white position-relative" data-aos="fade-up" runat="server">
                    </div>
                    <!--./caja-->
                </div>
                <!--./col_left-->
                <div class="col-12 col-md-6 col-lg-8 col_right">
                    <div id="panneBannerImage" runat="server"></div>
                    <!--<img src="https://mineros.com.co/Portals/0/skins/mineros/images/2x/portada-inversionista-junta.jpg" alt="">-->
                </div>
                <!--./col_right-->
                <div class="line-color"></div>
            </div>
            <!--./row-->
        </div>
        <!--./container-fluid-->
    </section><!-- .new-banner -->

    <section class="section-navigation bg-pumice" data-aos="fade-left">
        <div class="container">
            <div id="panneBreadCrumb" runat="server"></div>
        </div>
    </section>
    <section id="contact" class="section-contact">
        <div class="container-fluid section-contact-row">
            <div class="row">
                <div class="col-sm-12 col-lg-6 section-contact-col-left" data-aos="fade-right">
                    <div id="textLeftForm" runat="server" style="width: 100%">

                    </div>
                </div>
                <div class="col-sm-12 col-lg-6 section-contact-col-right">

                    <div class="form-frame bg-very-light-gray" data-aos="fade-left">
                        <div class="form librefranklin-regular color-very-dark-gray bg-very-light-gray">
                            <div id="formPane" runat="server"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</body>
<!-- Modal -->
<div class="modal fade page_construct" id="inconstruction" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="inconstructionLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="inconstructionLabel"><span>Pop up para</span> sección en inglés</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="card-text mb-0">En construcción....</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary px-4 rounded-0" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
    <footer class="bg-mineral-green" data-aos="fade-down">
        <div id="panneFooter" runat="server"></div>
    </footer>
<dnn:DnnCssInclude runat="server" FilePath="~/Resources/Libraries/kendo-ui-angular/css/kendo.common-material.min.css" />
<dnn:DnnCssInclude runat="server" FilePath="~/Resources/Libraries/kendo-ui-angular/css/kendo.material.min.css" />
<dnn:DnnCssInclude runat="server" FilePath="~/Resources/Libraries/angularAutocomplete/multiple-select.min.css" />
<dnn:DnnJsInclude runat="server" ID="DnnJsInclude2" FilePath="js/popper.min.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="DnnJsInclude3" FilePath="js/bootstrap.min.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="DnnJsInclude4" FilePath="js/aos.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsEcharts" FilePath="libs/incubator-echarts-4.2.1/js/echarts.min.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsMain" FilePath="js/main.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" FilePath="~/Resources/Libraries/kendo-ui-angular/js/kendo.all.min.js" Priority="10" />
<dnn:DnnJsInclude runat="server" FilePath="~/Resources/Libraries/kendo-ui-angular/js/kendo.aspnetmvc.min.js" Priority="10" />
<dnn:DnnJsInclude runat="server" FilePath="~/Resources/Libraries/angularAutocomplete/multiple-select.min.js" Priority="10" />

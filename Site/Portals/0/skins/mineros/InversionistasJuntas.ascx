<%@ Control Language="c#" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>
<%@ Register TagPrefix="dnn" TagName="USER" Src="~/Admin/Skins/user.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGIN" Src="~/Admin/Skins/Login.ascx" %>
<%@ Register TagPrefix="dnn" TagName="STYLES" Src="~/Admin/Skins/Styles.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LINKTOMOBILE" Src="~/Admin/Skins/LinkToMobileSite.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LANGUAGE" Src="~/Admin/Skins/Language.ascx" %>
<%@ Register TagPrefix="dnn" TagName="SEARCH" Src="~/Admin/Skins/Search.ascx" %>
<%@ Register TagPrefix="dnn" TagName="META" Src="~/Admin/Skins/Meta.ascx" %>
<%@ Register TagPrefix="dnn" TagName="jQuery" Src="~/Admin/Skins/jQuery.ascx" %>
<%@ Register TagPrefix="dnn" TagName="MENU" Src="~/DesktopModules/DDRMenu/Menu.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGO" Src="~/Admin/Skins/Logo.ascx" %>
<dnn:META ID="META1" runat="server" Name="viewport" Content="width=device-width,initial-scale=1, maximum-scale=1" />
<dnn:META runat="server" Name="description" Content="Mineros" />
<!-- Css Includes -->
<dnn:DnnCssInclude runat="server" ID="cssBootstrap" FilePath="css/bootstrap.min.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssAos" FilePath="css/aos.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssHover" FilePath="css/hover-min.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssLighSlider" FilePath="css/lightSilder.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssFonts" FilePath="css/fonts.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssColors" FilePath="css/colors.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssClassFonts" FilePath="css/class-fonts.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssStyles" FilePath="css/styles.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssInverJuntas" FilePath="css/inversionista-junta.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssBanner" FilePath="css/style-banner.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssFontawesome" FilePath="css/fonts/fontawesome/css/all.css" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" FilePath="~/Resources/Libraries/kendo-ui-angular/js/kendo.all.min.js" Priority="10" />
<dnn:DnnJsInclude runat="server" FilePath="~/Resources/Libraries/kendo-ui-angular/js/kendo.aspnetmvc.min.js" Priority="10" />
<dnn:DnnJsInclude runat="server" FilePath="~/Resources/Libraries/angularAutocomplete/multiple-select.min.js" Priority="10" />

<body class="informacion-financiera">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5ZKGB6Z" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <div id="hed-top-green" class="bg-mineral-green d-flex">
<!--         <ul id="bar-lang" class="nav justify-content-around" data-aos="fade-left">
            <li class="nav-item">
                <a class="nav-link librefranklin-thin text-white" href="/en-us" data-toggle="modal" data-target="#inconstruction">English</a>
                <span></span>
            </li>
            <li class="nav-item">
                <a class="nav-link librefranklin-thin text-white active" href="/es-es">Español</a>
                <span></span>
            </li>
        </ul> -->
        <div id="bar-lang" class="nav justify-content-around" data-aos="fade-left">
            <dnn:LANGUAGE runat="server" ID="dnnLANGUAGE" ShowLinks="True" ShowMenu="False" ItemTemplate=''/>
        </div>           
        <ul id="hed-top-band" class="nav justify-content-end" data-aos="fade-left">
            <li class="nav-item">
                <a class="nav-link" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/argentina" target="_blank">
                    <span class="box-img-loc hvr-icon-bob"><img src="/Portals/0/skins/mineros/images/2x/argentina.png" srcset="/Portals/0/skins/mineros/images/svg/argentina.svg" alt="Bandera de Argentina" class="hvr-icon"></span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/Chile" target="_blank">
                    <span class="box-img-loc hvr-icon-bob"><img src="/Portals/0/ban-4-1.png"  class="hvr-icon" alt="Bandera de Chile"></span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/Colombia" target="_blank">
                    <span class="box-img-loc hvr-icon-bob"><img src="/Portals/0/skins/mineros/images/2x/colombia.png" srcset="/Portals/0/skins/mineros/images/svg/colombia.svg" alt="Bandera de Colombia" class="hvr-icon"></span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="http://hemco.com.ni/" target="_blank">
                    <span class="box-img-loc hvr-icon-bob"><img src="/Portals/0/skins/mineros/images/2x/nicaragua.png" srcset="/Portals/0/skins/mineros/images/svg/nicaragua.svg" alt="Bandera de Nicaragua" class="hvr-icon"></span>
                </a>
            </li>
        </ul>
    </div>
    <style>
    #dnn_panneBreadcrumb {
        min-height: auto !important;
    }
    </style>
    <header id="masthead" class="site-header js" role="banner" data-aos="fade-down">
        <div class="navigation-top bg-menu-trans">
            <div class="wrap d-flex  justify-content-between">
                <div id="logo-header" class="logo-header text-center position-relative">
                    <dnn:LOGO runat="server" ID="dnnLOGO" />
                    <div class="d-flex justify-content-center align-items-center line-logo">
                        <span class="d-flex"></span>
                    </div>
                </div>
                <div class="d-flex box-menu">
                    <dnn:MENU ID="MENU" MenuStyle="Menus/MainMenu" runat="server" NodeSelector="*" />
                </div>
            </div>
            <div class="d-flex mr-auto flex-wrap">
                <a class="img-margin" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/argentina" target="_blank">
                    <span class="box-img-loc hvr-icon-bob">
                        <img src="/Portals/0/skins/mineros/images/Flag_of_Argentina.png" srcset="/Portals/0/skins/mineros/images/Flag_of_Argentina.svg" alt="Bandera De Argentina" class="icon-flag hvr-icon">
                    </span>
                </a>
                <a class="img-margin" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/Chile" target="_blank">
                    <span class="box-img-loc hvr-icon-bob">
                        <img src="/Portals/0/skins/mineros/images/2x/ban-4-1.png" srcset="/Portals/0/skins/mineros/images/SVG/ban-4-1.svg" alt="Bandera de Chile" class="icon-flag hvr-icon">
                    </span>
                </a>
                <a class="img-margin" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/Colombia" target="_blank">
                    <span class="box-img-loc hvr-icon-bob">
                        <img src="/Portals/0/skins/mineros/images/Flag-of-colombia.png" srcset="/Portals/0/skins/mineros/images/flag_of_colombia.svg" alt="Bandera de Colombia" class="icon-flag hvr-icon">
                    </span>
                </a>
                <a class="img-margin" href="http://hemco.com.ni/" target="_blank">
                    <span class="box-img-loc hvr-icon-bob">
                        <img src="/Portals/0/skins/mineros/images/Flag_of_Nicaragua.png" srcset="/Portals/0/skins/mineros/images/Flag_of_Nicaragua.svg" alt="Bandera de nicaragua" class="icon-flag hvr-icon">
                    </span>
                </a>
                <div class="box-icon-rrss w-100 d-flex align-items-center">
                    <a class="link-rrss text-white aos-init aos-animate" href="https://www.facebook.com/MinerosSA" data-aos="flip-down" target="_blank"><span><i class="text-white fab fa-facebook-f"></i></span></a>
                    <a class="link-rrss text-white aos-init aos-animate" href="https://twitter.com/mineros_sa" data-aos="flip-down" target="_blank"><span><i class="text-white fab fa-twitter"></i></span></a>
                    <a class="link-rrss text-white aos-init aos-animate" href="https://www.youtube.com/user/MINEROSSA" data-aos="flip-down" target="_blank"><span><i class="text-white fab fa-youtube"></i></span></a>
                    <a class="link-rrss text-white aos-init aos-animate" href="http://instagram.com/mineros_sa" data-aos="flip-down" target="_blank"><span><i class="text-white fab fa-instagram"></i></span></a>
                    <a class="link-rrss text-white aos-init aos-animate" href="https://co.linkedin.com/company/mineros-s.a." data-aos="flip-down" target="_blank"><span><i class="text-white fab fa-linkedin-in"></i></span></a>
                </div>
            </div>
        </div>
    </header>
       <section class="new_banner">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-md-6 col-lg-4 col_left bg-fern d-flex justify-content-end align-items-center">
                <img src="/portals/0/Images/NosotrosQuienesSomos/adorno-baner.png" srcset="/portals/0/Images/NosotrosQuienesSomos/adorno-baner.svg" class="adorno" alt="Image de adorno">
                <div id="panneCaja" class="caja bg-white position-relative" data-aos="fade-up" runat="server">
                </div><!--./caja-->
            </div><!--./col_left-->
            <div class="col-12 col-md-6 col-lg-8 col_right">
                <div id="panneBannerImage" runat="server"></div>
                <!--<img src="https://mineros.com.co/Portals/0/skins/mineros/images/2x/portada-inversionista-junta.jpg" alt="">-->
            </div><!--./col_right-->
            <div class="line-color"></div>
        </div><!--./row-->
    </div><!--./container-fluid-->
</section><!-- .new-banner --> 
    <div id="ContentPane" runat="server"></div>
    <style>
    .btn-icon:hover path.cls-1 {
        fill: #000;
    }

    .btn-icon {
        color: rgb(90, 182, 95);
        font-size: .825rem;
        padding-right: 15px;
        padding-bottom: 15px;
        display: flex;
        align-items: center;
        transition: .35s ease all;
    }

    .btn-icon svg {
        margin-left: 15px;
        transition: .35s ease all;
    }

    .btn-icon:hover {
        color: #000;
        text-decoration: none;
    }

    @media screen and (min-width: 1500px) {
        .section-baner .min-height .col-12 {
            min-height: 40vh;
        }
    }
    </style>
    <section class="section-navigation bg-pumice" data-aos="fade-left">
        <div class="container">
            <div id="panne" runat="server"></div>
        </div>
        <!--.container-->
    </section>
    <!--.navigation-->
    <section id="section-1">
        <div class="container">
            <div class="row">
                <div id="panneTitle" runat="server"></div>
            </div>
        </div>
    </section>
    <section id="section-2">
        <div class="container">
            <div class="row">
                <div class="d-block w-100" data-aos="fade-up">
                    <div id="panneDos" runat="server"></div>
                </div>
                <div class="box-target-user card-deck w-100 justify-content-between">
                    <div id="panneTarg1" runat="server"></div>
                    <div id="panneTarg2" runat="server"></div>
                    <div id="panneTarg3" runat="server"></div>
                    <div id="panneTarg4" runat="server"></div>
                    <div id="panneTarg5" runat="server"></div>
                    <div id="panneTarg6" runat="server"></div>
                    <div id="panneTarg7" runat="server"></div>
                    <div id="panneTarg8" runat="server"></div>
                    <div id="panneTarg9" runat="server"></div>
                </div>
            </div>
        </div>
    </section>
    <section id="section-3" class="bg-alto" style="display:none!important;">
        <div class="container">
            <div class="row justify-content-center">
                <div id="tittle-section-3" class="col-12 title px-0 d-flex box-duo-title" data-aos="fade-down">
                    <div id="pannetitlesection3" runat="server"></div>
                </div>
                <div class="target-section-3 d-flex flex-wrap">
                    <div id="panneMedia1" runat="server"></div>
                    <div id="panneMedia2" runat="server"></div>
                    <div id="panneMedia3" runat="server"></div>
                    <div id="panneMedia4" runat="server"></div>
                    <div id="panneMedia5" runat="server"></div>
                    <div id="panneMedia6" runat="server"></div>
                    <div id="panneMedia7" runat="server"></div>
                </div>
                <div class="box-green-text-section-3 bg-jungle-green" data-aos="zoom-in">
                    <div id="panneBoxGreen" runat="server"></div>
                </div>
            </div>
        </div>
    </section>
    <section id="section-4" class="mt-5 pt-5">
        <div class="container">
            <div class="row">
                <div id="tittle-section-4" class="col-12 title px-0 d-flex box-duo-title" data-aos="fade-up">
                    <div id="panneTitleGreen" runat="server"></div>
                </div>
                <div id="box-card-section-4">
                    <div id="panneBoxCard1" class="col-12 col-sm-6 col-lg-4 px-0" runat="server"></div>
                    <div id="panneBoxCard2" class="col-12 col-sm-6 col-lg-4 px-0" runat="server"></div>
                    <div id="panneBoxCard3" class="col-12 col-sm-6 col-lg-4 px-0" runat="server"></div>
                    <div id="panneBoxCard4" class="col-12 col-sm-6 col-lg-4 px-0" runat="server"></div>
                    <div id="panneBoxCard5" class="col-12 col-sm-6 col-lg-4 px-0" runat="server"></div>
                    <div id="panneBoxCard6" class="col-12 col-sm-6 col-lg-4 px-0" runat="server"></div>
                </div>
            </div>
        </div>
    </section>
    <section id="section-5" class="bg-jungle-green">
        <div class="container">
            <div class="row justify-content-center align-items-center flex-column">
                <div id="panneBoxIcon" runat="server"></div>
                <div id="panneBoxText" runat="server"></div>
            </div>
        </div>
    </section>
    <section id="section-6" class="bg-white">
        <div class="container">
            <div class="row justify-content-center align-items-center flex-column">
                <div id="panneTitleSection" runat="server" class="col-12 title-section px-0 pb-4"></div>
                <div id="panneSectionContent" runat="server" class="col-12 pt-2"></div>
            </div>
        </div>
    </section>
    <!--.section-informacion-->
    <!-- Modal -->
<div class="modal fade page_construct" id="inconstruction" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="inconstructionLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="inconstructionLabel"><span>Pop up para</span> sección en inglés</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="card-text mb-0">En construcción....</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary px-4 rounded-0" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
    <!-- Footer -->
    <footer class="bg-mineral-green" data-aos="fade-down">
        <div id="panneFooter" runat="server"></div>
    </footer>
    <!-- JS Includes -->
    <dnn:DnnJsInclude runat="server" ID="jsPopper" FilePath="js/popper.min.js" PathNameAlias="SkinPath" />
    <dnn:DnnJsInclude runat="server" ID="jsBootstrap" FilePath="js/bootstrap.min.js" PathNameAlias="SkinPath" />
    <dnn:DnnJsInclude runat="server" ID="jsAos" FilePath="js/aos.js" PathNameAlias="SkinPath" />
    <dnn:DnnJsInclude runat="server" ID="jsEcharts" FilePath="libs/incubator-echarts-4.2.1/js/echarts.min.js" PathNameAlias="SkinPath" />
    <dnn:DnnJsInclude runat="server" ID="jsSlider" FilePath="js/lightslider.js" PathNameAlias="SkinPath" />
    <dnn:DnnJsInclude runat="server" ID="jsMain" FilePath="js/main.js" PathNameAlias="SkinPath" />
    <dnn:DnnJsInclude runat="server" ID="jsCustom" FilePath="js/custom.js" PathNameAlias="SkinPath" />
    <dnn:DnnJsInclude runat="server" ID="jsFiliales" FilePath="js/filiales_slider.js" PathNameAlias="SkinPath" />
</body>
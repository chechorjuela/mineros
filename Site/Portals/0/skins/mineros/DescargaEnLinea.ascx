<%@ Control Language="c#" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>
<%@ Register TagPrefix="dnn" TagName="USER" Src="~/Admin/Skins/user.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGIN" Src="~/Admin/Skins/Login.ascx" %>
<%@ Register TagPrefix="dnn" TagName="STYLES" Src="~/Admin/Skins/Styles.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LINKTOMOBILE" Src="~/Admin/Skins/LinkToMobileSite.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LANGUAGE" Src="~/Admin/Skins/Language.ascx" %>
<%@ Register TagPrefix="dnn" TagName="SEARCH" Src="~/Admin/Skins/Search.ascx" %>
<%@ Register TagPrefix="dnn" TagName="META" Src="~/Admin/Skins/Meta.ascx" %>
<%@ Register TagPrefix="dnn" TagName="jQuery" Src="~/Admin/Skins/jQuery.ascx" %>
<%@ Register TagPrefix="dnn" TagName="MENU" Src="~/DesktopModules/DDRMenu/Menu.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGO" Src="~/Admin/Skins/Logo.ascx" %>
<dnn:META ID="META1" runat="server" Name="viewport" Content="width=device-width,initial-scale=1, maximum-scale=1" />
<dnn:META runat="server" Name="description" Content="Mineros" />
<!-- Css Includes -->
<dnn:DnnCssInclude runat="server" ID="cssBootstrap" FilePath="css/bootstrap.min.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssFonts" FilePath="css/fonts.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssclassFonts" FilePath="css/class-fonts.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssColors" FilePath="css/colors.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssLighSlider" FilePath="css/lightSilder.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssHover" FilePath="css/hover-min.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssStyles" FilePath="css/styles.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssAos" FilePath="css/aos.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssStylesOperacionesCielo" FilePath="css/style-nosotros-Descargas.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssStylesSidebar" FilePath="css/sidebar.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssBanner" FilePath="css/style-banner.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssFontawesome" FilePath="css/fonts/fontawesome/css/all.css" PathNameAlias="SkinPath" />
<!-- JS Includes -->
<dnn:DnnJsInclude runat="server" ID="jsPopper" FilePath="js/popper.min.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsSlider" FilePath="js/lightslider.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsBootstrap" FilePath="js/bootstrap.min.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsAos" FilePath="js/aos.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsCustom" FilePath="js/custom.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsFiliales" FilePath="js/filiales_slider.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" FilePath="~/Resources/Libraries/kendo-ui-angular/js/kendo.all.min.js" Priority="10" />
<dnn:DnnJsInclude runat="server" FilePath="~/Resources/Libraries/kendo-ui-angular/js/kendo.aspnetmvc.min.js" Priority="10" />
<dnn:DnnJsInclude runat="server" FilePath="~/Resources/Libraries/angularAutocomplete/multiple-select.min.js" Priority="10" />

<div id="hed-top-green" class="bg-mineral-green d-flex">
    <div id="bar-lang" class="nav justify-content-around" data-aos="fade-left">
        <dnn:LANGUAGE runat="server" ID="dnnLANGUAGE" ShowLinks="True" ShowMenu="False" ItemTemplate='' />
    </div>
    <ul id="hed-top-band" class="nav justify-content-end" data-aos="fade-left">
        <li class="nav-item">
            <a class="nav-link" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/argentina" target="_blank">
                <span class="box-img-loc hvr-icon-bob"><img src="/Portals/0/skins/mineros/images/2x/argentina.png" srcset="/Portals/0/skins/mineros/images/svg/argentina.svg" alt="Bandera de Argentina" class="hvr-icon"></span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/Chile" target="_blank">
                <span class="box-img-loc hvr-icon-bob"><img src="/Portals/0/ban-4-1.png" class="hvr-icon" alt="Bandera de Chile"></span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/Colombia" target="_blank">
                <span class="box-img-loc hvr-icon-bob"><img src="/Portals/0/skins/mineros/images/2x/colombia.png" srcset="/Portals/0/skins/mineros/images/svg/colombia.svg" alt="Bandera de Colombia" class="hvr-icon"></span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="http://hemco.com.ni/" target="_blank">
                <span class="box-img-loc hvr-icon-bob"><img src="/Portals/0/skins/mineros/images/2x/nicaragua.png" srcset="/Portals/0/skins/mineros/images/svg/nicaragua.svg" alt="Bandera de Nicaragua" class="hvr-icon"></span>
            </a>
        </li>
    </ul>
</div>
<header id="masthead" class="site-header js" role="banner" data-aos="fade-down">
    <div class="navigation-top bg-menu-trans">
        <div class="wrap d-flex  justify-content-between">
            <div id="logo-header" class="logo-header text-center position-relative">
                <dnn:LOGO runat="server" ID="dnnLOGO" />
                <div class="d-flex justify-content-center align-items-center line-logo">
                    <span class="d-flex"></span>
                </div>
            </div>
            <div class="d-flex box-menu">
                <dnn:MENU ID="MENU" MenuStyle="Menus/MainMenu" runat="server" NodeSelector="*" />
            </div>
        </div>
        <div class="d-flex mr-auto flex-wrap">
            <a class="img-margin" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/argentina" target="_blank">
                <span class="box-img-loc hvr-icon-bob">
                    <img src="/Portals/0/skins/mineros/images/Flag_of_Argentina.png" srcset="/Portals/0/skins/mineros/images/Flag_of_Argentina.svg" alt="Bandera De Argentina" class="icon-flag hvr-icon">
                </span>
            </a>
            <a class="img-margin" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/Chile" target="_blank">
                <span class="box-img-loc hvr-icon-bob">
                    <img src="/Portals/0/skins/mineros/images/2x/ban-4-1.png" srcset="/Portals/0/skins/mineros/images/SVG/ban-4-1.svg" alt="Bandera de Chile" class="icon-flag hvr-icon">
                </span>
            </a>
            <a class="img-margin" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/Colombia" target="_blank">
                <span class="box-img-loc hvr-icon-bob">
                    <img src="/Portals/0/skins/mineros/images/Flag-of-colombia.png" srcset="/Portals/0/skins/mineros/images/flag_of_colombia.svg" alt="Bandera de Colombia" class="icon-flag hvr-icon">
                </span>
            </a>
            <a class="img-margin" href="http://hemco.com.ni/" target="_blank">
                <span class="box-img-loc hvr-icon-bob">
                    <img src="/Portals/0/skins/mineros/images/Flag_of_Nicaragua.png" srcset="/Portals/0/skins/mineros/images/Flag_of_Nicaragua.svg" alt="Bandera de nicaragua" class="icon-flag hvr-icon">
                </span>
            </a>
            <div class="box-icon-rrss w-100 d-flex align-items-center">
                <a class="link-rrss text-white aos-init aos-animate" href="https://www.facebook.com/MinerosSA" data-aos="flip-down" target="_blank"><span><i class="text-white fab fa-facebook-f"></i></span></a>
                <a class="link-rrss text-white aos-init aos-animate" href="https://twitter.com/mineros_sa" data-aos="flip-down" target="_blank"><span><i class="text-white fab fa-twitter"></i></span></a>
                <a class="link-rrss text-white aos-init aos-animate" href="https://www.youtube.com/user/MINEROSSA" data-aos="flip-down" target="_blank"><span><i class="text-white fab fa-youtube"></i></span></a>
                <a class="link-rrss text-white aos-init aos-animate" href="http://instagram.com/mineros_sa" data-aos="flip-down" target="_blank"><span><i class="text-white fab fa-instagram"></i></span></a>
                <a class="link-rrss text-white aos-init aos-animate" href="https://co.linkedin.com/company/mineros-s.a." data-aos="flip-down" target="_blank"><span><i class="text-white fab fa-linkedin-in"></i></span></a>
            </div>
        </div>
    </div>
</header>
<section class="new_banner">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-md-6 col-lg-4 col_left bg-fern d-flex justify-content-end align-items-center">
                <img src="/portals/0/Images/NosotrosQuienesSomos/adorno-baner.png" srcset="/portals/0/Images/NosotrosQuienesSomos/adorno-baner.svg" class="adorno" alt="Image de adorno">
                <div id="panneCaja" class="caja bg-white position-relative" data-aos="fade-up" runat="server">
                </div>
                <!--./caja-->
            </div>
            <!--./col_left-->
            <div class="col-12 col-md-6 col-lg-8 col_right">
                <div id="panneBannerImage" runat="server"></div>
                <!--<img src="https://mineros.com.co/Portals/0/skins/mineros/images/2x/portada-inversionista-junta.jpg" alt="">-->
            </div>
            <!--./col_right-->
            <div class="line-color"></div>
        </div>
        <!--./row-->
    </div>
    <!--./container-fluid-->
</section><!-- .new-banner -->
<section class="section-navigation bg-pumice aos-init aos-animate" data-aos="fade-left">
    <div class="container">
        <div id="dnn_panneBreadCrumb" runat="server">
        </div>
    </div>
    <!--.container-->
</section>
<div class="container content-section-aside">
    <div class="row">
        <div class="col-12 col-lg-8 column_content">
            <section id="info-block" class="section-info-block border-color-de-york" data-aos="fade-down">
                <div class="decoration bg-white text-center">
                    <!--<img src="img/icon-section-1.png" srcset="img/icon-section-1.svg" class="icon">-->
                    <svg style="width: 71px;height: 18px;">
                        <g style="">
                            <path d="M14.1,3l-3.9,4.6c-0.3-0.2-0.6-0.3-1-0.4l0.5-6.3c0-0.2-0.1-0.3-0.3-0.4c-0.2,0-0.3,0.1-0.4,0.3L8.6,7.2
                                C8.3,7.2,8,7.3,7.7,7.5L3.9,2.7C3.8,2.5,3.6,2.5,3.4,2.6C3.3,2.7,3.3,2.9,3.4,3.1l3.8,4.8C7,8.2,6.8,8.4,6.8,8.7L0.9,6.9
                                c-0.2-0.1-0.4,0-0.4,0.2c-0.1,0.2,0,0.4,0.2,0.4l5.9,1.9c0,0.5,0.2,1,0.5,1.4c0.8,1,2.2,1.1,3.2,0.3c0.1-0.1,0.2-0.2,0.2-0.2
                                c0.2-0.2,0.3-0.4,0.4-0.6l6.1-1.8c0.1,0,0.1,0,0.1-0.1c0.1-0.1,0.1-0.2,0.1-0.3c-0.1-0.2-0.2-0.3-0.4-0.2l-5.7,1.7
                                C11.2,9,11,8.5,10.7,8l3.9-4.6c0.1-0.1,0.1-0.3,0-0.5C14.4,2.9,14.2,2.9,14.1,3z" style="fill: #ACD68D;stroke: #ACD68D;"></path>
                            <path d="M41.1,3l-3.9,4.6c-0.3-0.2-0.6-0.3-1-0.4l0.5-6.3c0-0.2-0.1-0.3-0.3-0.4c-0.2,0-0.3,0.1-0.4,0.3l-0.5,6.4
                                c-0.3,0-0.6,0.2-0.9,0.3l-3.8-4.8c-0.1-0.1-0.3-0.2-0.5-0.1c-0.1,0.1-0.2,0.3-0.1,0.5l3.8,4.8c-0.2,0.2-0.4,0.5-0.4,0.8l-5.8-1.9
                                c-0.2-0.1-0.4,0-0.4,0.2c-0.1,0.2,0,0.4,0.2,0.4l5.9,1.9c0,0.5,0.2,1,0.5,1.4c0.8,1,2.2,1.1,3.2,0.3c0.1-0.1,0.2-0.2,0.2-0.2
                                c0.2-0.2,0.3-0.4,0.4-0.6l6.1-1.8c0.1,0,0.1,0,0.1-0.1c0.1-0.1,0.1-0.2,0.1-0.3c-0.1-0.2-0.2-0.3-0.4-0.2l-5.7,1.7
                                C38.2,9,38,8.5,37.7,8l3.9-4.6c0.1-0.1,0.1-0.3,0-0.5C41.4,2.9,41.2,2.9,41.1,3z"></path>
                            <path d="M68.1,3l-3.9,4.6c-0.3-0.2-0.6-0.3-1-0.4l0.5-6.3c0-0.2-0.1-0.3-0.3-0.4c-0.2,0-0.3,0.1-0.4,0.3l-0.5,6.4
                                c-0.3,0-0.6,0.2-0.9,0.3l-3.8-4.8c-0.1-0.1-0.3-0.2-0.5-0.1c-0.1,0.1-0.2,0.3-0.1,0.5l3.8,4.8c-0.2,0.2-0.4,0.5-0.4,0.8l-5.8-1.9
                                c-0.2-0.1-0.4,0-0.4,0.2c-0.1,0.2,0,0.4,0.2,0.4l5.9,1.9c0,0.5,0.2,1,0.5,1.4c0.8,1,2.2,1.1,3.2,0.3c0.1-0.1,0.2-0.2,0.2-0.2
                                c0.2-0.2,0.3-0.4,0.4-0.6l6.1-1.8c0.1,0,0.1,0,0.1-0.1c0.1-0.1,0.1-0.2,0.1-0.3c-0.1-0.2-0.2-0.3-0.4-0.2l-5.7,1.7
                                C65.2,9,65,8.5,64.7,8l3.9-4.6c0.1-0.1,0.1-0.3,0-0.5C68.4,2.9,68.2,2.9,68.1,3z"></path>
                        </g>
                    </svg>
                </div>
                <div class="row m-0 content-info" data-aos="fade-down">
                    <div id="idpanneTitle" class="col-12 col-md-5 border-color-de-york border-left-0 border-top-0 border-bottom-0" data-aos="fade-down" runat="server"></div>
                    <!--<div class="col col-12 col-md-5 border-color-de-york" data-aos="fade-down">
                            <h3 class="title mb-0 color-gray librefranklin-bold text-center text-md-left">
                                Descargas
                                <br>
                                en línea
                            </h3>
                        </div>-->
                    <div id="idpanneContent" class="col col-12 col-md-7 align-items-center d-flex" data-aos="fade-down" runat="server"></div>
                    <!--<div class="col col-12 col-md-7 align-items-center d-flex" data-aos="fade-down"></div>-->
                </div>
                <div id="panneButtom" class="btn-bottom" runat="server"></div>
                <!--<div class="btn-bottom">
                        <a class="btn-click librefranklin-bold text-white bg-jungle-green bg-hover-corn d-inline-flex align-items-center justify-content-center text-center" href="#">Haga click en el certificado que desea descargar:</a>
                    </div>-->
            </section>
            <section id="tags" class="section-tags">
                <div class="row">
                    <div class="col col-12 col-md-6 d-flex">
                        <div class="tag w-100 d-flex align-items-start justify-content-center text-center flex-wrap border-color-alto">
                            <img src="/Portals/0/skins/mineros/images/icon-tag-2.png" srcset="/Portals/0/skins/mineros/images/icon-tag-2.svg" class="icon" width="56" style="width: 56px; height: 56px;">
                            <div id="panneTag1Content" class="title librefranklin-bold color-nevada w-100" runat="server"></div>
                            <div id="panneTag1Buttom" class="" runat="server"></div>
                        </div>
                    </div>
                    <div class="col col-12 col-md-6 d-flex">
                        <div class="tag w-100 d-flex align-items-start justify-content-center text-center flex-wrap border-color-alto">
                            <img src="/Portals/0/skins/mineros/images/icon-tag-2.png" srcset="/Portals/0/skins/mineros/images/icon-tag-2.svg" class="icon" width="56" style="width: 56px; height: 56px;">
                            <div id="panneTag2Content" class="title librefranklin-bold color-nevada w-100" runat="server"></div>
                            <div id="panneTag2Buttom" class="" runat="server"></div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <div class="col-12 col-lg-4 aside pr-0 mt-0 pl-lg-0">
            <div class="row m-0 tarjetas">
                <div class="col-12 col-sm-6 col-lg-12 pr-0 trimestre pt-0">
                    <div id="ResultadosFinancieros" class="card rounded-0 border-0" data-aos="fade-right" runat="server"></div>
                </div>
                <div class="col-12 col-sm-6 col-lg-12 pr-0 boletines">
                    <div id="BoletinesDePrensa" class="card rounded-0 border-0" data-aos="fade-right" runat="server"></div>
                </div>
            </div>
            <!--/.tarjetas-->
        </div>
    </div>
</div>
<style>
.content-section-aside .section-info-block svg path {
    fill: #ACD68D;
    stroke: #ACD68D;
}

.content-section-aside .border-color-de-york {
    border-width: 2px;
    padding-bottom: 0;
    position: relative;
}

.border-color-de-york {
    border-width: 2px;
}

.btn-bottom div#dnn_panneButtom {
    min-height: auto !important;
}

@media (min-width: 768px) {
    .section-info-block .content-info .title {
        padding: 18px 0;
        padding-right: 25px;
    }
}

div#dnn_panneBreadCrumb .DnnModule {
    width: 100%;
    height: max-content;
}

.section-tags .tag .title {
    min-height: 50px;
}

div#dnn_panneTag1Buttom {
    width: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    height: 100%;
}

div#dnn_panneTag2Buttom {
    width: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    height: 100%;
}

div#dnn_panneTag1Buttom .DnnModule {
    width: 100%;
}

div#dnn_panneTag2Buttom .DnnModule {
    width: 100%;
}

@media screen and (min-width: 1200px) {
    .column_content {
        max-width: calc(100% - 280px);
        flex-basis: 100%;
    }
}

@media screen and (min-width: 1500px) {
    .section-baner .col {
        min-height: 40vh;
    }
}

@media screen and (max-width: 991.8px) {
    .aside .tarjetas .boletines {
        padding-top: 0;
    }

    .aside {
        max-width: 100%;
        margin: auto;
    }
}

/**---------------------------------------------------------
    section navigation
---------------------------------------------------------**/

.section-navigation .container nav .breadcrumb-item {
    display: flex;
}

.section-navigation .container nav .breadcrumb-item a {
    text-decoration: none;
    font-size: 1rem;
    line-height: 19.2px;
    letter-spacing: 0rem;
}

.section-navigation .container nav .breadcrumb-item.active {
    text-decoration: none;
    font-size: 1rem;
    line-height: 19.2px;
    letter-spacing: 0rem;
}

@media screen and (max-width: 575px) {
    .section-navigation .container nav .breadcrumb-item.active {
        flex-wrap: nowrap;
        align-items: center;
    }
}

@media screen and (max-width: 575px) {
    .section-navigation .container nav .breadcrumb-item {
        flex-wrap: nowrap;
        align-items: center;
    }
}

.section-navigation .container nav ol.breadcrumb {
    display: flex;
}

@media screen and (max-width: 575.8px) {
    #section-6 .col-img-gall {
        margin-top: 0;
    }

    #section-6 .tittle-section-6 {
        max-width: 100%;
        width: 100%;
    }

    #SliderResponsive.section-6-content-box {
        margin-top: 35px;
    }
}
</style>
<div id="ContentPane" runat="server"></div>
<footer class="bg-mineral-green" data-aos="fade-down">
    <div id="panneFooter" runat="server"></div>
</footer>
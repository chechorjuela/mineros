<%@ Control Language="c#" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>
<%@ Register TagPrefix="dnn" TagName="USER" Src="~/Admin/Skins/user.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGIN" Src="~/Admin/Skins/Login.ascx" %>
<%@ Register TagPrefix="dnn" TagName="STYLES" Src="~/Admin/Skins/Styles.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LINKTOMOBILE" Src="~/Admin/Skins/LinkToMobileSite.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LANGUAGE" Src="~/Admin/Skins/Language.ascx" %>
<%@ Register TagPrefix="dnn" TagName="SEARCH" Src="~/Admin/Skins/Search.ascx" %>
<%@ Register TagPrefix="dnn" TagName="META" Src="~/Admin/Skins/Meta.ascx" %>
<%@ Register TagPrefix="dnn" TagName="jQuery" Src="~/Admin/Skins/jQuery.ascx" %>
<%@ Register TagPrefix="dnn" TagName="MENU" Src="~/DesktopModules/DDRMenu/Menu.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGO" Src="~/Admin/Skins/Logo.ascx" %>
<dnn:META ID="META1" runat="server" Name="viewport" Content="width=device-width,initial-scale=1, maximum-scale=1" />
<dnn:META runat="server" Name="description" Content="Mineros" />
<!-- Css Includes -->
<dnn:DnnCssInclude runat="server" ID="cssBootstrap" FilePath="css/bootstrap.min.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssInternaBoletines" FilePath="css/InternaBoletines.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssFonts" FilePath="css/fonts.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssclassFonts" FilePath="css/class-fonts.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssLighSlider" FilePath="css/lightSilder.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssColors" FilePath="css/colors.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssStyles" FilePath="css/styles.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssAos" FilePath="css/aos.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssHover" FilePath="css/hover-min.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssInfo" FilePath="css/informacion-financiera.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssBanner" FilePath="css/style-banner.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssFontawesome" FilePath="css/fonts/fontawesome/css/all.css" PathNameAlias="SkinPath" />

<body class="nosotros_gobierno">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5ZKGB6Z" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <div id="hed-top-green" class="bg-mineral-green d-flex">       
        <div id="bar-lang" class="nav justify-content-around" data-aos="fade-left">
            <dnn:LANGUAGE runat="server" ID="dnnLANGUAGE" ShowLinks="True" ShowMenu="False" ItemTemplate=''/>
        </div>
        <div id="headerBanderasResponsive" runat="server"></div>
    </div>
    <header id="masthead" class="site-header js" role="banner" data-aos="fade-down">
        <div class="navigation-top bg-menu-trans">
            <div class="wrap d-flex  justify-content-between">
                <div id="logo-header" class="logo-header text-center position-relative">
                    <dnn:LOGO runat="server" ID="dnnLOGO" />
                    <div class="d-flex justify-content-center align-items-center line-logo">
                        <span class="d-flex"></span>
                    </div>
                </div>
                <div class="d-flex box-menu">
                    <dnn:MENU ID="MENU" MenuStyle="Menus/MainMenu" runat="server" NodeSelector="*" />
                </div>
            </div>
            <div id="headerBanderasRRSS" runat="server"></div>
        </div>
    </header>
    <!--./site-header-->
<style>
/*2 cuadros de decoración, una línea divisora y 2 párrafos*/
.info_pp .title:before{background:#ACD68D;left:0px;top:7px;}
.info_pp .title:after{background:#CEA800;opacity:0.77;left:30px;top:65px;}
.info_pp .title:before, .info_pp .title:after{content:"";width:80px;height:110px;position:absolute;display:inline-block;z-index:1;}
.info_pp .title{width:200px;padding-right:10px;color:#62A472;font-family:"Franklin Gothic Medium", arial;font-weight:700;font-size:3.75rem;line-height:1;letter-spacing:0rem;padding-top:35px;position:relative;}
.info_pp .text{width:calc(100% - 200px);padding-left:47px;border-left:1px solid #CEA800;color:#606060;font-family:"LibreFranklin", sans-serif;font-weight:400;font-size:1.125rem;line-height:1.1666666667;letter-spacing:0rem;padding-top:14px;padding-bottom:10px;}
p{font-family:"LibreFranklin",sans-serif;font-weight:400;font-size:1rem;line-height:1.3125rem;color:#606060;}
@media (min-width:1023px){
    p{
        text-align:justify;
    }
    .mb-0.librefranklin-bold.text-capitalize.color-scorpion{
        text-align:left;
    }
}
@media screen and (max-width: 991.8px){
    .info_pp{
        margin-bottom:0px!important;
        margin-top:0px!important;
    }
    .info_pp .title{max-width:100%;width:100%;}
    .info_pp .text{max-width:100%;width:100%;border-left:0;}
}
/*Icono con título*/
.content_info .text{color:#606060;font-size:1rem;line-height:1.3125rem;letter-spacing:0rem;font-family:"LibreFranklin", sans-serif;font-weight:400;}
.content_info .title_tag,.title_tag{background:#CEA800;min-height:61px;display:inline-flex;align-items:center;}
.content_info .title_tag .content_icon,.title_tag .content_icon{background:#62A472;display:inline-flex;align-items:center;justify-content:center;width:61px;min-width:66px;height:61px;min-height:100%;}
.content_info .title_tag .content_icon svg,.title_tag .content_icon svg{max-width:90%;max-height:90%;}
.content_info .title_tag .content_icon svg path,.title_tag .content_icon svg path{fill:#fff;}
.content_info .title_tag .text,.title_tag .text{color:#fff;padding:0 12px;font-size:1.25rem;line-height:1;letter-spacing:0rem;font-family:"LibreFranklin", sans-serif;font-weight:700;}
/*Contenido de Minas proyectos y targets*/
.minas-proyectos-contenido{
    margin-top:30px;
}
.minas-proyectos-contenido-2{
    margin-left: 60px;
    margin-bottom: -20px;
}
.minas-proyectos-contenido-3{
    margin-left:60px;
}
.minas-proyectos-contenido-4{
    margin-left:90px;
    margin-top:10px;
}
@media (max-width:991px){
    .minas-proyectos-contenido-2{
        margin-left:0px;
    }
    .minas-proyectos-contenido-3{
        margin-left:0px;
    }
    .minas-proyectos-contenido-4{
        margin-left:30px;
    }
}
.minas-proyectos-contenido-2 .title-variant{
    box-shadow:none;
    padding-bottom:0px;
    padding-left:0px;
    margin-bottom:0px;
    font-size:1rem;
    padding-top:0px;
}
.minas-proyectos-contenido-4 .title-variant{
    margin-bottom:-7px;
}
/*Varición de título*/
.title-variant{color:#536D61;font-size:1.25rem;line-height:1.2;letter-spacing:0rem;font-family:"LibreFranklin", sans-serif;font-weight:700;margin-bottom:20px;padding-top:15px;padding-bottom:8px;padding-left:18px;box-shadow:35px 0px 0px 0px #ACD68D inset;display:flex;align-items:center;min-height:62px;}
/*Estilos de la lista*/
.list{margin:20px 0;padding:0;padding-left:25px;display:block;}
.list-2{margin:0;}
.list:not(.list_2){column-count:1;column-gap:97px;}
@media (max-width: 767px){
    .list:not(.list_2){column-count:1;}
}
.list .item{padding-left:25px;display:block;margin-bottom:10px;color:#606060;font-size:1rem;line-height:1.3125rem;letter-spacing:0rem;font-family:"LibreFranklin", sans-serif;font-weight:400;position:relative;}
.list-2 .item{font-size:0.85rem}
.list .item:not(.not_before):before{content:"";position:absolute;width:12px;height:12px;display:inline-block;top:3px;left:0;border-radius:100%;background:#62A472;}
.list .list .item:not(.not_before):before {
    content: "";
    background: #62a472;
    width: 9px;
    height: 9px;
    top: 5px;
    left: 5px;
}
.list-2 .item:not(.not_before):before {
    content: "";
    position: absolute;
    width: 9px;
    height: 9px;
    display: inline-block;
    top: 5px;
    left: 3px;
    border-radius: 100%;
    background: #62A472;
}
.list-2 .list .item:not(.not_before):before {
    content: "";
    background: #62a472;
    width: 9px;
    height: 9px;
    top: 5px;
    left: 5px;
}
.list-2 .list{
    margin:0;
    margin-top:8px;
}
/*Estilos de la tabla*/
.table thead th{
    vertical-align:middle;
}
.table-variant-1{
    padding-left:60px;
    margin-bottom:50px;
    padding-right:30px;
}
.table-variant-1 table{
    margin-top:40px;
}
@media (max-width:991px){
    .table-variant-1{
        padding-left:5px;
        overflow: hidden;
        overflow-x: scroll;
    }
}
.table-variant-1 .table-bordered td, .table-variant-1 .table-bordered th, .table-variant-1 .table-bordered, .table-variant-1 .border-color-alto{
    border:none;
}
.table-variant-1 table tr:last-child th:first-child {
    border-top-left-radius: 100px;
    border-bottom-left-radius: 100px;
}
.table-variant-1 table tr:last-child th:last-child {
    border-top-right-radius: 100px;
    border-bottom-right-radius: 100px;
}
.table-variant-1 .table td, .table-variant-1 .table th{
    padding-top:7px;
    padding-bottom:7px;
}
.table-variant-1 .table tbody td{
    padding-top:3px;
    padding-bottom:3px;
}
.table-variant-1 .space-vertical-1 hr{
    border-top:2px solid #68b58d;
    width:95%;
    margin:auto;
    margin-top:20px;
}
.table-variant-1 .space-vertical-2 hr{
    border-top:2px solid #68b58d;
    width:95%;
    margin:auto;
}
.table-variant-1 .country,
.table-variant-1 .country2{
    position:relative;
    padding-left:0;
    padding-right:0;
}
.table-variant-1 .country div{
    width:47px;
    height:47px;
    overflow:hidden;
    position:absolute;
    left:0;
    top: -8px;
    border-radius:100px;
}
.table-variant-1 .country2 div div{
    width:47px;
    height:47px;
    overflow:hidden;
    border-radius:100px;
    margin-right: 20px;
}
@media (max-width:991px){
    .table-variant-1 .country2 div div{
        margin-right: 0px;
    }
}
.table-variant-1 .country div img,
.table-variant-1 .country2 div div img{
    width:auto;
    height:100%;
    margin-left:-13.5px;
}
.table-variant-1 .country span,
.table-variant-1 .country2 span{
    margin-left: 48px;
}
@media (min-width:1024px){
    .table-variant-1 .country span,
    .table-variant-1 .country2 span{
        margin-left: 15px;
    }
}
.table-variant-1 .country2 > div > div{
    position:absolute;
    left:0;
    top: -28px;
}
.table-variant-1 .country2 span{
    display: block;
    margin-top: -16px!important;
}
.table-variant-1 td, .table-variant-1 span, .table-variant-1 th{
    color:#4b4b4d;
}
.bg-custom{
    background-color: rgb(206 168 0);
}
.aclaratoria{
    font-size:0.9rem;
    margin-top:50px;
}
@media(max-width:991px){
    .table-variant-1 th:nth-of-type(4) span{
        min-width: 90px;
        display: inline-block;
    }
    .table-variant-1 th:nth-of-type(6) span{
        min-width: 130px;
        display: inline-block;
    }
}
.table-variant-2{
    margin-top:0px;
    margin-bottom:30px;
}
.table-variant-2 thead table{
    margin-top:0px;
}
/*
.table-variant-2 thead th:nth-of-type(3){
    min-width:260px;
}
.table-variant-2 thead th:nth-of-type(4){
    min-width:336px;
}*/
.table-variant-2 tbody td:nth-of-type(3),
.table-variant-2 tbody td:nth-of-type(4),
.table-variant-2 tbody td:nth-of-type(5){
    min-width:86.66px;
}
.table-variant-2 tbody td:nth-of-type(6),
.table-variant-2 tbody td:nth-of-type(7),
.table-variant-2 tbody td:nth-of-type(8){
    min-width:108px;
}
.table-variant-2 thead th:nth-of-type(3),
.table-variant-2 thead th:nth-of-type(4){
    padding-left:0px;
    padding-right:0px;
    border-left:2px solid #25a671;
}
.table-variant-2 thead th:nth-of-type(3) tbody td{
    min-width:81.66px;
}
.table-variant-2 thead th:nth-of-type(4) tbody td{
    min-width:108px;
}
.table-variant-2 thead th{
    padding-top:0px!important;
    padding-bottom:0px!important;
}
.table-variant-2 :not(thead) tbody td:nth-of-type(3),
.table-variant-2 :not(thead) tbody td:nth-of-type(6){
    border-left:2px solid #25a671;
}
.table-variant-2 thead tbody td:nth-of-type(3),
.table-variant-2 thead tbody td:nth-of-type(6){
    border-left:none;
}
.table-variant-2 .border-color-alto tr:nth-last-child(1),
.table-variant-2 .cierre-total{
    border-top:2px solid #25a671;
}
.table-variant-2 .border-color-alto tr:nth-last-child(1) td
.table-variant-2 .cierre-total td{
    padding-top:12px;
}
.table-variant-2 .border-color-alto tr:nth-last-child(2),
.table-variant-2 .border-color-alto tr:nth-last-child(2) td{
    padding-bottom:0px;
}
.table-variant-2 thead th{
    font-size:0.95rem;
}
.table-variant-2 .border-color-alto tr td{
    font-size:0.82rem;
    vertical-align:middle;
}
.table-variant-2 thead th:nth-of-type(1),
.table-variant-2 tbody td:nth-of-type(1){
    padding-left:30px;
    min-width:240px;
}
.table-variant-2 thead tbody td:nth-of-type(1){
    padding-left:0px;
}
.table-variant-2 .space-vertical-1 td {
    padding-top: 20px!important;
}
.table-variant-2 > p{
    margin-bottom:0px;
}
/*Página propiedades*/
.propiedades-tab{
    border-bottom:none;
    margin-top:40px;
    display:flex;
    justify-content:space-around;
    padding: 24px 14px;
    box-shadow: 0 0 10px 0 #bfbfbf;
    margin-left:0px;
}
.propiedades-tab .nav-link{
    border:none;
    font-size: 1rem;
    font-family: "LibreFranklin",sans-serif;
    font-weight: 400;
    line-height: 1.3125rem;
    color: #536d61;
}
.propiedades-tab .nav-link.active{
    font-weight:bold;
    color: #536d61;
    position:relative;
}
@media (min-width:1023px){
    .propiedades-tab .nav-link.active:after{
        content: "";
        position: absolute;
        width: 35px;
        height: 35px;
        transform: rotateZ(45deg);
        border-radius: 5px;
        left: 0;
        right: 0;
        margin: auto;
        bottom: -40px;
        background: #fff;
        box-shadow: 4px 4px 5px 0px #e4e4e4;
    }
}
@media (max-width:767px){
    .propiedades-tab{
        justify-content:flex-start;
    }
    .propiedades-tab .nav-item:nth-last-of-type(1) .nav-link{
        position:relative;
    }
    .propiedades-tab .nav-item:nth-last-of-type(1) .nav-link:after{
        content: "";
        position: absolute;
        width: 35px;
        height: 35px;
        transform: rotateZ(45deg);
        border-radius: 5px;
        left: 0;
        right: 0;
        margin: auto;
        bottom: -40px;
        background: #fff;
        box-shadow: 4px 4px 5px 0px #e4e4e4;
    }
}
.propiedades-tab .icon-tab{
    width:40px;
    height:40px;
    border-radius:100px;
    object-fit: cover;
    margin-right:10px;
}
@media (min-width:850px){
    .video-yt{
        margin-left:0px;
    }
}
@media (max-width:425px){
    .propiedades-tab .nav-link{
        font-size:0.9rem;
    }
    .propiedades-tab .icon-tab{
        width:30px;
        height:30px;
    }
}
a{
    color:#62A472;
}
.list-group-item{
    max-width: 360px;
    margin: auto;
}
</style>
    <section class="new_banner">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-md-6 col-lg-4 col_left bg-fern d-flex justify-content-end align-items-center">
                    <img src="/portals/0/Images/NosotrosQuienesSomos/adorno-baner.png" srcset="/portals/0/Images/NosotrosQuienesSomos/adorno-baner.svg" class="adorno" alt="Image de adorno">
                    <div id="panneCaja" class="caja bg-white position-relative" data-aos="fade-up" runat="server">
                    </div>
                    <!--./caja-->
                </div>
                <!--./col_left-->
                <div class="col-12 col-md-6 col-lg-8 col_right">
                    <div id="panneBannerImage" runat="server"></div>
                </div>
                <!--./col_right-->
                <div class="line-color"></div>
            </div>
            <!--./row-->
        </div>
        <!--./container-fluid-->
    </section><!-- .new-banner -->
    <section class="section-navigation bg-pumice" data-aos="fade-left">
        <div class="container">
            <div id="panneBreadCrumb1" runat="server"></div>
        </div>
        <!--.container-->
    </section><!-- /.section-navigation-->
    <div class="container content-section-aside">
        <div class="row">
            <div class="col-12 col-lg-12">
                <section id="info-pp" class="section-info-pp" data-aos="fade-down">
                    <div id="panneTitle" runat="server"></div>
                    <div id="panneText1" runat="server"></div>
                    <div id="panneText2" runat="server"></div>
                    <div id="panneText3" runat="server"></div>
                    <div class="content-descargra mt-3" data-aos="fade-down">
                        <div id="panneTitleDescarga" runat="server"></div>
                        <div class="">
                            <div id="panneLink1" runat="server"></div>
                            <div id="panneLink2" runat="server"></div>
                            <div id="panneLink3" runat="server"></div>
                        </div>
                    </div>
                    <div class="content-descargra " data-aos="fade-down">
                        <div id="panneTitleDescargab" runat="server"></div>
                        <div class="content-link border-color-alt m-0 p-0">
                            <div id="panneLink1a" runat="server"></div>
                        </div>
                        <div id="test" runat="server" class=""></div>
                    </div>
                </section>
            </div>
            <div class="col-12 col-lg-3 pr-sm-0 d-none">
                <aside id="aside" class="aside">
                    <!--.indicadores-->
                    <div class="row m-0 tarjetas">
                        <div class="col-12 col-sm-6 col-lg-12 pr-0 trimestre">
                            <div id="panneInfoFinanciera" runat="server"></div>
                        </div>
                        <!--.col-12-->
                        <div class="col-12 col-sm-6 col-lg-12 pr-0 boletines">
                            <div id="panneBoletin" runat="server"></div>
                        </div>
                        <!--.col-12-->
                    </div>
                    <!--.tarjetas-->
                    <div class="row m-0 sucribir" style="display: none;">
                        <div class="col-12 col-sm-9 col-lg-12 m-auto pr-0">
                            <div class="card rounded-0 bg-jungle-green" data-aos="fade-up-right">
                                <div class="card-body rounded-0 text-center">
                                    <div data-icon="a" class="icon" style="color: #fff;"></div>
                                    <h3 class="h4 mb-0 text-center text-white librefranklin-bold" data-aos="fade-up-left">Suscríbase aquí</h3>
                                    <p class="h4 mb-0 card-text text-white text-center librefranklin-regular" data-aos="fade-up-right">para recibir información corporativa de interés en su correo electrónico.</p>
                                    <div class="card-footer border-0 rounded-0 bg-transparent">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1" class="sr-only" data-aos="fade-right">Email address</label>
                                            <input type="email" class="form-control rounded-0" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Inscríbete" data-aos="fade-right">
                                        </div>
                                        <button type="submit" class="btn btn-link text-decoration-none librefranklin-bold text-white d-none">Submit</button>
                                    </div>
                                    <!--.card-footer-->
                                </div>
                                <!--.card-body-->
                            </div>
                            <!--.card-->
                        </div>
                        <!--.col-12-->
                    </div>
                    <!--.sucribir-->
                </aside>
            </div>
        </div>
    </div>
    <div id="contentPane" runat="server"></div>
    <!-- Modal -->
    <div class="modal fade page_construct" id="inconstruction" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="inconstructionLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="inconstructionLabel"><span>Pop up para</span> sección en inglés</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="card-text mb-0">En construcción....</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary px-4 rounded-0" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <footer class="bg-mineral-green" data-aos="fade-down">
        <div id="panneFooter" runat="server"></div>
    </footer>
    <!-- JS Includes -->
    <dnn:DnnJsInclude runat="server" ID="jsPopper" FilePath="js/popper.min.js" PathNameAlias="SkinPath" />
    <dnn:DnnJsInclude runat="server" ID="jsSlider" FilePath="js/lightslider.js" PathNameAlias="SkinPath" />
    <dnn:DnnJsInclude runat="server" ID="jsBootstrap" FilePath="js/bootstrap.min.js" PathNameAlias="SkinPath" />
    <dnn:DnnJsInclude runat="server" ID="jsAos" FilePath="js/aos.js" PathNameAlias="SkinPath" />
    <dnn:DnnJsInclude runat="server" ID="jsCustom" FilePath="js/custom.js" PathNameAlias="SkinPath" />
    <dnn:DnnJsInclude runat="server" ID="jsFiliales" FilePath="js/filiales_slider.js" PathNameAlias="SkinPath" />
    <dnn:DnnJsInclude runat="server" FilePath="~/Resources/Libraries/kendo-ui-angular/js/kendo.all.min.js" Priority="10" />
    <dnn:DnnJsInclude runat="server" FilePath="~/Resources/Libraries/kendo-ui-angular/js/kendo.aspnetmvc.min.js" Priority="10" />
    <dnn:DnnJsInclude runat="server" FilePath="~/Resources/Libraries/angularAutocomplete/multiple-select.min.js" Priority="10" />
    
</body>
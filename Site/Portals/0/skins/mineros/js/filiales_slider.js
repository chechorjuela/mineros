jQuery(window).ready(function() {
    /*
     * Arctive slider
     */
    var slider = jQuery('#SliderResponsive').lightSlider({
        slideMargin: 20,
        // slideWidth:200,
        loop: true,
        responsive: [{
                breakpoint: 991,
                settings: {
                    item: 2,
                    slideMove: 1,
                }
            },
            {
                breakpoint: 575,
                settings: {
                    item: 1,
                    slideMove: 1,
                }
            }
        ]
    });
    var width_ = (screen.width > window.outerWidth) ? window.outerWidth : screen.width;
    if (width_ >= 992) {
        slider.destroy();
    } else if (!slider.lightSlider) {
        slider = jQuery('#SliderResponsive').lightSlider({
            item: 3,
            autoWidth: true,
            slideMargin: 20,
            slideWidth: 200,
            loop: true,
            responsive: [{
                breakpoint: 575,
                settings: {
                    item: 1,
                    slideMove: 1,
                }
            }]
        });
    }

    $(window).resize(function() {
        console.log(screen.width);
        var width_ = (screen.width > window.outerWidth) ? window.outerWidth : screen.width;
        if (width_ <= 991) {
            if (!slider.lightSlider) {
                slider = jQuery('#SliderResponsive').lightSlider({
                    item: 3,
                    slideMargin: 20,
                    slideWidth: 200,
                    loop: true,
                    responsive: [{
                        breakpoint: 575,
                        settings: {
                            item: 1,
                            slideMove: 1,
                        }
                    }]
                });
            }
        } else {
            slider.destroy();
        }
    });
});
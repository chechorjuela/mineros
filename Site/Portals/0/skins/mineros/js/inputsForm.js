/*Versión en español*/

function checkNumber(e) {
    tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite
    if (tecla == 8) {
        return true;
    }

    // Patron de entrada, en este caso solo acepta numeros y letras
    patron = /[0-9]/;
    tecla_final = String.fromCharCode(tecla);
    if(patron.test(tecla_final))
       {
            document.getElementById("dnn_ctr1933_Feedback_valTelephone").style.display='none' ;
       }
    else
       document.getElementById("dnn_ctr1933_Feedback_valTelephone").style.display='inline' ;
    return patron.test(tecla_final);
}
$(document).ready(function () {
	/*Add subject because this is field text*/
	$position = $('#dnn_ctr1933_Feedback_txtSubject2').parent();
	$('#dnn_ctr1933_Feedback_txtSubject2').remove();
	$position.before('<input name="dnn$ctr1933$Feedback$txtSubject2" type="text" maxlength="200" id="dnn_ctr1933_Feedback_txtSubject2" class="dnnFormRequired NormalTextBox Feedback_ControlWidth2 form-control" placeholder="Asunto *">');

	var linkSend = $("#dnn_ctr1933_Feedback_cmdSend").attr("href");
	$("#dnn_ctr1933_Feedback_cmdSend").removeAttr("href");
	$(".dnnFormSectionHead").remove();
	$("#dnn_ctr1933_Feedback_txtEmail").attr('placeholder','Email *');
	$("#dnn_ctr1933_Feedback_txtName").attr('placeholder','Nombre completo *');
	$("#dnn_ctr1933_Feedback_txtBody").attr('placeholder','Mensaje');
	$("#dnn_ctr1933_Feedback_txtTelephone").attr('placeholder','Teléfono');
	$("#dnn_ctr1933_Feedback_txtCity").attr('placeholder','Ciudad *');
	//$("#dnn_ctr1933_Feedback_cboCategory option:selected").text('--- Seleccione un asunto ---');
	$("<p class='form-footer bg-fern text-white text-center textInformation'>Tenga en cuenta que al utilizar sus datos personales para contactarnos, autoriza y acepta las políticas de Habeas Data de Mineros S.A. </p><br/	><br/><br/>").insertBefore("#dnn_ctr1933_Feedback_cmdSend");
	$("ul.dnnActions li:first").css({"text-align":"center","width":"100%"});
	$("#dnn_ctr1933_Feedback_cmdSend").text("Enviar");
	$("#dnn_ctr1933_Feedback_cmdSend").removeClass();
	$("#dnn_ctr1933_Feedback_cmdSend").addClass("buttonSendForm form-footer bg-fern text-white text-center");
	$(".Feedback_CharCount").remove();
	$(".dnnLabel").remove();
	$("input").addClass("form-control");
	$("select").addClass("form-control custom-select");
	$("textarea").addClass("form-control");
	$(".dnnFormItem").removeClass().addClass("form-group");
	$("#dnn_ctr1933_Feedback_valSummary").remove();

	$("#dnn_ctr1933_Feedback_divFeedbackFormContent fieldset div:first").insertAfter("#dnn_ctr1933_Feedback_divName");
	$("#dnn_ctr1933_Feedback_divCity").remove();$("#dnn_ctr1933_Feedback_divTelephone").remove();
	$("<div class='row' id='dnnRowTelpCity'><div class='col-12 col-md-6' id='colTel'><div id='dnn_ctr1933_Feedback_divTelephone' class='Feedback_Field'><div class='form-group'><input name='dnn$ctr1933$Feedback$txtTelephone' type='text' maxlength='16' id='dnn_ctr1933_Feedback_txtTelephone' class='NormalTextBox Feedback_ControlWidth form-control' placeholder='Teléfono'><span id='dnn_ctr1933_Feedback_valTelephone' class='dnnFormMessage dnnFormError' style='display:none;'>Telephone is required.</span><span id='dnn_ctr1933_Feedback_valTelephone2' class='dnnFormMessage dnnFormError' style='display:none;'>Telephone must be valid format.</span> </div> </div></div><div class='col-12 col-md-6' id='colCity'><div id='dnn_ctr1933_Feedback_divCity' class='Feedback_Field'><div class='form-group'>		        <input name='dnn$ctr1933$Feedback$txtCity' type='text' maxlength='35' id='dnn_ctr1933_Feedback_txtCity' class='dnnFormRequired NormalTextBox Feedback_ControlWidth2 form-control' placeholder='Ciudad *'><span id='dnn_ctr1933_Feedback_valCity' class='dnnFormMessage dnnFormError' style='display:none;'>City is required.</span></div></div></div></div>").insertAfter("#dnn_ctr1933_Feedback_divEmail");
	$("<div class='form-group'><div class='custom-control custom-checkbox'><input type='checkbox' class='custom-control-input' id='customControlAutosizing'><label class='custom-control-label librefranklin-regular color-very-dark-gray' for='customControlAutosizing'>Aceptar las <a class=' color-very-dark-gray librefranklin-regular' href='/Portals/0/Politica_Corporativa_%20datos%20personales%2020200114%20%281%29.pdf' target='_blank'>Política de Manejo de Datos</a></label></div></div>").insertAfter("#dnn_ctr1933_Feedback_divMessage");
	$("#dnn_ctr1933_Feedback_txtTelephone").attr('type','tel');
	$("#dnn_ctr1933_Feedback_valTelephone").text("Solo se aceptan números");
	$("#dnn_ctr1933_Feedback_valName").text("Nombre es requerido");
	$("#dnn_ctr1933_Feedback_valEmail1").text("Email es requerido");
	$("#dnn_ctr1933_Feedback_valEmail2").text("Email no tiene un formato valido");
	$("#dnn_ctr1933_Feedback_valCity").text("Ciudad es requerida");
	//$("#dnn_ctr1933_Feedback_valCategory").text("Debe seleccionar una opción");
	$("#dnn_ctr1933_Feedback_cmdSend").click(function(){
		var nombre = $("#dnn_ctr1933_Feedback_txtName").val();
		var mailc = $("#dnn_ctr1933_Feedback_txtEmail").val();
		var categoria = $("#dnn_ctr1933_Feedback_txtSubject2").val();
		var ciudad    = $("#dnn_ctr1933_Feedback_txtCity").val();
		console.log("longitud de ciudad: "+ciudad.length);
		if($("#customControlAutosizing").is(':checked') && nombre.length>0 && mailc.length>0 && categoria.length>0 && ciudad.length>0)
			WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("dnn$ctr1933$Feedback$cmdSend", "", true, "FeedbackForm_1933", "", false, true))
		else
			{
				if(!$("#customControlAutosizing").is(':checked'))
					{
						$(".custom-control-label").addClass('uncheck');
						$(".custom-control-label").removeClass('check');

					}
				else
				{
					$(".custom-control-label").addClass('check');
						$(".custom-control-label").removeClass('uncheck');
				}
				if(nombre.length>0)
					$("#dnn_ctr1933_Feedback_valName").css('display','none');
				else
					$("#dnn_ctr1933_Feedback_valName").css('display','inline');
				if(mailc.length>0)
					$("#dnn_ctr1933_Feedback_valEmail1").css('display','none');
				else
					$("#dnn_ctr1933_Feedback_valEmail1").css('display','inline');
				if(categoria.length>0)
					$("#dnn_ctr1933_Feedback_valCategory").css('display','none');
				else
					$("#dnn_ctr1933_Feedback_valCategory").css('display','inline');
				if(ciudad.length>0)
					$("#dnn_ctr1933_Feedback_valCity").css('display','none');
				else
					$("#dnn_ctr1933_Feedback_valCity").css('display','inline');
			}
	});
	$("#dnn_ctr1933_Feedback_txtTelephone").keypress(function(e){
		 tecla = (document.all) ? e.keyCode : e.which;

	    //Tecla de retroceso para borrar, siempre la permite
	    if (tecla == 8) {
	        return true;
	    }

	    // Patron de entrada, en este caso solo acepta numeros y letras
	    patron = /[0-9]/;
	    tecla_final = String.fromCharCode(tecla);
	    if(patron.test(tecla_final))
	       {
	            document.getElementById("dnn_ctr1933_Feedback_valTelephone").style.display='none' ;
	       }
	    else
	       document.getElementById("dnn_ctr1933_Feedback_valTelephone").style.display='inline' ;
	    return patron.test(tecla_final);
	})
})


/*Versión en inglés*/

function checkNumber(e) {
    tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite
    if (tecla == 8) {
        return true;
    }

    // Patron de entrada, en este caso solo acepta numeros y letras
    patron = /[0-9]/;
    tecla_final = String.fromCharCode(tecla);
    if(patron.test(tecla_final))
       {
            document.getElementById("dnn_ctr4036_Feedback_valTelephone").style.display='none' ;
       }
    else
       document.getElementById("dnn_ctr4036_Feedback_valTelephone").style.display='inline' ;
    return patron.test(tecla_final);
}

$(document).ready(function () {
	/*Add subject because this is field text*/
	$position = $('#dnn_ctr4036_Feedback_txtSubject2').parent();
	$('#dnn_ctr4036_Feedback_txtSubject2').remove();
	$position.before('<input name="dnn$ctr4036$Feedback$txtSubject2" type="text" maxlength="200" id="dnn_ctr4036_Feedback_txtSubject2" class="dnnFormRequired NormalTextBox Feedback_ControlWidth2 form-control" placeholder="Subject *">');

	var linkSend = $("#dnn_ctr4036_Feedback_cmdSend").attr("href");
	$("#dnn_ctr4036_Feedback_cmdSend").removeAttr("href");
	$(".dnnFormSectionHead").remove();
	$("#dnn_ctr4036_Feedback_txtEmail").attr('placeholder','E-mail *');
	$("#dnn_ctr4036_Feedback_txtName").attr('placeholder','Full name *');
	$("#dnn_ctr4036_Feedback_txtBody").attr('placeholder','Message');
	$("#dnn_ctr4036_Feedback_txtTelephone").attr('placeholder','Phone');
	$("#dnn_ctr4036_Feedback_txtCity").attr('placeholder','City *');
	//$("#dnn_ctr4036_Feedback_cboCategory option:selected").text('--- Select a subject ---');
	$("<p class='form-footer bg-fern text-white text-center textInformation'>Please note that by using your personal data to contact us, you authorize and accept the policies of Habeas Data de Mineros S.A. </p><br/	><br/><br/>").insertBefore("#dnn_ctr4036_Feedback_cmdSend");
	$("ul.dnnActions li:first").css({"text-align":"center","width":"100%"});
	$("#dnn_ctr4036_Feedback_cmdSend").text("Send");
	$("#dnn_ctr4036_Feedback_cmdSend").removeClass();
	$("#dnn_ctr4036_Feedback_cmdSend").addClass("buttonSendForm form-footer bg-fern text-white text-center");
	$(".Feedback_CharCount").remove();
	$(".dnnLabel").remove();
	$("input").addClass("form-control");
	$("select").addClass("form-control custom-select");
	$("textarea").addClass("form-control");
	$(".dnnFormItem").removeClass().addClass("form-group");
	$("#dnn_ctr4036_Feedback_valSummary").remove();

	$("#dnn_ctr4036_Feedback_divFeedbackFormContent fieldset div:first").insertAfter("#dnn_ctr4036_Feedback_divName");
	$("#dnn_ctr4036_Feedback_divCity").remove();$("#dnn_ctr4036_Feedback_divTelephone").remove();
	$("<div class='row' id='dnnRowTelpCity'><div class='col-12 col-md-6' id='colTel'><div id='dnn_ctr4036_Feedback_divTelephone' class='Feedback_Field'><div class='form-group'><input name='dnn$ctr4036$Feedback$txtTelephone' type='text' maxlength='16' id='dnn_ctr4036_Feedback_txtTelephone' class='NormalTextBox Feedback_ControlWidth form-control' placeholder='Phone'><span id='dnn_ctr4036_Feedback_valTelephone' class='dnnFormMessage dnnFormError' style='display:none;'>Telephone is required.</span><span id='dnn_ctr4036_Feedback_valTelephone2' class='dnnFormMessage dnnFormError' style='display:none;'>Telephone must be valid format.</span> </div> </div></div><div class='col-12 col-md-6' id='colCity'><div id='dnn_ctr4036_Feedback_divCity' class='Feedback_Field'><div class='form-group'>		        <input name='dnn$ctr4036$Feedback$txtCity' type='text' maxlength='35' id='dnn_ctr4036_Feedback_txtCity' class='dnnFormRequired NormalTextBox Feedback_ControlWidth2 form-control' placeholder='City *'><span id='dnn_ctr4036_Feedback_valCity' class='dnnFormMessage dnnFormError' style='display:none;'>City is required.</span></div></div></div></div>").insertAfter("#dnn_ctr4036_Feedback_divEmail");
	$("<div class='form-group'><div class='custom-control custom-checkbox'><input type='checkbox' class='custom-control-input' id='customControlAutosizing'><label class='custom-control-label librefranklin-regular color-very-dark-gray' for='customControlAutosizing'>Accept the <a class=' color-very-dark-gray librefranklin-regular' href='/Portals/0/documentos/home/Policies%20handling%20data.pdf' target='_blank'>Data Management Policy</a></label></div></div>").insertAfter("#dnn_ctr4036_Feedback_divMessage");
	$("#dnn_ctr4036_Feedback_txtTelephone").attr('type','tel');
	$("#dnn_ctr4036_Feedback_valTelephone").text("Only numbers are accepted");
	$("#dnn_ctr4036_Feedback_valName").text("Name is required");
	$("#dnn_ctr4036_Feedback_valEmail1").text("E-mail is required");
	$("#dnn_ctr4036_Feedback_valEmail2").text("E-mail no holds a valid format");
	$("#dnn_ctr4036_Feedback_valCity").text("City is required");
	//$("#dnn_ctr4036_Feedback_valCategory").text("You must select an option");
	$("#dnn_ctr4036_Feedback_cmdSend").click(function(){
		var nombre = $("#dnn_ctr4036_Feedback_txtName").val();
		var mailc = $("#dnn_ctr4036_Feedback_txtEmail").val();
		var categoria = $("#dnn_ctr4036_Feedback_txtSubject2").val();
		var ciudad    = $("#dnn_ctr4036_Feedback_txtCity").val();
		//console.log("longitud de ciudad: "+ciudad.length);
		if($("#customControlAutosizing").is(':checked') && nombre.length>0 && mailc.length>0 && categoria.length>0 && ciudad.length>0)
			WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("dnn$ctr4036$Feedback$cmdSend", "", true, "FeedbackForm_4036", "", false, true))
		else
			{
				if(!$("#customControlAutosizing").is(':checked'))
					{
						$(".custom-control-label").addClass('uncheck');
						$(".custom-control-label").removeClass('check');

					}
				else
				{
					$(".custom-control-label").addClass('check');
						$(".custom-control-label").removeClass('uncheck');
				}
				if(nombre.length>0)
					$("#dnn_ctr4036_Feedback_valName").css('display','none');
				else
					$("#dnn_ctr4036_Feedback_valName").css('display','inline');
				if(mailc.length>0)
					$("#dnn_ctr4036_Feedback_valEmail1").css('display','none');
				else
					$("#dnn_ctr4036_Feedback_valEmail1").css('display','inline');
				if(categoria.length>0)
					$("#dnn_ctr4036_Feedback_valCategory").css('display','none');
				else
					$("#dnn_ctr4036_Feedback_valCategory").css('display','inline');
				if(ciudad.length>0)
					$("#dnn_ctr4036_Feedback_valCity").css('display','none');
				else
					$("#dnn_ctr4036_Feedback_valCity").css('display','inline');
			}
	});
	$("#dnn_ctr4036_Feedback_txtTelephone").keypress(function(e){
		 tecla = (document.all) ? e.keyCode : e.which;

	    //Tecla de retroceso para borrar, siempre la permite
	    if (tecla == 8) {
	        return true;
	    }

	    // Patron de entrada, en este caso solo acepta numeros y letras
	    patron = /[0-9]/;
	    tecla_final = String.fromCharCode(tecla);
	    if(patron.test(tecla_final))
	       {
	            document.getElementById("dnn_ctr4036_Feedback_valTelephone").style.display='none' ;
	       }
	    else
	       document.getElementById("dnn_ctr4036_Feedback_valTelephone").style.display='inline' ;
	    return patron.test(tecla_final);
	})
})
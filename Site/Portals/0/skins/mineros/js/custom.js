jQuery(document).ready(function(){
	if($('html[lang="en-US"]').length){
        $('#primary-menu li:nth-of-type(1) .text-esq .title-menu').text('About us');
        $('#primary-menu li:nth-of-type(2) .text-esq .title-menu').text('Sustainability');
        $('#primary-menu li:nth-of-type(3) .text-esq .title-menu').text('Investors');
		$('#primary-menu li:nth-of-type(4) .text-esq .title-menu').html('Operations and</br> Projects');
		$('#val-mineros h4 span:nth-of-type(1)').text('MINEROS');
		$('#val-mineros h4 span:nth-of-type(2)').text('ACTION');
		$('.content_AS .card .card-body .btn.btn-mas').text('Read more');
    }

  if ( document.querySelectorAll('.collapse-conatiner>a.btn') ){
    var collapse = document.querySelectorAll('.collapse-conatiner>a.btn');
    //collapse[0].getAttribute('aria-expanded');
  }

	$('.btn-collpase').click( function () {
		$(this).toggleClass('bg-alto');
		$(this).find('.icon-collapse').toggleClass('icon-rotateZ');
	});


    AOS.init({
        
          // Global settings:
          disable: false, // accepts following values: 'phone', 'tablet', 'mobile', boolean, expression or function
          startEvent: 'DOMContentLoaded', // name of the event dispatched on the document, that AOS should initialize on
          initClassName: 'aos-init', // class applied after initialization
          animatedClassName: 'aos-animate', // class applied on animation
          useClassNames: false, // if true, will add content of `data-aos` as classes on scroll
          disableMutationObserver: false, // disables automatic mutations' detections (advanced)
          debounceDelay: 50, // the delay on debounce used while resizing window (advanced)
          throttleDelay: 12, // the delay on throttle used while scrolling the page (advanced)
          
        
          // Settings that can be overridden on per-element basis, by `data-aos-*` attributes:
          offset: 0, // offset (in px) from the original trigger point
          delay: 50, // values from 0 to 3000, with step 50ms
          duration: 400, // values from 0 to 3000, with step 50ms
          easing: 'ease', // default easing for AOS animations
          once: 1, // whether animation should happen only once - while scrolling down
          mirror: false, // whether elements should animate out while scrolling past them
          anchorPlacement: 'center-bottom', // defines which position of the element regarding to window should trigger the animation
      
      });	


if ( document.querySelector('.language-object')) {
    document.querySelectorAll('.language-object .Language').forEach(function(a,i){
        a.innerHTML += '<span></span>'
        var languaje_name = a.getAttribute('title');
        jQuery('.Languaje').addClass('nav-item');
        jQuery('.Language a ').addClass('nav-link librefranklin-thin text-white active');
        a.querySelectorAll('a').forEach(function(c){
            c.innerHTML += languaje_name;
            c.innerHTML = c.innerHTML.split('(')[0];
        })
        
    });
}    
	
});
﻿var require_phone = "El numero telefónico no es valido.";

$(document).ready(function () {
    var select_position = [];
    var select_process = [];
    var select_location = [];
    var validation_file = false;
    var required_field = "Este campo es requerido";
    var required_email = "Debe ser un email";
    var required_file = "El archivo debe ser Pdf, Doc, Docx.";
    var required_length = "Por favor ingrese minímo 8 digitos.";
    var required_check = "Debes aceptar términos y condiciones";
    var required_save = "Se ha guardado tu información.";
    $(".message_interest").html("<strong>" + required_save + "</strong>");
    var location_module = window.location.pathname.replace(/\//g, "");
    if (location_module.substring(0, 2) === "en") {
        required_check = "You must accept terms and conditions"
        required_field = "This field is required";
        required_length = "Please enter at least 8 characters.";
        required_save = "Your information has been saved.";
        required_email = "Email Incorrect";
        required_file = "The file must be Pdf, Doc, Docx.";
        require_phone = "The phone number is wrong."
        $("input[name='namefull']").attr("placeholder", "Full Name *");
        $("input[name='nit']").attr("placeholder", "Number of personal ID *");
        $("input[name='email']").attr("placeholder", "Email *");
        $("input[name='phone']").attr("placeholder", "Phone (+indicative #) *");
        $("input[name='city']").attr("placeholder", "Current city of residence *");
        $("input[name='country']").attr("placeholder", "Current country of residence *");
        $("select[name='interest_first'] option").text("* Job position level of your interest 1");
        $("select[name='interest_second'] option").text("Job position level of your interest 2");
        $("select[name='proccess_first'] option").text( "* Process/specialty of your interest 1");
        $("select[name='proccess_second'] option").text("Process/specialty of your interest 2");
        $("select[name='location_first'] option").text( "* Location of your interest 1");
        $("select[name='location_second'] option").text("Location of your interest 2");
        $("input[name='application']").attr("placeholder", "Job vacancy");
        $(".title_interest_h2").text("Application");
        $(".field_required").text("* require fields.");
        $(".date_contact").text("Contact information");
        $(".title_interes").text("Interest");
        $("label[for='terminos']").html("I accept terms and conditions <a class='color-jungle-green accept_terms' target='_blank' href='#'>(See conditions)</a>");
        $("label[for='botonFileInterest']").text("Attach CV");
        $("#submit_interest").text("Send");
        $(".small_city_message").text('Type your City');
        $(".workwithus").text('Jobs');
        $(".message_interest").html("<strong>" + required_save+"</strong>");
        $(".message_send").text("Ups. error on system!");
        $("#file-error span").text(required_file);

    };
    $("form.interest_contact").validate({
        rules: {
            namefull: {
                required: true,
            },
            nit: {
                required: true,
                minlength: 8
            },
            phone: {
                required: true,
                minlength: 8,
                phonenumber:true
            },
            email: {
                required: true,
                email: true
            },
            city: {
                required: true,
            },
            country: {
                required: true,
            },
            interest_first: {
                required: true,
            },
            proccess_first: {
                required: true,
            },
            location_first: {
                required: true,
            }
        },
        messages: {
            namefull: {
                required: required_field,
            },
            nit: {
                required: required_field,
                minlength: required_length,
            },
            phone: {
                required: required_field,
                minlength: required_length,
            },
            email: {
                required: required_field,
                email: required_email,
            },
            city: {
                required: required_field,
            },
            country: {
                required: required_field,
            },
            interest_first: {
                required: required_field,
            },
            proccess_first: {
                required: required_field,
            },
            location_first: {
                required: required_field,
            },
           
        }
    })
    $.ajax({
        url: api_url + "InterestContact/index",
        data: "",
        method: "GET",
        dataType: "JSON",
        success: function (response) {
            if (response.Header.Code === 200) {
                select_position = response.Data.filter(f => f.type_interest == 1);
                select_process = response.Data.filter(f => f.type_interest == 2);
                select_location = response.Data.filter(f => f.type_interest == 3);
                options_position = "";
                options_proccess = "";
                options_location = "";
                select_position.sort(function (a, b) {
                    if (location_module.substring(0, 2) === "es") {
                        var keyA = a.name_spanish,
                            keyB = b.name_spanish;
                    } else {
                        var keyA = a.name_english,
                            keyB = b.name_english;
                    }
                    // Compare the 2 dates
                    if (keyA < keyB) return -1;
                    if (keyA > keyB) return 1;
                    return 0;
                })
                select_process.sort(function (a, b) {
                    if (location_module.substring(0, 2) === "es") {
                        var keyA = a.name_spanish,
                            keyB = b.name_spanish;
                    } else{
                        var keyA = a.name_english,
                            keyB = b.name_english;
                    }
                    // Compare the 2 dates
                    if (keyA < keyB) return -1;
                    if (keyA > keyB) return 1;
                    return 0;
                });
                select_location.sort(function (a, b) {
                    if (location_module.substring(0, 2) === "es") {
                        var keyA = a.name_spanish,
                            keyB = b.name_spanish;
                    } else {
                        var keyA = a.name_english,
                            keyB = b.name_english;
                    }
                    // Compare the 2 dates
                    if (keyA < keyB) return -1;
                    if (keyA > keyB) return 1;
                    return 0;
                });
                $.each(select_position, function (index, i) {
                    if (location_module.substring(0, 2) === "es") {
                        options_position += "<option value=" + i.Id + ">" + i.name_spanish + "</option>";

                    } else {
                        options_position += "<option value=" + i.Id + ">" + i.name_english + "</option>";
                    }
                })
                $.each(select_process, function (index, i) {
                    if (location_module.substring(0, 2) === "es") {
                        options_proccess += "<option value=" + i.Id + ">" + i.name_spanish + "</option>";

                    } else {
                        options_proccess += "<option value=" + i.Id + ">" + i.name_english + "</option>";
                    }
                });
                $.each(select_location, function (index, i) {
                    if (location_module.substring(0, 2) === "es") {
                        options_location += "<option value=" + i.Id + ">" + i.name_spanish + "</option>";

                    } else {
                        options_location += "<option value=" + i.Id + ">" + i.name_english + "</option>";
                    }
                });
                $("select[name='interest_first']").append(options_position);
                $("select[name='interest_second']").append(options_position);

                $("select[name='proccess_first']").append(options_proccess);
                $("select[name='proccess_second']").append(options_proccess);

                $("select[name='location_first']").append(options_location);
                $("select[name='location_second']").append(options_location);
            }
        }
    });
    $("#botonFileInterest").change(function () {
        var allowedFiles = [".doc", ".docx", ".pdf"];
        var fileUpload = $("#botonFileInterest");
        var regex = new RegExp("\.(" + allowedFiles.join('|') + ")$", "i");
        if (!regex.test(fileUpload.val().toLowerCase())) {
            validation_file = false;
            $("#file-error").fadeIn();
            $(".toFile").html("");
            $("#file-error").html("<span class='error'>"+required_file+"</span>")

        } else {
           
            $("#file-error").fadeOut();

            var fullPath = fileUpload.val();
            var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
            var filename = fullPath.substring(startIndex);
            if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                filename = filename.substring(1);
            }
            $(".toFile").html("<span>"+filename+"</span>");
            validation_file = true;
        }
      
    });
    $(".accept_terms").click(function (event) {
        event.preventDefault();
        var url_path = $("footer .container-fluid .col.box-col-2-footer ul li a.text-white.librefranklin-thin").attr("href")
        window.open(window.location.protocol + "//" + window.location.host + url_path, "_blank")

    })
    $("#submit_interest").click(function (e) {
        e.preventDefault();
       
        if ($("form.interest_contact").valid()) {
            if ($("#terminos_interest").is(':checked')) {
                $("form.interest_contact").trigger("submit");
                if (validation_file) {
                    $("#file-error").fadeOut();
                    $("#file-error").html("<span>" + required_file + "</span>");

                } else {
                    $("#file-error").fadeIn();
                    $("#file-error").html("<span>" + required_file + "</span>");

                }
            } else {
                $("#checkterms-error-accept").html("<span>" + required_check + "</span>");
                $("#checkterms-error-accept").fadeIn();
                $(".checkterms_interest").fadeIn();
                if (validation_file) {
                    $("#file-error").fadeOut();
                    $("#file-error").html("<span>" + required_file + "</span>");

                } else {
                    $("#file-error").fadeIn();
                    $("#file-error").html("<span>" + required_file + "</span>");

                }
            }
        } else {
            $("#checkterms-error-accept").html("<span>" + required_check + "</span>");
            $("#checkterms-error-accept").fadeIn();
        }
    });

    $("#interest_contact").submit(function (e) {
        e.preventDefault();
        
            var files = $("#botonFileInterest").get(0).files;
            var formData = new FormData($(this)[0]);
            formData.append("application", "");
         

        if (validation_file) {
            $(".LoaderBalls").fadeIn();
            if ($("#terminos_interest").is(':checked')) {

                $("#submit_interest").attr("disabled", true)
                $("#file-error").fadeOut();
                $.ajax({
                    url: api_url + "InterestContact/SendFormInterest",
                    data: formData,
                    async: true,
                    cache: false,
                    contentType: false,
                    processData: false,
                    method: "POST",
                    dataType: 'JSON',
                    success: function (response) {
                        $(".LoaderBalls").fadeOut();
                        $("#submit_interest").removeAttr("disabled", true)

                        if (response.Header.Message == null) {
                            $(".message_send").fadeOut();
                            $("select").val("");
                            $(".toFile").text("");
                            $("input[name='checkterms']").prop('checked', false);
                            $('#modal_interest_contact').modal('hide');
                            $(".message_interest").fadeIn();
                            setInterval(function () {
                                $(".message_interest").fadeOut(1500);
                            }, 1500)
                            $(".checkterms_interest").fadeOut();
                            $("input[type='text']").val("");
                        } else {
                            $(".message_send").fadeIn();

                        }

                    }
                });
            } else {
                $("#checkterms-error-accept").html("<span>" + required_check + "</span>");
                $(".checkterms_interest").fadeIn();
            }
        } else {
            $("#file-error").fadeIn();
            $("#file-error").html("<span>" + required_file+"</span>");
        }
       
    })
})

function initMap() {
    var options = {
        types: ['(cities)'],
    };
    var origin_input = document.getElementById('locationfirst');
    var origin_second_input = document.getElementById('locationSecond');
    var origin_autocomplete = new google.maps.places.Autocomplete(origin_input, options);
    var origin_second_autocomplete = new google.maps.places.Autocomplete(origin_second_input, options);
    var origin_contact_input = document.getElementById('locationContact');
    var origin_contact_autocomplate = new google.maps.places.Autocomplete(origin_contact_input, options);
    origin_autocomplete.addListener('place_changed', function () {
        var place = origin_autocomplete.getPlace();
        if (!place.geometry) {

           
            return;
        }
        formated_addres = place.formatted_address.split(",");
        country = formated_addres[formated_addres.length - 1];
        document.getElementById("country_field").value = country.trim();
        var city = formated_addres[0]
        if (formated_addres.length > 2) {
            city += "," + formated_addres[1]
        }

        document.getElementById("locationfirst").value = city;

    })
 
}
jQuery.validator.addMethod("namefull", function (value, element) {
    $pattern = '^[A-Z][a-zA-Z]{3,}(?: [A-Z][a-zA-Z]*){0,2}$';
    var re = new RegExp($pattern);
    console.info(re.test(value));
}, "Please enter a valid date."
);
jQuery.validator.addMethod("phonenumber", function (value, element) {
    var phoneno = /((?:\+|00)[17](?: |\-)?|(?:\+|00)[1-9]\d{0,2}(?: |\-)?|(?:\+|00)1\-\d{3}(?: |\-)?)?(0\d|\([0-9]{3}\)|[1-9]{0,3})(?:((?: |\-)[0-9]{2}){4}|((?:[0-9]{2}){4})|((?: |\-)[0-9]{3}(?: |\-)[0-9]{4})|([0-9]{7}))/;
    if (value.match(phoneno)) {
        return true;
    }
    else {
        
        return false;
    }
 
}, require_phone);
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="View.ascx.cs" Inherits="DesktopModules_Mineros_InterestContactModule_View" %>
<style>
    .message_interest {
        max-width: 320px;
        background-color: #22a571;
        text-align: center;
        padding: 15px;
        color: #fff;
        font-weight: 600;
        margin-top: 10px;
        position: absolute;
        z-index: 999;
        box-shadow: 2px 2px 4px #a7a7a7;
        display: none;
    }

    #file-error {
        display: none;
    }
    #phone-error{
        margin: 0;
        display: inherit;
    }
</style>
<div class="container">
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB18VJBwo1GssQFBWnZe6t3YJxIesc8KZc&libraries=places&callback=initMap"
        defer>
    </script>
    <a href="#" class="btn butn librefranklin-regular text-white workwithus" data-toggle="modal" data-target="#modal_interest_contact" role="btn btn-primary" aria-pressed="true">Trabaja con nosotros</a>
    <div class="message_interest"><strong></strong></div>

    <div class="modal" tabindex="-1" role="dialog" id="modal_interest_contact">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header bg-alto position-relative">
                    <h2 class="modal-title librefranklin-bold color-jungle-green header-mineros ng-binding title_interest_h2">Postúlate</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form></form>
                            <form name="interest_contact" id="interest_contact" class="interest_contact" novalidate>
                                <p class="field_required librefranklin-reg">(*) Campos Obligatorios</p>
                                <fieldset>

                                    <strong class="date_contact librefranklin-bold">Datos de contacto:</strong>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input id="firstname" type="text" name="namefull" class="form-control bg-very-light-gray librefranklin-reg" placeholder="Nombre Completo" />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control bg-very-light-gray librefranklin-reg" name="nit" placeholder="Identificación (CC / DNI)" />
                                            </div>
                                        </div>
                                        <div class="col-md-6">

                                            <div class="form-group">
                                                <input type="text" class="form-control bg-very-light-gray librefranklin-reg" name="email" placeholder="Correo electrónico" />
                                            </div>
                                        </div>
                                        <div class="col-md-6">

                                            <div class="form-group">
                                                <input type="text" class="form-control bg-very-light-gray" name="phone" placeholder="Teléfono de contacto (+Indicativo #)" />
                                                <small>+XX XXX XXXXXXX,+XX-XXX-XXX-XXXX,+XX(XXX)-XXX-XXXX </small>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" id="locationfirst" ng-autocomplete placeholder="Ciudad de residencia actual" name="city" class="form-control bg-very-light-gray" />
                                            <div>
                                                <small class="small_city_message librefranklin-reg">Si no sale su ciudad puede escribirla de la siguiente forma "Ciudad País"</small>
                                            </div>
                                        </div>
                                        <div class="col-md-6">

                                            <div class="form-group">
                                                <input type="text" id="country_field" class="form-control bg-very-light-gray" name="country" readonly placeholder="País de residencia actual" />

                                            </div>
                                        </div>
                                </fieldset>
                                <fieldset>
                                    <legend>
                                        <strong class="title_interes librefranklin-bold ">Interés:</strong>
                                    </legend>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <select class="form-control bg-very-light-gray" name="interest_first" placeholder="*Nivel de cargo de su interés 1:">
                                                    <option value="">*Nivel de cargo de su interés 1:</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <select class="form-control bg-very-light-gray" name="interest_second" placeholder="Nivel de cargo de su interés 2:">
                                                    <option value="">Nivel de cargo de su interés 2:</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <select class="form-control bg-very-light-gray" name="proccess_first" placeholder="*Proceso / especialidad de su interés 1">
                                                    <option value="">*Proceso / especialidad de su interés 1:</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <select class="form-control bg-very-light-gray" name="proccess_second" placeholder="Proceso / especialidad de su interés 2:">
                                                    <option value="">Proceso / especialidad de su interés 2:</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <select name="location_first" class="form-control bg-very-light-gray" name="location_first" placeholder="*Ubicación de su interés 1:">
                                                    <option value="">*Ubicación de su interés 1:</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <select name="location_second" class="form-control bg-very-light-gray librefranklin-reg librefranklin-reg" name="location_second" placeholder="*Ubicación de su interés 2:">
                                                    <option value="">Ubicación de su interés 2:</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group curriculum">
                                                <input type="file" name="file" style="opacity: 0" id="botonFileInterest">
                                                <label for="botonFileInterest" style="margin-top: -55px" class="btn btn-secondary color-very-dark-gray bg-light-gray librefranklin-reg">Adjuntar hoja de vida</label>
                                                <label class="toFile"></label>
                                                <label id="file-error" class="error"><span class="librefranklin-reg">El archivo debe ser Pdf,Doc,Docx.</span></label>
                                            </div>
                                        </div>
                                        <div class="col-md-12 text-center">
                                            <div class="form-group form-check d-flex">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="checkterms" value="" class="form-check-input" id="terminos_interest" />
                                                        <span class="cr border-color-pumice"><i class="cr-icon fas fa-check"></i></span>
                                                    </label>
                                                </div>
                                                <label class="form-check-label librefranklin-regular color-very-dark-gray" for="terminos">Acepto términos y condiciones <a href="#" class="color-jungle-green accept_terms">(Ver Condiciones)</a></label>
                                                <div class="checkterms_interest">

                                                    <label id="checkterms-error-accept" class="error" for="checkterms"><span class="librefranklin-reg">Debes aceptar términos y condiciones</span></label>
                                                </div>
                                            </div>


                                        </div>
                                        <div class="col-md-6">
                                            <button class="btn btn-succes bg-jungle-green librefranklin-regular text-white" id="submit_interest">Enviar</button>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="message_send">
                                                <p>Hubo un error.</p>
                                            </div>
                                            <div class="LoaderBalls">
                                                <div class="LoaderBalls__item"></div>
                                                <div class="LoaderBalls__item"></div>
                                                <div class="LoaderBalls__item"></div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .pac-container {
        z-index: 999999;
    }

    .modal-body label.btn.btn-secondary.color-very-dark-gray.bg-light-gray {
        border-radius: 0;
        background-color: rgb(221, 221, 221);
        border-color: rgb(221, 221, 221);
        font-family: 'LibreFranklin', sans-serif !important;
        height: 47px;
        display: flex;
        justify-content: center;
        align-items: center;
        max-width: 185px;
    }

    form.interest_contact .form-group {
        height: 60px;
    }

    label.error {
        color: darkred !important;
        font-weight: 600 !important;
    }

    .form-control#country_field:disabled {
        background-color: rgb(221, 221, 221) !important;
    }

    #modal_interest_contact .modal-body .form-group input {
        height: 38px;
    }

    .message_send {
        display: none;
    }

        .message_send p.success {
        }

        .message_send p.warning {
        }

    .LoaderBalls {
        width: 90px;
        display: flex;
        justify-content: space-between;
        align-items: center;
        margin: 0 auto;
        display: none;
        min-width: 120px;
    }

    .LoaderBalls__item {
        width: 20px;
        height: 20px;
        border-radius: 50%;
        background: #00f1ca;
        display: inline-block;
    }

        .LoaderBalls__item:nth-child(1) {
            animation: bouncing 0.4s alternate infinite cubic-bezier(0.6, 0.05, 0.15, 0.95);
        }

        .LoaderBalls__item:nth-child(2) {
            animation: bouncing 0.4s 0.1s alternate infinite cubic-bezier(0.6, 0.05, 0.15, 0.95) backwards;
        }

        .LoaderBalls__item:nth-child(3) {
            animation: bouncing 0.4s 0.2s alternate infinite cubic-bezier(0.6, 0.05, 0.15, 0.95) backwards;
        }

    @keyframes bouncing {
        0% {
            transform: translate3d(0, 10px, 0) scale(1.2, 0.85);
        }

        100% {
            transform: translate3d(0, -20px, 0) scale(0.9, 1.1);
        }
    }

    #modal_interest_contact small {
        font-size: 14px !important;
    }

    #modal_interest_contact strong {
        font-weight: 600 !important;
        font-size: 16px !important;
    }

    #modal_interest_contact a {
        color: #0056b3 !important;
    }

    #modal_interest_contact p, #modal_interest_contact small, #modal_interest_contact strong, #modal_interest_contact label, #modal_interest_contact option, #modal_interest_contact a {
        font-family: 'LibreFranklin', sans-serif !important;
        font-size: 1rem;
        line-height: 1.3125rem;
        font-weight: 400;
        color: rgb(128, 128, 128);
        line-height: normal;
    }

    #modal_interest_contact .form-control:focus {
        border: 0;
        outline: 0;
        box-shadow: inherit;
        background: #dddddd;
    }
</style>
<script src="/DesktopModules/Mineros/scripts/helpers/helpers.js"></script>
<script src="/DesktopModules/Mineros/scripts/libs/jquery.validate.min.js"></script>
<script src="/DesktopModules/Mineros/InterestContactModule/js/InterestContactModule.controller.js"></script>


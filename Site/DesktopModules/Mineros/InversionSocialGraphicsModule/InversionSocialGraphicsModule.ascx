<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InversionSocialGraphicsModule.ascx.cs" Inherits="DesktopModules_Mineros_InversionSocialGraphicsModule_InversionSocialGraphicsModule" %>

<div id="cab-section-4" class="cab-rigt-sections d-flex w-100 position-relative border-color-corn">
    <div class="icon-cab-sections bg-jungle-green d-flex justify-content-center align-items-center">
        <img src="/DesktopModules/Mineros/InversionSocialGraphicsModule/assets/icons/barras.png" srcset="/DesktopModules/Mineros/InversionSocialGraphicsModule/assets/svg/barras.svg">
    </div>
    <div class="box-cab-tittle-sections bg-corn h-100 d-flex justify-content-center align-items-center">
        <h2 class="librefranklin-semibold text-white mb-0" data-aos="flip-left">Inversión social en cifras</h2>
    </div>
</div>
<div id="box-graf-section-4" class="d-block border-color-jungle-green bg-white" data-aos="flip-left">
    <ul class="nav nav-tabs" id="myTab" role="tablist">
      	<li class="nav-item col p-0 border-0 rounded-0">
            <a class="nav-link rounded-0 border-0 librefranklin-semibold text-center " id="inversion-social-argentina-tab"
                data-toggle="tab" href="#inversion-social-argentina" role="tab" aria-controls="inversion-social-argentina" aria-selected="false">Argentina <!--Inversión Social--></a>
        </li>
        <li class="nav-item col p-0 border-0 rounded-0">
            <a class="nav-link rounded-0 border-0 librefranklin-semibold text-center " id="inversion-social-tab"
                data-toggle="tab" href="#inversion-social" role="tab" aria-controls="inversion-social" aria-selected="false">Hemco <!--Inversión Social--></a>
        </li>
        <li class="nav-item col p-0 border-0 rounded-0">
            <a class="nav-link rounded-0 border-0 librefranklin-semibold text-center active" id="action-social-tab"
                data-toggle="tab" href="#action-social" role="tab" aria-controls="action-social" aria-selected="false">Colombia <!--Acción Social - Donaciones--></a>
        </li>
    </ul>
    <div class="tab-content w-100 position-relative" id="myTabContent">
        <div class="tab-pane fade" id="inversion-social-argentina" role="tabpanel" aria-labelledby="inversion-social-argentina-tab" style="height:400px;"></div>
        <div class="tab-pane fade" id="inversion-social" role="tabpanel" aria-labelledby="inversion-social-tab" style="height:400px;"></div>
        <div class="tab-pane fade show active" id="action-social" role="tabpanel" aria-labelledby="action-social-tab" style="height:400px;"></div>
    </div>
</div>



<script src="/DesktopModules/Mineros/scripts/libs/aos-2.3.1/js/aos.js"></script>
<script src="/DesktopModules/Mineros/scripts/libs/incubator-echarts-4.2.1/js/echarts.min.js"></script>
<script src="/DesktopModules/Mineros/scripts/libs/bootstrap-4.3.1-dist/js/bootstrap.min.js"></script>

<script src="/DesktopModules/Mineros/InversionSocialGraphicsModule/js/InversionSocialGraphicsModule.js"></script>








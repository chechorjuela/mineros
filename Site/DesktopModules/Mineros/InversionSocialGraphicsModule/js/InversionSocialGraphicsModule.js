$(document).ready(function () {

    /*Traducciones forzadas*/
    if($('html[lang="en-US"]').length){
        cifras_en_dolares = 'Figures in dollars';
    }else{
        cifras_en_dolares = 'Cifras en dólares';
    }

    var pest2 = document.getElementById('action-social');
    pest2.classList.add('d-block');
    pest2.style.width = '100%';
    pest2.style.height = '100%';
    var donacion = echarts.init(document.getElementById('action-social'));
    var colors = ['#676767', '#000', '#675bba'];
    var option = {
        media: [ // each rule of media query is defined here
            {
                query: {
                    minWidth: 551
                },   // write rule here
                option: {
                    dataZoom: [
                        {
                            type: 'inside',
                            xAxisIndex: [0],
                            realtime: false,
                        }
                    ],
                    xAxis: {
                        type: 'category',
                        name: cifras_en_dolares,
                        nameLocation: 'center',
                        nameTextStyle: {
                            color: colors[0],
                            fontFamily: 'LibreFranklin',
                            fontSize: 14,
                            align: 'center',
                            verticalAlign: 'bottom',
                            lineHeight: 19.2,
                            padding: [10, 0, 0, 0],
                            fontWeight: 700
                        },
                        maxInterval: 1,
                        splitNumber: 1,
                        data: ['2016', '2017', '2018', '2019'],
                        min: '2016',
                        axisLine: {
                            show: false,
                        },
                        axisLabel: {
                            show: 'true',
                            textStyle: {
                                color: colors[0],
                                fontSize: 10.6,
                                fontFamily: 'LibreFranklin',
                            },
                            verticalAlign: 'center',
                            padding: [10, 0, 0, 0]
                        },
                    },
                    yAxis: {
                        type: 'value',
                        position: 'left',
                        maxInterval: 200000,
                        splitNumber: 1,
                        boundaryGap: false,
                        data: [0, 675, 1350, 2025, 2700],
                        offset: -20,
                        axisLine: {
                            show: false,
                        },
                        axisLabel: {
                            show: 'true',
                            textStyle: {
                                color: colors[0],
                                fontSize: 10.6,
                                fontFamily: 'LibreFranklin',
                            },
                            formatter: '${value}',
                            verticalAlign: 'bottom',
                            padding: [0, 0, 2, 0]
                        },
                        splitLine: {
                            interval: 1,
                            lineStyle: {
                                color: '#c2c2c2'
                            }
                        },
                    },
                    grid: {
                        right: 0,
                        // bottom: 90
                    },
                    series: [{
                        name: ' ',
                        type: 'bar',
                        smooth: true,
                        symbol: 'none',
                        barWidth: 80,
                        barGap: '-100%',
                        barCategoryGap: '10%',
                        label: {
                        	show: true,
                        	position: 'top',
                          	color: '#FFFFFF',
                          	fontFamily: 'LibreFranklin',
                        	fontSize: 10.6,
                          	lineHeight: 19.2,
                        	backgroundColor: 'rgba(34,165,113,1)',
                        	padding: [8, 6, 5, 6],
                          	formatter: function(params) {
            					return new Intl.NumberFormat('ja-US', { style: 'currency', currency: 'USD' }).format( params.value); 
            				}
                        },
                        data: [1708238, 1880027, 1676437, 1393282],

                    }],
                    animationEasing: 'elasticOut',
                    color: '#22a571',
                }
            },
            {
                query: {
                    maxWidth: 550
                },   // write rule here
                option: {
                    dataZoom: [
                        {
                            type: 'inside',
                            xAxisIndex: [0],
                            realtime: false,
                        }
                    ],
                    xAxis: {
                        type: 'category',
                        name: cifras_en_dolares,
                        nameLocation: 'center',
                        nameTextStyle: {
                            color: colors[0],
                            fontFamily: 'LibreFranklin',
                            fontSize: 14,
                            align: 'center',
                            verticalAlign: 'bottom',
                            lineHeight: 19.2,
                            padding: [10, 0, 0, 0],
                            fontWeight: 600
                        },
                        maxInterval: 1,
                        splitNumber: 1,
                        data: ['2016', '2017', '2018', '2019'],
                        min: '2016',
                        axisLine: {
                            show: false,
                        },
                        axisLabel: {
                            show: 'true',
                            textStyle: {
                                color: colors[0],
                                fontSize: 10.6,
                                fontFamily: 'LibreFranklin',
                            },
                            verticalAlign: 'center',
                            padding: [10, 0, 0, 0]
                        },
                    },
                    yAxis: {
                        type: 'value',
                        position: 'left',
                        maxInterval: 100000,
                        splitNumber: 1,
                        boundaryGap: false,
                        data: [0, 675, 1350, 2025, 2700],
                        offset: -20,
                        axisLine: {
                            show: false,
                        },
                        axisLabel: {
                            show: 'true',
                            textStyle: {
                                color: colors[0],
                                fontSize: 10.6,
                                fontFamily: 'LibreFranklin',
                            },
                            formatter: '${value}',
                            verticalAlign: 'bottom',
                            padding: [0, 0, 2, 0]
                        },
                        splitLine: {
                            interval: 1,
                            lineStyle: {
                                color: '#c2c2c2'
                            }
                        },
                    },
                    grid: {
                        right: 0,
                        // bottom: 90
                    },
                    series: [{
                        name: ' ',
                        type: 'bar',
                        smooth: true,
                        symbol: 'none',
                        barWidth: 40,
                        barGap: '-100%',
                        barCategoryGap: '10%',
                        label: {
                        	show: true,
                        	position: 'top',
                          	color: '#FFFFFF',
                          	fontFamily: 'LibreFranklin',
                        	fontSize: 10.6,
                          	lineHeight: 19.2,
                        	backgroundColor: 'rgba(34,165,113,1)',
                        	padding: [8, 6, 5, 6],
                          	formatter: function(params) {
            					return new Intl.NumberFormat('ja-US', { style: 'currency', currency: 'USD' }).format( params.value); 
            				}
                        },
                        data: [1708238, 1880027, 1676437, 1393282],

                    }],
                    animationEasing: 'elasticOut',
                    color: '#22a571',
                }
            },
            {
                query: {
                    maxWidth: 425
                },   // write rule here
                option: {
                    dataZoom: [
                        {
                            type: 'inside',
                            xAxisIndex: [0],
                            realtime: false,
                        }
                    ],
                    xAxis: {
                        type: 'category',
                        name: cifras_en_dolares,
                        nameLocation: 'center',
                        nameTextStyle: {
                            color: colors[0],
                            fontFamily: 'LibreFranklin',
                            fontSize: 14,
                            align: 'center',
                            verticalAlign: 'bottom',
                            lineHeight: 19.2,
                            padding: [10, 0, 0, 0],
                            fontWeight: 600
                        },
                        maxInterval: 1,
                        splitNumber: 1,
                        data: ['2016', '2017', '2018', '2019'],
                        min: '2016',
                        axisLine: {
                            show: false,
                        },
                        axisLabel: {
                            show: 'true',
                            textStyle: {
                                color: colors[0],
                                fontSize: 10.6,
                                fontFamily: 'LibreFranklin',
                            },
                            verticalAlign: 'center',
                            padding: [10, 0, 0, 0]
                        },
                    },
                    yAxis: {
                        type: 'value',
                        position: 'left',
                        maxInterval: 100000,
                        splitNumber: 1,
                        boundaryGap: false,
                        data: [0, 675, 1350, 2025, 2700],
                        offset: -20,
                        axisLine: {
                            show: false,
                        },
                        axisLabel: {
                            show: 'true',
                            textStyle: {
                                color: colors[0],
                                fontSize: 10.6,
                                fontFamily: 'LibreFranklin',
                            },
                            formatter: '${value}',
                            verticalAlign: 'bottom',
                            padding: [0, 0, 2, 0]
                        },
                        splitLine: {
                            interval: 1,
                            lineStyle: {
                                color: '#c2c2c2'
                            }
                        },
                    },
                    grid: {
                        right: 0,
                        // bottom: 90
                    },
                    series: [{
                        name: ' ',
                        type: 'bar',
                        smooth: true,
                        symbol: 'none',
                        barWidth: 25,
                        barGap: '-100%',
                        barCategoryGap: '10%',
                        label: {
                        	show: true,
                        	position: 'top',
                          	color: '#FFFFFF',
                          	fontFamily: 'LibreFranklin',
                        	fontSize: 10.6,
                          	lineHeight: 19.2,
                        	backgroundColor: 'rgba(34,165,113,1)',
                        	padding: [8, 6, 5, 6],
                          	formatter: function(params) {
            					return new Intl.NumberFormat('ja-US', { style: 'currency', currency: 'USD' }).format( params.value); 
            				}
                        },
                        data: [1708238, 1880027, 1676437, 1393282],

                    }],
                    animationEasing: 'elasticOut',
                    color: '#22a571',
                }
            }]
    };
    donacion.setOption(option);
    donacion.resize();
    var inverSocial = echarts.init(document.getElementById('inversion-social'));
    var optionSocial = {
        media: [ // each rule of media query is defined here
            {
                query: {
                    minWidth: 551
                },   // write rule here
                option: {
                    dataZoom: [
                        {
                            type: 'inside',
                            xAxisIndex: [0],
                            realtime: false,
                        }
                    ],
                    xAxis: {
                        type: 'category',
                        name: cifras_en_dolares,
                        nameLocation: 'center',
                        nameTextStyle: {
                            color: colors[0],
                            fontFamily: 'LibreFranklin',
                            fontSize: 14,
                            align: 'center',
                            verticalAlign: 'bottom',
                            lineHeight: 19.2,
                            padding: [10, 0, 0, 0],
                            fontWeight: 700
                        },
                        maxInterval: 1,
                        splitNumber: 1,
                        data: ['2016', '2017', '2018', '2019'],
                        min: '2016',
                        axisLine: {
                            show: false,
                        },
                        axisLabel: {
                            show: 'true',
                            textStyle: {
                                color: colors[0],
                                fontSize: 10.6,
                                fontFamily: 'LibreFranklin',
                            },
                            verticalAlign: 'center',
                            padding: [10, 0, 0, 0]
                        },
                    },
                    yAxis: {
                        type: 'value',
                        position: 'left',
                        maxInterval: 100000,
                        splitNumber: 1,
                        boundaryGap: false,
                        data: [0, 675, 1350, 2025, 2700],
                        offset: -20,
                        axisLine: {
                            show: false,
                        },
                        axisLabel: {
                            show: 'true',
                            textStyle: {
                                color: colors[0],
                                fontSize: 10.6,
                                fontFamily: 'LibreFranklin',
                            },
                            formatter: '${value}',
                            verticalAlign: 'bottom',
                            padding: [0, 0, 2, 0]
                        },
                        splitLine: {
                            interval: 1,
                            lineStyle: {
                                color: '#c2c2c2'
                            }
                        },
                    },
                    grid: {
                        right: 0,
                        // bottom: 90
                    },
                    series: [{
                        name: ' ',
                        type: 'bar',
                        smooth: true,
                        symbol: 'none',
                        barWidth: 80,
                        barGap: '-100%',
                        barCategoryGap: '10%',
                        label: {
                        	show: true,
                        	position: 'top',
                          	color: '#FFFFFF',
                          	fontFamily: 'LibreFranklin',
                        	fontSize: 10.6,
                          	lineHeight: 19.2,
                        	backgroundColor: 'rgba(34,165,113,1)',
                        	padding: [8, 6, 5, 6],
                          	formatter: function(params) {
            					return new Intl.NumberFormat('ja-US', { style: 'currency', currency: 'USD' }).format( params.value); 
            				}
                        },
                        data: [953000, 965000, 972000, 1809849],

                    }],
                    animationEasing: 'elasticOut',
                    color: '#22a571',
                }
            },
            {
                query: {
                    maxWidth: 550
                },   // write rule here
                option: {
                    dataZoom: [
                        {
                            type: 'inside',
                            xAxisIndex: [0],
                            realtime: false,
                        }
                    ],
                    xAxis: {
                        type: 'category',
                        name: cifras_en_dolares,
                        nameLocation: 'center',
                        nameTextStyle: {
                            color: colors[0],
                            fontFamily: 'LibreFranklin',
                            fontSize: 14,
                            align: 'center',
                            verticalAlign: 'bottom',
                            lineHeight: 19.2,
                            padding: [10, 0, 0, 0],
                            fontWeight: 600
                        },
                        maxInterval: 1,
                        splitNumber: 1,
                        data: ['2016', '2017', '2018', '2019'],
                        min: '2016',
                        axisLine: {
                            show: false,
                        },
                        axisLabel: {
                            show: 'true',
                            textStyle: {
                                color: colors[0],
                                fontSize: 10.6,
                                fontFamily: 'LibreFranklin',
                            },
                            verticalAlign: 'center',
                            padding: [10, 0, 0, 0]
                        },
                    },
                    yAxis: {
                        type: 'value',
                        position: 'left',
                        maxInterval: 100000,
                        splitNumber: 1,
                        boundaryGap: false,
                        data: [0, 675, 1350, 2025, 2700],
                        offset: -20,
                        axisLine: {
                            show: false,
                        },
                        axisLabel: {
                            show: 'true',
                            textStyle: {
                                color: colors[0],
                                fontSize: 10.6,
                                fontFamily: 'LibreFranklin',
                            },
                            formatter: '${value}',
                            verticalAlign: 'bottom',
                            padding: [0, 0, 2, 0]
                        },
                        splitLine: {
                            interval: 1,
                            lineStyle: {
                                color: '#c2c2c2'
                            }
                        },
                    },
                    grid: {
                        right: 0,
                        // bottom: 90
                    },
                    series: [{
                        name: ' ',
                        type: 'bar',
                        smooth: true,
                        symbol: 'none',
                        barWidth: 40,
                        barGap: '-100%',
                        barCategoryGap: '10%',
                        label: {
                        	show: true,
                        	position: 'top',
                          	color: '#FFFFFF',
                          	fontFamily: 'LibreFranklin',
                        	fontSize: 10.6,
                          	lineHeight: 19.2,
                        	backgroundColor: 'rgba(34,165,113,1)',
                        	padding: [8, 6, 5, 6],
                          	formatter: function(params) {
            					return new Intl.NumberFormat('ja-US', { style: 'currency', currency: 'USD' }).format( params.value); 
            				}
                        },
                        data: [953000, 965000, 972000, 1809849],

                    }],
                    animationEasing: 'elasticOut',
                    color: '#22a571',
                }
            },
            {
                query: {
                    maxWidth: 425
                },   // write rule here
                option: {
                    dataZoom: [
                        {
                            type: 'inside',
                            xAxisIndex: [0],
                            realtime: false,
                        }
                    ],
                    xAxis: {
                        type: 'category',
                        name: cifras_en_dolares,
                        nameLocation: 'center',
                        nameTextStyle: {
                            color: colors[0],
                            fontFamily: 'LibreFranklin',
                            fontSize: 14,
                            align: 'center',
                            verticalAlign: 'bottom',
                            lineHeight: 19.2,
                            padding: [10, 0, 0, 0],
                            fontWeight: 600
                        },
                        maxInterval: 1,
                        splitNumber: 1,
                        data: ['2016', '2017', '2018', '2019'],
                        min: '2016',
                        axisLine: {
                            show: false,
                        },
                        axisLabel: {
                            show: 'true',
                            textStyle: {
                                color: colors[0],
                                fontSize: 10.6,
                                fontFamily: 'LibreFranklin',
                            },
                            verticalAlign: 'center',
                            padding: [10, 0, 0, 0]
                        },
                    },
                    yAxis: {
                        type: 'value',
                        position: 'left',
                        maxInterval: 100000,
                        splitNumber: 1,
                        boundaryGap: false,
                        data: [0, 675, 1350, 2025, 2700],
                        offset: -20,
                        axisLine: {
                            show: false,
                        },
                        axisLabel: {
                            show: 'true',
                            textStyle: {
                                color: colors[0],
                                fontSize: 10.6,
                                fontFamily: 'LibreFranklin',
                            },
                            formatter: '${value}',
                            verticalAlign: 'bottom',
                            padding: [0, 0, 2, 0]
                        },
                        splitLine: {
                            interval: 1,
                            lineStyle: {
                                color: '#c2c2c2'
                            }
                        },
                    },
                    grid: {
                        right: 0,
                        // bottom: 90
                    },
                    series: [{
                        name: ' ',
                        type: 'bar',
                        smooth: true,
                        symbol: 'none',
                        barWidth: 25,
                        barGap: '-100%',
                        barCategoryGap: '10%',
                        label: {
                        	show: true,
                        	position: 'top',
                          	color: '#FFFFFF',
                          	fontFamily: 'LibreFranklin',
                        	fontSize: 10.6,
                          	lineHeight: 19.2,
                        	backgroundColor: 'rgba(34,165,113,1)',
                        	padding: [8, 6, 5, 6],
                          	formatter: function(params) {
            					return new Intl.NumberFormat('ja-US', { style: 'currency', currency: 'USD' }).format( params.value); 
            				}
                        },
                        data: [953000, 965000, 972000, 1809849],

                    }],
                    animationEasing: 'elasticOut',
                    color: '#22a571',
                }
            }]
    };
    inverSocial.setOption(optionSocial);
  	inverSocial.resize();
    
    $(window).on('resize', function(){
    	donacion.resize();
      	inverSocial.resize();
    });
    $("#inversion-social-tab").click(function (e) {
        e.preventDefault();
        var pest2 = document.getElementById('action-social');
        pest2.classList.remove('d-block');
        var pest3 = document.getElementById('inversion-social-argentina');
        pest3.classList.remove('d-block');

        var pest1 = document.getElementById('inversion-social');
        pest1.classList.add('d-block');
        pest1.style.width = '100%';
        pest1.style.height = '100%';
      	
      
          var inverSocial = echarts.init(document.getElementById('inversion-social'));
    var optionSocial = {
        media: [ // each rule of media query is defined here
            {
                query: {
                    minWidth: 551
                },   // write rule here
                option: {
                    dataZoom: [
                        {
                            type: 'inside',
                            xAxisIndex: [0],
                            realtime: false,
                        }
                    ],
                    xAxis: {
                        type: 'category',
                        name: cifras_en_dolares,
                        nameLocation: 'center',
                        nameTextStyle: {
                            color: colors[0],
                            fontFamily: 'LibreFranklin',
                            fontSize: 14,
                            align: 'center',
                            verticalAlign: 'bottom',
                            lineHeight: 19.2,
                            padding: [10, 0, 0, 0],
                            fontWeight: 700
                        },
                        maxInterval: 1,
                        splitNumber: 1,
                        data: ['2016', '2017', '2018', '2019'],
                        min: '2016',
                        axisLine: {
                            show: false,
                        },
                        axisLabel: {
                            show: 'true',
                            textStyle: {
                                color: colors[0],
                                fontSize: 10.6,
                                fontFamily: 'LibreFranklin',
                            },
                            verticalAlign: 'center',
                            padding: [10, 0, 0, 0]
                        },
                    },
                    yAxis: {
                        type: 'value',
                        position: 'left',
                        maxInterval: 100000,
                        splitNumber: 1,
                        boundaryGap: false,
                        data: [0, 675, 1350, 2025, 2700],
                        offset: -20,
                        axisLine: {
                            show: false,
                        },
                        axisLabel: {
                            show: 'true',
                            textStyle: {
                                color: colors[0],
                                fontSize: 10.6,
                                fontFamily: 'LibreFranklin',
                            },
                            formatter: '${value}',
                            verticalAlign: 'bottom',
                            padding: [0, 0, 2, 0]
                        },
                        splitLine: {
                            interval: 1,
                            lineStyle: {
                                color: '#c2c2c2'
                            }
                        },
                    },
                    grid: {
                        right: 0,
                        // bottom: 90
                    },
                    series: [{
                        name: ' ',
                        type: 'bar',
                        smooth: true,
                        symbol: 'none',
                        barWidth: 80,
                        barGap: '-100%',
                        barCategoryGap: '10%',
                        data: [953000, 965000, 972000, 1809849],

                    }],
                    animationEasing: 'elasticOut',
                    color: '#22a571',
                }
            },
            {
                query: {
                    maxWidth: 550
                },   // write rule here
                option: {
                    dataZoom: [
                        {
                            type: 'inside',
                            xAxisIndex: [0],
                            realtime: false,
                        }
                    ],
                    xAxis: {
                        type: 'category',
                        name: cifras_en_dolares,
                        nameLocation: 'center',
                        nameTextStyle: {
                            color: colors[0],
                            fontFamily: 'LibreFranklin',
                            fontSize: 14,
                            align: 'center',
                            verticalAlign: 'bottom',
                            lineHeight: 19.2,
                            padding: [10, 0, 0, 0],
                            fontWeight: 600
                        },
                        maxInterval: 1,
                        splitNumber: 1,
                        data: ['2016', '2017', '2018', '2019'],
                        min: '2016',
                        axisLine: {
                            show: false,
                        },
                        axisLabel: {
                            show: 'true',
                            textStyle: {
                                color: colors[0],
                                fontSize: 10.6,
                                fontFamily: 'LibreFranklin',
                            },
                            verticalAlign: 'center',
                            padding: [10, 0, 0, 0]
                        },
                    },
                    yAxis: {
                        type: 'value',
                        position: 'left',
                        maxInterval: 100000,
                        splitNumber: 1,
                        boundaryGap: false,
                        data: [0, 675, 1350, 2025, 2700],
                        offset: -20,
                        axisLine: {
                            show: false,
                        },
                        axisLabel: {
                            show: 'true',
                            textStyle: {
                                color: colors[0],
                                fontSize: 10.6,
                                fontFamily: 'LibreFranklin',
                            },
                            formatter: '${value}',
                            verticalAlign: 'bottom',
                            padding: [0, 0, 2, 0]
                        },
                        splitLine: {
                            interval: 1,
                            lineStyle: {
                                color: '#c2c2c2'
                            }
                        },
                    },
                    grid: {
                        right: 0,
                        // bottom: 90
                    },
                    series: [{
                        name: ' ',
                        type: 'bar',
                        smooth: true,
                        symbol: 'none',
                        barWidth: 40,
                        barGap: '-100%',
                        barCategoryGap: '10%',
                        data: [953000, 965000, 972000, 1809849],

                    }],
                    animationEasing: 'elasticOut',
                    color: '#22a571',
                }
            },
            {
                query: {
                    maxWidth: 425
                },   // write rule here
                option: {
                    dataZoom: [
                        {
                            type: 'inside',
                            xAxisIndex: [0],
                            realtime: false,
                        }
                    ],
                    xAxis: {
                        type: 'category',
                        name: cifras_en_dolares,
                        nameLocation: 'center',
                        nameTextStyle: {
                            color: colors[0],
                            fontFamily: 'LibreFranklin',
                            fontSize: 14,
                            align: 'center',
                            verticalAlign: 'bottom',
                            lineHeight: 19.2,
                            padding: [10, 0, 0, 0],
                            fontWeight: 600
                        },
                        maxInterval: 1,
                        splitNumber: 1,
                        data: ['2016', '2017', '2018', '2019'],
                        min: '2016',
                        axisLine: {
                            show: false,
                        },
                        axisLabel: {
                            show: 'true',
                            textStyle: {
                                color: colors[0],
                                fontSize: 10.6,
                                fontFamily: 'LibreFranklin',
                            },
                            verticalAlign: 'center',
                            padding: [10, 0, 0, 0]
                        },
                    },
                    yAxis: {
                        type: 'value',
                        position: 'left',
                        maxInterval: 100000,
                        splitNumber: 1,
                        boundaryGap: false,
                        data: [0, 675, 1350, 2025, 2700],
                        offset: -20,
                        axisLine: {
                            show: false,
                        },
                        axisLabel: {
                            show: 'true',
                            textStyle: {
                                color: colors[0],
                                fontSize: 10.6,
                                fontFamily: 'LibreFranklin',
                            },
                            formatter: '${value}',
                            verticalAlign: 'bottom',
                            padding: [0, 0, 2, 0]
                        },
                        splitLine: {
                            interval: 1,
                            lineStyle: {
                                color: '#c2c2c2'
                            }
                        },
                    },
                    grid: {
                        right: 0,
                        // bottom: 90
                    },
                    series: [{
                        name: ' ',
                        type: 'bar',
                        smooth: true,
                        symbol: 'none',
                        barWidth: 25,
                        barGap: '-100%',
                        barCategoryGap: '10%',
                        data: [953000, 965000, 972000, 1809849],

                    }],
                    animationEasing: 'elasticOut',
                    color: '#22a571',
                }
            }]
    };
    inverSocial.setOption(optionSocial);
  	inverSocial.resize();
    });

    $("#action-social-tab").click(function (e) {
        e.preventDefault();
        var pest2 = document.getElementById('action-social');
        pest2.classList.add('d-block');
        pest2.style.width = '100%';
        pest2.style.height = '100%';

        var pest1 = document.getElementById('inversion-social');
        var pest3 = document.getElementById('inversion-social-argentina');
        pest1.classList.remove('d-block');
        pest3.classList.remove('d-block');
      
          var donacion = echarts.init(document.getElementById('action-social'));
    var colors = ['#676767', '#000', '#675bba'];
    var option = {
        media: [ // each rule of media query is defined here
            {
                query: {
                    minWidth: 551
                },   // write rule here
                option: {
                    dataZoom: [
                        {
                            type: 'inside',
                            xAxisIndex: [0],
                            realtime: false,
                        }
                    ],
                    xAxis: {
                        type: 'category',
                        name: cifras_en_dolares,
                        nameLocation: 'center',
                        nameTextStyle: {
                            color: colors[0],
                            fontFamily: 'LibreFranklin',
                            fontSize: 14,
                            align: 'center',
                            verticalAlign: 'bottom',
                            lineHeight: 19.2,
                            padding: [10, 0, 0, 0],
                            fontWeight: 700
                        },
                        maxInterval: 1,
                        splitNumber: 1,
                        data: ['2016', '2017', '2018', '2019'],
                        min: '2016',
                        axisLine: {
                            show: false,
                        },
                        axisLabel: {
                            show: 'true',
                            textStyle: {
                                color: colors[0],
                                fontSize: 10.6,
                                fontFamily: 'LibreFranklin',
                            },
                            verticalAlign: 'center',
                            padding: [10, 0, 0, 0]
                        },
                    },
                    yAxis: {
                        type: 'value',
                        position: 'left',
                        maxInterval: 200000,
                        splitNumber: 1,
                        boundaryGap: false,
                        data: [0, 675, 1350, 2025, 2700],
                        offset: -20,
                        axisLine: {
                            show: false,
                        },
                        axisLabel: {
                            show: 'true',
                            textStyle: {
                                color: colors[0],
                                fontSize: 10.6,
                                fontFamily: 'LibreFranklin',
                            },
                            formatter: '${value}',
                            verticalAlign: 'bottom',
                            padding: [0, 0, 2, 0]
                        },
                        splitLine: {
                            interval: 1,
                            lineStyle: {
                                color: '#c2c2c2'
                            }
                        },
                    },
                    grid: {
                        right: 0,
                        // bottom: 90
                    },
                    series: [{
                        name: ' ',
                        type: 'bar',
                        smooth: true,
                        symbol: 'none',
                        barWidth: 80,
                        barGap: '-100%',
                        barCategoryGap: '10%',
                        data: [1708238, 1880027, 1676437, 1393282],

                    }],
                    animationEasing: 'elasticOut',
                    color: '#22a571',
                }
            },
            {
                query: {
                    maxWidth: 550
                },   // write rule here
                option: {
                    dataZoom: [
                        {
                            type: 'inside',
                            xAxisIndex: [0],
                            realtime: false,
                        }
                    ],
                    xAxis: {
                        type: 'category',
                        name: cifras_en_dolares,
                        nameLocation: 'center',
                        nameTextStyle: {
                            color: colors[0],
                            fontFamily: 'LibreFranklin',
                            fontSize: 14,
                            align: 'center',
                            verticalAlign: 'bottom',
                            lineHeight: 19.2,
                            padding: [10, 0, 0, 0],
                            fontWeight: 600
                        },
                        maxInterval: 1,
                        splitNumber: 1,
                        data: ['2016', '2017', '2018', '2019'],
                        min: '2016',
                        axisLine: {
                            show: false,
                        },
                        axisLabel: {
                            show: 'true',
                            textStyle: {
                                color: colors[0],
                                fontSize: 10.6,
                                fontFamily: 'LibreFranklin',
                            },
                            verticalAlign: 'center',
                            padding: [10, 0, 0, 0]
                        },
                    },
                    yAxis: {
                        type: 'value',
                        position: 'left',
                        maxInterval: 100000,
                        splitNumber: 1,
                        boundaryGap: false,
                        data: [0, 675, 1350, 2025, 2700],
                        offset: -20,
                        axisLine: {
                            show: false,
                        },
                        axisLabel: {
                            show: 'true',
                            textStyle: {
                                color: colors[0],
                                fontSize: 10.6,
                                fontFamily: 'LibreFranklin',
                            },
                            formatter: '${value}',
                            verticalAlign: 'bottom',
                            padding: [0, 0, 2, 0]
                        },
                        splitLine: {
                            interval: 1,
                            lineStyle: {
                                color: '#c2c2c2'
                            }
                        },
                    },
                    grid: {
                        right: 0,
                        // bottom: 90
                    },
                    series: [{
                        name: ' ',
                        type: 'bar',
                        smooth: true,
                        symbol: 'none',
                        barWidth: 40,
                        barGap: '-100%',
                        barCategoryGap: '10%',
                        data: [1708238, 1880027, 1676437, 1393282],

                    }],
                    animationEasing: 'elasticOut',
                    color: '#22a571',
                }
            },
            {
                query: {
                    maxWidth: 425
                },   // write rule here
                option: {
                    dataZoom: [
                        {
                            type: 'inside',
                            xAxisIndex: [0],
                            realtime: false,
                        }
                    ],
                    xAxis: {
                        type: 'category',
                        name: cifras_en_dolares,
                        nameLocation: 'center',
                        nameTextStyle: {
                            color: colors[0],
                            fontFamily: 'LibreFranklin',
                            fontSize: 14,
                            align: 'center',
                            verticalAlign: 'bottom',
                            lineHeight: 19.2,
                            padding: [10, 0, 0, 0],
                            fontWeight: 600
                        },
                        maxInterval: 1,
                        splitNumber: 1,
                        data: ['2016', '2017', '2018', '2019'],
                        min: '2016',
                        axisLine: {
                            show: false,
                        },
                        axisLabel: {
                            show: 'true',
                            textStyle: {
                                color: colors[0],
                                fontSize: 10.6,
                                fontFamily: 'LibreFranklin',
                            },
                            verticalAlign: 'center',
                            padding: [10, 0, 0, 0]
                        },
                    },
                    yAxis: {
                        type: 'value',
                        position: 'left',
                        maxInterval: 100000,
                        splitNumber: 1,
                        boundaryGap: false,
                        data: [0, 675, 1350, 2025, 2700],
                        offset: -20,
                        axisLine: {
                            show: false,
                        },
                        axisLabel: {
                            show: 'true',
                            textStyle: {
                                color: colors[0],
                                fontSize: 10.6,
                                fontFamily: 'LibreFranklin',
                            },
                            formatter: '${value}',
                            verticalAlign: 'bottom',
                            padding: [0, 0, 2, 0]
                        },
                        splitLine: {
                            interval: 1,
                            lineStyle: {
                                color: '#c2c2c2'
                            }
                        },
                    },
                    grid: {
                        right: 0,
                        // bottom: 90
                    },
                    series: [{
                        name: ' ',
                        type: 'bar',
                        smooth: true,
                        symbol: 'none',
                        barWidth: 25,
                        barGap: '-100%',
                        barCategoryGap: '10%',
                        data: [1708238, 1880027, 1676437, 1393282],

                    }],
                    animationEasing: 'elasticOut',
                    color: '#22a571',
                }
            }]
    };
    donacion.setOption(option);
    donacion.resize();
    });

    /*Code for Graphic of Argentina*/
    $("#inversion-social-argentina-tab").click(function (e) {
        var pest3 = document.getElementById('inversion-social-argentina');
        pest3.classList.add('d-block');
        pest3.style.width = '100%';
        pest3.style.height = '100%';

        var pest1 = document.getElementById('inversion-social');
        var pest2 = document.getElementById('action-social');
        pest1.classList.remove('d-block');
        pest2.classList.remove('d-block');
        
        var donacion2 = echarts.init(document.getElementById('inversion-social-argentina'));
        var colors = ['#676767', '#000', '#675bba'];
        var option = {
            media: [ // each rule of media query is defined here
                {
                    query: {
                        minWidth: 551
                    },   // write rule here
                    option: {
                        dataZoom: [
                            {
                                type: 'inside',
                                xAxisIndex: [0],
                                realtime: false,
                            }
                        ],
                        xAxis: {
                            type: 'category',
                            name: cifras_en_dolares,
                            nameLocation: 'center',
                            nameTextStyle: {
                                color: colors[0],
                                fontFamily: 'LibreFranklin',
                                fontSize: 14,
                                align: 'center',
                                verticalAlign: 'bottom',
                                lineHeight: 19.2,
                                padding: [10, 0, 0, 0],
                                fontWeight: 700
                            },
                            maxInterval: 1,
                            splitNumber: 1,
                            data: ['2019'],
                            min: '2019',
                            axisLine: {
                                show: false,
                            },
                            axisLabel: {
                                show: 'true',
                                textStyle: {
                                    color: colors[0],
                                    fontSize: 10.6,
                                    fontFamily: 'LibreFranklin',
                                },
                                verticalAlign: 'center',
                                padding: [10, 0, 0, 0]
                            },
                        },
                        yAxis: {
                            type: 'value',
                            position: 'left',
                            maxInterval: 200000,
                            splitNumber: 1,
                            boundaryGap: false,
                            data: [0, 675, 1350, 2025, 2700],
                            offset: -20,
                            axisLine: {
                                show: false,
                            },
                            axisLabel: {
                                show: 'true',
                                textStyle: {
                                    color: colors[0],
                                    fontSize: 10.6,
                                    fontFamily: 'LibreFranklin',
                                },
                                formatter: '${value}',
                                verticalAlign: 'bottom',
                                padding: [0, 0, 2, 0]
                            },
                            splitLine: {
                                interval: 1,
                                lineStyle: {
                                    color: '#c2c2c2'
                                }
                            },
                        },
                        grid: {
                            right: 0,
                            // bottom: 90
                        },
                        series: [{
                            name: ' ',
                            type: 'bar',
                            smooth: true,
                            symbol: 'none',
                            barWidth: 80,
                            barGap: '-100%',
                            barCategoryGap: '10%',
                            label: {
                                show: true,
                                position: 'top',
                                  color: '#FFFFFF',
                                  fontFamily: 'LibreFranklin',
                                fontSize: 10.6,
                                  lineHeight: 19.2,
                                backgroundColor: 'rgba(34,165,113,1)',
                                padding: [8, 6, 5, 6],
                                  formatter: function(params) {
                                    return new Intl.NumberFormat('ja-US', { style: 'currency', currency: 'USD' }).format( params.value); 
                                }
                            },
                            data: [1986310],
    
                        }],
                        animationEasing: 'elasticOut',
                        color: '#22a571',
                    }
                },
                {
                    query: {
                        maxWidth: 550
                    },   // write rule here
                    option: {
                        dataZoom: [
                            {
                                type: 'inside',
                                xAxisIndex: [0],
                                realtime: false,
                            }
                        ],
                        xAxis: {
                            type: 'category',
                            name: cifras_en_dolares,
                            nameLocation: 'center',
                            nameTextStyle: {
                                color: colors[0],
                                fontFamily: 'LibreFranklin',
                                fontSize: 14,
                                align: 'center',
                                verticalAlign: 'bottom',
                                lineHeight: 19.2,
                                padding: [10, 0, 0, 0],
                                fontWeight: 600
                            },
                            maxInterval: 1,
                            splitNumber: 1,
                            data: ['2019'],
                            min: '2019',
                            axisLine: {
                                show: false,
                            },
                            axisLabel: {
                                show: 'true',
                                textStyle: {
                                    color: colors[0],
                                    fontSize: 10.6,
                                    fontFamily: 'LibreFranklin',
                                },
                                verticalAlign: 'center',
                                padding: [10, 0, 0, 0]
                            },
                        },
                        yAxis: {
                            type: 'value',
                            position: 'left',
                            maxInterval: 100000,
                            splitNumber: 1,
                            boundaryGap: false,
                            data: [0, 675, 1350, 2025, 2700],
                            offset: -20,
                            axisLine: {
                                show: false,
                            },
                            axisLabel: {
                                show: 'true',
                                textStyle: {
                                    color: colors[0],
                                    fontSize: 10.6,
                                    fontFamily: 'LibreFranklin',
                                },
                                formatter: '${value}',
                                verticalAlign: 'bottom',
                                padding: [0, 0, 2, 0]
                            },
                            splitLine: {
                                interval: 1,
                                lineStyle: {
                                    color: '#c2c2c2'
                                }
                            },
                        },
                        grid: {
                            right: 0,
                            // bottom: 90
                        },
                        series: [{
                            name: ' ',
                            type: 'bar',
                            smooth: true,
                            symbol: 'none',
                            barWidth: 40,
                            barGap: '-100%',
                            barCategoryGap: '10%',
                            label: {
                                show: true,
                                position: 'top',
                                  color: '#FFFFFF',
                                  fontFamily: 'LibreFranklin',
                                fontSize: 10.6,
                                  lineHeight: 19.2,
                                backgroundColor: 'rgba(34,165,113,1)',
                                padding: [8, 6, 5, 6],
                                  formatter: function(params) {
                                    return new Intl.NumberFormat('ja-US', { style: 'currency', currency: 'USD' }).format( params.value); 
                                }
                            },
                            data: [1986310],
    
                        }],
                        animationEasing: 'elasticOut',
                        color: '#22a571',
                    }
                },
                {
                    query: {
                        maxWidth: 425
                    },   // write rule here
                    option: {
                        dataZoom: [
                            {
                                type: 'inside',
                                xAxisIndex: [0],
                                realtime: false,
                            }
                        ],
                        xAxis: {
                            type: 'category',
                            name: cifras_en_dolares,
                            nameLocation: 'center',
                            nameTextStyle: {
                                color: colors[0],
                                fontFamily: 'LibreFranklin',
                                fontSize: 14,
                                align: 'center',
                                verticalAlign: 'bottom',
                                lineHeight: 19.2,
                                padding: [10, 0, 0, 0],
                                fontWeight: 600
                            },
                            maxInterval: 1,
                            splitNumber: 1,
                            data: ['2019'],
                            min: '2019',
                            axisLine: {
                                show: false,
                            },
                            axisLabel: {
                                show: 'true',
                                textStyle: {
                                    color: colors[0],
                                    fontSize: 10.6,
                                    fontFamily: 'LibreFranklin',
                                },
                                verticalAlign: 'center',
                                padding: [10, 0, 0, 0]
                            },
                        },
                        yAxis: {
                            type: 'value',
                            position: 'left',
                            maxInterval: 100000,
                            splitNumber: 1,
                            boundaryGap: false,
                            data: [0, 675, 1350, 2025, 2700],
                            offset: -20,
                            axisLine: {
                                show: false,
                            },
                            axisLabel: {
                                show: 'true',
                                textStyle: {
                                    color: colors[0],
                                    fontSize: 10.6,
                                    fontFamily: 'LibreFranklin',
                                },
                                formatter: '${value}',
                                verticalAlign: 'bottom',
                                padding: [0, 0, 2, 0]
                            },
                            splitLine: {
                                interval: 1,
                                lineStyle: {
                                    color: '#c2c2c2'
                                }
                            },
                        },
                        grid: {
                            right: 0,
                            // bottom: 90
                        },
                        series: [{
                            name: ' ',
                            type: 'bar',
                            smooth: true,
                            symbol: 'none',
                            barWidth: 25,
                            barGap: '-100%',
                            barCategoryGap: '10%',
                            label: {
                                show: true,
                                position: 'top',
                                  color: '#FFFFFF',
                                  fontFamily: 'LibreFranklin',
                                fontSize: 10.6,
                                  lineHeight: 19.2,
                                backgroundColor: 'rgba(34,165,113,1)',
                                padding: [8, 6, 5, 6],
                                  formatter: function(params) {
                                    return new Intl.NumberFormat('ja-US', { style: 'currency', currency: 'USD' }).format( params.value); 
                                }
                            },
                            data: [1986310],
    
                        }],
                        animationEasing: 'elasticOut',
                        color: '#22a571',
                    }
                }]
        };
        donacion2.setOption(option);
        donacion2.resize();
    });

});













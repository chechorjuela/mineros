﻿var mounths = ["Ene'", "Feb'", "Mar'", "Abr'", "May'", "Jun'", "Jul'", "Ago'", "Sep'", "Oct'", "Nov'", "Dic'"];
var days = ["Dom'","Lun'","Mar'","Mie'","Jue'","Vie'","Sab'"];

function format_convert_number(number) {
    return number.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
}

function convert_date(date_formate) {
    date_formate = new Date(parseInt(date_formate));
    var month = date_formate.getMonth() + 1 < 10 ? "0" + (date_formate.getMonth() + 1) : (date_formate.getMonth() + 1);
    //d-m-y
    var formatHout = formatAMPM(date_formate);
    var location_module = window.location.pathname.replace(/\//g, "");
    var return_format = date_formate.getDate() + "-" + month + "-" + date_formate.getFullYear() + " " + formatHout;
    if (location_module.substring(0, 2) === "en") {
        return_format = month + "-" + date_formate.getDate() + "-" + date_formate.getFullYear() + " " + formatHout;
    }
    return return_format;
}

function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'P M' : 'A M';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}

function formatDateGraphics(date) {
    date = new Date(date);
    var fullYearString = date.getFullYear().toString();
    return "" + date.getDate() + " " + days[date.getDay()] + " " + mounths[date.getMonth()] + " " + fullYearString.substring(2, fullYearString.length);

}
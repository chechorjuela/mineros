﻿/***SETTINGS APP*/
var Authorization = "Basic c2VjdXJpdHl2cGM6SW5mby1GYWN0b3I=";
var region_pref = "es";
if (window.LANGUAGE === "english") {
    region_pref = "en";
}
// First, checks if it isn't implemented yet.
var api_url = "/DesktopModules/services/API/";
var KEYGOOGLE = "AIzaSyAhpdC-ZJFqXO3VbqR9-L6GeAnCw1KO9nY";
function angularInit(data) {
    /*** LIBRARY ngYoutubeEmbed call */
    //var jQueryScript = document.createElement('script');
    //jQueryScript.setAttribute('src', 'https://cdn.jsdelivr.net/npm/ng-youtube-embed@1.7.15/build/ng-youtube-embed.min.js');
    //document.head.appendChild(jQueryScript);
    /********************************/

    var sf = $.ServicesFramework(data.moduleId);
    app = angular.module('itemApp' + data.moduleId, ['ngAnimate', 'ngSanitize', 'kendo.directives','multipleSelect']).run(['$rootScope', '$sce', function ($rootScope, $sce) {


        $rootScope.guidGenerator = function () {
            var S4 = function () {
                return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
            };
            return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
        };
        $rootScope.eventGuidAux = $rootScope.guidGenerator();

        //Async Loader
        $rootScope.asyncLoader = function (status) {
            var load = angular.element('#load');
            if (status) {
                load.show();
            } else {
                load.hide();
            }
        };

        //Capital Letter
        $rootScope.capitalizeFirstLetter = function (string) {
            return string.charAt(0).toUpperCase() + string.slice(1);
        }
    }]);

    app.constant("sf", sf);
    app.constant("module_resources", data.module_resources);
    app.constant("common_resources", data.common_resources);
    app.constant("portal_id", data.portalId);
    app.constant("module_id", data.moduleId);
    app.constant("userID", data.userID || 0);
    app.constant("api_url", "/DesktopModules/services/API/");
    app.constant("culture", data.culture);
    app.config(['$httpProvider', '$locationProvider', function ($httpProvider, $locationProvider) {
        var httpHeaders = {
            "PortalId": data.portalId, "RequestVerificationToken": sf.getAntiForgeryValue(),
            "Authorization": Authorization
        };
        angular.extend($httpProvider.defaults.headers.common, httpHeaders);

        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    }]);

    app.directive('nullForm', [function () {
        return {
            restrict: 'A',
            require: '?form',
            link: function link(scope, element, iAttrs, formController) {
                if (!formController) {
                    return;
                }

                var parentFormController = element.parent().controller('form');
                parentFormController.$removeControl(formController);
            }
        };
    }]);
    return app;
}
﻿app.factory('notService', [function () {
    var notS = this;

    this.kendoNotificationControl = null;
    this.setControl = function (control) {
        this.kendoNotificationControl = control;
    }

    this.success = function (message) {
        message = message || "";
        this.kendoNotificationControl.show({ kValue: message, kType: "success" }, "template");
    }

    this.error = function (message) {
        message = message || "";
        this.kendoNotificationControl.show({ kValue: message, kType: "error" }, "template");
        $(".loading").removeClass("show");
    }

    this.info = function (message) {
        message = message || "";
        this.kendoNotificationControl.show({ kValue: message, kType: "info" }, "template");
    }

    this.warning = function (message) {
        message = message || "";
        this.kendoNotificationControl.show({ kValue: message, kType: "warning" }, "template");
        $(".loading").removeClass("show");
    }

    return this;
}]);
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="View.ascx.cs" Inherits="DesktopModules_Mineros_FoundationMinerosModule_View" %>

<div class="" ng-app="itemApp<%=ModuleId%>" ng-controller="FoundationMinerosModuleController">
    <div class="row ng-cloak" ng-cloak>
        <div class="content_descarga col-md-12">
            <div class="" ng-repeat="category in categories">
                <div class="title">{{category}}</div>
                <div class="info">
                    <a target="_blank" href="/DesktopModules/EasyDNNNews/DocumentDownload.ashx?portalid={{list.PortalId}}&moduleid={{list.ModuleId}}&articleid={{list.ArticleID}}&documentid={{list.DocEntryID}}" class="descarga" download="" ng-repeat="list in list_news[categories.indexOf(category)]">
                    <div class="icon">
                        <svg>
                            <g>
                                <path d="M14.6,13.9c-0.6-0.7-1.3-1.5-1.8-2.2c-0.3-0.3-0.5-0.7-0.8-1l0,0c0.4-1,0.6-1.8,0.6-2.4 c0.1-1.6-0.1-2.6-0.5-3.1c-0.3-0.3-0.8-0.5-1.3-0.3C10.5,5,10,5.3,9.8,6c-0.4,1.1-0.2,3.2,0.9,4.9c-0.5,1.2-1.1,2.7-1.9,4 c-1.4,0.5-2.5,1.1-3.3,1.9c-1,1-1.5,2-1.2,2.8c0.2,0.5,0.6,0.7,1.2,0.7c0.4,0,0.8-0.1,1.2-0.4c1-0.6,2.3-2.8,3-4 c1.4-0.4,2.8-0.6,3.6-0.7c0.3,0,0.6-0.1,1-0.1c1.3,1.3,2.3,2,3.2,2.2c0.2,0,0.4,0.1,0.6,0.1c0.8,0,1.4-0.3,1.7-0.8 c0.2-0.4,0.2-0.8,0-1.2c-0.5-0.9-2.1-1.4-4.5-1.4C15,13.9,14.8,13.9,14.6,13.9z M6,19c-0.2,0.1-0.4,0.2-0.5,0.2c0,0,0,0-0.1,0 c0-0.1,0.1-0.6,0.9-1.4c0.4-0.4,0.9-0.7,1.5-1C7.1,17.8,6.4,18.7,6,19z M10.9,6.4C11,6.1,11.1,6,11.2,6c0,0,0,0,0,0 c0.1,0.1,0.3,0.5,0.2,2.2c0,0.3-0.1,0.7-0.2,1.1C10.8,8.3,10.7,7.1,10.9,6.4z M13,14c-0.6,0.1-1.6,0.2-2.7,0.4 c0.4-0.8,0.8-1.7,1.1-2.5c0.1,0.1,0.2,0.3,0.3,0.4C12.2,12.9,12.7,13.5,13,14L13,14z M18.6,15.9C18.6,16,18.6,16,18.6,15.9 c0,0.1-0.3,0.3-0.7,0.3c-0.1,0-0.2,0-0.3,0c-0.5-0.1-1.1-0.4-1.7-1C17.7,15.2,18.4,15.7,18.6,15.9z"></path>
                                <path d="M17.1,0H1.7C0.8,0,0,0.8,0,1.7V29c0,0.9,0.8,1.7,1.7,1.7h20.6c0.9,0,1.7-0.8,1.7-1.7V6.9L17.1,0z M17.1,2.4 l4.4,4.4h-4.4V2.4z M22.3,29H1.7V1.7h13.7v5.1c0,0.9,0.8,1.7,1.7,1.7h5.1L22.3,29L22.3,29z"></path>
                                <path d="M9.2,21.9c-0.1-0.1-0.3-0.2-0.5-0.3c-0.2-0.1-0.5-0.1-0.8-0.1H6.7c-0.2,0-0.4,0-0.5,0.1 c-0.1,0.1-0.2,0.3-0.2,0.5v3.4c0,0.2,0,0.4,0.1,0.5c0.1,0.1,0.2,0.2,0.4,0.2c0.2,0,0.3-0.1,0.4-0.2c0.1-0.1,0.1-0.3,0.1-0.5v-1.2 h0.7c0.6,0,1-0.1,1.3-0.4c0.3-0.2,0.4-0.6,0.4-1.1c0-0.2,0-0.4-0.1-0.6C9.4,22.2,9.3,22.1,9.2,21.9z M8.4,23.3 c-0.1,0.1-0.2,0.1-0.3,0.2c-0.1,0-0.3,0.1-0.5,0.1H7.1v-1.2h0.5c0.5,0,0.7,0.1,0.7,0.2c0.1,0.1,0.1,0.2,0.1,0.4 C8.5,23.1,8.5,23.3,8.4,23.3z"></path>
                                <path d="M13.4,22c-0.2-0.2-0.4-0.3-0.6-0.3c-0.2-0.1-0.5-0.1-0.8-0.1h-1.2c-0.2,0-0.4,0.1-0.5,0.2s-0.2,0.3-0.2,0.5 v3.2c0,0.2,0,0.3,0,0.4c0,0.1,0.1,0.2,0.2,0.3c0.1,0.1,0.2,0.1,0.4,0.1H12c0.2,0,0.4,0,0.6,0c0.2,0,0.3-0.1,0.5-0.1 c0.1-0.1,0.3-0.2,0.4-0.3c0.2-0.1,0.3-0.3,0.4-0.5c0.1-0.2,0.2-0.4,0.2-0.6c0-0.2,0.1-0.5,0.1-0.7C14.1,23,13.9,22.4,13.4,22z M12.6,25.1c-0.1,0.1-0.1,0.1-0.2,0.1c-0.1,0-0.2,0-0.2,0.1c-0.1,0-0.2,0-0.3,0h-0.6v-2.8h0.5c0.3,0,0.5,0,0.7,0.1 c0.2,0.1,0.3,0.2,0.4,0.4c0.1,0.2,0.2,0.5,0.2,0.9C13,24.4,12.9,24.8,12.6,25.1z"></path>
                                <path d="M17.4,21.6h-2.1c-0.1,0-0.3,0-0.3,0.1c-0.1,0-0.2,0.1-0.2,0.2c0,0.1-0.1,0.2-0.1,0.3v3.4c0,0.2,0,0.4,0.1,0.5 c0.1,0.1,0.2,0.2,0.4,0.2c0.2,0,0.3-0.1,0.4-0.2c0.1-0.1,0.1-0.3,0.1-0.5v-1.4h1.4c0.2,0,0.3,0,0.4-0.1c0.1-0.1,0.1-0.2,0.1-0.3 c0-0.1,0-0.2-0.1-0.3c-0.1-0.1-0.2-0.1-0.4-0.1h-1.4v-1h1.7c0.2,0,0.3,0,0.4-0.1c0.1-0.1,0.1-0.2,0.1-0.3c0-0.1,0-0.2-0.1-0.3 C17.7,21.6,17.6,21.6,17.4,21.6z"></path>
                            </g></svg>
                    </div>
                     <div ng-if="list.LocaleCode=='es-ES'" id='eningles' class="text" ng-bind-html="SkipValidation(list.Title)"></div>
                                  
                      <div ng-if="list.LocaleCode=='en-US'" id='enespañol' class="text"  ng-bind-html="SkipValidation(list.Description)"></div>  
                   <!-- <div class="text" ng-bind-html="SkipValidation(list.Title)"></div>-->
                    <div class="icon_d" >
                        <svg>
                            <g>
                                <g>
                                    <path d="M15.4,10.9c-0.3,0-0.6,0.3-0.6,0.6v2.9c0,0.3-0.3,0.6-0.6,0.6H1.7c-0.3,0-0.6-0.3-0.6-0.6v-2.9 c0-0.3-0.3-0.6-0.6-0.6S0,11.1,0,11.4v2.9C0,15.2,0.8,16,1.7,16h12.6c0.9,0,1.7-0.8,1.7-1.7v-2.9C16,11.1,15.7,10.9,15.4,10.9z"></path>
                                    <path d="M7.6,13c0.2,0.2,0.6,0.2,0.8,0l4-4c0.2-0.2,0.2-0.6-0.1-0.8c-0.2-0.2-0.5-0.2-0.7,0l-3,3V0.6 C8.6,0.3,8.3,0,8,0C7.7,0,7.4,0.3,7.4,0.6v10.6l-3-3C4.2,8,3.8,8,3.6,8.2C3.4,8.4,3.4,8.8,3.6,9L7.6,13z"></path>
                                </g>
                            </g></svg>
                    </div>
                </a>
                </div>
            </div>
           
        </div>
    </div>
</div>
<script>
    var data = {};
    data.moduleId = <%=ModuleId%>;
    data.module_resources = '<%=Resources%>';
    data.portalId = <%=PortalId%>;
    data.common_resources = '<%=CommonResources%>';
    data.culture = '<%=System.Threading.Thread.CurrentThread.CurrentCulture.Name %>';
    var app = angularInit(data);
</script>
<script src="/DesktopModules/Mineros/scripts/helpers/helpers.js"></script>
<script src="/DesktopModules/Mineros/FoundationMinerosModule/js/FoundationMinerosModule.controller.js"></script>
<script src="/DesktopModules/Mineros/FoundationMinerosModule/js/FoundationMinerosModule.service.js"></script>





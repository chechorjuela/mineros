﻿app.factory('secondSocialInvestmentService', ['api_url', '$http', function (api_url, $http) {
    var service = {};

    const route_cost = "/Index/GetRequestBvcByNemo?urlName=";
    const route_stock_file = "Index/FileGetStock";
    const route_fileGetStockHome = "Index/FileGetStockHome";

    service.GetCostByName = function (name) {
        var promise = $http.get(api_url + route_cost + name);

        promise.catch(function (data) {

        });

        return promise;
    };

    service.GetStock = function () {
        var promise = $http.get(api_url + route_stock_file );

        promise.catch(function (data) {

        });

        return promise;
    };

    service.FileGetStockHome = function () {
        var promise = $http.get(api_url + route_fileGetStockHome);

        promise.catch(function (data) {

        });

        return promise;
    }
    return service;
}]);
﻿app.controller('secondSocialInvestmentController', function ($scope, module_resources, common_resources, secondSocialInvestmentService, $q) {
    var name_enterprise = "Mineros";
    $scope.stockDataSencond;
    this.$onInit = function () {

        initializeFunctions();
       
    };
    //var name_enterprise = "Mineros"; 
    //// Variables 
    //$scope.module_resources = JSON.parse(module_resources);
    //$scope.common_resources = JSON.parse(common_resources);

    // Creates Functions or Actions to be used then.
    function initializeFunctions() {

        // Parallel loads
        $scope.asyncLoader(true);
        $q.all([
            secondSocialInvestmentService.GetStock(),
            secondSocialInvestmentService.FileGetStockHome()
        ]).then(function (data) {

            $scope.stockDataSencond = data[1].data.Data;
            //inicializarGraficaLinearlHome(data[1].data.Data);
            $scope.asyncLoader(false);
        });

    }

    var inicializarGraficaLinearlHome = function (stockData) {
        //console.info(stockData);
        //fechas_cemargos_pfcemargos = stockData.map(a => new Date(a.Fecha));
        //datos_cemargos = stockData.map(a => a.CEMARGOS);
        //datos_pfcemargos = stockData.map(a => a.PFCEMARGOS);
        //var max_cemargos = datos_cemargos.reduce(function (a, b) { return Math.max(a, b); });
        //var max_pfcemargos = datos_pfcemargos.reduce(function (a, b) { return Math.max(a, b); });
        //var min_cemargos = datos_cemargos.filter(function (it) { return it != 0 }).reduce(function (a, b) { return Math.min(a, b); });
        //var min_pfcemargos = datos_pfcemargos.filter(function (it) { return it != 0 }).reduce(function (a, b) { return Math.min(a, b); });
        ///*Gráfíca lineal del home*/
        //var dom = document.getElementById("acciones-anio");
        //var myChart = echarts.init(dom);
        //var app = {};
        //option = null;
        //option = {
        //    dataZoom: [{
        //        start: 98,
        //        end: 100,
        //    }, {
        //        type: 'inside',
        //        show: true,

        //        handleSize: 8
        //    }],
        //    // dataZoom: [,

        //    //     {
        //    //         type: 'slider',
        //    //         show: true,
        //    //         start: 98,
        //    //         end: 100,
        //    //         handleSize: 8
        //    //     }
        //    // ],
        //    legend: {
        //        data: ['Cemargos', 'Pfcemargos'],
        //        textStyle: {
        //            fontSize: 16
        //        },
        //        right: '3%',
        //        top: 8,
        //    },
        //    grid: {
        //        x: '7%', y: '18%', width: '84%', height: '72%',
        //        bottom: '10%',
        //        containLabel: true
        //    },
        //    tooltip: {
        //        trigger: 'axis',
        //        axisPointer: {
        //            type: 'shadow',
        //        },
        //    },
        //    xAxis: {
        //        axisLine: {
        //            lineStyle: {
        //                color: '#DEDEDE',
        //            }
        //        },
        //        axisLabel: {
        //            color: '#676767',
        //            fontSize: 12,
        //            fontFamily: "Franklin Gothic Book",
        //            formatter: function (value, index) {
        //                // Formatted to be month/day; display year only in the first label
        //                var d = new Date(value);
        //                var ano = d.getFullYear();
        //                var mes = ((d.getMonth() + 1).toString().length == 1) ? ("0" + (d.getMonth() + 1).toString()) : (d.getMonth() + 1);
        //                var dia = ((d.getDate().toString().length == 1) ? "0" + d.getDate().toString() : d.getDate());
        //                var datestring = ano + "/" + mes + "/" + dia + " " +
        //                    d.getHours() + ":" + d.getMinutes();
        //                return datestring;
        //            }
        //        },
        //        axisTick: {
        //            lineStyle: {
        //                color: '#C4C4C4',
        //                shadowColor: '#C4C4C4',
        //                shadowOffsetY: 6.5
        //            },
        //            length: 6.5,
        //            alignWithLabel: true,
        //            inside: true,

        //        },
        //        //type: 'category',
        //        backgroundColor: '#676767',
        //        data: fechas_cemargos_pfcemargos,
        //    },
        //    yAxis: {
        //        axisLine: {
        //            lineStyle: {
        //                color: 'transparent',
        //            }
        //        },
        //        splitLine: {
        //            lineStyle: {
        //                color: '#DEDEDE',
        //            }
        //        },
        //        axisLabel: {
        //            color: '#676767',
        //            fontSize: 10,
        //            fontFamily: "Franklin Gothic Book",
        //            formatter: function (value, index) {
        //                // Formatted to be month/day; display year only in the first label
        //                return (value / 1000).toString() + "k";
        //            }
        //        },
        //        nameTextStyle: {
        //            color: '#676767',
        //            fontSize: 16,
        //        },
        //        type: 'value',
        //        position: 'right',
        //        nameLocation: 'center',
        //        name: 'Precio (COP $)',
        //        nameGap: 55,
        //        fontFamily: "Franklin Gothic Book",
        //        fontSize: 16,
        //        // max: (max_cemargos > max_pfcemargos) ? max_cemargos : max_pfcemargos,
        //        min: (min_cemargos < min_pfcemargos) ? min_cemargos : min_pfcemargos,
        //        color: '#676767',
        //    },
        //    // dataZoom: [{
        //    // }, {
        //    //     type: 'inside',
        //    // }],
        //    series: [{
        //        data: datos_cemargos,
        //        type: 'line',
        //        smooth: true,
        //        name: 'Cemargos',
        //        color: '#087256',

        //    },
        //    {
        //        data: datos_pfcemargos,
        //        type: 'line',
        //        smooth: true,
        //        name: 'Pfcemargos',
        //        color: '#8AB63F',
        //    },
        //    ]
        //};
        //if (option && typeof option === "object") {
        //    myChart.setOption(option, true);
        //}
    }
});
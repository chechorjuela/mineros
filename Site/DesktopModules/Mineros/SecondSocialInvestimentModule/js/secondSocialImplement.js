﻿$(document).ready(function () {
    const route_cost = "/Index/GetRequestBvcByNemo?urlName=";
    const route_stock_file = "Index/FileGetStock";
    const route_fileGetStockHome = "Index/FileGetStockHome";
    var module_search = "Gold";
    var search_module;
    var location_module = window.location.pathname.replace(/\//g, "");
    if (location_module.substring(0, 2) === "en") {
        $("#first-span-socialInvestiment").text("GOLD");
        $("#second-span-socialInvestiment").text("PRICE");
    } else {
        $("#second-span-socialInvestiment").text("DEL ORO");
    }
    $.ajax({
        url: api_url + route_fileGetStockHome,
        method: "GET",
        dataType: "JSON",
        data: "",
        success: function (resp) {
            if (resp.Header.Code === 200) {
                search_module = resp.Data.find(function (f) {
                    return f.name === module_search;
                });
                if (typeof search_module !== "undefined") {
                    var number_convert = format_convert_number(parseInt(search_module.value));
                    var date_convert = convert_date(search_module.timespan);
                    $("#val-oro .val-numerico").text(number_convert);
                    $("#val-oro .text-footer-fecha-mark ").text(date_convert);
                }
                
                $("#val-oro").fadeIn();
            }
        }
    })
})
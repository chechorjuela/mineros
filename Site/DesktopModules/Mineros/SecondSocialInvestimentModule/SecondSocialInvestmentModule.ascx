﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SecondSocialInvestmentModule.ascx.cs" Inherits="DesktopModules_Mineros_SecondSocialInvestmentModule_SecondSocialInvestmentModule" %>
<div id="val-oro" class="rounded-0 simbol-dolar card mb-3 target-med bg-corn position-relative overflow-hidden" style="max-width: 18rem;display:none;background-color: #daa900;" data-aos="flip-left">
   <div class="card-header bg-transparent border-success">
   <h4 class="text-left">
    <span id="first-span-socialInvestiment" class="title-target-sub text-uppercase text-target-card librefranklin-thin text-white d-block"><%=LocalizeString("firstposition.text") %></span>
    <span id="second-span-socialInvestiment" class="title-target-sub text-uppercase text-target-card librefranklin-semibold text-white d-block"><%=LocalizeString("firstposition.text") %></span>
   </h4>
    </div>
    <div class="card-body text-success border-bottom">
        <span class="val-numerico librefranklin-semibold text-white"></span>
        <span class="val-text librefranklin-semibold text-white">USD/Oz</span>
    </div>
    <div class="card-footer bg-transparent border-success">
        <span class="text-footer-tittle-mark librefranklin-regular text-white color-mineral-green d-block"><%=LocalizeString("qualitication.text")%></span>
        <span class="text-footer-fecha-mark librefranklin-bold text-white color-mineral-green d-block"></span>
    </div>
</div>

<script src="/DesktopModules/Mineros/scripts/helpers/helpers.js"></script>
<script src="/DesktopModules/Mineros/SecondSocialInvestimentModule/js/secondSocialImplement.js"></script>

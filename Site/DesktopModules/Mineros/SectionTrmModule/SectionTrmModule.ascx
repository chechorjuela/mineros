﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SectionTrmModule.ascx.cs" Inherits="DesktopModules_Mineros_SectionTrmModule_SectionTrmModule" %>
<script>
    var data = {};
    data.moduleId = <%=ModuleId%>;
    data.resources = '<%=Resources%>';
</script>

<div id="val-trm" class="rounded-0 simbol-dolar border-0 card border-success mb-3 target-med position-relative overflow-hidden" style="max-width: 18rem;display:none;background-color:#28a745!important" data-aos="flip-left">
    <div class="card-header bg-transparent border-success">
        <h4 class="text-left">
            <span class="title-target-sub text-uppercase text-target-card librefranklin-semibold text-white d-block">TRM</span>
        </h4>
    </div>
    <div class="card-body text-success border-bottom">
        <span class="val-numerico librefranklin-semibold text-white"></span>
        <span class="val-text librefranklin-semibold text-white"> COP</span>
    </div>
    <div class="card-footer bg-transparent border-success">
        <span id="lastqualification-trm" class="text-footer-tittle-mark librefranklin-regular text-white color-mineral-green d-block"><%=LocalizeString("marcacion.text") %></span>
        <span class="text-footer-fecha-mark librefranklin-bold text-white color-mineral-green d-block"></span>
    </div>
</div>

<script src="/DesktopModules/Mineros/scripts/helpers/helpers.js"></script>
<script src="/DesktopModules/Mineros/SectionTrmModule/js/SectionTrm.js"></script>

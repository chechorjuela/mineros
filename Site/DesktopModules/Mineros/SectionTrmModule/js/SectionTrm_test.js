﻿$(document).ready(function () {
    const route_getTrm = "Index/GetTrm";
    $.ajax({
        url: api_url + route_getTrm,
        method: "GET",
        dataType: "JSON",
        data: "",
        success: function (resp) {
            if (resp.Header.Code === 200) {
                if (resp.Data !== "") {
                    var toJson
                    objects_array = resp.Data.replace("[{", "").replace("}]", "");
                    var values_object = objects_array.split(",");
                    var formate_string = "";
                    var bool_more_string = false;
                    for (var i = 0; i < values_object.length; i++) {
                        var val = values_object[i].charAt(0) == ":" ? values_object[i].substring(1, values_object[i].length).split(":") : values_object[i].split(":");
                        var string_value = "";
                        for (var r = 0; r < val.length; r++) {
                            var string_val = "";
                            if (val[r] != "") {
                                if (val.length > 2) {
                                    bool_more_string = true;
                                }
                                if (r == 0) {
                                    string_val += '"' + val[r] + '":';
                                }
                                else if (r < val.length - 1) {
                                    var aux = "";
                                    if (r == 1) {
                                        aux += '"';
                                    }
                                    string_val += aux + val[r] + ":";
                                }

                                if (r == val.length - 1) {
                                    if (!bool_more_string && r > 1) {
                                          string_val+='"';
                                    }
                                    if (r == 1) {
                                        string_val+='"';
                                    }
                                    bool_more_string = false;
                                    var aux_s = ",";
                                    if (i == values_object.length-1) {
                                        aux_s = "";
                                    }
                                    string_val += val[r] + '"' + aux_s;
                                }
                                string_value += string_val.replace(/(\r\n|\n|\r)/gm, "");
                            }
                        }
                        formate_string += string_value;
                        toJson = "{" + formate_string + "}";

                    }
                    var obj_json = JSON.parse(toJson);
                    //console.info(obj_json);
                    $("#val-trm .val-numerico").text(format_convert_number(parseInt(obj_json.valor)));

                    var date_convert = convert_date(new Date(obj_json.vigenciadesde).getTime());
                    var index_first_spacing = date_convert.indexOf(" ");

                    $("#val-trm .text-footer-fecha-mark ").text(date_convert.slice(0, index_first_spacing));
                }

                $("#val-trm").fadeIn();
            }
        }
    });
})
﻿$(document).ready(function () {
    var location_module = window.location.pathname.replace(/\//g, "");
    if (location_module.substring(0, 2) === "en") {
        $("#lastqualification-trm").text("Lastest Update");
    }
    const route_getTrm = "Index/GetTrm";
    $.ajax({
        url: api_url + route_getTrm,
        method: "GET",
        dataType: "JSON",
        data: "",
        success: function (resp) {
            console.info(resp);
            if (resp.Header.Code === 200) {
                if (resp.Data !== "") {
                    $("#val-trm .val-numerico").text(resp.Data);

                    var date_convert = convert_date(new Date().getTime());
                    var index_first_spacing = date_convert.indexOf(" ");

                    $("#val-trm .text-footer-fecha-mark ").text(date_convert.slice(0, index_first_spacing));
                }

                $("#val-trm").fadeIn();
            }
        }
    });
})
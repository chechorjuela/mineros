﻿
!function ($) {

    "use strict";

    // Global Onyx object
    var Onyx = Onyx || {};

    $.ajax({
        type: "GET",
        url: api_url + "Certificate/GetCertificateType",
        success: function (response) {
            if (response.Header.Code == 200) {
                $.each(response.Data, function (index, value) {
                    $(".type_certificate").append("<option value='" + value.CertificateID + "'>" + value.NameSelect + "</option>")
                })
            }
        }
    });

    $.ajax({
        type: "GET",
        url: api_url + "Certificate/GetCustomers",
        success: function (response) {
            if (response.Header.Code == 200) {
                $.each(response.Data, function (index, value) {
                    $(".select_customer").append("<option value='" + value.CustomerId + "'>" + value.NameCustomer + "</option>")
                })
            }
        }
    });
    var years = [];
    var currentYarear = new Date().getFullYear();
    var starYear = starYear || 1960;
    var selected = "";

    while (starYear <= currentYarear) {
        if (starYear == currentYarear)
            selected = "selected";

        $(".select_year").append("<option " + selected + " value='" + starYear + "'>" + starYear + "</option>");
        starYear++;
    }
    Onyx = {

	    /**
		 * Fire all functions
		 */
        init: function () {
            var self = this,
                obj;
            for (obj in self) {
                if (self.hasOwnProperty(obj)) {
                    var _method = self[obj];
                    if (_method.selector !== undefined && _method.init !== undefined) {
                        if ($(_method.selector).length > 0) {
                            _method.init();
                        }
                    }
                }
            }
        },

		/**
		 * Files upload
		 */
        userFilesDropzone: {
            selector: 'form.dropzone',
            init: function () {
                var base = this,
                    container = $(base.selector);

                base.initFileUploader(base, 'form.dropzone');
            },
            initFileUploader: function (base, target) {
                var previewNode = document.querySelector("#onyx-dropzone-template");

                previewNode.id = "";

                var previewTemplate = previewNode.parentNode.innerHTML;
                previewNode.parentNode.removeChild(previewNode);

                var onyxDropzone = new Dropzone(target, {
                    url: api_url + 'Certificate/uploadFilesCertificate', // Check that our form has an action attr and if not, set one here
                    // maxFiles: 5,
                    method: "post",
                    // maxFilesize: 20,
                    autoProcessQueue: false,
                    uploadMultiple: true,
                    // autoQueue: false,
                    parallelUploads: 1000,
                    acceptedFiles: "application/pdf",
                    previewTemplate: previewTemplate,
                    previewsContainer: "#previews",
                    clickable: true,
					/**
					 * The text used before any files are dropped.
					 */
                    dictDefaultMessage: "Arrastra tus archivos aqui para subir.", // Default: Drop files here to upload
					/**
					 * The text that replaces the default message text it the browser is not supported.
					 */
                    dictFallbackMessage: "Your browser does not support drag'n'drop file uploads.", // Default: Your browser does not support drag'n'drop file uploads.
					/**
					 * If the filesize is too big.
					 * `{{filesize}}` and `{{maxFilesize}}` will be replaced with the respective configuration values.
					 */
                    dictFileTooBig: "File is too big ({{filesize}}MiB). Max filesize: {{maxFilesize}}MiB.", // Default: File is too big ({{filesize}}MiB). Max filesize: {{maxFilesize}}MiB.
					/**
					 * If the file doesn't match the file type.
					 */
                    dictInvalidFileType: "No puedes subir archivos de este tipo.", // Default: You can't upload files of this type.
					/**
					 * If the server response was invalid.
					 * `{{statusCode}}` will be replaced with the servers status code.
					 */
                    dictResponseError: "Servicio no responde.", // Default: Server responded with {{statusCode}} code.
					/**
					 * If `addRemoveLinks` is true, the text to be used for the cancel upload link.
					 */
                    dictCancelUpload: "Cancel upload.", // Default: Cancel upload
					/**
					 * The text that is displayed if an upload was manually canceled
					 */
                    dictUploadCanceled: "Upload canceled.", // Default: Upload canceled.
					/**
					 * If `addRemoveLinks` is true, the text to be used for confirmation when cancelling upload.
					 */
                    dictCancelUploadConfirmation: "Are you sure you want to cancel this upload?", // Default: Are you sure you want to cancel this upload?
					/**
					 * If `addRemoveLinks` is true, the text to be used to remove a file.
					 */
                    dictRemoveFile: "Remove file", // Default: Remove file
					/**
					 * If this is not null, then the user will be prompted before removing a file.
					 */
                    dictRemoveFileConfirmation: null, // Default: null
					/**
					 * Displayed if `maxFiles` is st and exceeded.
					 * The string `{{maxFiles}}` will be replaced by the configuration value.
					 */
                    dictMaxFilesExceeded: "Tu no puedes subir mas archivos.", // Default: You can not upload any more files.
					/**
					 * Allows you to translate the different units. Starting with `tb` for terabytes and going down to
					 * `b` for bytes.
					 */
                    dictFileSizeUnits: { tb: "TB", gb: "GB", mb: "MB", kb: "KB", b: "b" },

                });

                // Dropzone.autoDiscover = false;

                onyxDropzone.on("addedfile", function (file) {
                    $('.preview-container').css('visibility', 'visible');
                    file.previewElement.classList.add('type-' + base.fileType(file.name)); // Add type class for this element's preview
                });

                onyxDropzone.on("totaluploadprogress", function (progress) {

                    var progr = document.querySelector(".progress .determinate");

                    if (progr === undefined || progr === null) return;

                    progr.style.width = progress + "%";
                });

                onyxDropzone.on('dragenter', function () {
                    $(target).addClass("hover");
                });

                onyxDropzone.on('dragleave', function () {
                    $(target).removeClass("hover");
                });

                onyxDropzone.on('drop', function () {
                    $(target).removeClass("hover");
                });

                onyxDropzone.on('addedfile', function () {
                    // Remove no files notice
                    $(".no-files-uploaded").slideUp("easeInExpo", function () {
                        $(".uploaded-files-count").text($("#previews").find(".type-pdf").length);
                    });


                });

                onyxDropzone.on('removedfile', function (file) {

                    //$.ajax({
                    //    type: "POST",
                    //    url: ($(target).attr("action")) ? $(target).attr("action") : "../../file-upload.php",
                    //    data: {
                    //        target_file: file.upload_ticket,
                    //        delete_file: 1
                    //    }
                    //});

                    // Show no files notice
                    $(".uploaded-files-count").text($("#previews").find(".type-pdf").length);
                    if ($("#previews").find(".type-pdf").length == 0) {
                        $(".no-files-uploaded").slideDown("easeInExpo", function () {
                            $(".uploaded-files-count").text($("#previews").find(".type-pdf").length);

                        });
                       
                    }

                });

                onyxDropzone.on('sendingmultiple', function (data, xhr, formData) {
                    // this will get sent
                    // formData.append('name', jQuery('#name').val());

                    // this won't -- we don't need this rn, we can just use jQuery
                    // var myForm = document.querySelector('form');

                    // you are overwriting your formdata here.. remove this
                    //formData = new FormData(myForm);

                    // instead, just append the form elements to the existing formData
                    $("form.fields").find("input").each(function () {
                        formData.append($(this).attr("name"), $(this).val());
                    });
                    $("form.fields").find("select").each(function () {
                        formData.append($(this).attr("name"), $(this).children("option:selected").val());
                    });
                });
                onyxDropzone.on('successmultiple', function (files, response) {
                    //window.location.replace("/home");
                });
                onyxDropzone.on('errormultiple', function (files, response) {
                    // alert(response);
                });

                onyxDropzone.on("success", function (file, response) {
                    var warningsHolder = $("#warnings");
                    if (typeof response != "undefined") {
                        if (response.Header.Code == 200) {

                            let parsedResponse = response.Data;
                            // file.upload_ticket = parsedResponse.file_link;
                            if (typeof parsedResponse !== 'undefined') {
                                var text = "";
                                $.each(parsedResponse, function (index, value) {
                                    text += "<p>" + value.FileName + " <strong> Archivo Subido</strong></p>"
                                })
                                warningsHolder.children('span').html(text);
                                warningsHolder.slideDown("easeInExpo");
                            }

                            $("#previews").html("");
                            $("input").val("")
                            $(".no-files-uploaded").slideUp("easeInExpo", function () {
                                $(".uploaded-files-count").text($("#previews").find(".type-pdf").length);
                            });
                        }
                    }
                });
                onyxDropzone.autoDiscover = false;

                $(".send-files").click(function (e) {
                    $("button.send-files").attr('disabled', 'disabled');
                    e.preventDefault();
                    onyxDropzone.processQueue();
                    setTimeout(function () {
                        $(".send-files").removeAttr("disabled");
                        $("#previews").html("");
                        $(".no-files-uploaded").slideUp("easeInExpo", function () {
                            $(".uploaded-files-count").text($("#previews").find(".type-pdf").length);
                        });
                    }, 5000);
                })

            },

            dropzoneCount: function () {
                var filesCount = $("#previews > .dz-success.dz-complete").length;
                return filesCount;
            },
            fileType: function (fileName) {
                var fileType = (/[.]/.exec(fileName)) ? /[^.]+$/.exec(fileName) : undefined;
                return fileType[0];
            }
        }
    }

    $(document).ready(function () {
        Onyx.init();

    });

}(jQuery);


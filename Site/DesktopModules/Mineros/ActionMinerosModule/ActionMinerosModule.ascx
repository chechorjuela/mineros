﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ActionMinerosModule.ascx.cs" Inherits="DesktopModules_Mineros_ActionMinerosModule_ActionMinerosModule" %>

<div id="val-mineros" class="rounded-0 target-med simbol-dolar card border-success mb-3 espcial-card bg-jungle-green overflow-hidden aos-init aos-animate" style="max-width: 18rem;display:none" data-aos="flip-left">
    <div class="card-header bg-transparent border-success p-0 border-0">
        <h4 class="text-left">
            <span id="first-title-action" class="title-target-sub text-uppercase text-target-card librefranklin-thin text-white d-block"><%=LocalizeString("firstposition.text")%></span>
            <span id="second-title-action" class="title-target-sub text-uppercase text-target-card librefranklin-semibold text-white d-block"><%=LocalizeString("secondposition.text") %></span>
        </h4>
    </div>
    <div class="card-body text-success border-bottom">
        <span class="val-numerico librefranklin-semibold text-white">1.920,00</span>
        <span class="val-text librefranklin-semibold text-white">COP</span>
    </div>
    <div class="card-footer bg-transparent border-success">
        <span id="lastqualification-action" class="text-footer-tittle-mark librefranklin-regular text-white color-mineral-green d-block"><%=LocalizeString("lastqualification.text") %></span>
        <span class="text-footer-fecha-mark librefranklin-bold color-mineral-green text-white d-block">04-03-2019 06:08 A M</span>
    </div>
</div>
<script src="/DesktopModules/Mineros/scripts/helpers/helpers.js"></script>
<script src="/DesktopModules/Mineros/ActionMinerosModule/js/actionMinerosModule.js"></script>

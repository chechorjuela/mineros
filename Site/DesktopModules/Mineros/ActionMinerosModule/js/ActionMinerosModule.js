﻿$(document).ready(function () {
    const route_cost = "/Index/GetRequestBvcByNemo?urlName=";
    const route_stock_file = "Index/FileGetStock";
    const route_fileGetStockHome = "Index/FileGetStockHome";
    var search_accion = "MINEROS";
    var search_module;
    var location_module = window.location.pathname.replace(/\//g, "");
  
    if (location_module.substring(0,2) === "en") {
        $("#first-title-action").text("Stock price");
        $("#second-title-action").text("Mineros");
        $("#lastqualification-action").text("Lastest Update");
    }
    $.ajax({
        url: api_url + route_cost + search_accion,
        method: "GET",
        dataType: "JSON",
        data: "",
        success: function (resp) {
            if (resp.Header.Code == 200) {
                if (resp.Data !=null && typeof resp.Data != "undefined") {
                    search_module = resp.Data.find(function (f) {
                        return f.Accion === search_accion;
                    });
                    if (typeof search_module != "undefined") {

                        var number_convert = format_convert_number(search_module.Actual);
                        var date_convert = convert_date(new Date(search_module.Fecha).getTime());
                        $("#val-mineros .val-numerico").text(number_convert);
                        $("#val-mineros .text-footer-fecha-mark ").text(date_convert);
                    }

                    $("#val-mineros").fadeIn();
                }
                
            }
        }
    })
})
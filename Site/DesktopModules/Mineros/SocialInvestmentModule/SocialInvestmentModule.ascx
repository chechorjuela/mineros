﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SocialInvestmentModule.ascx.cs" Inherits="DesktopModules_Mineros_SocialInvestmentModule_SocialInvestmentModule" %>
<div id="val-plata" style="max-width: 18rem;background-color: #8a9597;" class="rounded-0 simbol-dolar border-0 card border-success mb-3 target-med bg-corn position-relative overflow-hidden" style="max-width: 18rem;display:none" data-aos="flip-left">
    <div class="card-header bg-transparent border-success">
        <h4 class="text-left">
            <span id="first_title_silver" class="title-target-sub text-uppercase text-target-card librefranklin-thin text-white d-block"><%=LocalizeString("value.text") %></span>
            <span id="second_title_silver" class="title-target-sub text-uppercase text-target-card librefranklin-semibold text-white d-block"><%=LocalizeString("silver.text") %></span>
        </h4>
    </div>
    <div class="card-body text-success border-bottom">
        <span class="val-numerico librefranklin-semibold text-white"></span>
        <span class="val-text librefranklin-semibold text-white">USD/Oz</span>
    </div>
    <div class="card-footer bg-transparent border-success">
        <span id="lastqualification-silver" class="text-footer-tittle-mark librefranklin-regular text-white color-mineral-green d-block"><%=LocalizeString("qualification.text") %></span>
        <span class="text-footer-fecha-mark librefranklin-bold text-white color-mineral-green d-block"></span>
    </div>
</div>
<script src="/DesktopModules/Mineros/scripts/helpers/helpers.js"></script>
<script src="/DesktopModules/Mineros/SocialInvestmentModule/js/socialImplement.js"></script>

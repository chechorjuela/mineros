﻿app.component("viewCategoryModuleComponent", {
    templateUrl: "/DesktopModules/Mineros/ProvidersAdminModule/components/ViewCategoryModule/viewCategoryModule.component.html",
    bindings: {
        goBack: '&?',
        viewCategory: '=',
        categorySettings: '=',
        updateSettings: '&?'
    },
    controllerAs: 'vm',
    controller: function ($scope, $http, api_url, $q, viewCategoryModuleService, editSubCategoryModuleService, portal_id, $window, $timeout, $sce) {
        var vm = this;
        vm.edit = false;
        vm.addSubcategory = false;
        vm.subcategoryItem = {};
        vm.typeActivities = [];
    
        vm.selectActivity = null;
        vm.$onInit = function () {
      
            vm.textAddProvider = "Categoria " + vm.viewCategory.Spanish;
            viewCategoryModuleService.getAllTypeActivity().then((response) => {
                vm.typeActivities = response.data.Data;
            })
            vm.selectActivity = parseInt(vm.viewCategory.TypeActivityId);
            vm.mainGridOptionsSubcategory = {
              
                dataSource: vm.getDatasourceMaster(),
                navigatable: true,
                sortable: true,
                resizable: true,
                reorderable: true,
                columnMenu: true,
                pageable: {
                    refresh: true,
                    alwaysVisible: true,
                    pageSizes: [5, 10, 20, 100]
                },

                filterable: {
                    width: "200px",
                    operators: {
                        //filter menu for "string" type columns
                        string: {
                            eq: "Es igual a...",
                            neq: "No sea igual a...",
                            startswith: "Comience en...",
                            contains: "Contenga...",
                            endswith: "Termine en..."
                        },
                        //filter menu for "number" type columns
                        number: {
                            eq: "Es igual a...",
                            neq: "No sea igual a...",
                            gte: "Es mayor o igual a...",
                            gt: "Es mayor a...",
                            lte: "Es menor o igual a...",
                            lt: "Es menos que..."
                        },
                        //filter menu for "date" type columns
                        date: {
                            eq: "Es igual a...",
                            neq: "No sea igual a...",
                            gte: "Es mayor o igual a...",
                            gt: "Es mayor a...",
                            lte: "Es menor o igual a...",
                            lt: "Es menos que..."
                        },
                        //filter menu for foreign key values
                        enums: {
                            eq: "Es igual a...",
                            neq: "No sea igual a..."
                        }
                    }
                },
                dataBound: function (e) {
                },
                columns: [{
                    field: "SubcategoryId",
                    width: "40px",
                    title: "Id",
                }, {
                    field: "SubcategorySpanish",
                    width: "120px",
                    title: "Valor Español",
                }, {
                    field: "SubcategoryEnglish",
                    width: "120px",
                    title: "Valor Ingles",
                },
                {
                    title: "Acciones",
                    width: "150px",
                    template: function (dataItem) {
                        return "<kendo-button type='button' class='k-primary btn-iconInnovacion' icon=\"'k-icon k-i-edit'\" ng-click='vm.editItemSub(dataItem)' > " +
                            "</kendo-button> " +
                            "<kendo-button type='button' class='btn-iconInnovacion' icon=\"'k-icon k-i-delete'\" ng-click='vm.deleteItemSubCategory(dataItem)' > " +
                            "</kendo-button>";
                    }

                }]
   
            };
        }

        vm.changeServices = function () {
            console.info();
            alert();
            vm.selectActivity = service;
        }
        vm.clickCancelar = function () {
            vm.goBack && vm.goBack();
        }
        vm.deleteItemSubCategory = function (item) {
            $("<div id='confirmPopupWindowSubCategory'></div>")
                .appendTo("body")
                .kendoConfirm({
                    title: "Eliminar",
                    content: "Desea eliminar esta subcategoria?",
                    messages: {
                        okText: "Sí",
                        cancel: "Cancelar"
                    }
                }).data("kendoConfirm").open()
                .result.done(function () {
                    editSubCategoryModuleService.deleteSubCategory(item.SubcategoryId).then((response) => {
                        $(document).find('[data-role="confirm"]').data("kendoConfirm").close()
                    })
                    kendo.ui.progress($("#mainGrid"), true);
                   
                });
        }
        vm.saveCategory = function (e, form_provider, providerForm) {
            var sendCategory = {
                Id: providerForm.Id,
                Spanish: providerForm.Spanish,
                English: providerForm.English,
                TypeActivityId: providerForm.TypeActivityId
            }
            
            viewCategoryModuleService.updateCategory(sendCategory).then((response) => {
                if (response.data.Header.Code == 200) {
                    var typeSelected = vm.typeActivities.find((t) => t.Id == providerForm.TypeActivityId);
                    vm.viewCategory.TypeActivities.Spanish = typeSelected.Spanish;
                    vm.viewCategory.TypeActivities.TypeActivityId = typeSelected.Id;
                    vm.edit = false;
                }
            });
        }

        vm.editCategory = function () {
            vm.edit = true;
        }
        vm.editItemSub = function (item) {
            vm.addSubcategory = true;
            vm.subcategoryItem = item;
        }
        vm.editClose = function () {
            vm.edit = false;
        }
        vm.addSub = function () {
            vm.addSubcategory = true;
        }
        vm.goBackForm = function () {
            vm.addSubcategory = false;
            vm.subcategoryItem = {};
            vm.$onInit();
        }
        vm.getDatasourceMaster = function () {
            return {
                type: 'aspnetmvc-ajax',
                dataType: "json",
                transport: {
                    read: {
                        url: "/DesktopModules/services/API/Provider/getAllSubcategory",
                        contentType: "application/json; charset=utf-8",
                        method: "get",
                        beforeSend: function (e, request) {
                            console.info(request.url);
                            $scope.url_request = request.url;
                        }
                    },


                },
                filter: {
                    field: 'CategoryId', operator: "eq", value: vm.viewCategory.Id,
                },
                pageSize: 5,
                serverPaging: true,
                serverSorting: true,
                serverFiltering: true,
                serverGrouping: true,
                serverAggregates: true,
                schema: {
                    data: "Data.Data",
                    total: "Data.Total",
                    errors: "Data.Errors",
                    model: {
                        fields: {
                            SubcategoryId: { type: "enums" },
                            SubcategorySpanish: { type: "string" },
                            SubcategoryEnglish: { type: "string" },
                            CategoryId: { type: "enums" }
                        }
                    },
                }
            }


        };

    }
});
﻿app.factory('viewCategoryModuleService', function (api_url, $http) {
    var s = {};
    const route_category_store = api_url + "Provider/CreateCategory";
    const route_category = api_url + "provider/getAllCategory";
    const route_update_category = api_url + "provider/updateCategory";
    const route_delete_category = api_url + "provider/DeleteCategory?id=";
    const route_type_activity_get = api_url + "provider/getAllActivity";

    s.updateItem = function (item) {
        
        var promise = $http.post(app_url_interest_put, data, {
            headers: { 'Content-Type': undefined }
        });

        promise.catch(function (response) {
            console.log("hubo un error");
        });

        return promise;
    }
    s.getAllTypeActivity = function () {
        var promise = $http.get(route_type_activity_get);
        promise.catch(function (response) {
            response
        });
        return promise;
    }
    s.getCategoryAll = function () {
        var promise = $http.get(route_category);
        promise.catch(function (data) {

        });

        return promise;
    }
    s.updateCategory = function (category) {
        var promise = $http.post(route_update_category, category);
        promise.catch(function (data) {

        });
        return promise;
    }
    s.storeCategory = function (category) {
        var promise = $http.post(route_category_store, category);
        promise.catch(function (data) {

        });
        return promise;
    }
    s.deleteCategory = function (id) {
        var promise = $http.get(route_delete_category + id);
        promise.catch(function (data) {

        });
        return promise;
    }
    return s;
});
﻿app.component("editSubCategoryModuleComponent", {
    templateUrl: "/DesktopModules/Mineros/ProvidersAdminModule/components/EditSubcategoryModule/editSubCategory.component.html",
    bindings: {
        goBack: '&?',
        editSubcategory: '=',
        categorySettings: '=',
    },
    controllerAs: 'sb',
    controller: function ($scope, $http, api_url, $q, editSubCategoryModuleService, portal_id, $window, $timeout, $sce) {
        var sb = this;
        sb.selectSubcategory = {};
        sb.categories;
        sb.textAddProvider = "Agregar SubCategoria"
        sb.$onInit = function () {
            
            sb.selectSubcategory = sb.editSubcategory;
            console.info(sb.selectSubcategory);
            if (sb.selectSubcategory != null) {
                sb.textAddProvider = "Actualizar Subcategoria";
            }
            editSubCategoryModuleService.getCategoryAll().then(function (response) {
                sb.categories = response.data.Data;
                console.info(sb.categories);
            })
       
        }
        sb.clickCancelar = function () {
            sb.goBack && sb.goBack();

        }

        sb.changeCategory = function (category) {
            sb.selectSubcategory.CategoryId = category.Id;
        }
        sb.updateSubCategory = function (event, form_subcategory, subcategory) {
            event.preventDefault();
            if (form_subcategory.$valid) {
                var sendCategory = {
                    Id: subcategory.SubcategoryId,
                    Spanish: subcategory.SubcategorySpanish,
                    English: subcategory.SubcategoryEnglish,
                    CategoryId: subcategory.CategoryId
                }

                editSubCategoryModuleService.updateSubcategory(sendCategory).then((response) => {
                    if (response.data.Header.Code == 200) {
                        sb.goBack && sb.goBack();
                    }
                })
            }
        }

    }
});
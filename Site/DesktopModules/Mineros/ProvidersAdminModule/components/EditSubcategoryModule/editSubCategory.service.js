﻿app.factory('editSubCategoryModuleService', function (api_url, $http) {
    var su = {};
    const route_create_subcategory = api_url + "Provider/createSubcategory";
    const route_category = api_url + "provider/getAllCategory";
    const route_update_category = api_url + "provider/updateSubcategory";
    const route_delete_subcategory = api_url +"provider/deleteSubCategory?id=";
  
    su.getCategoryAll = function () {
        var promise = $http.get(route_category);
        promise.catch(function (data) {

        });
        return promise;
    }
    su.createSubcategory = function (subcategory) {
        var promise = $http.post(route_create_subcategory, subcategory);

        promise.catch((data) => {
            console.info(data);
        })
        return promise;
    }
    su.updateSubcategory = function (subcategory) {
        var promise = $http.post(route_update_category, subcategory);
        promise.catch((data) => {
            console.info(data);
        })
        return promise;
    }
    su.deleteSubCategory = function (id) {
        var promise = $http.get(route_delete_subcategory+id);

        promise.catch((data) => {
            console.info(data);
        })
        return promise;
    }
    return su;
});
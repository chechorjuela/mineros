﻿app.component("editProviderModuleComponent", {
    templateUrl: "/DesktopModules/Mineros/ProvidersAdminModule/components/EditProviderModule/editProviderModule.component.html",
    bindings: {
        goBack: '&?',
        editProvider: '=',
        categorySettings: '=',
        updateSettings: '&?'
    },
    controllerAs: 'vm',
    controller: function ($scope, $http, api_url, $q, editProviderModuleService, portal_id, $window, $timeout, $sce) {

        $scope.uploadFileBoolean = false;
        $scope.arrayBrand = [];
        $scope.showCountry = false;
        $scope.brandInput = "";
        $scope.categories;
        $scope.listProducts = [];
        $scope.listServices = [];

        $scope.acceptTermsLabel = 'Acepto términos y condiciones <a href="#" class="color-jungle-green accept_terms"> (Ver Condiciones)</a>';
        $scope.message_send = 'Se ha guardado tu información!';
        $scope.acceptterms = "Debes aceptar los terminos";
        $scope.ProviderButtonText = "Sé proveedor";
        $scope.message_error = "Hubo un error";
        $scope.provider_exist = "Proveedor ya se encuentra registrado";
        $scope.fieldRequiredText = "Campos Obligatorios";
        $scope.required_field = "Este campo es requerido";
        $scope.selectRequiredCountry = "Debes seleccionar alguna opción o registrar algun país";
        $scope.canTypeCountryText = "Puedes buscar por ciudad pero se escojera el país. Selecciona el pais con la tecla enter.";
        $scope.brandTypeRequireText = "Ingresa tus marcas escribiendo en el campo de texto y enseguida con la tecla enter.";
        $scope.brandRegister = "Marcas registradas";
        $scope.categoryText = "Categoria";
        $scope.subcategoryText = "Subcategoias";
        $scope.maxSubCategoryText = "Puedes escoger maximo 5 Subcategorias";
        $scope.fileRequiredText = "Archivo es requerido";
        $scope.attachFile = "Adjuntar hoja de vida";
        $scope.sendText = "Actualizar";
        $scope.nameEnterpriseText = "Nombre empresa";
        $scope.hadProductText = "Tienes productos?";
        $scope.hadServiceText = "Tienes servicios?";
        $scope.representationCountryText = "¿Tiene Representación en alguno de los siguientes países?";
        $scope.productsAndServiceText = "Productos & Servicios";
        $scope.textAddProvider = "Actualizacion Proveedor";
        $scope.dataContactText = "Datos de contacto";
        $scope.nitPlaceholder = "Nit";
        $scope.siteUrlPlaceholder = "Sitio Url";
        $scope.nameAdiserText = "Nombre asesor";
        $scope.phoneAdviserText = "Telefono asesor";
        $scope.emailEnterpriseText = "Correo empresarial";
        $scope.emailAdviser = "Correo del asesor";
        $scope.otherText = "Otro";
        $scope.wichText = "¿Cual?";
        $scope.otherCountries = "Representaciones en paises";
        $scope.brandText = "Marcas"

     
        $scope.success_send = false;
        $scope.localate = "es";
        $scope.requiredSomeOption = "Debes seleccionar alguna opción";
        var vm = this;
        vm.selectServiceBoolean = false;
        vm.arrayCountryDefault = ["Colombia", "Honduras", "Argentina"];
        vm.arrayCountry = [];
        vm.send = false;
        vm.file_document=null;
        vm.sendProviderUpdate = {};
        vm.contactEdit = null;
        vm.providerForm = {};
        vm.providerForm.brands = [];
        vm.providerForm.codeCheck = false;
        vm.booleanotherCheck = false;

        vm.otherCountryBoolean = false;
        vm.contentProd = false;
        vm.contentService = false;
        vm.listProducts = null;
        vm.listServices = null;
        vm.listSubcategoryProd = [];
        vm.listSubcategoryService = [];
        vm.optionsListService = [];
        vm.optionsListProduct = [];
        vm.staticSubcatgoryProd = [];
        vm.staticSubcatgoryService = [];
        vm.providerFormMulti = {};
        vm.selectCountryBoolean = false;
        vm.booleanArrayCategory = false;
        $scope.errorRequest = false;

        vm.$onInit = function () {
            vm.providerForm = vm.editProvider;
            var brands = [];
            if (vm.providerForm.Brands.length>0) {
                vm.providerForm.Brands.map((b) => {
                    let brand = {
                        Id: b.Id,
                        Name: b.Name
                    }
                    brands.push(brand);
                });
            }
            vm.providerForm.Brands = brands;
            if (typeof vm.providerForm.Countries != "undefined") {
                vm.otherCountryBoolean = vm.providerForm.Countries.length > 0 ? true : false;

            }
            editProviderModuleService.getCategoryAll().then((response) => {
                $scope.categories = response.data.Data;
                vm.listProducts = $scope.categories.filter((l) => l.TypeActivityId == 1);
                var productItemsCategory = vm.providerForm.ItemCategory.filter((c) => c.TypeActivityId == 1);
                if (productItemsCategory.length > 0) {
                    vm.booleanArrayCategory = true;
                    vm.listSubcategoryProd = productItemsCategory;
                    var selectProd = vm.listProducts.find((p) => p.Id == productItemsCategory[0].CategoryId);
                    vm.optionsListProduct = selectProd.subCategories.filter((s) => {
                        if (!vm.providerForm.ItemCategory.find((p) => p.SubcategoryId == s.Id)) {
                            return s;
                        }
                    });
                    vm.staticSubcatgoryProd = vm.optionsListProduct;
                    vm.productSelect = selectProd;
                }

                vm.listServices = $scope.categories.filter((l) => l.TypeActivityId == 2);
                var serviceItemsCategory = vm.providerForm.ItemCategory.filter((c) => c.TypeActivityId == 2);
                if (serviceItemsCategory.length > 0) {
                    vm.booleanArrayCategory = true;
                    vm.listSubcategoryService = serviceItemsCategory;
                    var selectServ = vm.listServices.find((p) => p.Id == serviceItemsCategory[0].CategoryId);
                    vm.optionsListService = selectServ.subCategories.filter((s) => {
                        if (!vm.providerForm.ItemCategory.find((p) => p.SubcategoryId == s.Id)) {
                            return s;
                        }
                    });
                    vm.staticSubcatgoryProd = vm.optionsListService;
                    vm.serviceSelect = selectServ;
                }
            })
            var itemCountry = JSON.parse(vm.providerForm.Countries);
            if (itemCountry) {
                itemCountry.map((c) => {
                    var find = vm.arrayCountryDefault.find((a) => a.toLowerCase() == c.toLowerCase())
                    let objectCountry = {
                        'show': 0,
                        'name': c
                    };
                    if (typeof find != "undefined") {
                        objectCountry.show = 0;
                        $("#" + c.toLowerCase() + "Label").prop("checked", true);
                    } else {
                        objectCountry.show = 1;
                        vm.otherCountryBoolean = true;
                    }
                    vm.arrayCountry.push(objectCountry);
                })
            }
            var productItemsCategory = vm.providerForm.ItemCategory.filter((c) => c.TypeActivityId == 1);
            if (productItemsCategory.length > 0) {
                vm.selectServiceBoolean = true;
                vm.providerForm.productChck = true;
                vm.contentProd = true;
            }
            if (vm.providerForm.ItemCategory.filter((c) => c.TypeActivityId == 2).length > 0) {
                vm.providerForm.serviceChck = true;
                vm.contentService = true;
                vm.selectServiceBoolean = true;
            }
        }

        vm.changeServices = function (e, service) {
            if (service == 'product') {
                vm.contentProd = !vm.contentProd;
                if (!vm.contentProd) {
                    vm.providerForm.subcategoryProduct = [];
                    vm.optionsListProduct = [];
                }
            } else {
                vm.contentService = !vm.contentService;
                if (!vm.contentService) {
                    vm.providerForm.subcategoryService = [];
                    vm.optionsListService = [];
                }
            }
        }
        vm.clickCancelar = function () {
            vm.goBack && vm.goBack();
            $(".nameField").text("");
        }
        vm.checkCountry = function (e) {
            var country = 'Colombia';
            switch (e.target.id) {
                case 'checkArgentina':
                    country = "Argentina";
                    break;
                case 'checkHonduras':
                    country = "Honduras";
                    break;
                case 'checkOther':
                    vm.booleanotherCheck = !vm.booleanotherCheck;
                    break;
                default:
                    country = "Colombia";
                    break;
            }
            var indexCountry = vm.arrayCountry.findIndex((c) => c.name === country)
            if (indexCountry == -1) {
                let objectCountry = {
                    'show': 0,
                    'name': country
                };
                vm.arrayCountry.push(objectCountry);
            } else {
                vm.arrayCountry.splice(indexCountry, 1);

            }
        }
        vm.saveProvider = function (e, form_provider, providerForm) {
            vm.selectCountryBoolean = vm.arrayCountry.length > 0 ? true : false;
            $scope.errorRequest = false;
            vm.selectServiceBoolean = vm.providerForm.serviceChck || vm.providerForm.productChck ? true : false;
            vm.booleanArrayCategory = vm.listSubcategoryProd.length > 0 || vm.listSubcategoryService.length > 0;
            if (vm.selectCountryBoolean && vm.selectServiceBoolean && vm.booleanArrayCategory) {
                if (form_provider.$valid) {
                    let countries_array = [];
                    vm.arrayCountry.map((c) => {
                        countries_array.push(c.name);
                    })
                    vm.sendProviderUpdate.id = vm.providerForm.Id;
                    vm.sendProviderUpdate.nit = vm.providerForm.Nit;
                    vm.sendProviderUpdate.nameEnterprise = vm.providerForm.NameEnterprise;
                    vm.sendProviderUpdate.siteUrl = vm.providerForm.SiteUrl;
                    vm.sendProviderUpdate.phoneNumber = vm.providerForm.PhoneNumber;
                    vm.sendProviderUpdate.contactName = vm.providerForm.ContactName;
                    vm.sendProviderUpdate.contactEmail = vm.providerForm.ContactEmail;
                    vm.sendProviderUpdate.companyEmail = vm.providerForm.CompanyEmail;
                    var brand = []
                    vm.providerForm.Brands.map((b) => {
                        brand.push(b.Name);
                    })
                    vm.sendProviderUpdate.brands = brand;
                    var serviceprod = [];
                    var serviceServ = [];
                    vm.listSubcategoryProd.map((prod) => {
                        let objProd = {
                            "id": typeof prod.ProviderId != "undefined" ? prod.SubcategoryId:prod.Id,
                            "Spanish": prod.SubcategorySpanish,
                            "English": prod.SubcategoryEnglish
                        }
                        serviceprod.push(objProd);
                    });
                    vm.listSubcategoryService.map((prod) => {
                        let objServ = {
                            "id": typeof prod.ProviderId != "undefined" ? prod.SubcategoryId : prod.Id,
                            "Spanish": prod.SubcategorySpanish,
                            "English": prod.SubcategoryEnglish
                        }
                        serviceServ.push(objServ);
                    });
                    vm.sendProviderUpdate.subcategoryProduct = serviceprod;
                    vm.sendProviderUpdate.subcategoryService = serviceServ;
                    vm.sendProviderUpdate.countries = countries_array;
                    if (vm.file_document != null) {
                        vm.sendProviderUpdate.fileBrochure = {
                            FileName: vm.file_document.name,
                            SizeImage: vm.file_document.size,
                            TypeFileBase64: vm.file_document.type,
                            FileStringBase64: vm.file_document.FileStringBase64,
                        }
                    } else {
                        vm.sendProviderUpdate.fileBrochure = null;
                    }
                   vm.send = true;

                  
                 
                    editProviderModuleService.updateProvider(vm.sendProviderUpdate).then((response) => {
                        vm.send = false;
                        if (response.data.Header.Code == 200) {
                            if (response.data.Data != null) {
                                vm.goBack && vm.goBack();
                //                $scope.providerForm.brands = [];
                //                $("#modal_proveedor").modal("hide")
                //                $scope.success_send = true;
                //                $scope.arrayCountry = [];
                //                vm.listSubcategoryProd = [];
                //                $scope.listSubcategoryService = [];
                //                $scope.serviceSelect = "";
                //                $scope.productSelect = "";
                //                $("#colombiaLabel").prop('checked', false);
                //                $("#argentinaLabel").prop('checked', false);
                //                $("#hondurasLabel").prop('checked', false);

                //                $scope.file_document = null;
                //                setTimeout(() => {
                //                    vm.contentProd = false;
                //                    vm.contentService = false;
                //                    initializeFunctions();
                //                    $scope.success_send = false;
                //                }, 1500)
                            } else {
                                vm.errorRequest = true;
                                $scope.message_error = response.data.Header.Message == "provider_exist" ? $scope.provider_exist : $scope.message_error;
                            }
                        } else {
                            $scope.errorRequest = true;
                        }
                    })
                }
            }
        }
        vm.deleteTag = function (item, service) {
            switch (service) {
                case 'country':
                    let index_country = vm.arrayCountry.findIndex((c) => c.name === item);
                    vm.arrayCountry.splice(index_country, 1);
                    let country_show = vm.arrayCountry.filter((c) => c.show == 1);
                    if (country_show.length == 0) {
                        vm.showCountry = false;
                    }
                    break;
                case 'product':
                    var indexoption = vm.listSubcategoryProd.findIndex((p) => p.Id == item.Id);
                    if (indexoption >= 0) {
                        let findCategory = vm.staticSubcatgoryProd.find((p) => p.Id == vm.listSubcategoryProd[indexoption].Id)
                        let objSelect = {
                            Id: vm.listSubcategoryProd[indexoption].Id,
                            Spanish: vm.listSubcategoryProd[indexoption].SubcategorySpanish,
                            CategoryId: vm.listSubcategoryProd[indexoption].CategoryId,
                            English: vm.listSubcategoryProd[indexoption].SubcategoryEnglish,
                        }
                        vm.optionsListProduct.push(objSelect)
                        vm.listSubcategoryProd.splice(indexoption, 1);
                        vm.optionsListProduct = vm.optionsListProduct.sort(function (a, b) {
                            var keyA = a.Spanish,
                                keyB = b.Spanish;
                            if (keyA != "Seleccione" && keyB != "Seleccione") {
                                // Compare the 2 dates
                                if (keyA < keyB) return -1;
                                if (keyA > keyB) return 1;
                            }

                            return 0;
                        });
                    }
                    break;
                case 'service':
                    var indexoption = vm.listSubcategoryService.findIndex((p) => p.id == item.id);
                    if (indexoption >= 0) {
                        let findCategory = vm.staticSubcatgoryService.find((p) => p.id == vm.listSubcategoryService[indexoption].id)
                        let objSelect = {
                            Id: vm.listSubcategoryService[indexoption].Id,
                            Spanish: vm.listSubcategoryService[indexoption].SubcategorySpanish,
                            CategoryId: vm.listSubcategoryService[indexoption].CategoryId,
                            English: vm.listSubcategoryService[indexoption].SubcategoryEnglish,
                        }
                        vm.optionsListService.push(objSelect)
                        vm.listSubcategoryService.splice(indexoption, 1)
                        vm.optionsListService = vm.optionsListService.sort(function (a, b) {
                            var keyA = a.Spanish,
                                keyB = b.Spanish;
                            if (keyA != "Seleccione" && keyB != "Seleccione") {
                                // Compare the 2 dates
                                if (keyA < keyB) return -1;
                                if (keyA > keyB) return 1;
                            }
                            return 0;
                        });
                    }
                    break;
                default:
                    let indexBrand = vm.providerForm.Brands.findIndex((b) => b.Id == item.Id);
                    if (indexBrand>=0) {
                        vm.providerForm.Brands.splice(indexBrand, 1);
                    }
                    break;
            }
        }
        $('#modal_proveedor').on('hide.bs.modal', function (e) {
            vm.goBack && vm.goBack();
        });
        vm.selectService = function (category, service) {
            switch (service) {
                case 'product':
                    vm.optionsListProduct = [];
                    vm.listProduct = category.subCategories;
                    vm.listProduct.map((p) => {
                        let obj = {
                            id: p.Id,
                            Spanish: p.Spanish,
                            English: p.English
                        }
                        if (typeof vm.listSubcategoryProd.find((l) => l.SubcategoryId == obj.id) == "undefined") {
                            vm.optionsListProduct.push(obj);
                            vm.staticSubcatgoryProd.push(obj);
                        }


                    });
                    break;
                case 'service':
                    vm.optionsListService = [];
                    vm.providerForm.subcategoryService = [];
                    listService = category.subCategories;
                    listService.map((p) => {
                        let obj = {
                            id: p.Id,
                            Spanish: p.Spanish,
                            English: p.English
                        }
                        if (typeof vm.listSubcategoryService.find((l) => l.SubcategoryId == obj.id) == "undefined") {
                            vm.optionsListService.push(obj);
                            vm.staticSubcatgoryService.push(obj);
                        }
                      
                    });
                    break;
            }
        }
        vm.selectItemAfterProd = function (item) {
            vm.providerForm.subcategoryProduct = [];
            vm.providerFormMulti.subcategoryProduct = [];
            vm.multiselectServi = [];
            if (vm.listSubcategoryProd.length < 5) {
                var obj = {
                    Id: typeof item.id == "undefined" ? item.Id :item.id,
                    SubcategorySpanish: item.Spanish,
                    SubcategoryEnglish: item.English,
                }
                vm.listSubcategoryProd.push(obj);
                var indexoption = vm.optionsListProduct.findIndex((p) => p.Id == item.Id);
                if (indexoption >= 0) {
                    vm.optionsListProduct.splice(indexoption, 1)
                }
            }
        }
        vm.selectItemAfterServ = function (item) {
            vm.providerForm.subcategoryService = [];
            vm.providerFormMulti.subcategoryService = [];
            if (vm.listSubcategoryService.length < 5) {
                var obj = {
                    Id: typeof item.id == "undefined" ? item.Id : item.id,
                    SubcategorySpanish: item.Spanish,
                    SubcategoryEnglish: item.English,
                }
                vm.listSubcategoryService.push(obj);
                var indexoption = vm.optionsListService.findIndex((p) => p.Id == item.Id);
                if (indexoption >= 0) {
                    vm.optionsListService.splice(indexoption, 1)
                }
            }
        }
        vm.brandAdd = function (event) {
            console.info(event.keyCode);
            if (event.which == 13 && event.target.type !== 'submit') {
                let indexBrand = vm.providerForm.Brands.find((b) => b.Name == $scope.brandInput);
                if (typeof indexBrand == "undefined") {
                    vm.providerForm.Brands.push({ Name: $scope.brandInput });
                }
                $scope.brandInput = "";
                event.preventDefault();
                event.stopPropagation();

            }
        }
    }
});
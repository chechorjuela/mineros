﻿app.controller("providersAdminController", function ($scope, module_resources, $q, common_resources, providersAdminControllerService, viewCategoryModuleService, editSubCategoryModuleService, portal_id, $window, $sce, sf, $timeout,) {
    $scope.roles_user = roles.split(",");
    $scope.roles_user_buttons = typeof $scope.roles_user.find((r) => r == "Admin Proveedores" || r == "Administrators") != "undefined" ? true : false;
    $scope.selectedItem = false;
    $scope.showMaster = true;
    $scope.view_category = false;
    $scope.subcategory_form = false;
    $scope.subcategoryItem = null;
    $scope.showCategory = false;
    $scope.showSubCategory = false;
    $scope.categorySelected = {};
    $scope.subcategorySelected = {};
    $scope.subcategorySelected.SubcategorySpanish = "";
    $scope.subcategorySelected.SubcategoryEnglish = "";
    $scope.typeActivities = [];
    $scope.categories = [];
    $scope.form_category;
    $scope.form_subcategory;
    $scope.url_request = "";
    $scope.notf1Options = {
        templates: [{
            type: "ngTemplate",
            template: $("#notificationTemplate").html()
        }]
    };

    $scope.opt = {
        position: {
            top: 30,
            right: 30
        },
        templates: [{
            type: "growl",
            template: "<div class='growl'><h3>#= title #</h3><p>#= message #</p></div>"
        }]
    };

    $scope.getDatasource = function () {
        var model = {};
        return {
            type: 'aspnetmvc-ajax',
            dataType: "json",
            transport: {
                read: {
                    url: "/DesktopModules/services/API/provider/getAllProviders",
                    contentType: "application/json; charset=utf-8",
                    method: "get",
                    beforeSend: function (e, request) {
                        $scope.url_request = request.url;
                    }
                },
                //parameterMap: function (options, type) {
                //    //options.filters = typeof options.filter != "undefined" ? options.filter.filters:null;
                //    //options.sorts = [
                //    //    { "member": "number_phone","order":0}
                //    //]
                //    return JSON.stringify(options);
                //}
            },
            pageSize: 5,
            serverPaging: true,
            serverSorting: true,
            serverFiltering: true,
            serverGrouping: true,
            serverAggregates: true,
            schema: {
                data: "Data.Data",
                total: "Data.Total",
                errors: "Data.Errors",
                model: {
                    fields: {
                        Id: { type: "string" },
                        NameEnterprise: { type: "string" },
                        Nit: { type: "string" },
                        SiteUrl: { type: "string" },
                        Categories: { type: "string" },
                        SubCategories: { type: "string" },
                        ContactName: { type: "string" },
                        ContactEmail: { type: "string" },
                        CompanyEmail: { type: "string" },
                        Countries: { type: "string" },
                        status: { type: "enums" },
                        Brands: { type: "enums" },
                        PhoneNumber: { type: "string" },
                        FilePath: { type: "string" },
                        CreateAt: { type: "string" },
                        UpdateAt: { type: "string" },
                        ItemCategory:{ type: "enums"}
                    }
                },
            }
        }
    };
    $scope.getDatasourceMaster = function () {
        var model = {};
        return {
            type: 'aspnetmvc-ajax',
            dataType: "json",
            transport: {
                read: {
                    url: "/DesktopModules/services/API/Provider/getAllCategoryFilter",
                    contentType: "application/json; charset=utf-8",
                    method: "get",
                    beforeSend: function (e, request) {
                        //    $scope.url_request = request.url;
                    }
                },
            },
            pageSize: 5,
            serverPaging: true,
            serverSorting: true,
            serverFiltering: true,
            serverGrouping: true,
            serverAggregates: true,
            schema: {
                data: "Data.Data",
                total: "Data.Total",
                errors: "Data.Errors",
                model: {
                    fields: {
                        Id: { type: "enums" },
                        Spanish: { type: "string" },
                        English: { type: "string" },
                        TypeActivities: { type: "object" }
                    }
                },
            }
        }
    };
    $scope.mainGridOptionsMaster = {
        toolbar: [{
            name: "",
            text: "",
            template: function (grid) {
                return '<button class=\'k-button k-button-icontext\' ng-click=\'showMasterTable()\'><span class=\'k-icon\'></span>Proveedores</button>'
            }
        }, {
            name: "",
            text: "",
            template: function (grid) {
                return '<button class=\'k-button k-button-icontext\' ng-click=\'showSubCategoryTable()\'><span class=\'k-icon\'></span>Subcategorías</button>'
            }
        }],
        dataSource: $scope.getDatasourceMaster(),
        navigatable: true,
        sortable: true,
        resizable: true,
        reorderable: true,
        columnMenu: true,
        pageable: {
            refresh: true,
            alwaysVisible: true,
            pageSizes: [5, 10, 20, 100]
        },
        filter: function (e) {
            $scope.filter = e.filter;
        },
        filterable: {
            width: "200px",
            operators: {
                //filter menu for "string" type columns
                string: {
                    eq: "Es igual a...",
                    neq: "No sea igual a...",
                    startswith: "Comience en...",
                    contains: "Contenga...",
                    endswith: "Termine en..."
                },
                //filter menu for "number" type columns
                number: {
                    eq: "Es igual a...",
                    neq: "No sea igual a...",
                    gte: "Es mayor o igual a...",
                    gt: "Es mayor a...",
                    lte: "Es menor o igual a...",
                    lt: "Es menos que..."
                },
                //filter menu for "date" type columns
                date: {
                    eq: "Es igual a...",
                    neq: "No sea igual a...",
                    gte: "Es mayor o igual a...",
                    gt: "Es mayor a...",
                    lte: "Es menor o igual a...",
                    lt: "Es menos que..."
                },
                //filter menu for foreign key values
                enums: {
                    eq: "Es igual a...",
                    neq: "No sea igual a..."
                }
            }
        },
        dataBound: function (e) {
        },
        columns: [{
            field: "Id",
            width: "40px",
            title: "Id",
        }, {
            field: "Spanish",
            width: "120px",
            title: "Valor Español",
        }, {
            field: "English",
            width: "120px",
            title: "Valor Inglés",
        }, {
            field: "TypeActivities.Spanish",
            width: "120px",
            title: "Tipo Actividad Valor Español",
        },
        {
            field: "TypeActivities.English",
            width: "120px",
            title: "Tipo Actividad Valor Inglés",
        },
        {
            title: "Acciones",
            width: "150px",
            template: function (dataItem) {
                return "<kendo-button type='button' class='k-primary btn-iconInnovacion' icon=\"'k-icon k-i-edit'\" ng-click='editItemCategory(dataItem)' > " +
                    "</kendo-button> " +
                    "<kendo-button type='button' class='btn-iconInnovacion' icon=\"'k-icon k-i-delete'\" ng-click='deleteItemCategory(dataItem)' > " +
                    "</kendo-button>";
            }

        }]
        //selectable: "row",
        //change: function (e) {
        //    var gview = $("#mainGrid").data("kendoGrid");
        //    var selectedItem = gview.dataItem(gview.select());
        //    //SEND PREVIEW
        //    //alert("Selected value " + selectedItem.Id);
        //}
    };
    $scope.editItemCategory = function (dataItem) {
        $scope.categorySelected = dataItem;
        $("form[name='form_category'] input[name='id']").val(dataItem.Id)
        $("form[name='form_category'] input[name='Spanish']").val(dataItem.Spanish)
        $("form[name='form_category'] input[name='English']").val(dataItem.English)
        $("form[name='form_category'] select[name='typeActivity']").val("number:" + dataItem.TypeActivityId)
        $("#modalCategory").modal('show');
    }

    $scope.editItemSub = function (dataItem) {
        const itemSelected = dataItem;
        $scope.subcategorySelected = dataItem;
        $("#modalSubCategory").modal("show");
    }
    $scope.loadingMaster = function () {
        $scope.mainGridOptions = {
            toolbar: [{
                name: "",
                text: "",
                template: function (grid) {

                    return '<button class=\'k-button k-button-icontext\' ng-click=\'exportToExcel($event)\'><span class=\'k-icon k-i-file-excel\'></span>Exportar a excel</button>'

                }
            }, {
                name: "",
                text: "",
                template: function (grid) {
                    var boolexport = typeof $scope.roles_user.find((r) => r == "Admin Proveedores" || r == "Administrators") != "undefined" ? true : false;
                    if (boolexport) {
                        return '<button class=\'k-button k-button-icontext\' ng-click=\'showCategoryTable()\'><span class=\'k-icon\'></span>Categoría</button>'
                    }
                    return "";
                }
            }, {
                name: "",
                text: "",
                template: function (grid) {
                    var boolexport = typeof $scope.roles_user.find((r) => r == "Admin Proveedores" || r == "Administrators") != "undefined" ? true : false;
                    if (boolexport) {
                        return '<button class=\'k-button k-button-icontext\' ng-click=\'showSubCategoryTable()\'><span class=\'k-icon\'></span>Subcategoría</button>'
                    }
                    return "";
                }
            }],
            dataSource: $scope.getDatasource(),
            navigatable: true,
            sortable: true,
            resizable: true,
            reorderable: true,
            columnMenu: true,
            pageable: {
                refresh: true,
                alwaysVisible: true,
                pageSizes: [5, 10, 20, 100]
            },
            filter: function (e) {
                $scope.filter = e.filter;
            },
            filterable: {
                width: "200px",
                operators: {
                    //filter menu for "string" type columns
                    string: {
                        contains: "Contenga...",
                    },
                    //filter menu for "number" type columns
                    number: {
                        eq: "Es igual a...",
                        neq: "No sea igual a...",
                        gte: "Es mayor o igual a...",
                        gt: "Es mayor a...",
                        lte: "Es menor o igual a...",
                        lt: "Es menos que..."
                    },
                    //filter menu for "date" type columns
                    date: {
                        eq: "Es igual a...",
                        neq: "No sea igual a...",
                        gte: "Es mayor o igual a...",
                        gt: "Es mayor a...",
                        lte: "Es menor o igual a...",
                        lt: "Es menos que..."
                    },
                    //filter menu for foreign key values
                    enums: {
                        eq: "Es igual a...",
                        neq: "No sea igual a..."
                    }
                }
            },
            dataBound: function (e) {
            },
            columns: [
                {
                    field: "Nit",
                    width: "80px",
                    title: "Nit",
                    locked: true,
                    lockable: false,
                },
                {
                    field: "NameEnterprise",
                    width: "120px",
                    title: "Nombre proveedor",
                    locked: true,
                    lockable: false,
                },
                {
                    title: "Inf. Provedor",
                    locked: true,
                    lockable: false,
                    columns: [{
                        field: "ContactName",
                        title: "Nombre Asesor",
                        width: "150px",

                    }, {
                        field: "ContactEmail",
                        title: "Email Asesor",
                        width: "150px",

                    }, {
                        field: "CompanyEmail",
                        title: "Correo Empresarial",
                        width: "150px",
                    }
                    ],

                }, {
                    title: "Inf. Contacto",
                    locked: false,
                    columns: [{
                        field: "PhoneNumber",
                        title: "Telefono",
                        width: "100px",

                    }, {
                        field: "SiteUrl",
                        title: "Sitio Web",
                        width: "150px",
                    }]

                }, {
                    title: "Otra Inf",
                    columns: [{
                        field: "Countries",
                        title: "Ciudades",
                        width: "150px",
                        //filterable: {
                        //    ui: cityFilter
                        //},
                        template: function (dataItem) {
                            var htmlCountries = "";
                            var itemBrand = JSON.parse(dataItem.Countries);
                            if (itemBrand != null) {
                                itemBrand.map((b) => {
                                    htmlCountries = htmlCountries + "<div>" + b + "</div>";
                                })
                            }
                            return htmlCountries;
                        }
                    },
                    {
                        field: "BrandsString",
                        title: "Marcas",
                        width: "150px",
                        filterable:
                        {
                            cell:
                            {
                                operator: "contains"
                            }
                        }, 
                        template: function (dataItem) {
                            var itemBrand = "";
                            if (dataItem.Brands != null && dataItem.Brands != "") {
                                dataItem.Brands.map((b) => {
                                    itemBrand = itemBrand + "<div>" + b.Name + "</div>";
                                })
                            }
                            return itemBrand;
                        }
                    }],
                }, {
                    title: "Productos & Servicioa",
                    columns: [{
                        field: "Categories",
                        title: "Categoría",
                        width: "170px",
                    
                    }, {
                        field: "SubCategories",
                        title: "Subcategorías",
                        width: "170px",
                       
                    }],
                },{
                    title: "Fechas",
                    columns: [{
                        field: "CreateAt",  
                        title: "Creado",
                        width: "170px",
                        filterable: false,
                        template: function (dataItem) {
                            let indexT = dataItem.CreateAt.indexOf("T");
                            return dataItem.CreateAt.substring(0, indexT);

                        }

                    }, {
                        field: "UpdateAt",
                        title: "Actualizado",
                        width: "170px",
                        filterable: false,
                        template: function (dataItem) {
                            let indexT = dataItem.UpdateAt.indexOf("T");
                            return dataItem.UpdateAt.substring(0, indexT);
                        }
                    }],
                }, {
                    title: "Acciones",
                    width: "150px",
                    locked: true,
                    lockable: false,
                    template: function (dataItem) {
                        var dir_exist = "";
                        if (dataItem.FilePath != null && dataItem.FilePath != "") {
                            dir_exist = "<kendo-button type='button' class='k-primary btn-iconInnovacion' icon=\"'k-icon k-i-pdf'\" ng-click='downloadPDF(dataItem)' ></kendo-button> ";
                        }
                        var boolexport = typeof $scope.roles_user.find((r) => r == "Admin Proveedores" || r == "Administrators") != "undefined" ? true : false;
                        if (boolexport) {
                            dir_exist = dir_exist + "<kendo-button type='button' class='k-primary btn-iconInnovacion' icon=\"'k-icon k-i-edit'\" ng-click='editItem(dataItem)' > " +
                                "</kendo-button> " +
                                "<kendo-button type='button' class='btn-iconInnovacion' icon=\"'k-icon k-i-delete'\" ng-click='deleteItem(dataItem)' > " +
                                "</kendo-button>";
                        }
                        return dir_exist;
                    }

                }]
            //selectable: "row",
            //change: function (e) {
            //    var gview = $("#mainGrid").data("kendoGrid");
            //    var selectedItem = gview.dataItem(gview.select());
            //    //SEND PREVIEW
            //    //alert("Selected value " + selectedItem.Id);
            //}
        };
    }
    $scope.showMasterTable = function () {
        $scope.showCategory = false;
        $scope.showSubCategory = false;
        $scope.showMaster = true;

    }
    $scope.showSubCategoryTable = function () {
        $scope.showCategory = false;
        $scope.showSubCategory = true;
        $scope.showMaster = false;

    }
    $scope.showCategoryTable = function () {
        $scope.showCategory = true;
        $scope.showSubCategory = false;
        $scope.showMaster = false;
    }
    $scope.getDatasourceMasterSubCategory = function () {
        return {
            type: 'aspnetmvc-ajax',
            dataType: "json",
            transport: {
                read: {
                    url: "/DesktopModules/services/API/Provider/getAllSubcategory",
                    contentType: "application/json; charset=utf-8",
                    method: "get",
                    beforeSend: function (e, request) {
                        // $scope.url_request = request.url;
                    }
                },
            },
            pageSize: 5,
            serverPaging: true,
            serverSorting: true,
            serverFiltering: true,
            serverGrouping: true,
            serverAggregates: true,
            schema: {
                data: "Data.Data",
                total: "Data.Total",
                errors: "Data.Errors",
                model: {
                    fields: {
                        SubcategoryId: { type: "enums" },
                        SubcategorySpanish: { type: "string" },
                        SubcategoryEnglish: { type: "string" },
                        CategoryId: { type: "enums" }
                    }
                },
            }
        }
    };

    $scope.mainGridOptionsSubcategory = {
        dataSource: $scope.getDatasourceMasterSubCategory(),
        toolbar: [{
            name: "",
            text: "",
            template: function (grid) {
                return '<button class=\'k-button k-button-icontext\' ng-click=\'showMasterTable()\'><span class=\'k-icon\'></span>Proveedores</button>'
            }
        }, {
            name: "",
            text: "",
            template: function (grid) {
                return '<button class=\'k-button k-button-icontext\'  ng-click=\'showCategoryTable()\'><span class=\'k-icon\'></span>Categorías</button>'
            }
        }],
        navigatable: true,
        sortable: true,
        resizable: true,
        reorderable: true,
        columnMenu: true,
        pageable: {
            refresh: true,
            alwaysVisible: true,
            pageSizes: [5, 10, 20, 100]
        },
        filterable: {
            width: "200px",
            operators: {
                //filter menu for "string" type columns
                string: {
                    eq: "Es igual a...",
                    neq: "No sea igual a...",
                    startswith: "Comience en...",
                    contains: "Contenga...",
                    endswith: "Termine en..."
                },
                //filter menu for "number" type columns
                number: {
                    eq: "Es igual a...",
                    neq: "No sea igual a...",
                    gte: "Es mayor o igual a...",
                    gt: "Es mayor a...",
                    lte: "Es menor o igual a...",
                    lt: "Es menos que..."
                },
                //filter menu for "date" type columns
                date: {
                    eq: "Es igual a...",
                    neq: "No sea igual a...",
                    gte: "Es mayor o igual a...",
                    gt: "Es mayor a...",
                    lte: "Es menor o igual a...",
                    lt: "Es menos que..."
                },
                //filter menu for foreign key values
                enums: {
                    eq: "Es igual a...",
                    neq: "No sea igual a..."
                }
            }
        },
        dataBound: function (e) {
        },
        columns: [{
            field: "SubcategoryId",
            width: "40px",
            title: "Id",
        }, {
            field: "SubcategorySpanish",
            width: "120px",
            title: "Valor Español",
        }, {
            field: "SubcategoryEnglish",
            width: "120px",
            title: "Valor Inglés",
        }, {
            field: "CategorySpanish",
            width: "120px",
            title: "Categoría",
        }, {
            title: "Acciones",
            width: "150px",
            template: function (dataItem) {
                return "<kendo-button type='button' class='k-primary btn-iconInnovacion' icon=\"'k-icon k-i-edit'\" ng-click='editItemSub(dataItem)' > " +
                    "</kendo-button> " +
                    "<kendo-button type='button' class='btn-iconInnovacion' icon=\"'k-icon k-i-delete'\" ng-click='deleteItemSubCategory(dataItem)' > " +
                    "</kendo-button>";
            }

        }]

    };

    this.$onInit = function () {
        initializeFunctions();
    };
    function initializeFunctions() {
        $scope.getDatasourceMaster();
        $scope.loadingMaster();
        editSubCategoryModuleService.getCategoryAll().then(function (response) {
            $scope.categories = response.data.Data;
        })
        viewCategoryModuleService.getAllTypeActivity().then((response) => {
            $scope.typeActivities = response.data.Data;
        })

    }
    $scope.downloadPDF = function (dataItem) {
        var a = document.createElement("a");
        a.target = "_blank";
        if (dataItem.FilePath != null) {
            a.href = window.location.protocol + "//" + window.location.host + "/portals/" + data.portalId + "/" + dataItem.FilePath;
            var ext = dataItem.FilePath.substring(dataItem.FilePath.lastIndexOf(".") + 1, dataItem.FilePath.length);
            a.download = dataItem.Nit + "_" + dataItem.NameEnterprise + "." + ext;
            document.body.appendChild(a);
            a.click();
        }

    }
    $('#modalSubCategory').on('hidden.bs.modal', function () {
        // do something ...
        $("form[name='form_subcategory'] input[name='id']").val("")
        $("form[name='form_subcategory'] input[name='spanish']").val("")
        $("form[name='form_subcategory'] input[name='english']").val("")
        $("form[name='form_subcategory'] select[name='CategoryId']").val("")
        $scope.subcategorySelected = {};
        $("a[title='Refresh']").trigger("click");
    });
    $('#modalCategory').on('hidden.bs.modal', function () {
        // do something ...
        $("a[title='Refresh']").trigger("click");
        $("form[name='form_category'] input[name='id']").val("")
        $("form[name='form_category'] input[name='Spanish']").val("")
        $("form[name='form_category'] input[name='English']").val("")
        $("form[name='form_category'] select[name='typeActivity']").val("")
        $scope.categorySelected = {};
    });
    $scope.modalSubcategory = function () {
        $scope.subcategory_form = true;
        $scope.subcategoryItem = null;
        $('#modal_proveedor').on('hidden.bs.modal', function () {
            // do something ...
            $scope.subcategoryItem = null;
            $scope.subcategory_form = false;
        });
    }
    $scope.editItem = function (item) {
        $scope.providerSelected = item;
        $scope.subcategory_form = true;
        $scope.selectedItem = !$scope.selectedItem;
        setTimeout(function () {
            $("#modal_proveedor").modal("show");

        }, 100)
        $('#modal_proveedor').on('hidden.bs.modal', function () {
            // do something ...
            $scope.subcategoryItem = null;
            $scope.subcategory_form = false;
        });
    }
    $scope.goBackFromForm = function () {
        $scope.selectedItem = !$scope.selectedItem;
        $scope.providerSelected = null;
    }
    $scope.goBackFromFormSubcategory = function () {
        $scope.subcategoryItem = null;
        $scope.subcategory_form = false;
    }
    $scope.viewCategory = function (dataItem) {
        $scope.view_category = true;
        $scope.categoryItem = dataItem;
    }
    $scope.deleteItemCategory = function (item) {
        $scope.getDatasource();
        $("<div id='confirmPopupWindow'></div>")
            .appendTo("body")
            .kendoConfirm({
                title: "Eliminar",
                content: "Desea eliminar el recurso?",
                messages: {
                    okText: "Sí",
                    cancel: "Cancelar"
                }
            }).data("kendoConfirm").open()
            .result.done(function () {
                $scope.asyncLoader(true);
                kendo.ui.progress($("#mainGrid"), true);
                viewCategoryModuleService.deleteCategory(item.Id).then((response) => {
                    if (response.data.Data) {
                        $scope.notif.show({
                            title: "Satisfactorio",
                            message: "Recurso Eliminado correctamente."
                        }, "growl");
                        $("a[title='Refresh']").trigger("click");
                        $("a[title='Actualizar']").trigger("click");
                        $scope.asyncLoader(false);
                        $scope.getDatasourceMaster();
                    } else {
                        $scope.notif.show({
                            title: "Error!",
                            message: "No se pudo eliminar este recurso."
                        }, "growl");
                    }
                })
            });
    }
    $scope.saveCategory = function (e, form_provider, category) {
        if (form_provider.$valid) {
            var sendCategory = {
                Id: category.Id,
                Spanish: category.Spanish,
                English: category.English,
                TypeActivityId: category.TypeActivityId
            }
            if (typeof sendCategory.Id != "undefined") {
                viewCategoryModuleService.updateCategory(sendCategory).then((response) => {
                    if (response.data.Header.Code == 200) {
                        $('#modalCategory').modal("hide")
                        $scope.notif.show({
                            title: "Realizado",
                            message: "Recurso Actualzizado."
                        }, "growl");
                        initializeFunctions();
                    }
                });
            } else {
                viewCategoryModuleService.storeCategory(sendCategory).then((response) => {
                    if (response.data.Header.Code == 200) {
                        $('#modalCategory').modal("hide")
                        $scope.notif.show({
                            title: "Realizado",
                            message: "Recurso Guardado."
                        }, "growl");
                        initializeFunctions();
                    }
                });
            }
        }
    }
    $scope.deleteItemSubCategory = function (item) {
        $("<div id='confirmPopupWindowSubCategory'></div>").appendTo("body")
            .kendoConfirm({
                title: "Eliminar",
                content: "Desea eliminar esta subcategoria?",
                messages: {
                    okText: "Sí",
                    cancel: "Cancelar"
                }
            }).data("kendoConfirm").open()
            .result.done(function () {
                editSubCategoryModuleService.deleteSubCategory(item.SubcategoryId).then((response) => {
                    $("a[title='Refresh']").trigger("click");
                })
                kendo.ui.progress($("#mainGrid"), true);

            });
    }
    $scope.saveSubCategory = function (event, form_subcategory, subcategorySelected) {
        if (form_subcategory.$valid) {
            if (typeof subcategorySelected.SubcategoryId != "undefined") {

                var SendSubcategory = {
                    Id: subcategorySelected.SubcategoryId,
                    Spanish: subcategorySelected.SubcategorySpanish,
                    English: subcategorySelected.SubcategoryEnglish,
                    CategoryId: subcategorySelected.CategoryId,
                }
                editSubCategoryModuleService.updateSubcategory(SendSubcategory).then((response) => {
                    if (response.data.Header.Code == 200) {
                        $scope.notif.show({
                            title: "Realizado",
                            message: "Recurso Actualizado."
                        }, "growl");
                        $("#modalSubCategory").modal("hide");
                    }
                })
            } else {
                var SendSubcategory = {
                    Spanish: subcategorySelected.SubcategorySpanish,
                    English: subcategorySelected.SubcategoryEnglish,
                    CategoryId: subcategorySelected.CategoryId,
                }
                editSubCategoryModuleService.createSubcategory(SendSubcategory).then((response) => {
                    if (response.data.Header.Code == 200) {
                        $scope.notif.show({
                            title: "Realizado",
                            message: "Recurso Guardado."
                        }, "growl");
                        initializeFunctions();
                        $("#modalSubCategory").modal("hide");

                    }
                })
            }
        }
    }
    $scope.deleteItem = function (item) {
        $scope.getDatasource();
        $("<div id='confirmPopupWindow'></div>")
            .appendTo("body")
            .kendoConfirm({
                title: "Eliminar",
                content: "Desea eliminar el recurso?",
                messages: {
                    okText: "Sí",
                    cancel: "Cancelar"
                }
            }).data("kendoConfirm").open()
            .result.done(function () {
                $scope.asyncLoader(true);
                kendo.ui.progress($("#mainGrid"), true);
                if (typeof item.SubcategoryId != "undefined") {
                    providersAdminControllerService.deleteSubcategory(item.SubcategoryId).then(function (response) {
                        if (response.data.Header.Code == 200) {
                            $scope.getDatasourceMaster();
                            $scope.notif.show({
                                title: "Realizado",
                                message: "Operación realizada."
                            }, "growl");
                            initializeFunctions();
                            $("a[title='Refresh']").trigger("click");
                            $scope.asyncLoader(false);
                        } else {
                            switch (response.data.Message) {
                                case "EC999":
                                    $scope.not_service.warning($scope.common_resources.ConstrainGeneralError_Text);
                                    console.log("Error");
                                    kendo.ui.progress($("#mainGrid"), false);
                                    break;
                                default:
                                    kendo.ui.progress($("#mainGrid"), false);
                                    $scope.not_service.error(response.data.Message);
                            }
                        }
                    })
                } else {
                    providersAdminControllerService.deleteProvider(item.Id).then(function (response) {
                        if (response.data.Header.Code == 200) {
                            initializeFunctions();
                            $scope.getDatasource();
                            $scope.notif.show({
                                title: "Realizado",
                                message: "Operación realizada."
                            }, "growl");
                            initializeFunctions();
                            $("a[title='Refresh']").trigger("click");
                            $scope.asyncLoader(false);
                        } else {
                            switch (response.data.Message) {
                                case "EC999":
                                    $scope.not_service.warning($scope.common_resources.ConstrainGeneralError_Text);
                                    console.log("Error");
                                    kendo.ui.progress($("#mainGrid"), false);
                                    break;
                                default:
                                    kendo.ui.progress($("#mainGrid"), false);
                                    $scope.not_service.error(response.data.Message);
                            }
                        }
                    }, function (response) {
                        //$scope.not_service.error("Error: " + response.status + " " + response.statusText);
                        console.log("Error");
                        kendo.ui.progress($("#mainGrid"), false);
                        $scope.asyncLoader(false);
                    });
                }
            });



    }
    $scope.getCountriesToString = function (countries) {
        var htmlCountries = "";
        if (countries != "null") {
            var itemBrand = JSON.parse(countries);
            if (itemBrand != null) {
                var i = 0;
                itemBrand.map((b) => {
                    var separe = "";

                    if (i < (itemBrand.length-1)) {
                        separe = ",";
                    }
                    i++;
                    htmlCountries = htmlCountries + "" + b + separe;
                })
            }
        }
        return htmlCountries;
    }
    $scope.getBrandsToString = function (brands) {
        itemBrand = "";
        if (brands != null && brands != "") {
            var i = 0;
            brands.map((b) => {
                var separe = "";
                if (i < (brands.length - 1)) {
                    separe = ",";
                }
                i++;
                itemBrand = itemBrand + b.Name + separe;
            })
        }
        return itemBrand;
    }
    $scope.getCategory = function (category, type) {
        var category_return = "";
        var itemBrand = "";
        var category_filter = category.filter((c) => c.TypeActivityId == type);
        if (category_filter.length > 0) {
            var e = 0;
            category_filter.map((c) => {
                var separe = "";
                if (e < (category_filter.length - 1)) {
                    separe = ",";
                }
                e++;
                category_return = category_return + c.CategorySpanish + separe;
            })
        }
        return category_return;
    }
    $scope.getSubCategory = function (subcagories, type) {
        var category_return = "";
        var itemBrand = "";
        var category_filter = subcagories.filter((c) => c.TypeActivityId == type);
        if (category_filter.length > 0) {
            var a = 0;
            category_filter.map((c) => {
                var separe = "";
                if (a < (category_filter.length - 1)) {
                    separe = ",";
                }
                a++;
                category_return = category_return + c.SubcategorySpanish + separe;
            })
        }
        return category_return;
    }
    $scope.exportToExcel = function (e) {
        
        event.preventDefault();
        var params_index = $scope.url_request.indexOf("?");
        var params_send = $scope.url_request.substring(params_index, $scope.url_request.length);
        let index_chart = [];
        for (i = 0; i <= params_send.length; i++) {
            if (params_send[i] == "&") {
                index_chart.push(i)
            }
        }
        var params_page = params_send.substring(index_chart[0] + 1, index_chart[1]);
        var params_pageSize = params_send.substring(index_chart[1] + 1, index_chart[2]);

        params_send = params_send.replace(params_pageSize, "pageSize=1000000000");
        params_send = params_send.replace(params_page, "page=1");
        providersAdminControllerService.getListInterestFilter(params_send).then(response => {
            $scope.excelList = response.data.Data.Data;
            if ($scope.excelList.length === 0) {
                $scope.excelList = $scope.listData.slice();
            }
            var rows_Transactions = [{
                cells: [
                    { value: "Nit", background: "#426d61", bold: true, color: "#FFF" },
                    { value: "Nombre Empresa", background: "#426d61", bold: true, color: "#FFF" },
                    { value: "Nombre Asesor", background: "#426d61", bold: true, color: "#FFF" },
                    { value: "Correo Asesor", background: "#426d61", bold: true, color: "#FFF" },
                    { value: "Correo Empresa", background: "#426d61", bold: true, color: "#FFF" },
                    { value: "Teléfono", background: "#426d61", bold: true, color: "#FFF" },
                    { value: "Sitio Web", background: "#426d61", bold: true, color: "#FFF" },
                    { value: "Ciudades", background: "#426d61", bold: true, color: "#FFF" },
                    { value: "Marcas", background: "#426d61", bold: true, color: "#FFF" },
                    { value: "Producto Categorías", background: "#426d61", bold: true, color: "#FFF" },
                    { value: "Producto Subcategorías", background: "#426d61", bold: true, color: "#FFF" },
                    { value: "Servicio Categorías", background: "#426d61", bold: true, color: "#FFF" },
                    { value: "Servicio Subcategorías", background: "#426d61", bold: true, color: "#FFF" },
                ]
            }];
            if ($scope.excelList) {
                for (var i = 0; i < $scope.excelList.length; i++) {
                    rows_Transactions.push({
                        cells: [
                            { value: $scope.excelList[i].Nit },
                            { value: $scope.excelList[i].NameEnterprise },
                            { value: $scope.excelList[i].ContactName },
                            { value: $scope.excelList[i].ContactEmail },
                            { value: $scope.excelList[i].CompanyEmail },
                            { value: $scope.excelList[i].PhoneNumber },
                            { value: $scope.excelList[i].SiteUrl },
                            { value: $scope.getCountriesToString($scope.excelList[i].Countries) },
                            { value: $scope.getBrandsToString($scope.excelList[i].Brands) },
                            { value: $scope.getCategory($scope.excelList[i].ItemCategory, 1) },
                            { value: $scope.getSubCategory($scope.excelList[i].ItemCategory, 1) },
                            { value: $scope.getCategory($scope.excelList[i].ItemCategory, 2) },
                            { value: $scope.getSubCategory($scope.excelList[i].ItemCategory, 2) },
                        ]
                    });
                }

                // Create workbook
                var workbook = new kendo.ooxml.Workbook({
                    sheets: [
                        {
                            columns: [
                                { autoWidth: true },
                                { autoWidth: true },
                                { autoWidth: true },
                                { autoWidth: true },
                                { autoWidth: true },
                                { autoWidth: true },
                                { autoWidth: true },
                                { autoWidth: true },
                                { autoWidth: true },
                                { autoWidth: true },
                                { autoWidth: true },
                                { autoWidth: true },
                                { autoWidth: true },
                            ],
                            title: "Proveedores",
                            rows: rows_Transactions
                        }
                    ]
                });
                var date = new Date();
                kendo.saveAs({ dataURI: workbook.toDataURL(), fileName: "Proveedores_" + date.format('dd/MM/yyyy') + ".xlsx" });

            }
        }, function (response) {
            //$scope.not_service.error("Error: " + response.status + " " + response.statusText);

        });
        
    }
    function convertHtmlCategory(dataItem, typeActivity) {
        var htmlCountries = "";
        var listCategory = [];
        if (dataItem.ItemCategory != null) {
            var products = dataItem.ItemCategory.filter((c) => c.TypeActivityId == typeActivity);
            var products_category = products.sort(function (a, b) {

                var keyA = a.SubcategorySpanish,
                    keyB = b.SubcategorySpanish;
                // Compare the 2 dates
                if (keyA < keyB) return -1;
                if (keyA > keyB) return 1;

                return 0;
            })
            if (products_category.length > 0) {
                products_category.map((p) => {
                    var objCategory = {
                        id: p.CategoryId,
                        name: p.CategorySpanish
                    }
                    if (listCategory.length > 0) {
                        var list = listCategory.find((l) => l.id == p.CategoryId);
                        if (typeof list == "undefined") {
                            listCategory.push(objCategory);
                        }
                    } else {
                        listCategory.push(objCategory);
                    }
                })
            }
            listCategory.map((l) => {
                htmlCountries = htmlCountries + "<ul class='ul_category'><li class='li_category'>" + l.name + "</li></ul>";
                
            });
        }
        return htmlCountries;
    }
    function convertHtmlSubCategory(dataItem, typeActivity) {
        var htmlCountries = "";
        var products = dataItem.ItemCategory.filter((c) => c.TypeActivityId == typeActivity);

        var products_category = products.sort(function (a, b) {

            var keyA = a.SubcategorySpanish,
                keyB = b.SubcategorySpanish;
            // Compare the 2 dates
            if (keyA < keyB) return -1;
            if (keyA > keyB) return 1;

            return 0;
        })
        products_category.map((c) => {
            htmlCountries = htmlCountries + "<label class='text-left'>" + c.SubcategorySpanish + "</label>";
        })
        return htmlCountries;
    }
    function cityFilter(element) {
        element.kendoDropDownList({
            dataSource: [
                "Apples",
                "Oranges"
            ],
            optionLabel: "--Select Value--"
        });
    }
});

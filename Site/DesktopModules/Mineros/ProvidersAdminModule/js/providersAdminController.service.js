﻿app.factory('providersAdminControllerService', ['api_url', '$http', function (api_url, $http) {
    var service = {};
    const route_save_provider = api_url + "Provider/createProvider";
    const route_category = api_url + "provider/getAllCategory";
    const route_delete_provider = api_url +"provider/deleteProviderById?id="
    const app_url_provder = api_url + "/provider/getAllProviders"
    service.saveProvider = function (provider) {
        var promise = $http.post(route_save_provider, provider);
        promise.catch(function (data) {

        });

        return promise;
    }
    service.getListInterestFilter = function (filter) {
        var promise = $http.get(app_url_provder + filter);

        promise.catch(function (data) {

        });

        return promise;
    }
    service.getCategoryAll = function () {
        var promise = $http.get(route_category);
        promise.catch(function (data) {

        });

        return promise;
    }
    service.deleteProvider = function (id) {
        var promise = $http.get(route_delete_provider + id);
        promise.catch(function (data) {

        })
        return promise;
    }
    service.deleteSubcategory = function (id) {
        var promise = $http.get(route_delete_subcategory + id);
        promise.catch(function (data) {

        })
        return promise;
    }
    return service;
}]);
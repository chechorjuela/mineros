﻿app.directive('googleplace', function () {
    return {
        require: 'ngModel',
        scope: {
            ngModel: '=',
            details: '=?'
        },
        link: function (scope, element, attrs, model) {
            var Restrictions = [];
            var options = {
                types: ['(regions)'],
                componentRestrictions: { country: Restrictions}
            };
            scope.gPlace = new google.maps.places.Autocomplete(element[0], options);

            google.maps.event.addListener(scope.gPlace, 'place_changed', function () {
                scope.$apply(function () {
                    formated_addres = element.val().split(",");
                    country = formated_addres[formated_addres.length - 1];
                    document.getElementById("country_enterprise").value = "";
                    var city = formated_addres[0]
                    if (formated_addres.length > 2) {
                        city += "," + formated_addres[1]
                    }
                    let objectCountry = {
                        'show': 1,
                        'name': country
                    }
                    let index_country = scope.$parent.vm.arrayCountry.findIndex((c) => c.name === country);
                    if (index_country<0) {
                        scope.$parent.vm.arrayCountry.push(objectCountry);
                        scope.$parent.vm.objectCountry = true;

                    }
                });
            });
        }
    };

});

app.directive("fileModel", ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            element.bind('change', function () {
                scope.$apply(function () {
                    modelSetter(scope, element[0].files[0]);
                    let fileObj = element[0].files[0];
                    const reader = new FileReader();
                    reader.readAsDataURL(fileObj);
                    reader.onload = function () {
                        fileObj.FileStringBase64 = reader.result;
                        var obj = {
                            FileName: fileObj.name,
                            SizeImage: fileObj.size,
                            FormatType: fileObj.type,
                            FileStringBase64: fileObj.FileStringBase64,

                        }
                        //scope.$parent.vm.providerForm.fileBrochure = obj;
                    };
                    reader.onerror = function (error) {
                        console.log('Error: ', error);
                    };
                  

                });
            });
        }
    };
}]);

app.directive("validFile", function () {
    return {
        restrict: "A",
        require: '^form',

        link: function (scope, elem, attrs, ctrl) {

            elem.bind("change", function (e) {
                scope.$apply(function () {
                    ctrl.$valid = true;
                    ctrl.$invalid = false;
                });
            });

        }
    };
});
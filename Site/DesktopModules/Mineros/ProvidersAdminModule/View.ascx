﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="View.ascx.cs" Inherits="DesktopModules_Mineros_ProvidersAdminModule_View" %>

<div ng-app="itemApp<%=ModuleId%>" ng-controller="providersAdminController">
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB18VJBwo1GssQFBWnZe6t3YJxIesc8KZc&libraries=places&sensor=false"></script>
    <span kendo-notification="notif" k-options="opt" style="display: none;"></span>
    <div class="row" class="row ng-cloak" ng-cloak>
        <div class="col-md-12">

            <a class="btn btn-succes bg-jungle-green librefranklin-regular text-white float-right" ng-show="roles_user_buttons"
                data-toggle="modal" data-target="#modalSubCategory" role="btn btn-primary" aria-pressed="true">Agregar SubCategoría
            </a>
            <a class="btn btn-succes bg-jungle-green librefranklin-regular text-white float-right" ng-show="roles_user_buttons" style="margin-right: 15px; margin-bottom: 10px"
                data-toggle="modal" data-target="#modalCategory" role="btn btn-primary" aria-pressed="true">Agregar Categoría
            </a>
        </div>
        <div class="col-md-12" ng-if="showMaster && !selectedItem">
            <kendo-grid id="gridMain" options="mainGridOptions">
            </kendo-grid>
        </div>
        <div class="col-md-12" ng-if="showCategory">
            <kendo-grid options="mainGridOptionsMaster">
            </kendo-grid>
        </div>
        <div class="col-md-12" ng-if="showSubCategory">
            <kendo-grid options="mainGridOptionsSubcategory">
            </kendo-grid>
        </div>
        <div ng-if="selectedItem">
            <edit-provider-module-component go-back="goBackFromForm()" update-settings="updateItem()" edit-provider="providerSelected" category-settings="categories"></edit-provider-module-component>
        </div>
    </div>
    <div ng-if="showMaster">
        <div ng-if="view_category">
            <view-category-module-component go-back="goBackFromFormSubcategory()" view-category="categoryItem" category-settings="categories"></view-category-module-component>
        </div>
    </div>
    <div ng-if="showCategory">
        <div ng-if="selectCategory">
            <edit-sub-category-module-component go-back="vm.goBackForm()" edit-subcategory="vm.subcategoryItem" category-settings="categories"></edit-sub-category-module-component>
        </div>
    </div>
    <div ng-if="showCategory">
        <div ng-if="selectCategory">
            <edit-sub-category-module-component go-back="vm.goBackForm()" edit-subcategory="vm.subcategoryItem" category-settings="categories"></edit-sub-category-module-component>
        </div>
    </div>
    <div ng-if="showSubCategory">
        <div ng-if="selectSubCategory">
            <edit-sub-category-module-component go-back="vm.goBackForm()" edit-subcategory="vm.subcategoryItem" category-settings="categories"></edit-sub-category-module-component>
        </div>
    </div>
    <form></form>
    <div class="modal fade" id="modalCategory" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Agregar Categoria</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form name="form_category">
                    <div class="modal-body row">
                        <input type="hidden" name="id" ng-value="form_category.id" ng-model="categorySelected.Id" />
                        <div class="col-md-6 form-group">
                            <strong>Valor Español:</strong>
                            <input class="form-control" name="Spanish" ng-value="form_category.spanish" required ng-model="categorySelected.Spanish" />
                            <div ng-show="form_category.$submitted || form_category.Spanish.$touched">
                                <span class="error_forms" ng-show="form_category.Spanish.$error.required ">Este campo es requerido.</span>
                            </div>
                        </div>
                        <div class="col-md-6 form-group">
                            <strong>Valor Inglés:</strong>
                            <input class="form-control" name="English" ng-value="form_category.english" required ng-model="categorySelected.English" />
                            <div ng-show="form_category.$submitted || form_category.English.$touched">
                                <span class="error_forms" ng-show="form_category.English.$error.required ">Este campo es requerido.</span>
                            </div>
                        </div>
                        <div class="col-md-6 form-group">
                            <strong>Tipo de Actividad:</strong>
                            <select ng-options="type.Id as type.Spanish for type in typeActivities" name="typeActivity" required ng-model="categorySelected.TypeActivityId" class="form-control bg-very-light-gray">
                                <option value="">Selecionar..</option>
                            </select>
                            <div ng-show="form_category.$submitted || form_category.typeActivity.$touched">
                                <span class="error_forms" ng-show="form_category.typeActivity.$error.required ">Este campo es requerido.</span>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary  k-button k-primary" ng-click="saveCategory($event,form_category,categorySelected)">Guardar</button>
                        <button type="button" class="btn btn-secondary  k-button" data-dismiss="modal">Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalSubCategory" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Agregar Subcategoría</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form name="form_subcategory">
                    <div class="modal-body row">
                        <div class="col-md-6">
                            <input type="hidden" name="id" ng-model="subcategorySelected.SubcategoryId" />
                            <div class="form-group">
                                <strong>Valor Español:</strong>
                                <input type="text" name="spanish" required ng-model="subcategorySelected.SubcategorySpanish"
                                    class="form-control bg-very-light-gray librefranklin-reg" />
                                <div ng-show="form_subcategory.$submitted || form_subcategory.spanish.$touched">
                                    <span class="error_forms" ng-show="form_subcategory.spanish.$error.required ">Este campo es requerido.</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <strong>Valor Inglés:</strong>
                                <input type="text" name="english" required ng-model="subcategorySelected.SubcategoryEnglish"
                                    class="form-control bg-very-light-gray librefranklin-reg" />
                                <div ng-show="form_subcategory.$submitted || form_subcategory.english.$touched">
                                    <span class="error_forms" ng-show="form_subcategory.english.$error.required ">Este campo es requerido.</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <strong>Categoría:</strong>
                            <select ng-options="category.Id as category.Spanish for category in categories"
                                ng-model="subcategorySelected.CategoryId" name="CategoryId" class="form-control bg-very-light-gray">
                                <option value="">Selecionar..</option>
                            </select>
                            <div ng-show="form_providers.$submitted ">
                                <span class="error_forms" ng-show="productSelect.$error.required ">Este campo es requerido.</span>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary k-button k-primary" ng-click="saveSubCategory($event,form_subcategory,subcategorySelected)">Guardar</button>
                        <button type="button" class="btn btn-secondary k-button" data-dismiss="modal">Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    var data = {};
    var roles = "";
    data.moduleId = <%=ModuleId%>;
    data.module_resources = '<%=Resources%>';
    data.portalId = <%=PortalId%>;
    data.common_resources = '<%=CommonResources%>';
    <%int i = 0; foreach (string role in UserController.Instance.GetCurrentUserInfo().Roles) {
        i++;
        %>
    roles = roles+'<%= role %>' ;
    <% 
        if (i<UserController.Instance.GetCurrentUserInfo().Roles.Length)
        {%>
    roles = roles + ","; 
        <%}
    } %>
    data.culture = '<%=System.Threading.Thread.CurrentThread.CurrentCulture.Name %>';
    var app = angularInit(data);
</script>
<link rel="stylesheet" href="/DesktopModules/Mineros/ProvidersAdminModule/css/styleprovider.css" />
<script src="/DesktopModules/Mineros/scripts/helpers/helpers.js"></script>
<script src="/DesktopModules/Mineros/ProvidersAdminModule/js/providersAdminController.component.js"></script>
<script src="/DesktopModules/Mineros/ProvidersAdminModule/js/providersAdminController.service.js"></script>
<script src="/DesktopModules/Mineros/ProvidersAdminModule/js/providersAdminController.directive.js"></script>
<script src="/DesktopModules/Mineros/ProvidersAdminModule/components/EditProviderModule/editProviderModule.component.js"></script>
<script src="/DesktopModules/Mineros/ProvidersAdminModule/components/EditProviderModule/editProviderModule.service.js"></script>
<script src="/DesktopModules/Mineros/ProvidersAdminModule/components/EditSubCategoryModule/editSubCategory.component.js"></script>
<script src="/DesktopModules/Mineros/ProvidersAdminModule/components/EditSubCategoryModule/editSubCategory.service.js"></script>
<script src="/DesktopModules/Mineros/ProvidersAdminModule/components/ViewCategoryModule/viewCategoryModule.component.js"></script>
<script src="/DesktopModules/Mineros/ProvidersAdminModule/components/ViewCategoryModule/viewCategoryModule.service.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.4.0/jszip.min.js"></script>

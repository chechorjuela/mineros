﻿app.controller("CertificatesShareholderController", function ($scope, module_resources, $q, common_resources, CertificatesShareholderService, portal_id, $window, $sce) {

    //Variables
    $scope.module_resources = JSON.parse(module_resources);
    $scope.common_resources = JSON.parse(common_resources);
    $scope.firstLoad = false;
    $scope.certificasDeceval = [];
    $scope.submit = false;
    $scope.clientDeceval = null;
    //end variables

    this.$onInit = function () {
        initializerPage();
    }

    function initializerPage() {

    }

    $scope.searchCertificate = function (e, formCertificate, formModelCertificate) {
        e.preventDefault();
        $scope.submit = true;
        $scope.certificasDeceval = [];
        if (formCertificate.$valid) {

            CertificatesShareholderService.GetClientDeceval(formModelCertificate).then((response) => {
                $scope.clientDeceval = null;
                if (response.status == 200) {
                    if (response.data.Data != null) {
                        $scope.clientDeceval = response.data.Data;
                    }
                }
            });

            //CertificatesShareholderService.GetCertificates(formModelCertificate).then((response) => {
            //    if (response.status == 200) {
            //        if (response.data.Data != null) {
            //            $scope.certificasDeceval = response.data.Data;
            //        }
            //    }
            //});
        }
    }

    $scope.download_file = function (url_file) {
        window.open(url_file, '_blank', '');
    }
});
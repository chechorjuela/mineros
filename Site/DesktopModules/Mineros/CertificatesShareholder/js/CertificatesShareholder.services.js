﻿app.factory('CertificatesShareholderService', ['api_url', '$http', function (api_url, $http) {
    var service = {};

    const route_getCertificates = '/Certificate/GetCerticatesDeceval';
    const route_getClientDeceval = '/Certificate/GetClientDecevalByCodeAndNit';


    service.GetCertificates = function (itemCertificate) {
        var promise = $http.post(api_url + route_getCertificates, itemCertificate);

        promise.catch(function (data) {

        });
        return promise;
    }

    service.GetClientDeceval = function (clientDeceval) {
    

        var promise = $http.get(api_url + route_getClientDeceval + '?nit_request=' + clientDeceval.identification + '&codeDeceval_request=' + clientDeceval.codeDeceval);

        promise.catch(function (data) {

        });
        return promise;
    }

    return service;
}]);
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="View.ascx.cs" Inherits="DesktopModules_Mineros_CertificatesShareholder_View" %>

<div class="container" ng-app="itemApp<%=ModuleId%>" ng-controller="CertificatesShareholderController" class="ng-cloak" ng-cloak>
    <div class="row ">
        <div class="col-md-12">
            <h1 class="uk-article-title">Certificados tributarios</h1>
            <div class="row">
                <div class="col-md-12">
                    <p>
                        Para descargar los certificados tributarios emitidos por las diferentes filiales del Grupo MINEROS, ingrese los siguientes datos:
                    </p>
                </div>
                <div class="col-md-12">
                    <form></form>

                    <form name="form_certificate" class="form_certificate" novalidate>
                        <div class="form-group">
                            <input type="text" class="form-control" name="identification" ng-model="formCertificate.identification" required placeholder="Número de identificación o N.I.T*:">
                             <div ng-show="form_certificate.$submitted || form_certificate.identification.$touched">
                                <span class="error_forms" ng-show="form_certificate.identification.$error.required ">Número de identificación incorrecto.</span>
                            </div>
                            <small class="form-text text-muted">Sin puntos ni dígito de verificación.</small>
                            <small class="form-text text-muted">Si su identificación es NIT, ingrese el número sin puntos y adicionando el dígito de verificación (sin guión)..</small>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="codeDeceval" ng-model="formCertificate.codeDeceval" required placeholder="Número cuenta DECEVAL*:">
                            <div ng-show="form_certificate.$submitted || form_certificate.codeDeceval.$touched">
                                <span class="error_forms" ng-show="form_certificate.codeDeceval.$error.required ">Número cuenta DECEVAL incorrecto.</span>
                            </div>
                        </div>
                        <button type="submit" ng-disabled="form_certificate.$invalid" ng-click="searchCertificate($event,form_certificate,formCertificate)" class="btn btn-primary">Enviar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <p>Número cuenta DECEVAL:Si no recuerda el número de cuenta de DECEVAL, lo puede encontrar en el último extracto de acciones o se puede comunicar a través de las líneas de atención al accionista:</p>
            <p>Medellín (4) 311 73 83 ext. 1305, Bogotá (1) 307 71 27 ó en el resto del país 01 8000 111 901.</p>
            <p>Correo electrónico servicioalcliente@deceval.com.co</p>
        </div>
    </div>
    <div class="row content-download" ng-show="submit">

        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <p ng-show="clientDeceval.nameClient!=null"><label>Asociado: </label>{{clientDeceval.nameClient}}</p>
                    <p ng-show="clientDeceval.nameClient==null">No Existe asociado</p>
                </div>
            </div>
            
            <ul class="download-list" ng-show="clientDeceval.fileExist">
                <li>
                    <img ng-src="/DesktopModules/Mineros/CertificatesTributariesModule/assets/pdf.png" />
                    <a ng-href="/portals/0/FileDocument/Accionistas/{{clientDeceval.codeDeceval}}.pdf" download="{{clientDeceval.codeDeceval}}">{{clientDeceval.codeDeceval}}</a>
                </li>
            </ul>
            <p ng-show="!clientDeceval.fileExist && submit">No se encontraron archivos</p>
        </div>
    </div>
</div>

<script>
    var data = {};
    data.moduleId = <%=ModuleId%>;
    data.module_resources = '<%=Resources%>';
    data.portalId = <%=PortalId%>;
    data.common_resources = '<%=CommonResources%>';
    data.culture = '<%=System.Threading.Thread.CurrentThread.CurrentCulture.Name %>';
    var app = angularInit(data);
</script>
<link rel="stylesheet" type="text/css" href="/DesktopModules/Mineros/CertificatesShareholder/css/CertificatesShareholder.css"></link>
<script src="/DesktopModules/Mineros/scripts/helpers/helpers.js"></script>
<script src="/DesktopModules/Mineros/CertificatesShareholder/js/CertificatesShareholder.controller.js"></script>
<script src="/DesktopModules/Mineros/CertificatesShareholder/js/CertificatesShareholder.services.js"></script>

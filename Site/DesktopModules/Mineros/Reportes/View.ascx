﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="View.ascx.cs" Inherits="Mineros.ReportesTrimestral.View" %>

<script>
    $(document).ready(function () {
        $('.urlMoreInfoReport').attr("href", $('.urlmoreReport').text());
        var panelFirst = $(".panel.panel-default.collapse-conatiner").find(".panel-heading")[0];
        $(panelFirst).find("a").trigger("click")
    });
</script>

<asp:Panel runat="server" ID="pnlReports">

    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

        <asp:Repeater ID="rptReportsYears" runat="server" OnItemDataBound="rptReportsYears_ItemDataBound" Visible="true">
            <ItemTemplate>
                <asp:HiddenField ID="hdnTrimItemCategoryId" runat="server" Value='<%# Eval("CategoryID") %>'></asp:HiddenField>

                <asp:HiddenField ID="hdnCategoryId" runat="server" Value='<%# Eval("CategoryID") %>'></asp:HiddenField>
                <div class="panel panel-default collapse-conatiner">
                    <div class="panel-heading" role="tab" id="headingOne" runat="server" visible='<%#YearHasArticle((int)Eval("CategoryID"))%>'>
                        <a class="btn btn-link btn-collpase rounded-0 w-100 text-left text-decoration-none border-color-alto bg-alto" data-toggle="collapse" href="#reporte<%# Eval("CategoryID") %>" role="button" aria-expanded="false" aria-controls="collapseExample">
                            <span class="icon-collapse icon-rotateZ">
                                <img src="/Portals/0/skins/mineros/images/arrow-circle-top.png" width="27.068" height="27.07" data-aos="flip-left">
                            </span>
                            <span class="text librefranklin-bold color-mineral-green"><%# Eval("CategoryName") %></span>
                        </a>
                    </div>
                    <div id="reporte<%# Eval("CategoryID") %>" class="collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="card card-body border-color-alto rounded-0">
                            <div class="collapse-trimestres">
                                <asp:Repeater ID="rptTrimestresHeader" runat="server" OnItemDataBound="rptTrimestresHeader_ItemDataBound">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdnTrimHeaderCategoryId" runat="server" Value='<%# Eval("CategoryID") %>'></asp:HiddenField>

                                        <asp:Repeater ID="rptTrimestresItems" runat="server" OnItemDataBound="rptTrimestreItems_ItemDataBound">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnTrimItemCategoryId" runat="server" Value='<%# Eval("CategoryID") %>'></asp:HiddenField>
                                                <asp:HiddenField ID="hdnTrimHeaderCategoryId" runat="server" Value='<%# Eval("CategoryID") %>'></asp:HiddenField>

                                                <div class="category-item-report-div" runat="server" visible='<%#HasArticles((int)Eval("CategoryID")) %>'>

                                                    <a class="btn btn-link rounded-0 w-100 text-left text-decoration-none border-color-alto bg-alto" href="#trimestre-<%#Eval("CategoryID") %>" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="trimestre-<%#Eval("CategoryID") %>" data-toggle="tab" role="tab" aria-controls="home" aria-selected="true">
                                                        <span class="icon-collapseExampleInterno">
                                                            <img src="/Portals/0/skins/mineros/images/icon-user-2.png" width="24.177" height="26.809"></span>
                                                        <span class="text librefranklin-bold color-scorpion"><%=LocalizeString("trimestre.Text") %> <%# Eval("CategoryName") %></span>
                                                    </a>
                                                </div>
                                                <div class="collapse content-item-report" id="trimestre-<%#Eval("CategoryID")%>" role="tabpanel" aria-labelledby="home-tab">

                                                    <ul class="list-group">

                                                        <asp:Repeater ID="rptReportes" runat="server" OnItemDataBound="rptReportes_ItemDataBound">
                                                            <ItemTemplate>
                                                                <asp:HiddenField ID="hdnReporteId" runat="server" Value='<%# Eval("ArticleID") %>'></asp:HiddenField>

                                                                <asp:Repeater ID="rptDocuments" runat="server">
                                                                    <ItemTemplate>
                                                                        <asp:HiddenField ID="hdnDocumentId" runat="server" Value='<%# Eval("DocEntryID") %>'></asp:HiddenField>
                                                                        <asp:HiddenField ID="ArtDocument" runat="server" Value='<%# Eval("ArticleID") %>'></asp:HiddenField>
                                                                        <li class="list-group-item rounded-0 border-0 px-0 pb-0">
                                                                            <div class="text-decoration-none d-flex">
                                                                                <span class="icon-pdf">
                                                                                    <img src="/Portals/0/skins/mineros/images/icon-pdf-2.png" srcset="/Portals/0/skins/mineros/images/icon-pdf-2.svg" alt="" width="24.001" height="30.668">
                                                                                </span>
                                                                                <p class="mb-0 color-scorpion librefranklin-regular"><%# Eval("Title") %></p>
                                                                                <span class="icon-download ml-auto">
                                                                                    <a href='<%#ResolveUrl(string.Format("~/DesktopModules/EasyDNNNews/DocumentDownload.ashx?portalid={0}&moduleid={1}&articleid={2}&documentid={3}",Eval("PortalId"),Eval("ModuleID"),Eval("ArticleID"),Eval("DocEntryID"))) %>' target="_blank">
                                                                                        <img src="/Portals/0/skins/mineros/images/icon-download.png" srcset="/Portals/0/skins/mineros/images/icon-download.svg" alt="" width="16">
                                                                                    </a>
                                                                                </span>
                                                                            </div>
                                                                        </li>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </ul>

                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>

                                        <%--                                        <asp:Repeater ID="rptTrimestresItemsTabContent" runat="server" OnItemDataBound="rptTrimestresItems_ItemDataBound">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnTrimItemCategoryId" runat="server" Value='<%# Eval("CategoryID") %>'></asp:HiddenField>
                                                <div class="collapse" id="trimestre-<%#Eval("CategoryID")%>" role="tabpanel" aria-labelledby="home-tab">
                                                    <div class="card card-body rounded-0 border-color-alto">

                                                        <ul class="list-group">
                                                            <asp:Repeater ID="rptReportes" runat="server" OnItemDataBound="rptReportes_ItemDataBound">
                                                                <ItemTemplate>
                                                                    <asp:HiddenField ID="hdnReporteId" runat="server" Value='<%# Eval("ArticleID") %>'></asp:HiddenField>
                                                                    <asp:Repeater ID="rptDocuments" runat="server">
                                                                        <ItemTemplate>
                                                                            <asp:HiddenField ID="hdnDocumentId" runat="server" Value='<%# Eval("DocEntryID") %>'></asp:HiddenField>
                                                                            <li class="list-group-item rounded-0 border-0 px-0 pb-0">
                                                                                <div class="text-decoration-none d-flex">
                                                                                    <span class="icon-pdf">
                                                                                        <img src="/Portals/0/skins/mineros/images/icon-pdf-2.png" srcset="/Portals/0/skins/mineros/images/icon-pdf-2.svg" alt="" width="24.001" height="30.668">
                                                                                    </span>
                                                                                    <p class="mb-0 color-scorpion librefranklin-regular"><%# Eval("Title") %>---</p>
                                                                                    <span class="icon-download ml-auto">
                                                                                        <a href='<%#ResolveUrl(string.Format("~/DesktopModules/EasyDNNNews/DocumentDownload.ashx?portalid={0}&moduleid={1}&articleid={2}&documentid={3}",Eval("PortalId"),Eval("ModuleID"),Eval("ArticleID"),Eval("DocEntryID"))) %>' target="_blank">
                                                                                            <img src="/Portals/0/skins/mineros/images/icon-download.png" srcset="/Portals/0/skins/mineros/images/icon-download.svg" alt="" width="16">
                                                                                        </a>
                                                                                    </span>


                                                                                </div>
                                                                            </li>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>--%>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <div class="col-12 col-md-3 alineacion-left">
                                    <div class="barra"></div>
                                    <div class="row archivos">

                                        <asp:Repeater ID="repeaterAnual" runat="server" OnItemDataBound="repeaterAnual_ItemDataBound">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnAnualCategoryId" runat="server" Value='<%# Eval("CategoryID") %>'></asp:HiddenField>
                                                <asp:Repeater ID="rptAnualArticles" runat="server" OnItemDataBound="rptAnualArticles_ItemDataBound">
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="hdnAnualArticleID" runat="server" Value='<%#Eval("ArticleID") %>' />
                                                        <asp:Repeater ID="rptDocumentsAnual" runat="server">
                                                            <ItemTemplate>
                                                                <div class="col-12 col-md-6 anual">
                                                                    <a href='<%#ResolveUrl(string.Format("~/DesktopModules/EasyDNNNews/DocumentDownload.ashx?portalid={0}&moduleid={1}&articleid={2}&documentid={3}",Eval("PortalId"),Eval("ModuleId"),Eval("ArticleID"),Eval("DocEntryID"))) %>'>

                                                                        <%--<img src='<%#DataBinder.Eval(Container.Parent.Parent, "DataItem.ArticleImage")%> ' alt="" />--%>
                                                                        <img src='<%#DataBinder.Eval(Container.Parent.Parent, "DataItem.ArticleImage")%> ' alt="" />

                                                                        <span class="icon icon-arrow-down"></span>
                                                                    </a>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </ItemTemplate>
                                        </asp:Repeater>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</asp:Panel>

<asp:Panel ID="pnlSideBar" runat="server" Visible="false">
    <div class="row icon-title">
        <div class="col-3">
            <span class="icon-reporte"></span>
        </div>
        <div class="col-9">
            <h6 class="sidebar-titulo"><%=LocalizeString("ultimoReporte.Text") %>
                <br>
                <span><%=LocalizeString("trimestre.Text") %>
                    <asp:Label ID="lblTrimestre" runat="server"></asp:Label>
                </span>
            </h6>
        </div>
    </div>

    <div class="row reportes">
        <div class="col-12">
            <div class="row presentacion">
                <asp:Repeater ID="rptDocumentsSidebar" runat="server">
                    <ItemTemplate>
                        <div class="col-9">
                            <h6><%# Eval("Document.Title") %></h6>
                        </div>

                        <div class="col-3">
                            <div class="row">
                                <a id="search-in-file" class="icon-view" href='<%#string.Format("{0}", Eval("Document.UrlView").ToString()) %>' target="_blank"></a>

                                <asp:LinkButton ID="lnkDownload" runat="server" CssClass="icon-download" PostBackUrl='<%# ResolveUrl(string.Format("~/DesktopModules/EasyDNNNews/DocumentDownload.ashx?portalid={0}&moduleid={1}&articleid={2}&documentid={3}", Eval("Document.PortalId"), Eval("Document.ModuleId"), Eval("Document.ArticleID"), Eval("Document.DocEntryID")))%>'
                                    Visible='<%#Eval("Document")!=null && Eval("Document.ArticleID").ToString()!="0"%>' OnClientClick="window.document.forms[0].target='_blank';"></asp:LinkButton>

                                <asp:HiddenField ID="hiddenArticle" runat="server" Value='<%#Eval("ArticleID") %>' />
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <a id="lnkurl" class="urlMoreInfoReport cta1" href="">
                <%=LocalizeString("masInfo.Text") %>
            </a>
            <asp:Label ID="lblurl" CssClass="urlmoreReport d-none" runat="server" Text=""></asp:Label>
        </div>
    </div>
</asp:Panel>
<style>
    .category-item-report-div {
        margin-top: 10px;
    }

    .content-item-report {
        border-right: 1px solid rgb(210, 210, 210);
        border-left: 1px solid rgb(210, 210, 210);
        border-bottom: 1px solid rgb(210, 210, 210);
        padding-bottom: 15px;
    }
</style>

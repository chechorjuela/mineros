﻿/*
' Copyright (c) 2018  Mineros.co
'  All rights reserved.
' 
' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
' TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
' THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
' CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
' DEALINGS IN THE SOFTWARE.
' 
*/

using System;
using DotNetNuke.Security;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Localization;
using System.Collections.Generic;
using Mineros.Data;
using System.Linq;
using System.Web.UI.WebControls;
using System.Globalization;
using DotNetNuke.Entities.Tabs;
using DotNetNuke.Common;
using Mineros.Data.Models;

namespace Mineros.ReportesTrimestral
{
    /// -----------------------------------------------------------------------------
    /// <summary>
    /// The View class displays the content
    /// 
    /// Typically your view control would be used to display content or functionality in your module.
    /// 
    /// View may be the only control you have in your project depending on the complexity of your module
    /// 
    /// Because the control inherits from ReportesModuleBase you have access to any custom properties
    /// defined there, as well as properties from DNN such as PortalId, ModuleId, TabId, UserId and many more.
    /// 
    /// </summary>
    /// -----------------------------------------------------------------------------
    public partial class View : ReportesModuleBase, IActionable
    {
        public List<EasyDnnNewsApp> ListNews { get; set; }
        public List<EasyDnnNewsDocuments> ListDocuments { get; set; }

        private int countListItem = 0;

        private string CultureCountry
        {
            get
            {
                CultureInfo culture2 = CultureInfo.CurrentCulture;
                //return culture2.Parent.ToString() ?? "es";
                return culture2.Name ?? "es-ES";
            }
        }

        public List<EasyDnnNewsCategories> Categories
        {
            get
            { return ViewState["Catetories"] as List<EasyDnnNewsCategories>; }
            set { ViewState["Catetories"] = value; }
        }

        public List<viewItem> listArticles
        {
            get
            { return ViewState["listArticles"] as List<viewItem>; }
            set { ViewState["listArticles"] = value; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                EasyDnnNewsData easyDnnNewsData = new EasyDnnNewsData();
                try
                {
                    ListNews = easyDnnNewsData.GetArticleNews();

                }
                catch (Exception ex)
                {
                    Exceptions.LogException(ex);
                }

                try
                {
                    cargarReportes();

                }
                catch (Exception ex)
                {
                    Exceptions.LogException(ex);

                }

                /*
                if (Settings["SideBar"] != null)
                {
                    try
                    {
                        if (Convert.ToBoolean(Settings["SideBar"]))
                            cargarSideBar();
                        else
                            cargarReportes();

                    }
                    catch (Exception ex)
                    {
                        Exceptions.LogException(ex);
                    }
                }*/
            }
        }

        private void cargarReportes()
        {
            EasyDnnNewsData easyDnnNewsData = new EasyDnnNewsData();
            List<EasyDnnNewsCategories> list = new List<EasyDnnNewsCategories>();

            //Categories = easyDnnNewsData.GetCategoriesArgosReportes();
            Categories = easyDnnNewsData.GetAllCategoriesNews(CultureCountry);

            if (Settings["Category"] == null)
            {
                Settings["Category"] = "Reportes";
            }

            var parentCategoryReportes = Categories.FirstOrDefault(s => s.CategoryName.ToLower() == Settings["Category"].ToString().ToLower());
            if (parentCategoryReportes != null)
            {
                var reportsYears = Categories.Where(s => s.ParentCategory == parentCategoryReportes.CategoryID).OrderByDescending(c => c.CategoryName).ToList();
                var artcilesList = new List<EasyDnnNewsApp>();
                foreach (var item in reportsYears)
                {
                    //var countDocs = 0;
                    var trimestreHeader = GetSubCategories(item.CategoryID).Where(s => s.CategoryName.ToLower() == "trimestre").ToList();

                    foreach (var trimH in trimestreHeader)
                    {
                        var trimItems = GetSubCategories(trimH.CategoryID);
                        //int countItems = 0;
                        foreach (var trimI in trimItems)
                        {
                            //var reps = easyDnnNewsData.GetArticles(null, trimI.CategoryID, 0, 0, ref countItems, CultureCountry).Count;
                            var reps = ListNews.Where(x => x.CategoryID == trimI.CategoryID).OrderByDescending((l) => l.CategoryName).Distinct().ToList();
                            foreach (var trimItem in reps)
                                artcilesList.Add(trimItem);
                            //        countDocs += reps;
                        }
                    }

                    //var articles = ListNews.Where(x => x.CategoryID == item.CategoryID).Distinct().ToList();

                    //var artcilesList = new List<EasyDnnNewsApp>();
                    /*foreach(var art in artcilesList)
                    {
                        if (artcilesList.Where(i => i.CategoryName == art.CategoryName).ToList().Count <= 0)
                        {
                            artcilesList.Add(art);
                        }
                    }*/
                    var haveDocuments = false;

                    foreach (var art in artcilesList)
                    {
                        if (!haveDocuments)
                            haveDocuments = easyDnnNewsData.GetALLEasyDnnNewsDocuments(art.ArticleID, CultureCountry).Count > 0;
                    }

                    if (haveDocuments)
                        //if (countDocs > 0)
                        list.Add(item);
                }

                rptReportsYears.DataSource = list.ToList();
                rptReportsYears.DataBind();
            }

            //pnlReports.Visible = true;
            //pnlSideBar.Visible = false;
        }

        public void cargarSideBar()
        {
            EasyDnnNewsData easyDnnNewsData = new EasyDnnNewsData();

            //lblurl.Text = lnkurl_Click();

            bool findLast = false;
            //Categories = easyDnnNewsData.GetCategoriesArgosReportes();
            //Categories = easyDnnNewsData.GetCategories(CultureCountry);
            Categories = easyDnnNewsData.GetAllCategoriesNews(CultureCountry);

            if (Settings["Category"] == null)
            {
                Settings["Category"] = "Reportes";
            }

            var parentCategoryReportes = Categories.FirstOrDefault(s => s.CategoryName.ToLower() == Settings["Category"].ToString().ToLower());
            if (parentCategoryReportes != null)
            {
                var reportsYears = Categories.Where(s => s.ParentCategory == parentCategoryReportes.CategoryID).OrderByDescending(s => s.CategoryName).ToList();
                foreach (var item in reportsYears)
                {
                    if (!findLast)
                    {
                        var countDocs = 0;
                        var trimestreHeader = GetSubCategories(item.CategoryID).Where(s => s.CategoryName.ToLower() == "trimestre").ToList();

                        foreach (var trimH in trimestreHeader)
                        {
                            var trimItems = GetSubCategories(trimH.CategoryID).OrderByDescending(s => s.CategoryName).ToList();
                            foreach (var trimI in trimItems)
                            {
                                //int countItems = 0;
                                var artList = ListNews.Where(x => x.CategoryID == trimI.CategoryID).ToList();
                                //var artList = easyDnnNewsData.GetArticles(null, trimI.CategoryID, 0, 0, ref countItems, cultureCountry).OrderByDescending(s => s.PublishDate).ToList();
                                if (artList != null && artList.Count > 0)
                                {
                                    var listReports = artList.Take(5).ToList();

                                    listArticles = new List<viewItem>();

                                    foreach (var lastArt in listReports)
                                    {
                                        //var documents = easyDnnNewsData.GetEasyDnnNewsDocuments(lastArt.ArticleID, cultureCountry);
                                        var documents = easyDnnNewsData.GetALLEasyDnnNewsDocuments(lastArt.ArticleID, CultureCountry);

                                        if (documents != null && documents.Count > 0)
                                        {
                                            foreach (var docs in documents)
                                            {
                                                viewItem itemVe = new viewItem();
                                                itemVe.Document = docs;
                                                listArticles.Add(itemVe);

                                                itemVe.Title = lastArt.Title;
                                                itemVe.ArticleID = lastArt.ArticleID;
                                            }

                                            //lblTrimestre.Text = trimI.CategoryName;
                                        }
                                    }

                                    findLast = true;

                                    // rptDocumentsSidebar.DataSource = listArticles;
                                    // rptDocumentsSidebar.DataBind();

                                    break;
                                }
                                var reps = artList.Count;

                                countDocs += reps;
                            }
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            }

            // pnlReports.Visible = false;
            // pnlSideBar.Visible = true;
        }

        public ModuleActionCollection ModuleActions
        {
            get
            {
                var actions = new ModuleActionCollection
                    {
                        {
                            GetNextActionID(), Localization.GetString("EditModule", LocalResourceFile), "", "", "",
                            EditUrl(), false, SecurityAccessLevel.Edit, true, false
                        }
                    };
                return actions;
            }
        }

        protected void rptReportsYears_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item is RepeaterItem item)
                {
                    var hdnCategoryId = item.FindControl("hdnCategoryId") as HiddenField;
                    var rptTrimestres = item.FindControl("rptTrimestresHeader") as Repeater;
                    var repeaterAnual = item.FindControl("repeaterAnual") as Repeater;

                    if (hdnCategoryId != null && rptTrimestres != null && !string.IsNullOrEmpty(hdnCategoryId.Value) && Categories != null
                        && repeaterAnual != null)
                    {
                        int yearCategoryId = int.Parse(hdnCategoryId.Value);
                        rptTrimestres.DataSource = Categories.Where(s => s.ParentCategory == yearCategoryId && s.CategoryName.ToLower() == "trimestre").ToList();
                        rptTrimestres.DataBind();

                        var anualidad = Categories.Where(s => s.ParentCategory == yearCategoryId && s.CategoryName.ToLower().Trim() == "anuales").ToList();

                        repeaterAnual.DataSource = anualidad;
                        repeaterAnual.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.LogException(ex);
            }
        }

        protected void rptTrimestreItems_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item is RepeaterItem item)
                {
                    var rptTrimestresItemsTabContent = item.FindControl("rptTrimestresItemsTabContent") as Repeater;
                    var hdnTrimHeaderCategoryId = item.FindControl("hdnTrimHeaderCategoryId") as HiddenField;
                    int trimHeaderCategoryId = int.Parse(hdnTrimHeaderCategoryId.Value);
                    var trimestresList = Categories.Where(s => s.ParentCategory == trimHeaderCategoryId).ToList();
                    var rptReportes = item.FindControl("rptReportes") as Repeater;
                    var hdnTrimItemCategoryId = item.FindControl("hdnTrimItemCategoryId") as HiddenField;
                    if (hdnTrimItemCategoryId != null && rptReportes != null && !string.IsNullOrEmpty(hdnTrimItemCategoryId.Value))
                    {
                        int trimItemCategoryId = int.Parse(hdnTrimItemCategoryId.Value);
                        var res = ListNews.Where(x => x.CategoryID == trimItemCategoryId).ToList();
                        rptReportes.DataSource = ListNews.Where(x => x.CategoryID == trimItemCategoryId).ToList();
                        //rptReportes.DataSource = easyDnnNewsData.GetArticles(null, trimItemCategoryId, 0, 0, ref countItems, cultureCountry);
                        rptReportes.DataBind();
                    }

                    if (rptTrimestresItemsTabContent != null)
                    {
                        rptTrimestresItemsTabContent.DataSource = trimestresList;
                        rptTrimestresItemsTabContent.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.LogException(ex);
            }
        }
        protected void rptTrimestresHeader_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item is RepeaterItem item)
                {
                    var hdnTrimHeaderCategoryId = item.FindControl("hdnTrimHeaderCategoryId") as HiddenField;
                    var rptTrimestresItems = item.FindControl("rptTrimestresItems") as Repeater;


                    int trimHeaderCategoryId = int.Parse(hdnTrimHeaderCategoryId.Value);
                    var trimestresList = Categories.Where(s => s.ParentCategory == trimHeaderCategoryId).ToList();


                    if (hdnTrimHeaderCategoryId != null && rptTrimestresItems != null && !string.IsNullOrEmpty(hdnTrimHeaderCategoryId.Value))
                    {

                        rptTrimestresItems.DataSource = trimestresList.OrderByDescending((l) => l.CategoryName);
                        rptTrimestresItems.DataBind();


                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.LogException(ex);
            }
        }

        protected void rptTrimestresItems_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            EasyDnnNewsData easyDnnNewsData = new EasyDnnNewsData();

            try
            {
                if (e.Item is RepeaterItem item)
                {
                    //var rptTrimestresItemsTabContent = item.FindControl("rptTrimestresItemsTabContent") as Repeater;
                    //hdnTrimItemCategoryId
                    //var hdnTrimHeaderCategoryId = item.FindControl("hdnTrimHeaderCategoryId") as HiddenField;
                    //int trimHeaderCategoryId = int.Parse(hdnTrimHeaderCategoryId.Value);
                    //var trimestresList = Categories.Where(s => s.ParentCategory == trimHeaderCategoryId).ToList();
                    var hdnTrimHeaderCategoryId = item.FindControl("hdnTrimHeaderCategoryId") as HiddenField;
                    int trimHeaderCategoryId = int.Parse(hdnTrimHeaderCategoryId.Value);
                    var trimestresList = Categories.Where(s => s.ParentCategory == trimHeaderCategoryId).ToList();
                    //rptTrimestresItemsTabContent.DataSource = trimestresList;
                    //rptTrimestresItemsTabContent.DataBind();

                    var hdnTrimItemCategoryId = item.FindControl("hdnTrimItemCategoryId") as HiddenField;
                    var rptReportes = item.FindControl("rptReportes") as Repeater;

                    if (hdnTrimItemCategoryId != null && rptReportes != null && !string.IsNullOrEmpty(hdnTrimItemCategoryId.Value))
                    {
                        int trimItemCategoryId = int.Parse(hdnTrimItemCategoryId.Value);
                        var res = ListNews.Where(x => x.CategoryID == trimItemCategoryId).ToList();
                        rptReportes.DataSource = res;
                        //rptReportes.DataSource = easyDnnNewsData.GetArticles(null, trimItemCategoryId, 0, 0, ref countItems, cultureCountry);
                        rptReportes.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.LogException(ex);
            }
        }

        protected void rptReportes_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            EasyDnnNewsData easyDnnNewsData = new EasyDnnNewsData();
            try
            {
                if (e.Item is RepeaterItem item)
                {

                    var hdnReporteId = item.FindControl("hdnReporteId") as HiddenField;
                    var rptDocuments = item.FindControl("rptDocuments") as Repeater;

                    if (hdnReporteId != null && rptDocuments != null && !string.IsNullOrEmpty(hdnReporteId.Value))
                    {
                        int articleID = int.Parse(hdnReporteId.Value);

                        //var res = easyDnnNewsData.GetALLEasyDnnNewsDocuments(articleID, CultureCountry);
                        //rptDocuments.DataSource = easyDnnNewsData.GetEasyDnnNewsDocuments(articleID, cultureCountry);
                        rptDocuments.DataSource = easyDnnNewsData.GetALLEasyDnnNewsDocuments(articleID, CultureCountry);
                        rptDocuments.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.LogException(ex);
            }
        }

        public bool HasArticles(int categoryId)
        {
            var haveDocuments = false;

            var articles = ListNews.Where(x => x.CategoryID == categoryId);
            EasyDnnNewsData easyDnnNewsData = new EasyDnnNewsData();

            foreach (var art in articles)
            {
                if (!haveDocuments)
                    haveDocuments = easyDnnNewsData.GetALLEasyDnnNewsDocuments(art.ArticleID, CultureCountry).Count > 0;
            }

            return haveDocuments;
        }

        public bool YearHasArticle(int yearCategoryId)
        {
            EasyDnnNewsData easyDnnNewsData = new EasyDnnNewsData();
            Categories = easyDnnNewsData.GetAllCategoriesNews(CultureCountry);
            if (Settings["Category"] == null)
            {
                Settings["Category"] = "Reportes";
            }
            var artcilesList = new List<EasyDnnNewsApp>();
            var trimestreHeader = GetSubCategories(yearCategoryId).Where(s => s.CategoryName.ToLower() == "trimestre").ToList();

            foreach (var trimH in trimestreHeader)
            {
                var trimItems = GetSubCategories(trimH.CategoryID);
                foreach (var trimI in trimItems)
                {
                    var reps = ListNews.Where(x => x.CategoryID == trimI.CategoryID).OrderByDescending((l) => l.CategoryName).Distinct().ToList();
                    foreach (var trimItem in reps)
                        artcilesList.Add(trimItem);
                    //        countDocs += reps;
                }
            }

            var haveDocuments = false;

            foreach (var art in artcilesList)
            {
                if (!haveDocuments)
                    haveDocuments = easyDnnNewsData.GetALLEasyDnnNewsDocuments(art.ArticleID, CultureCountry).Count > 0;
            }

            return haveDocuments;
        }
        public bool yearTrimHasArticle(int categoryId)
        {
            return true;
        }
        public bool HasTrimestres(int categoryId)
        {
            bool result = false;

            var count = Categories.Where(s => s.ParentCategory == categoryId).Count();
            if (count > 0)
            {
                result = true;
            }

            return result;
        }

        public List<EasyDnnNewsCategories> GetSubCategories(int categoryId)
        {
            return Categories.Where(s => s.ParentCategory == categoryId).OrderByDescending(c => c.CategoryName).ToList();
        }

        protected void repeaterAnual_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item is RepeaterItem item)
                {
                    var hdnAnualCategoryId = item.FindControl("hdnAnualCategoryId") as HiddenField;
                    var rptAnualArticles = item.FindControl("rptAnualArticles") as Repeater;
                    var titleAnuales = item.Parent.Parent.FindControl("titleAnuales") as Label;

                    if (hdnAnualCategoryId != null && rptAnualArticles != null && !string.IsNullOrEmpty(hdnAnualCategoryId.Value))
                    {
                        //int countItems = 0;
                        int anualidadCategoryId = int.Parse(hdnAnualCategoryId.Value);
                        EasyDnnNewsData easyDnnNewsData = new EasyDnnNewsData();

                        //var articles = easyDnnNewsData.GetArticles(null, anualidadCategoryId, 0, 0, ref countItems, cultureCountry);
                        var articlesList = ListNews.Where(x => x.CategoryID == anualidadCategoryId).ToList();

                        var articles = new List<EasyDnnNewsApp>();

                        foreach (var article in articlesList)
                        {
                            if (CultureCountry == "es-ES")
                            {
                                articles = articlesList.Where(s => s.ArticleImage.Contains("-esp")).ToList();
                            }
                            else
                            {
                                articles = articlesList.Where(s => s.ArticleImage.Contains("-eng")).ToList();
                            }
                        }
                        //var articles = ListNews.Where(x => x.CategoryID == anualidadCategoryId).ToList();

                        //foreach (var article in articles)
                        //{
                        //    //if (!string.IsNullOrEmpty(article.ArticleImage))
                        //    //{
                        //    //    article.ArticleImage = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + ResolveUrl("~/" + article.ArticleImage);

                        //    //}
                        //}

                        rptAnualArticles.DataSource = articles;
                        rptAnualArticles.DataBind();

                        if (titleAnuales != null)
                        {
                            if (articles != null && articles.Count > 0)
                            {
                                titleAnuales.Text = CultureCountry == "es-ES" ? Categories.First(s => s.CategoryID == anualidadCategoryId).CategoryName : Categories.First(s => s.CategoryID == anualidadCategoryId).CategoryNameLocalize;
                                titleAnuales.Visible = true;
                            }
                            else
                            {
                                titleAnuales.Visible = false;
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.LogException(ex);
            }
        }

        protected void rptAnualArticles_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item is RepeaterItem item)
                {
                    var hdnAnualArticleID = item.FindControl("hdnAnualArticleID") as HiddenField;
                    var rptDocumentsAnual = item.FindControl("rptDocumentsAnual") as Repeater;

                    if (hdnAnualArticleID != null && rptDocumentsAnual != null && !string.IsNullOrEmpty(hdnAnualArticleID.Value))
                    {
                        int articleID = int.Parse(hdnAnualArticleID.Value);
                        EasyDnnNewsData easyDnnNewsData = new EasyDnnNewsData();
                        //rptDocumentsAnual.DataSource = easyDnnNewsData.GetEasyDnnNewsDocuments(articleID, cultureCountry);

                        //var res = easyDnnNewsData.GetALLEasyDnnNewsDocuments(articleID, CultureCountry);
                        rptDocumentsAnual.DataSource = easyDnnNewsData.GetALLEasyDnnNewsDocuments(articleID, CultureCountry);
                        rptDocumentsAnual.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.LogException(ex);
            }
        }

        [Serializable]
        public class viewItem
        {
            public int ArticleID { get; set; }
            public EasyDnnNewsDocuments Document { get; set; }
            public string Title { get; set; }
        }

        protected string lnkurl_Click()
        {
            TabController objTab = new TabController();
            TabInfo tabInfo = new TabInfo();
            var locale = LocaleController.Instance.GetCurrentLocale(PortalId);
            tabInfo = objTab.GetTabByName(LocalizeString("tabName.Text"), PortalId);
            var tabCulture = objTab.GetTabByCulture(tabInfo.TabID, PortalId, locale);
            return Globals.NavigateURL(tabCulture.TabID);
        }
    }
}
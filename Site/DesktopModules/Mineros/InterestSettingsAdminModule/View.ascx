﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="View.ascx.cs" Inherits="DesktopModules_Mineros_InterestSettingsAdminModule_View" %>

<div class="container container-grid-interes" ng-app="itemApp<%=ModuleId%>" ng-controller="InteresSettingsAdminModuleController">

    <style>
        td.k-command-cell {
            padding-left: 0px !important;
            padding-right: 5px;
        }

        .k-grid-content-locked table tbody tr td {
            padding-top: 0;
            padding-bottom: 0;
        }

            .k-grid-content-locked table tbody tr td button {
                max-width: 35px;
            }

        .k-header.k-grid-toolbar, .k-header, .k-grid .k-header .k-button, .k-primary {
            background: #426d61;
            background-color: #426d61;
        }

        .k-button {
            border-color: #426d61 !important;
        }

        .k-header.k-grid-toolbar {
            border-color: #426d61;
        }

        .k-primary:focus {
            border-color: #426d61 !important;
            background: #426d61;
            background-color: #426d61;
        }

        .k-filter-item, .k-item {
            background: #fafafa;
        }

        .growl {
            background: #426d61;
            background-color: #426d61;
            border-radius: 8px !important;
            border-color: #426d61 !important;
            color: #ffffff;
            border: 0;
            height: 80px;
            width: 300px;
            font-weight: 600;
            -moz-border-radius: 3px;
            -webkit-border-radius: 3px;
            overflow: hidden;
            padding: 15px;
        }

            .growl h3 {
                color: #fafafa;
            }

        .k-animation-container {
            background: #426d61;
            background-color: #426d61;
            border-radius: 8px !important;
            border-color: #426d61 !important;
            border: 0px !important color: #ffffff;
            font-weight: 600;
            -moz-border-radius: 3px;
            -webkit-border-radius: 3px;
            overflow: hidden;
            padding: 0px;
            margin: 0;
            font-size: 1.25rem !important;
            line-height: 24px !important;
            font-family: 'LibreFranklin', sans-serif !important;
            font-weight: 700 !important;
        }
    </style>

    <div class="row ng-cloak" ng-cloak>
        <span kendo-notification="notif" k-options="opt" style="display: none;"></span>


        <div class="col-md-12">

            <div style="padding-top: 25px;" ng-if="!formSettings">
                <kendo-grid id="gridMain" options="mainGridSettings"> 
                </kendo-grid>
            </div>
        </div>
        <div class="demo-section k-content">
        </div>
        <div class="modal fade " id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="background-color: #426d61">
                        <h5 class="modal-title" style="color: #fafafa; font-weight: 600">Agregar Interés</h5>
                        <button type="button" style="color: #fafafa; font-weight: 600" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form></form>
                    <form name="formInterest" id="interestSettingsForm" novalidate>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-4 form-group">
                                    <input type="text" class="form-control" ng-model="interestNews.name_spanish" required name="name_spanish" placeholder="Valor En Español" />
                                    <div ng-show="formInterest.$submitted || formInterest.name_spanish.$touched">
                                        <span class="error_forms" ng-show="formInterest.name_spanish.$error.required ">Este campo es requerido.</span>
                                    </div>
                                </div>
                                <div class="col-md-4 form-group">
                                    <input type="text" class="form-control" ng-model="interestNews.name_english" required name="name_english" placeholder="Valor En Inglés" />
                                    <div ng-show="formInterest.$submitted || formInterest.name_english.$touched">
                                        <span class="error_forms" ng-show="formInterest.name_english.$error.required ">Este campo es requerido.</span>
                                    </div>
                                </div>
                                <div class="col-md-4 form-group">
                                    <select ng-pattern="/[1-9]/" kendo-drop-down-list class="form-control" k-options="optionsInterestSelect" required name="type_interest" required ng-model="interestNews.type_interest"></select>
                                    <div ng-show="formInterest.$submitted || formInterest.type_interest.$touched || formInterest.type_interest.$error.number">
                                        <span class="error_forms" ng-show="formInterest.type_interest.$error.required ">Este campo es requerido.</span>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                            <button type="button" type="submit" ng-disabled="submited" ng-click="saveItemsInterest($event,interestNews,formInterest)" class="btn btn-succes bg-jungle-green librefranklin-regular text-white">Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-12" ng-if="formSettings">
            <edit-interest-setting-component go-back-settings="clickCancelarSettings()" update-settings="updateItem()" edit-setting="settingInterestSeleted" interest-options="optionsInterest"></edit-interest-setting-component>
        </div>
    </div>

    <script>
        var data = {};
        data.moduleId = <%=ModuleId%>;
        data.module_resources = '<%=Resources%>';
        data.portalId = <%=PortalId%>;
        data.common_resources = '<%=CommonResources%>';
        data.culture = '<%=System.Threading.Thread.CurrentThread.CurrentCulture.Name %>';
        var app = angularInit(data);
    </script>

    <script src="/DesktopModules/Mineros/scripts/helpers/helpers.js"></script>
    <script src="/DesktopModules/Mineros/InterestSettingsAdminModule/js/interestSettingsAdmin.controller.js"></script>
    <script src="/DesktopModules/Mineros/InterestSettingsAdminModule/js/interestSettingsAdmin.service.js"></script>
    <script src="/DesktopModules/Mineros/InterestSettingsAdminModule/js/components/EditInterestSettings/editInterestSetting.component.js"></script>
    <script src="/DesktopModules/Mineros/InterestSettingsAdminModule/js/components/EditInterestSettings/editInterestSetting.service.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.4.0/jszip.min.js"></script>
    <script src="https://kendo.cdn.telerik.com/2017.3.1026/js/messages/kendo.messages.es-ES.min.js"></script>

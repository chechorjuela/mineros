﻿app.controller("InteresSettingsAdminModuleController", function ($scope, module_resources, $q, common_resources, InteresSettingsAdminModuleService, editInterestSettingService, portal_id, $window, $sce, sf, $timeout, ) {

    $scope.optionsInterest = [
        { "value": "", "name": "Tipo de interés" },
        { "value": 1, "name": "Nivel de Cargo" },
        { "value": 2, "name": "Proceso/Especialidad" },
        {"value": 3, "name": "Ubicación"}];

    $scope.detailContact = {};
    $scope.InterestSettings = {};
    $scope.form = false;
    $scope.listSettings = true;
    $scope.formSettings = false;
    $scope.interestContact;
    $scope.excelList = [];
    $scope.url_request = "";
    $scope.error_field = false;
    $scope.interestNews = {
        name_spanish: "",
        name_english: "",
        type_interest:""
    }
    $scope.submited = false;
    $scope.getDatasourceSettings = function () {
        return {
            type: 'aspnetmvc-ajax',
            dataType: "json",
            transport: {
                read: {
                    url: "/DesktopModules/services/API/InterestContact/listSettingsFilter",
                    contentType: "application/json; charset=utf-8",
                    method: "get",
                    beforeSend: function (e, request) {
                        $scope.url_request = request.url;
                    }
                },

                //parameterMap: function (options, type) {
                //    //options.filters = typeof options.filter != "undefined" ? options.filter.filters:null;
                //    //options.sorts = [
                //    //    { "member": "number_phone","order":0}
                //    //]

                //    return JSON.stringify(options);
                //}
            },
            pageSize: 5,
            serverPaging: true,
            serverSorting: true,
            serverFiltering: true,
            serverGrouping: true,
            serverAggregates: true,
            schema: {
                data: "Data.Data",
                total: "Data.Total",
                errors: "Data.Errors",
                model: {
                    fields: {
                        Id: { type: "number" },
                        name_spanish: { type: "string" },
                        name_english: { type: "string" },
                        type_interest: { type: "number" },
                    }
                },
            }
        }
    };


    $scope.goBackFromForm = function () {
        $scope.form = false;

    }
    this.$onInit = function () {
        initializeFunctions();
        var data_contact = null;
    };
    function initializeFunctions() {
        $scope.interesPositionFirst = new kendo.data.DataSource({ data: $scope.optionsInterest });
        $scope.optionsInterestSelect = {
            dataSource: $scope.interesPositionFirst,
            dataTextField: 'name',
            dataValueField: 'value',
            dataBound: function () {
                this.select("");
                console.log(this.value());
            }
           
        };
        $scope.asyncLoader(true);
        $q.all([
            
            InteresSettingsAdminModuleService.GetInterestSettings(),
        ]).then(function (data) {
            $scope.InterestSettings = data[0].data.Data;
            $scope.asyncLoader(false);
        });
        $scope.mainGridSettings = {
            toolbar: [
            {
                name: "Listado Interés",
                text: "Listado De Contactos",
                template: function (grid) {
                    return '<a data-ng-click=\'backlistInterest($event)\' class=\'k-button\'><span class=\'k-icon k-i-file-zip k-i-zip\'></span> Volver  Información de interés</a>'
                }
            }, {
                name: "AddInterest",
                text: "Agregar Interést",
                template: function (grid) {
                    return '<button type=\'button\' class=\'k-button pull-right\' data-toggle=\'modal\' data-target=\'#exampleModal\'><span class=\'k-icon k-i-file-add\'></span> Agregar Interés Maestro</button>'
                }
            }],
            dataSource: $scope.getDatasourceSettings(),
            navigatable: true,
            sortable: true,
            resizable: true,
            reorderable: true,
            columnMenu: true,
            pageable: {
                refresh: true,
                alwaysVisible: true,
                pageSizes: [5, 10, 20, 100]
            },
            columns: [
                {
                    field: "Id",
                    width: "30px",
                    title: "ID",

                }, {
                    field: "name_spanish",
                    width: "120px",
                    title: "Texto en Español",
                }, {
                    field: "name_english",
                    width: "120px",
                    title: "Texto en Inglés",
                }, {
                    field: "type_interest",
                    width: "90px",
                    title: "Tipo de interés",
                    template: function (dataItem) {
                        var tipe = "";
                        switch (dataItem.type_interest) {
                            case 1:
                                tipe = "Nivel de Cargo";
                                break;
                            case 2:
                                tipe = "Proceso/Especialidad";

                                break;
                            case 3:
                                tipe = "Ubicación";
                                break;
                        }
                        return "<p>" + tipe + "</p>";
                    },
                },
                {
                    title: "Acciones",
                    width: "60px",
                    command: [
                        {
                            field: "Acciones",
                            template:"<kendo-button type='button' class='k-primary btn-iconInnovacion' icon=\"'k-icon k-i-edit'\" ng-click='editItemSettigs(dataItem)' > " +
                                "</kendo-button> " +
                                "<kendo-button type='button' class='btn-iconInnovacion' icon=\"'k-icon k-i-delete'\" ng-click='deleteItemSetting(dataItem)' > " +
                                "</kendo-button>"
                        }
                    ],
                }
                ]
        }
        // Parallel loads
    }
    $scope.ngValue = "";

    $scope.notf1Options = {
        templates: [{
            type: "ngTemplate",
            template: $("#notificationTemplate").html()
        }]
    };
  


    $scope.opt = {
        position: {
            top: 30,
            right: 30
        },
        templates: [{
            type: "growl",
            template: "<div class='growl'><h3>#= title #</h3><p>#= message #</p></div>"
        }]
    };
    $scope.deleteItemSetting = function (item) {
        $("<div id='confirmPopupWindow'></div>")
            .appendTo("body")
            .kendoConfirm({
                title: "Eliminar",
                content: "Desea eliminar el interés",
                messages: {
                    okText: "Sí",
                    cancel: "Cancelar"
                }
            }).data("kendoConfirm").open()
            .result.done(function () {
                $scope.asyncLoader(true);
                kendo.ui.progress($("#mainGrid"), true);

                editInterestSettingService.delete(item.Id).then(function (response) {
                    if (response.data.Header.Code == 200) {
                        initializeFunctions();
                        if (response.data.Data) {
                            $("#exampleModal").modal("hide");
                            $scope.notif.show({
                                title: "Realizado",
                                message: "Operación realizada."
                            }, "growl");
                            $("a[title='Actualizar']").trigger("click");
                        }
                        else {
                            $scope.notif.show({
                                title: "Error!",
                                message: "No se pudo eliminar. este recurso esta asociado a un interés de contacto"
                            }, "growl");
                        }
                        $("a[title='Actualizar']").trigger("click");
                        $scope.asyncLoader(false);
                    } else {
                        switch (response.data.Message) {
                            case "EC999":
                                $scope.not_service.warning($scope.common_resources.ConstrainGeneralError_Text);
                                console.log("Error");
                                kendo.ui.progress($("#mainGrid"), false);
                                break;
                            default:
                                kendo.ui.progress($("#mainGrid"), false);
                                $scope.not_service.error(response.data.Message);
                        }
                    }
                }, function (response) {
                    //$scope.not_service.error("Error: " + response.status + " " + response.statusText);
                    console.log("Error");
                    kendo.ui.progress($("#mainGrid"), false);
                    $scope.asyncLoader(false);

                });

            });
    };

    $scope.getInterestContact = function (id) {
        if (typeof id != "undefined") {
            var name_settings = $scope.InterestSettings.find(i => i.Id == id);
            return name_settings.name_spanish;

        }
        return "";
    }
    $scope.backlistInterest = function (event) {
        $scope.listSettings = true;
        const page = "/Postulaciones";
        window.location.replace(window.location.protocol + "//" + window.location.host + page);
    }
    $scope.showListSettings = function () {
        $scope.listSettings = true;

    }
    $scope.editItemSettigs = function (dataItem) {
        $scope.formSettings = true;
        $scope.settingInterestSeleted = dataItem;

    }
    $scope.clickCancelarSettings = function () {
        $scope.formSettings = false;
        $scope.settingInterestSeleted = null;
    }
    $scope.goBackFromForm = function () {
        $scope.form = false;
        $scope.interestContact = null;
        $window.scrollTo(0, 0);
    }
    $scope.updateItem = function(){
        $scope.notif.show({
            title: "Realizado",
            message: "Recurso Actualizado."
        }, "growl");
    }
    $scope.saveItemsInterest = function (event, interestNews, formInterestNews) {
        event.preventDefault();
    
     
        if (formInterestNews.$valid ) {
            $scope.submited = true;
            if (interestNews.type_interest != 0) {
                editInterestSettingService.storeItem(interestNews).then(function (response) {
                    $scope.submited = false;
                    if (response.data.Header.Code == 200) {
                        initializeFunctions();
                        if (response.data.Data) {
                            $("#exampleModal").modal("hide");
                            $scope.notif.show({
                                title: "Realizado",
                                message: "Recurso Guardado."
                            }, "growl");
                            $("a[title='Actualizar']").trigger("click");
                        }
                     
                        $scope.asyncLoader(false);
                    } else {
                        switch (response.data.Message) {
                            case "EC999":
                                $scope.not_service.warning($scope.common_resources.ConstrainGeneralError_Text);
                                console.log("Error");
                                kendo.ui.progress($("#mainGrid"), false);
                                break;
                            default:
                                kendo.ui.progress($("#mainGrid"), false);
                                $scope.not_service.error(response.data.Message);
                        }
                    }
                }, function (response) {
                    //$scope.not_service.error("Error: " + response.status + " " + response.statusText);
                    console.log("Error");
                    kendo.ui.progress($("#mainGrid"), false);
                    $scope.asyncLoader(false);
                })
            } else {
                if (interestNews.type_interest == 0) {
                    $scope.error_field = true;

                }
            }
           
        } 
      
    }
    $scope.validationFunc = function (value) {
        console.info(value);
    }
});

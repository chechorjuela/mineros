﻿app.component("editInterestSettingComponent", {
    templateUrl: "/DesktopModules/Mineros/InterestSettingsAdminModule/js/components/EditInterestSettings/editInterestSetting.component.html",
    bindings: {
        goBackSettings: '&?',
        editSetting: '=',
        interestOptions: '=',
        updateSettings:'&?'
    },
    controllerAs: 'vm',
    controller: function ($scope, $http, api_url, $q, editInterestSettingService,portal_id, $window, $timeout, $sce) {
        var vm = this;
        vm.settingEdit = null;
      
        vm.$onInit = function () {
            vm.settingEdit = vm.editSetting;
            vm.interesPositionFirst = new kendo.data.DataSource({ data: vm.interestOptions });
            vm.optionsInterest = {
                dataSource: vm.interesPositionFirst,
                dataTextField: 'name',
                dataValueField: 'value',
            };
          

        }

        vm.clickCancelarSettings = function () {
            vm.goBackSettings && vm.goBackSettings();
        }
        vm.onSubmitSettings = function (e,settingEdit, formSetting) {
            if (formSetting.$valid) {
                editInterestSettingService.update(settingEdit).then(function (response) {
                    if (response.data.Header.Code == 200) {
                        vm.goBackSettings && vm.goBackSettings();
                        vm.updateSettings();
                    }
                }, function (response) {
                    //$scope.not_service.error("Error: " + response.status + " " + response.statusText);
                    console.log("Error");
                    kendo.ui.progress($("#mainGrid"), false);
                })
            }
        }
       
    }
    
});
﻿app.factory('editInterestSettingService', function (api_url, $http) {
    var s = {};
    const app_url_interest_settings_post = api_url + "InterestContact/storeSettings";
    const app_url_interest_settings_put = api_url + "InterestContact/updateSettings";
    const route_delete_interest_settings = api_url + "InterestContact/delelteSettings?id=";

    s.storeItem = function (item) {

        var promise = $http.post(app_url_interest_settings_post, item);

        promise.catch(function (response) {
            console.log("hubo un error");
        });

        return promise;
    }

    s.update = function (item) {
        var data = new FormData();
     
        var promise = $http.post(app_url_interest_settings_put, item);

        promise.catch(function (response) {
            console.log("hubo un error");
        });

        return promise;
    }
    s.delete = function (id) {
        var promise = $http.get(route_delete_interest_settings + id);
        promise.catch(function (data) {

        })
        return promise;
    }
    return s;
});
﻿using DotNetNuke.Entities.Modules;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using DotNetNuke.UI.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Linq;
using System.Resources;
using System.Web.UI.WebControls;

public partial class DesktopModules_Mineros_AsambleaDocumentsModule_View : PortalModuleBase
{
    protected string Resources
    {
        get
        {
            return ClientAPI.GetSafeJSString(JsonConvert.SerializeObject(Localization.GetString("Resources", LocalResourceFile)));
        }
    }

    protected string CommonResources
    {
        get
        {
            return ClientAPI.GetSafeJSString(JsonConvert.SerializeObject(Localization.GetString("CommonResources", "/App_GlobalResources/SharedResources/SharedResources")));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            DotNetNuke.Framework.ServicesFramework.Instance.RequestAjaxScriptSupport();
            DotNetNuke.Framework.ServicesFramework.Instance.RequestAjaxAntiForgerySupport();

        }
        catch (Exception exc) //Module failed to load
        {
            Exceptions.ProcessModuleLoadException(this, exc);
        }
    }
}
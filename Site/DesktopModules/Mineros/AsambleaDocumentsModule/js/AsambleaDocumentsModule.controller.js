app.controller("AsambleaDocumentsModuleController", function ($scope, module_resources, $q, common_resources, AsambleaDocumentsModuleService, portal_id, $window, $sce) {
    filter = "Asamblea";

    //Variables
    $scope.module_resources = JSON.parse(module_resources);
    $scope.common_resources = JSON.parse(common_resources);
    $scope.firstLoad = false;
    $scope.categories = [];
    $scope.list_news = [];
    var count_category = 0;
    //end variables
    $scope.list_asamblea = [];
    this.$onInit = function () {
        initializerPage();
    }

    function initializerPage() {
        AsambleaDocumentsModuleService.GetFoundationDocument(filter).then((response) => {
            if (response.status == 200) {
              
                angular.forEach(response.data.Data, function (value, key) {
                    if ($scope.categories.indexOf(value.CategoryName) < 0) {
                        $scope.categories.push(value.CategoryName);
                        var index_category = $scope.categories.indexOf(value.CategoryName);
                        $scope.list_news[index_category] = response.data.Data.filter(l => l.CategoryName == value.CategoryName);
                      
                    }
                });


            }
        });
    }

    $scope.SkipValidation = function (value) {
       
        return $sce.trustAsHtml(value);
    };
});



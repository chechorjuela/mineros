﻿app.factory('AsambleaDocumentsModuleService', ['api_url', '$http', function (api_url, $http) {
    var service = {};

    const route_certificates_type = "/Documents/GetDocumentFoundation";

    service.GetFoundationDocument = function (filter) {
        var promise = $http.get(api_url + route_certificates_type + "?filter=" + filter);

        promise.catch(function (data) {

        });

        return promise;
    };



    return service;
}]);
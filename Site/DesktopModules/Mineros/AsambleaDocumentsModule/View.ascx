<%@ Control Language="C#" AutoEventWireup="true" CodeFile="View.ascx.cs" Inherits="DesktopModules_Mineros_AsambleaDocumentsModule_View" %>

<div class="container" ng-app="itemApp<%=ModuleId%>" ng-controller="AsambleaDocumentsModuleController">
    <div class="row ng-cloak" ng-cloak>
        <div id="cards_asamblea" class="card w-100 border-0 rounded-0">
            <div class="card-body p-0">
                <ul class="list-group ajusted-de-pl-li">
                    <li ng-repeat="category in categories">
                        <div class="title">{{category}}</div>
                        <ul>
                            <li class="list-group-item rounded-0 aos-init aos-animate" data-aos="fade-right" ng-repeat="list in list_news[categories.indexOf(category)]">
                                <a target="_blank" href="/DesktopModules/EasyDNNNews/DocumentDownload.ashx?portalid={{list.PortalId}}&moduleid={{list.ModuleId}}&articleid={{list.ArticleID}}&documentid={{list.DocEntryID}}" class="d-flex w-100 text-decoration-none align-items-center">
                                    <span style="margin-right: 20px;" data-aos="flip-left" class="aos-init aos-animate">
                                        <img src="/portals/0/skins/mineros/images/icon-pdf-2.png" srcset="/portals/0/skins/mineros/images/icon-pdf-2.svg" class="img-fluid" align="Pdf Icon" width="24" height="30">
                                    </span>
                                  
                                    <p ng-if="list.LocaleCode=='es-ES'" class="mb-0 librefranklin-regular color-scorpion aos-init aos-animate" data-aos="fade-left" ng-bind-html="SkipValidation(list.Title)"></p>
                                  
                                  <p ng-if="list.LocaleCode=='en-US'" class="mb-0 librefranklin-regular color-scorpion aos-init aos-animate" data-aos="fade-left"  ng-bind-html="SkipValidation(list.Description)"></p>
                                    <span class="ml-auto">
                                        <img src="/portals/0/skins/mineros/images/icon-download.png" srcset="/portals/0/skins/mineros/images/icon-download.svg" class="img-fluid" align="Download Icon" width="16" height="16">
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <!--.list-group-->
            </div>
            <!--.card-body-->
        </div>
    </div>
</div>
<script>
    var data = {};
    data.moduleId = <%=ModuleId%>;
    data.module_resources = '<%=Resources%>';
    data.portalId = <%=PortalId%>;
    data.common_resources = '<%=CommonResources%>';
    data.culture = '<%=System.Threading.Thread.CurrentThread.CurrentCulture.Name %>';
    var app = angularInit(data);
</script>
<script src="/DesktopModules/Mineros/scripts/helpers/helpers.js"></script>
<script src="/DesktopModules/Mineros/AsambleaDocumentsModule/js/AsambleaDocumentsModule.controller.js"></script>
<script src="/DesktopModules/Mineros/AsambleaDocumentsModule/js/AsambleaDocumentsModule.service.js"></script>
<style>
        #cards_asamblea ul {
            margin-left: 0px;
        }

        #cards_asamblea ul > li {
            list-style: none;
            padding-left: 0 !important;
        }

            #cards_asamblea ul > li ul > li {
                padding-left: 35px !important;
                padding-bottom: 15px;
                padding-top: 12px;
            }

        #cards_asamblea ul li div.title {
            padding: 0 35px;
            height: 50px;
            background: #D7D7D7;
            color: #606060;
            font-size: 1.25rem;
            line-height: 1.3;
            letter-spacing: 0rem;
            font-family: "LibreFranklin";
            font-weight: 700;
            align-items: center;
            display: flex;
            width: 100%;
            max-width: 100%
        }
</style>



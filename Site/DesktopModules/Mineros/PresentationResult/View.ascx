﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="View.ascx.cs" Inherits="Mineros.ReportesReportes.View" %>

<script>
    $(document).ready(function () {
        $('.urlMoreInfoReport').attr("href", $('.urlmoreReport').text());
    });
</script>

<asp:Panel runat="server" ID="pnlReports">
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <asp:Repeater ID="rptReportsYears" runat="server" OnItemDataBound="rptReportsYears_ItemDataBound" Visible="true">
            <ItemTemplate>
                <asp:HiddenField ID="hdnCategoryId" runat="server" Value='<%# Eval("CategoryID") %>'></asp:HiddenField>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#reporte<%# Eval("CategoryID") %>" aria-expanded="true" aria-controls="reporte" class="ftopen">
                                <img class="rota" src="/portals/1/skins/argos/images/reports-right.png" alt="">
                                <%=LocalizeString("tabName.Text") %> <%# Eval("CategoryName") %>
                            </a>
                        </h4>
                    </div>
                    <div id="reporte<%# Eval("CategoryID") %>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            <div class="row">
                                <asp:Repeater ID="rptTrimestresHeader" runat="server" OnItemDataBound="rptTrimestresHeader_ItemDataBound">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdnTrimHeaderCategoryId" runat="server" Value='<%# Eval("CategoryID") %>'></asp:HiddenField>
                                        <div class="col-12 col-md alineacion-right">
                                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                                <asp:Repeater ID="rptTrimestresItems" runat="server">
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="hdnTrimItemCategoryId" runat="server" Value='<%# Eval("CategoryID") %>'></asp:HiddenField>
                                                        <li class="nav-item" runat="server" visible='<%#HasArticles((int)Eval("CategoryID")) %>'>
                                                            <a class="nav-link" id="home-tab" data-toggle="tab" href="#trimestre-<%#Eval("CategoryID") %>" role="tab" aria-controls="home" aria-selected="true">
                                                                <%=LocalizeString("trimestre.Text") %> <%# Eval("CategoryName") %>
                                                            </a>
                                                        </li>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </ul>

                                            <asp:Repeater ID="rptTrimestresItemsTabContent" runat="server" OnItemDataBound="rptTrimestresItems_ItemDataBound">
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hdnTrimItemCategoryId" runat="server" Value='<%# Eval("CategoryID") %>'></asp:HiddenField>
                                                    <div class="tab-content" id="myTabContent">
                                                        <div class="tab-pane" id="trimestre-<%#Eval("CategoryID")%>" role="tabpanel" aria-labelledby="home-tab">
                                                            <div class="documentos">
                                                                <div class="row">
                                                                    <asp:Repeater ID="rptReportes" runat="server" OnItemDataBound="rptReportes_ItemDataBound">
                                                                        <ItemTemplate>
                                                                            <asp:HiddenField ID="hdnReporteId" runat="server" Value='<%# Eval("ArticleID") %>'></asp:HiddenField>
                                                                            <asp:Repeater ID="rptDocuments" runat="server">
                                                                                <ItemTemplate>
                                                                                    <asp:HiddenField ID="hdnDocumentId" runat="server" Value='<%# Eval("DocEntryID") %>'></asp:HiddenField>
                                                                                    <div class="col-12 col-md-12 alineacion-right">
                                                                                        <div class="files">
                                                                                            <div class="row columna">
                                                                                                <div class="col-12 col-md">
                                                                                                    <span class="icon-document"></span>
                                                                                                    <span class="texto"><%# Eval("Title") %></span>
                                                                                                </div>
                                                                                                <div class="col-12 col-md-2">
                                                                                                    <div class="end">
                                                                                                        <a href='<%#string.Format("{0}", Eval("UrlView").ToString()) %>'
                                                                                                            target="_blank">
                                                                                                            <span class="icon-view"></span>
                                                                                                            <span id="hdnExtension" style="display: none;"><%#Eval("FileExtension") %></span>

                                                                                                        </a>
                                                                                                        <a href='<%#ResolveUrl(string.Format("~/DesktopModules/EasyDNNNews/DocumentDownload.ashx?portalid={0}&moduleid={1}&articleid={2}&documentid={3}",Eval("PortalId"),Eval("ModuleId"),Eval("ArticleID"),Eval("DocEntryID"))) %>' target="_blank">
                                                                                                            <span class="icon-download"></span>
                                                                                                        </a>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:Repeater>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <div class="col-12 col-md-3 alineacion-left">
                                    <div class="barra"></div>
                                    <div class="row archivos">
                                        <asp:Repeater ID="repeaterAnual" runat="server" OnItemDataBound="repeaterAnual_ItemDataBound">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnAnualCategoryId" runat="server" Value='<%# Eval("CategoryID") %>'></asp:HiddenField>
                                                <asp:Repeater ID="rptAnualArticles" runat="server" OnItemDataBound="rptAnualArticles_ItemDataBound">
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="hdnAnualArticleID" runat="server" Value='<%#Eval("ArticleID") %>' />
                                                        <asp:Repeater ID="rptDocumentsAnual" runat="server">
                                                            <ItemTemplate>
                                                                <div class="col-12 col-md-6 anual">
                                                                    <a href='<%#ResolveUrl(string.Format("~/DesktopModules/EasyDNNNews/DocumentDownload.ashx?portalid={0}&moduleid={1}&articleid={2}&documentid={3}",Eval("PortalId"),Eval("ModuleId"),Eval("ArticleID"),Eval("DocEntryID"))) %>'>

                                                                        <%--<img src='<%#DataBinder.Eval(Container.Parent.Parent, "DataItem.ArticleImage")%> ' alt="" />--%>
                                                                        <img src='<%#DataBinder.Eval(Container.Parent.Parent, "DataItem.ArticleImage")%> ' alt="" />

                                                                        <span class="icon icon-arrow-down"></span>
                                                                    </a>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </ItemTemplate>
                                        </asp:Repeater>

                                        <div class="col-12">
                                            <asp:Label class="text-center" ID="titleAnuales" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</asp:Panel>

<asp:Panel ID="pnlSideBar" runat="server" Visible="false">
    <div class="row icon-title">
        <div class="col-3">
            <span class="icon-reporte"></span>
        </div>
        <div class="col-9">
            <h6 class="sidebar-titulo"><%=LocalizeString("ultimoReporte.Text") %>
                <br>
                <span><%=LocalizeString("trimestre.Text") %>
                    <asp:Label ID="lblTrimestre" runat="server"></asp:Label>
                </span>
            </h6>
        </div>
    </div>

    <div class="row reportes">
        <div class="col-12">
            <div class="row presentacion">
                <asp:Repeater ID="rptDocumentsSidebar" runat="server">
                    <ItemTemplate>
                        <div class="col-9">
                            <h6><%# Eval("Document.Title") %></h6>
                        </div>

                        <div class="col-3">
                            <div class="row">
                                <a id="search-in-file" class="icon-view" href='<%#string.Format("{0}", Eval("Document.UrlView").ToString()) %>' target="_blank"></a>

                                <asp:LinkButton ID="lnkDownload" runat="server" CssClass="icon-download" PostBackUrl='<%# ResolveUrl(string.Format("~/DesktopModules/EasyDNNNews/DocumentDownload.ashx?portalid={0}&moduleid={1}&articleid={2}&documentid={3}", Eval("Document.PortalId"), Eval("Document.ModuleId"), Eval("Document.ArticleID"), Eval("Document.DocEntryID")))%>'
                                    Visible='<%#Eval("Document")!=null && Eval("Document.ArticleID").ToString()!="0"%>' OnClientClick="window.document.forms[0].target='_blank';"></asp:LinkButton>

                                <asp:HiddenField ID="hiddenArticle" runat="server" Value='<%#Eval("ArticleID") %>' />
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <a id="lnkurl" class="urlMoreInfoReport cta1" href="">
                <%=LocalizeString("masInfo.Text") %>
            </a>
            <asp:Label ID="lblurl" CssClass="urlmoreReport d-none" runat="server" Text=""></asp:Label>
        </div>
    </div>
</asp:Panel>

$(document).ready(function () {
    var location_module = window.location.pathname.replace(/\//g, "");
    var mounth_array = [];
    var actions_array = [];
    var url_bvc = "http://bvc.com.co/mercados/GraficosServlet?home=no&tipo=ACCION&mercInd=RV&nemo=";
    var string_price = "Precio";
    if (location_module === "en") {
        var title = "Grupos Argos Stock Behavior";
        var lang = 'Price (COP $)';
        $(".shared_actions_title").text(title)
    }
    else {
        var lang = 'Precio (COP $)'
    }
    function inicializarGraficaLinearlHome(stockData) {
     
        //Fechas_CEMARGOS_PFCEMARGOS = stockData.map(a => new Date(a.Fecha));
        Fechas_CEMARGOS_PFCEMARGOS = stockData.map(function (a) { return new Date(a.Fecha); });
        //datos_CEMARGOS = stockData.map(a => a.CEMARGOS);
        datos_CEMARGOS = stockData.map(function (a) { return a.CEMARGOS; });
        //datos_PFCEMARGOS = stockData.map(a => a.PFCEMARGOS);
        datos_PFCEMARGOS = stockData.map(function (a) { return a.PFCEMARGOS; });
        var min_CEMARGOS = datos_CEMARGOS.filter(function (it) { return it != 0 }).reduce(function (a, b) { return Math.min(a, b); });
        var min_PFCEMARGOS = datos_PFCEMARGOS.filter(function (it) { return it != 0 }).reduce(function (a, b) { return Math.min(a, b); });
        /*Gr�f�ca lineal del home*/
        var dom = document.getElementById("box-graf-section-actions-3");
        dom.style.width = '100%';
        dom .style.height = '520px';
        var myChart = echarts.init(dom);
        var app = {};
        option = null;
        option = {
            dataZoom: [{
                start: 98,
                end: 100,
            }, {
                type: 'inside',
                show: true,

                handleSize: 8
            }],
            legend: {
                data: ['CEMARGOS', 'PFCEMARGOS'],
                textStyle: {
                    fontSize: 16
                },
                right: '3%',
                top: 8,
            },
            grid: {
                x: '7%', y: '18%', width: '84%', height: '72%',
                bottom: '10%',
                containLabel: true
            },
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'shadow',
                },
            },
            xAxis: {
                axisLine: {
                    lineStyle: {
                        color: '#DEDEDE',
                    }
                },
                axisLabel: {
                    color: '#676767',
                    fontSize: 12,
                    fontFamily: "Franklin Gothic Book",
                    formatter: function (value, index) {
                        // Formatted to be month/day; display year only in the first label
                        var d = new Date(value);
                        var ano = d.getFullYear();
                        var mes = ((d.getMonth() + 1).toString().length == 1) ? ("0" + (d.getMonth() + 1).toString()) : (d.getMonth() + 1);
                        var dia = ((d.getDate().toString().length == 1) ? "0" + d.getDate().toString() : d.getDate());
                        var datestring = ano + "/" + mes + "/" + dia + " " +
                            d.getHours() + ":" + d.getMinutes();
                        return datestring;
                    }
                },
                axisTick: {
                    lineStyle: {
                        color: '#C4C4C4',
                        shadowColor: '#C4C4C4',
                        shadowOffsetY: 6.5
                    },
                    length: 6.5,
                    alignWithLabel: true,
                    inside: true,

                },
                //type: 'category',
                backgroundColor: '#676767',
                data: Fechas_CEMARGOS_PFCEMARGOS,
            },
            yAxis: {
                axisLine: {
                    lineStyle: {
                        color: 'transparent',
                    }
                },
                splitLine: {
                    lineStyle: {
                        color: '#DEDEDE',
                    }
                },
                axisLabel: {
                    color: '#676767',
                    fontSize: 10,
                    fontFamily: "Franklin Gothic Book",
                    formatter: function (value, index) {
                        // Formatted to be month/day; display year only in the first label
                        return (value / 1000).toString() + "k";
                    }
                },
                nameTextStyle: {
                    color: '#676767',
                    fontSize: 16,
                },
                type: 'value',
                position: 'right',
                nameLocation: 'center',
                name: lang,
                nameGap: 55,
                fontFamily: "Franklin Gothic Book",
                fontSize: 16,
                // max: (max_CEMARGOS > max_PFCEMARGOS) ? max_CEMARGOS : max_PFCEMARGOS,
                min: (min_CEMARGOS < min_PFCEMARGOS) ? min_CEMARGOS : min_PFCEMARGOS,
                color: '#676767',
            },
            // dataZoom: [{
            // }, {
            //     type: 'inside',
            // }],
            series: [{
                data: datos_CEMARGOS,
                type: 'line',
                smooth: true,
                name: 'CEMARGOS',
                color: '#087256',

            },
            {
                data: datos_PFCEMARGOS,
                type: 'line',
                smooth: true,
                name: 'PFCEMARGOS',
                color: '#8AB63F',
            },
            ]
        };
        if (option && typeof option === "object") {
            myChart.setOption(option, true);
        }
    }
 
    
 
    let config = {
        headers: {'Access-Control-Allow-Origin': '*','Access-Control-Allow-Headers':'Origin, X-Requested-With, Content-Type,Accept'},
        proxy:{
            host:'104.236.174.88',
            port:3128
        }
      }
      axios.all([
        axios.get(`${url_bvc}CEMARGOS`,config),
        axios.get(`${url_bvc}PFCEMARGOS`,config)
      ])
      .then(responseArr => {
        //this will be executed only when all requests are complete
          console.log('Date created: ', responseArr);
      });
 
      
    
    function build_graphics_actions() {

        var pest = document.getElementById('box-graf-section-actions-3');
        pest.classList.add('d-block');
        pest.style.width = '100%';
        pest.style.height = '520px';
        var myChartGraphics = echarts.init(document.getElementById('box-graf-section-actions-3'));
        var colors = ['#676767', '#000', '#675bba'];
        var optionGraphics = {
            media: [ // each rule of media query is defined here
                {
                    query: {
                        minWidth: 551,
                    },   // write rule here
                    option: {       // write options accordingly
                        dataZoom: [
                            {
                                type: 'slider',
                                xAxisIndex: [0],
                                realtime: false,
                                filterMode: 'weakFilter',
                                start: 95,
                                end: 100,
                                bottom: 8,
                                height: 20,
                                handleIcon: 'M10.7,11.9H9.3c-4.9,0.3-8.8,4.4-8.8,9.4c0,5,3.9,9.1,8.8,9.4h1.3c4.9-0.3,8.8-4.4,8.8-9.4C19.5,16.3,15.6,12.2,10.7,11.9z M13.3,24.4H6.7V23h6.6V24.4z M13.3,19.6H6.7v-1.4h6.6V19.6z',
                                handleSize: '120%',
                                showDetail: false
                            },
                        ],
                        legend: {
                            left: 'right',
                            itemwidth: 46,
                            itemHeight: 7
                        },
                        xAxis: {
                            type: 'category',
                            boundaryGap: false,
                            maxInterval: 100,
                            splitNumber: 100,
                            data: mounth_array,
                            min: mounth_array[0],
                            axisLabel: {
                                show: 'true',
                                textStyle: {
                                    color: colors[0],
                                    fontSize: 10.6,
                                    fontFamily: 'LibreFranklin',
                                },
                                verticalAlign: 'center',
                                padding: [10, 0, 0, 0]
                            },
                        },
                        yAxis: {
                            type: 'value',
                            name: string_price + ' (COP $)',
                            nameLocation: 'middle',
                            nameRotate: 270,
                            nameTextStyle: {
                                color: colors[0],
                                fontFamily: 'LibreFranklin',
                                fontSize: 14,
                                align: 'center',
                                verticalAlign: 'middle',
                                lineHeight: 19.2,
                                padding: [0, 0, 15, 0]
                            },
                            position: 'right',
                            maxInterval: actions_array[actions_array.length],
                            splitNumber: 10,
                            boundaryGap: false,
                            min: actions_array[0],
                            max: Math.max(...actions_array),
                            data: actions_array,
                            axisLine: {
                                show: false,
                            },
                            axisLabel: {
                                show: 'true',
                                textStyle: {
                                    color: colors[0],
                                    fontSize: 8.6,
                                    fontFamily: 'LibreFranklin',
                                },
                                formatter: '{value}',
                                verticalAlign: 'bottom'
                            },
                            splitLine: {
                                interval: 150,
                                lineStyle: {
                                    color: '#c2c2c2'
                                }
                            },
                        },
                        series: [{
                            name: ' ',
                            data: actions_array,
                            type: 'line',
                            smooth: true,
                            symbol: 'none',
                        }],
                        animationEasing: 'elasticOut',
                        color: '#daa900',
                    }
                },
                {
                    query: {
                        maxWidth: 550
                    },   // write rule here
                    option: {       // write options accordingly
                        dataZoom: [
                            {
                                type: 'slider',
                                xAxisIndex: [0],
                                realtime: false,
                                filterMode: 'weakFilter',
                                start: 95,
                                end: 100,
                                bottom: 8,
                                height: 20,
                                handleIcon: 'M10.7,11.9H9.3c-4.9,0.3-8.8,4.4-8.8,9.4c0,5,3.9,9.1,8.8,9.4h1.3c4.9-0.3,8.8-4.4,8.8-9.4C19.5,16.3,15.6,12.2,10.7,11.9z M13.3,24.4H6.7V23h6.6V24.4z M13.3,19.6H6.7v-1.4h6.6V19.6z',
                                handleSize: '100%',
                                showDetail: false
                            },
                        ],
                        legend: {
                            left: 'right',
                            itemwidth: 46,
                            itemHeight: 7
                        },
                        xAxis: {
                            type: 'category',
                            boundaryGap: false,
                            maxInterval: 100,
                            splitNumber: 100,
                            data: mounth_array,
                            min: mounth_array[0],
                            axisLabel: {
                                show: 'true',
                                textStyle: {
                                    color: colors[0],
                                    fontSize: 10.6,
                                    fontFamily: 'LibreFranklin',
                                },
                                verticalAlign: 'center',
                                padding: [10, 0, 0, 0]
                            },
                        },
                        yAxis: [{
                            type: 'value',
                            position: 'left',
                            maxInterval: actions_array[actions_array.length],
                            splitNumber: 10,
                            boundaryGap: false,
                            min: actions_array[0],
                            max: Math.max(...actions_array),
                            data: actions_array,
                            offset: -10,
                            axisLine: {
                                show: false,
                            },
                            splitLine: {
                                interval: 150,
                                lineStyle: {
                                    color: '#c2c2c2'
                                }
                            },
                            axisLabel: {
                                show: 'true',
                                textStyle: {
                                    color: colors[0],
                                    fontSize: 8.6,
                                    fontFamily: 'LibreFranklin',
                                },
                                formatter: '{value}',
                                verticalAlign: 'bottom',
                            },
                        },
                        {
                            type: 'value',
                            maxInterval: actions_array[actions_array.length],
                            splitNumber: 10,
                            boundaryGap: false,
                            min: actions_array[0],
                            max: Math.max(...actions_array),
                            data: actions_array,
                            offset: -10,
                            position: 'right',
                            name: string_price + ' (COP $)',
                            nameLocation: 'middle',
                            nameRotate: 270,
                            nameTextStyle: {
                                color: colors[0],
                                fontFamily: 'LibreFranklin',
                                fontSize: 14,
                                align: 'center',
                                verticalAlign: 'middle',
                                lineHeight: 19.2,
                                padding: [0, 0, 0, 0]
                            },
                            axisLine: {
                                show: false,
                            },
                            axisLabel: {
                                show: false,
                                textStyle: {
                                    color: colors[0],
                                    fontSize: 8.6,
                                    fontFamily: 'LibreFranklin',
                                },
                                formatter: '{value}',
                                verticalAlign: 'bottom',
                            },
                            splitLine: {
                                show: false,
                                interval: 150,
                                lineStyle: {
                                    color: '#c2c2c2'
                                }
                            },
                            minorSplitLine: {
                                show: false,
                            },
                            minorTick: {
                                show: false,
                            },
                            axisTick: {
                                show: false,
                            }
                        }],
                        series: [{
                            name: ' ',
                            data: actions_array,
                            type: 'line',
                            smooth: true,
                            symbol: 'none',
                        }],
                        animationEasing: 'elasticOut',
                        color: '#daa900',
                    }
                },
            ],

        };
        if (optionGraphics && typeof optionGraphics === "object") {
            myChartGraphics.setOption(optionGraphics);
        }
    }
})
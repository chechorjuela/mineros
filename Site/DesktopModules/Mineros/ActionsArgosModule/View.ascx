<%@ Control Language="C#" AutoEventWireup="true" CodeFile="View.ascx.cs" Inherits="DesktopModules_Mineros_ActionsArgosModule_View" %>

<div class="container" ng-app="itemApp<%=ModuleId%>" ng-controller="ActionsArgosModuleController">
   <h4 class="align-content-center border-color-corn d-flex librefranklin-blod mb-0 shared_actions_title"><span class="bg-corn librefranklin-blod p-2 text-white">Comportamiento de acciones</span></h4>
    <div id="box-graf-section-actions-3" class="bg-white d-block border-color-jungle-green mt-0" data-aos="flip-up"></div>
</div>
<script>
    var data = {};
    data.moduleId = <%=ModuleId%>;
    data.module_resources = '<%=Resources%>';
    data.portalId = <%=PortalId%>;
    data.common_resources = '<%=CommonResources%>';
    data.culture = '<%=System.Threading.Thread.CurrentThread.CurrentCulture.Name %>';
    var app = angularInit(data);
</script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="/DesktopModules/Mineros/scripts/helpers/helpers.js"></script>
<script src="/DesktopModules/Mineros/ActionsArgosModule/js/ActionsArgosModule.controller.js"></script>
<script src="/DesktopModules/Mineros/ActionsArgosModule/js/ActionsArgosModule.service.js"></script>


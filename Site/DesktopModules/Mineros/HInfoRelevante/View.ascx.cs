﻿/*
' Copyright (c) 2018  Mineros.co
'  All rights reserved.
' 
' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
' TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
' THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
' CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
' DEALINGS IN THE SOFTWARE.
' 
*/

using System;
using DotNetNuke.Security;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Localization;
using System.Collections.Generic;
using Mineros.Data.Models;
using System.Linq;
using Mineros.Data;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Collections;
using DotNetNuke.Entities.Tabs;
using DotNetNuke.Common;
using System.IO;
using DotNetNuke.Services.FileSystem;
using System.Security.AccessControl;
using System.Net;
using System.Web.UI.HtmlControls;

namespace Mineros.HInfoRelevanteHInfoRelevante
{
    /// -----------------------------------------------------------------------------
    /// <summary>
    /// The View class displays the content
    /// 
    /// Typically your view control would be used to display content or functionality in your module.
    /// 
    /// View may be the only control you have in your project depending on the complexity of your module
    /// 
    /// Because the control inherits from HInfoRelevanteModuleBase you have access to any custom properties
    /// defined there, as well as properties from DNN such as PortalId, ModuleId, TabId, UserId and many more.
    /// 
    /// </summary>
    /// -----------------------------------------------------------------------------
    public partial class View : HInfoRelevanteModuleBase, IActionable
    {
        public List<EasyDnnNewsApp> Articles
        {
            get
            {
                return ViewState["Articles"] as List<EasyDnnNewsApp>;
            }
            set { ViewState["Articles"] = value; }
        }

        private string CultureCountry
        {
            get
            {
                CultureInfo culture2 = CultureInfo.CurrentCulture;
                return culture2.Name ?? "es-ES";
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                EasyDnnNewsData easyDnnNewsData = new EasyDnnNewsData();

                lblurl.Text = lnkurl_Click();

                int countItems = 0;

                try
                {
                    var Categories = easyDnnNewsData.GetAllCategoriesNews(CultureCountry);
                    //lnkurl.PostBackUrl = ResolveUrl(string.Format("~/DesktopModules/EasyDNNNews/DocumentDownload.ashx?portalid={0}&moduleid={1}&articleid={2}&documentid={3}", PortalId, document.ModuleId, document.ArticleID, document.DocEntryID));

                    if (Settings["Category"] == null)
                    {
                        Settings["Category"] = "Info Relevante";
                    }

                    var parentCategory = Categories.FirstOrDefault(s => s.CategoryName.ToLower() == Settings["Category"].ToString().ToLower());
                    if (parentCategory != null)
                    {
                        Articles = easyDnnNewsData.GetArticles(parentCategory.CategoryID, null, 0, 0, ref countItems, CultureCountry);

                        var categoriesItems = (from ca in Articles.Select(s => s.CategoryID).Distinct()
                                               select new EasyDnnNewsCategories() { CategoryID = ca, CategoryName = Categories.First(s => s.CategoryID == ca).CategoryName }).Distinct().OrderByDescending(s => s.CategoryName).ToList();

                        ///Panel Home
                        ///
                        var lastCategory = categoriesItems.OrderByDescending(s => s.CategoryName).FirstOrDefault();
                        if (lastCategory != null)
                        {
                            var articlesSideBar = Articles
                                .Where(s => s.CategoryID == lastCategory.CategoryID && !String.IsNullOrEmpty(s.Title))
                                .OrderByDescending(p => p.PublishDate).Take(7)
                                .ToList();

                            foreach (var rangeArticle in articlesSideBar)
                            {
                                rangeArticle.Summary = rangeArticle.Summary ?? string.Empty;
                                if (rangeArticle.Summary != null && rangeArticle.Summary.Length > 150)
                                {
                                    rangeArticle.Summary = rangeArticle.Summary.Substring(0, 150);
                                }
                            }

                            rptItemsHome.DataSource = articlesSideBar;
                            rptItemsHome.DataBind();
                        }
                    }
                }
                catch (Exception exc) //Module failed to load
                {
                    Exceptions.ProcessModuleLoadException(this, exc);
                }
            }

        }

        public ModuleActionCollection ModuleActions
        {
            get
            {
                var actions = new ModuleActionCollection
                    {
                        {
                            GetNextActionID(), Localization.GetString("EditModule", LocalResourceFile), "", "", "",
                            EditUrl(), false, SecurityAccessLevel.Edit, true, false
                        }
                    };
                return actions;
            }
        }

        protected void rptItemsHome_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            EasyDnnNewsData data = new EasyDnnNewsData();

            RepeaterItem rptItemsHome = e.Item as RepeaterItem;
            Literal lnkSideDocument = rptItemsHome.FindControl("lnkSideDocument") as Literal;
            LinkButton lnkSideDownload = rptItemsHome.FindControl("lnkSideDownload") as LinkButton;

            var article = (EasyDnnNewsApp)e.Item.DataItem;

            if (lnkSideDocument != null && lnkSideDownload != null)
            {
                var documents = data.GetEasyDnnNewsDocuments(article.ArticleID, CultureCountry);

                if (documents != null && documents.Count > 0)
                {
                    var document = documents.First();

                    if (!String.IsNullOrEmpty(document.UrlView))
                    {
                        lnkSideDocument.Text = "<a class='icon icon-view' href=' " + document.UrlView + "' target='_blank' >" + "</a>";
                        lnkSideDownload.PostBackUrl = ResolveUrl(string.Format("~/DesktopModules/EasyDNNNews/DocumentDownload.ashx?portalid={0}&moduleid={1}&articleid={2}&documentid={3}", PortalId, document.ModuleId, document.ArticleID, document.DocEntryID));
                    }
                }
            }
        }

        protected string lnkurl_Click()
        {
            TabController objTab = new TabController();

            TabInfo tabInfo = new TabInfo();
            var locale = LocaleController.Instance.GetCurrentLocale(PortalId);
            tabInfo = objTab.GetTabByName(LocalizeString("infoRelevante.Text"), PortalId);
            var tabCulture = objTab.GetTabByCulture(tabInfo.TabID, PortalId, locale);
            return Globals.NavigateURL(tabCulture.TabID);
        }

        public string FormatDate(string DatePublish)
        {
            return "Hola";
        }
    }
}
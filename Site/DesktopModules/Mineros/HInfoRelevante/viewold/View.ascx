<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="View.ascx.cs" Inherits="Mineros.HInfoRelevanteHInfoRelevante.View" %>

<script>
    $(document).ready(function () {
        $('.urlMoreInfoHRelevant').attr("href", $('.urlmoreHRelevant').text());
    });
</script>

<asp:UpdatePanel runat="server">
    <ContentTemplate>
        <div class="box-text-icon-section-2 bg-corn text-white d-flex justify-content-between align-items-center" data-aos="fade-right">
            <h3 class="tittle-section-2-icon librefranklin-semibold text-white mb-0"><%=LocalizeString("infoRelevante.Text") %></h3>
            <a id="lnkurl" class="urlMoreInfoHRelevant more center" href="">
                <img class="w-100" srcset="/DesktopModules/Mineros/CashCostModule/assets/icons/right-row.png" src="/DesktopModules/Mineros/CashCostModule/assets/svg/right-row.svg">
            </a>
            <asp:Label ID="lblurl" CssClass="urlmoreHRelevant d-none" runat="server" Text=""></asp:Label>

        </div>
        <div class="box-text-list bg-alto position-relative">
            <ul class="list h-100 d-flex justify-content-around flex-column">

                <asp:Repeater ID="rptItemsHome" runat="server" OnItemDataBound="rptItemsHome_ItemDataBound">
                    <ItemTemplate>
                        <li class="color-outer-space librefranklin-regular position-relative" data-aos="flip-left"><a href="javascript:void(0);" class="text-decoration-none color-outer-space librefranklin-regular text-link-post"><%#Eval("Title")%></a></li>
                    </ItemTemplate>
                </asp:Repeater>
            </ul>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>




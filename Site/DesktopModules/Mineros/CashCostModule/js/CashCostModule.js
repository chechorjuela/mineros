﻿$(document).ready(function () {
    const urlTotalGraphics = "/Index/GetTotalGraphics?name=";
    var search_accion = "MINEROS";
    var mounth_array = [];
    var actions_array = [];
    var location_module = window.location.pathname.replace(/\//g, "");
    var string_price = "Precio";
    if (location_module === "en") {
        string_price = "Price";
    }
    $.ajax({
        url: 'https://test.mineros.com.co/DesktopModules/services/API' + urlTotalGraphics + search_accion,
        method: "GET",
        dataType: "JSON",
        data: "",
        success: function (resp) {
            if (resp.Header.Code === 200) {
                if (resp.Data.length > 0) {
                    resp.Data.forEach(function (element) {
                        var mounth_format = formatDateGraphics(element.Fecha);
                        mounth_array.push(mounth_format);
                        actions_array.push(element.Action);
                    });
                    build_graphics();
                }

            }
        }
    });


    function build_graphics() {
   
        var pest = document.getElementById('box-graf-section-3');
        pest.classList.add('d-block');
        pest.style.width = '100%';
        pest.style.height = '520px';
        var myChartGraphics = echarts.init(document.getElementById('box-graf-section-3'));
        var colors = ['#676767', '#000', '#675bba'];
        var optionGraphics = {
            media: [ // each rule of media query is defined here
                {
                    query: {
                        minWidth: 551,
                    },   // write rule here
                    option: {       // write options accordingly
                        dataZoom: [
                            {
                                type: 'slider',
                                xAxisIndex: [0],
                                realtime: false,
                                filterMode: 'weakFilter',
                                start:95,
                                end: 100,
                                bottom: 8,
                                height: 20,
                                handleIcon: 'M10.7,11.9H9.3c-4.9,0.3-8.8,4.4-8.8,9.4c0,5,3.9,9.1,8.8,9.4h1.3c4.9-0.3,8.8-4.4,8.8-9.4C19.5,16.3,15.6,12.2,10.7,11.9z M13.3,24.4H6.7V23h6.6V24.4z M13.3,19.6H6.7v-1.4h6.6V19.6z',
                                handleSize: '120%',
                                showDetail: false
                            },
                        ],
                        legend: {
                            left: 'right',
                            itemwidth: 46,
                            itemHeight: 7
                        },
                        xAxis: {
                            type: 'category',
                            boundaryGap: false,
                            maxInterval: 100,
                            splitNumber: 100,
                            data: mounth_array,
                            min: mounth_array[0],
                            axisLabel: {
                                show: 'true',
                                textStyle: {
                                    color: colors[0],
                                    fontSize: 10.6,
                                    fontFamily: 'LibreFranklin',
                                },
                                verticalAlign: 'center',
                                padding: [10, 0, 0, 0]
                            },
                        },
                        yAxis: {
                            type: 'value',
                            name: string_price + ' (COP $)',
                            nameLocation: 'middle',
                            nameRotate: 270,
                            nameTextStyle: {
                                color: colors[0],
                                fontFamily: 'LibreFranklin',
                                fontSize: 14,
                                align: 'center',
                                verticalAlign: 'middle',
                                lineHeight: 19.2,
                                padding: [0, 0, 15, 0]
                            },
                            position: 'right',
                            maxInterval: actions_array[actions_array.length],
                            splitNumber: 10,
                            boundaryGap: false,
                            min: actions_array[0],
                            max: Math.max(...actions_array),
                            data: actions_array,
                            axisLine: {
                                show: false,
                            },
                            axisLabel: {
                                show: 'true',
                                textStyle: {
                                    color: colors[0],
                                    fontSize: 8.6,
                                    fontFamily: 'LibreFranklin',
                                },
                                formatter: '{value}',
                                verticalAlign: 'bottom'
                            },
                            splitLine: {
                                interval: 150,
                                lineStyle: {
                                    color: '#c2c2c2'
                                }
                            },
                        },
                        series: [{
                            name: ' ',
                            data: actions_array,
                            type: 'line',
                            smooth: true,
                            symbol: 'none',
                        }],
                        animationEasing: 'elasticOut',
                        color: '#daa900',
                    }
                },
                {
                    query: {
                        maxWidth: 550
                    },   // write rule here
                    option: {       // write options accordingly
                        dataZoom: [
                            {
                                type: 'slider',
                                xAxisIndex: [0],
                                realtime: false,
                                filterMode: 'weakFilter',
                                start:95,
                                end: 100,
                                bottom: 8,
                                height: 20,
                                handleIcon: 'M10.7,11.9H9.3c-4.9,0.3-8.8,4.4-8.8,9.4c0,5,3.9,9.1,8.8,9.4h1.3c4.9-0.3,8.8-4.4,8.8-9.4C19.5,16.3,15.6,12.2,10.7,11.9z M13.3,24.4H6.7V23h6.6V24.4z M13.3,19.6H6.7v-1.4h6.6V19.6z',
                                handleSize: '100%',
                                showDetail: false
                            },
                        ],
                        legend: {
                            left: 'right',
                            itemwidth: 46,
                            itemHeight: 7
                        },
                        xAxis: {
                            type: 'category',
                            boundaryGap: false,
                            maxInterval: 100,
                            splitNumber: 100,
                            data: mounth_array,
                            min: mounth_array[0],
                            axisLabel: {
                                show: 'true',
                                textStyle: {
                                    color: colors[0],
                                    fontSize: 10.6,
                                    fontFamily: 'LibreFranklin',
                                },
                                verticalAlign: 'center',
                                padding: [10, 0, 0, 0]
                            },
                        },
                        yAxis: [{
                            type: 'value',
                            position: 'left',
                            maxInterval: actions_array[actions_array.length],
                            splitNumber: 10,
                            boundaryGap: false,
                            min: actions_array[0],
                            max: Math.max(...actions_array),
                            data: actions_array,
                            offset:-10,
                            axisLine: {
                                show: false,
                            },
                            splitLine: {
                                interval: 150,
                                lineStyle: {
                                    color: '#c2c2c2'
                                }
                            },
                            axisLabel: {
                                show: 'true',
                                textStyle: {
                                    color: colors[0],
                                    fontSize: 8.6,
                                    fontFamily: 'LibreFranklin',
                                },
                                formatter: '{value}',
                                verticalAlign: 'bottom',
                            },
                        },
                        {
                            type: 'value',
                            maxInterval: actions_array[actions_array.length],
                            splitNumber: 10,
                            boundaryGap: false,
                            min: actions_array[0],
                            max: Math.max(...actions_array),
                            data: actions_array,
                            offset:-10,
                            position: 'right',
                            name: string_price + ' (COP $)',
                            nameLocation: 'middle',
                            nameRotate: 270,
                            nameTextStyle: {
                                color: colors[0],
                                fontFamily: 'LibreFranklin',
                                fontSize: 14,
                                align: 'center',
                                verticalAlign: 'middle',
                                lineHeight: 19.2,
                                padding: [0, 0, 0, 0]
                            },
                            axisLine: {
                                show: false,
                            },
                            axisLabel: {
                                show: false,
                                textStyle: {
                                    color: colors[0],
                                    fontSize: 8.6,
                                    fontFamily: 'LibreFranklin',
                                },
                                formatter: '{value}',
                                verticalAlign: 'bottom',
                            },
                            splitLine:{
                                show: false,
                                interval: 150,
                                lineStyle: {
                                    color: '#c2c2c2'
                                }
                            },
                            minorSplitLine:{
                                show: false,
                            },
                            minorTick:{
                                show: false,
                            },
                            axisTick:{
                                show: false,
                            }
                        }],
                        series: [{
                            name: ' ',
                            data: actions_array,
                            type: 'line',
                            smooth: true,
                            symbol: 'none',
                        }],
                        animationEasing: 'elasticOut',
                        color: '#daa900',
                    }
                },
            ],

        };
        if (optionGraphics && typeof optionGraphics === "object") {
            myChartGraphics.setOption(optionGraphics);
        }
    }
})
<%@ Control Language="c#" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>
<%@ Register TagPrefix="dnn" TagName="USER" Src="~/Admin/Skins/user.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGIN" Src="~/Admin/Skins/Login.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGO" Src="~/Admin/Skins/Logo.ascx" %>
<%@ Register TagPrefix="dnn" TagName="STYLES" Src="~/Admin/Skins/Styles.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LINKTOMOBILE" Src="~/Admin/Skins/LinkToMobileSite.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LANGUAGE" Src="~/Admin/Skins/Language.ascx" %>
<%@ Register TagPrefix="dnn" TagName="META" Src="~/Admin/Skins/Meta.ascx" %>
<%@ Register TagPrefix="dnn" TagName="jQuery" Src="~/Admin/Skins/jQuery.ascx" %>
<%@ Register TagPrefix="dnn" TagName="MENU" Src="~/DesktopModules/DDRMenu/Menu.ascx" %>
<dnn:META ID="META1" runat="server" Name="viewport" Content="width=device-width,initial-scale=1, maximum-scale=1" />
<dnn:META runat="server" Name="description" Content="Mineros" />
<!-- Css Includes -->
<dnn:DnnCssInclude runat="server" ID="cssBootstrap" FilePath="css/bootstrap.min.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssAos" FilePath="css/aos.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssHover" FilePath="css/hover-min.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssFonts" FilePath="css/fonts.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssColors" FilePath="css/colors.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssClassFonts" FilePath="css/class-fonts.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssStyles" FilePath="css/styles.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssStylesHome" FilePath="css/style-home.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssFontawesome" FilePath="css/fonts/fontawesome/css/all.css" PathNameAlias="SkinPath" />
<dnn:DnnCssInclude runat="server" ID="cssLighSlider" FilePath="css/lightSilder.css" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" FilePath="~/DesktopModules/Mineros/scripts/app.js" Priority="10" />
<dnn:DnnJsInclude runat="server" FilePath="~/DesktopModules/Mineros/scripts/echarts.common.min.js" Priority="10" />
<dnn:DnnCssInclude runat="server" FilePath="~/Resources/Libraries/kendo-ui-angular/css/kendo.common-material.min.css" />
<dnn:DnnCssInclude runat="server" FilePath="~/Resources/Libraries/kendo-ui-angular/css/kendo.material.min.css" />
<dnn:DnnJsInclude runat="server" FilePath="~/Resources/Libraries/AngujarJS/01_04_05/angular.min.js" Priority="10" />
<dnn:DnnJsInclude runat="server" FilePath="~/Resources/Libraries/AngujarJS/01_04_05/angular-sanitize.min.js" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
<dnn:DnnJsInclude runat="server" FilePath="https://code.angularjs.org/1.6.9/angular-animate.min.js" Priority="10" />
<section class="overflowX-hidden">
    <div id="hed-top-green" class="bg-mineral-green d-flex">
                
        <div id="bar-lang" class="nav justify-content-around" data-aos="fade-left">
            <dnn:LANGUAGE runat="server" ID="dnnLANGUAGE" ShowLinks="True" ShowMenu="False" ItemTemplate=''/>
        </div>
        <ul id="hed-top-band" class="nav justify-content-end" data-aos="fade-left">
            <li class="nav-item">
                <a class="nav-link" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/argentina" target="_blank">
                    <span class="box-img-loc hvr-icon-bob"><img src="/Portals/0/skins/mineros/images/2x/argentina.png" srcset="/Portals/0/skins/mineros/images/svg/argentina.svg" alt="Bandera de Argentina" class="hvr-icon" title="Argentina"></span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/Chile" target="_blank">
                    <span class="box-img-loc hvr-icon-bob"><img src="/portals/0/ban-4-1.png" srcset="" class="hvr-icon" alt="Bandera de Chile" title="Chile"></span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/Colombia" target="_blank">
                    <span class="box-img-loc hvr-icon-bob"><img src="/Portals/0/skins/mineros/images/2x/colombia.png" srcset="/Portals/0/skins/mineros/images/svg/colombia.svg" alt="Bandera de Colombia" class="hvr-icon" title="Colombia"></span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="http://hemco.com.ni/" target="_blank">
                    <span class="box-img-loc hvr-icon-bob"><img src="/Portals/0/skins/mineros/images/2x/nicaragua.png" srcset="/Portals/0/skins/mineros/images/svg/nicaragua.svg" alt="Bandera de Nicaragua" class="hvr-icon" title="Nicaragua"></span>
                </a>
            </li>
        </ul>
    </div>
    <header id="masthead" class="site-header js" role="banner" data-aos="fade-down">
        <div class="navigation-top bg-menu-trans">
            <div class="wrap d-flex  justify-content-between">
                <div id="logo-header" class="logo-header text-center position-relative">
                    <dnn:LOGO runat="server" ID="dnnLOGO" />
                    <div class="d-flex justify-content-center align-items-center line-logo">
                        <span class="d-flex"></span>
                    </div>
                </div>
                <div class="d-flex box-menu">
                    <dnn:MENU ID="MENU" MenuStyle="Menus/MainMenu" runat="server" NodeSelector="*" />
                </div>
            </div>
            <div class="d-flex mr-auto flex-wrap">
                <a class="img-margin" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/argentina" target="_blank">
                    <span class="box-img-loc hvr-icon-bob">
                        <img src="/Portals/0/skins/mineros/images/Flag_of_Argentina.png" srcset="/Portals/0/skins/mineros/images/Flag_of_Argentina.svg" alt="Bandera De Argentina" class="icon-flag hvr-icon" title="Argentina">
                    </span>
                </a>
                <a class="img-margin" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/Chile" target="_blank">
                    <span class="box-img-loc hvr-icon-bob">
                        <img src="/Portals/0/skins/mineros/images/2x/ban-4-1.png" srcset="/Portals/0/skins/mineros/images/SVG/ban-4-1.svg" alt="Bandera de Chile" class="icon-flag hvr-icon" title="Chile">
                    </span>
                </a>
                <a class="img-margin" href="//<%= PortalSettings.Current.PrimaryAlias.HTTPAlias %>/Operaciones-y-proyectos/Colombia" target="_blank">
                    <span class="box-img-loc hvr-icon-bob">
                        <img src="/Portals/0/skins/mineros/images/Flag-of-colombia.png" srcset="/Portals/0/skins/mineros/images/flag_of_colombia.svg" alt="Bandera de Colombia" class="icon-flag hvr-icon" title="Colombia">
                    </span>
                </a>
                <a class="img-margin" href="http://hemco.com.ni/" target="_blank">
                    <span class="box-img-loc hvr-icon-bob">
                        <img src="/Portals/0/skins/mineros/images/Flag_of_Nicaragua.png" srcset="/Portals/0/skins/mineros/images/Flag_of_Nicaragua.svg" alt="Bandera de nicaragua" class="icon-flag hvr-icon" title="Nicargua">
                    </span>
                </a>
                <div class="box-icon-rrss w-100 d-flex align-items-center">
                    <a class="link-rrss text-white aos-init aos-animate" href="https://www.facebook.com/MinerosSA" data-aos="flip-down" target="_blank"><span><i class="text-white fab fa-facebook-f"></i></span></a>
                    <a class="link-rrss text-white aos-init aos-animate" href="https://twitter.com/mineros_sa" data-aos="flip-down" target="_blank"><span><i class="text-white fab fa-twitter"></i></span></a>
                    <a class="link-rrss text-white aos-init aos-animate" href="https://www.youtube.com/user/MINEROSSA" data-aos="flip-down" target="_blank"><span><i class="text-white fab fa-youtube"></i></span></a>
                    <a class="link-rrss text-white aos-init aos-animate" href="http://instagram.com/mineros_sa" data-aos="flip-down" target="_blank"><span><i class="text-white fab fa-instagram"></i></span></a>
                    <a class="link-rrss text-white aos-init aos-animate" href="https://co.linkedin.com/company/mineros-s.a." data-aos="flip-down" target="_blank"><span><i class="text-white fab fa-linkedin-in"></i></span></a>
                </div>
            </div>
        </div>
    </header>
    <main>
        <section id="section-1" class="bg-fern">
            <div class="container-fluid p-0">
                <div id="ContentPane" runat="server"></div>
            </div>
        </section>
        <section id="section-2" class="">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-6">
                        <div id="cab-section-2" class="cab-rigt-sections d-flex w-100 position-relative border-color-corn" data-aos="fade-up">
                            <div id="ResultadosFinancierosIco" class="icon-cab-sections bg-jungle-green d-flex justify-content-center align-items-center" runat="server"></div>
                            <div id="ResultadosFinancierosTitulo" class="box-cab-tittle-sections bg-corn h-100 d-flex justify-content-center align-items-center" runat="server"></div>
                        </div>
                        <div class="box-section-2 position-relative d-flex w-100 mt-0" data-aos="fade-right">
                            <div class="box-img-section-2 overflow-hidden position-relative">
                                <div id="ResultadosFinancierosBackground" runat="server"></div>
                                <div class="box-text-section-2 position-absolute bg-duocolor-corn-green">
                                    <div id="ResultadosFinancierosTitulo2" runat="server"></div>
                                    <div id="ResultadosFinancierosBoton" runat="server"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--./col-12-->
                    <div class="col-12 col-lg-6 pt-3 pt-lg-0 pb-3 pb-lg-0">
                        <div class="box-section-2 position-relative d-flex w-100 mt-0" data-aos="fade-right">
                            <div id="InformacionRelevanteHome" class="section-text-lateral-text d-flex flex-column" data-aos="fade-left" runat="server">
                            </div>
                        </div>
                    </div>
                    <!--./col-12-->
                </div>
                <!--./row-->
            </div>
        </section>
        <section id="section-3">
            <div class="container">
                <div class="row">
                    <div class="col-img-section-3 d-flex flex-column overflow-hidden" data-aos="fade-up-left">
                        <div class="box-img-col-section-3 overflow-hidden">
                            <div id="FundacionMinerosBackground" runat="server"></div>
                        </div>
                        <div class="box-tittle-1-col-section-3 d-flex align-items-end">
                            <span class="sub-box-tittle-1-section-3 bg-mineral-green d-flex align-items-center" data-aos="flip-up">
                                <div id="FundacionMinerosTitulo1" runat="server"></div>
                            </span>
                            <span class="sub-box-line-bottom bg-corn col">
                            </span>
                        </div>
                        <div class="box-tittle-2-col-section-3 d-flex bg-fern justify-content-between align-items-center">
                            <div id="FundacionMinerosTitulo2" runat="server"></div>
                            <div id="FundacionMinerosBoton" runat="server"></div>
                        </div>
                    </div>
                    <div class="col-graf-section-3 d-flex flex-column overflow-hidden col" data-aos="fade-up-right">
                        <div id="section-4" class="">
                            <div id="tabgrafica2" class="col-graf-section-4 d-flex flex-column overflow-hidden" data-aos="fade-up" runat="server"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="section-4" class="bg-duocolor-green-white">
            <div class="container">
                <div class="row">
                    <div id="section-3" class="col-graf-section-3 col-12 pb-4">
                        <div id="cab-section-3" class="cab-rigt-sections d-flex w-50 position-relative border-color-corn">
                            <div id="CashCostIco" class="icon-cab-sections bg-jungle-green d-flex justify-content-center align-items-center" runat="server"></div>
                            <div id="CashCostTitle" class="box-cab-tittle-sections bg-corn h-100 d-flex justify-content-center align-items-center" runat="server"></div>
                        </div>
                    </div>
                    <div id="section-3" class="col-graf-section-3 col-md-12 col-lg-4 mt-0">
                        <div id="boxgrafsection3" class="" data-aos="flip-up" runat="server"></div>
                    </div>
                    <div class="col-box-section-4 col-lg-8 col-md-12 d-flex flex-column overflow-hidden col mt-0" data-aos="fade-down">
                        <div id="box-sections" class="d-flex flex-sm-wrap flex-wrap justify-content-between">
                            <div id="valgrafic1" class="" data-aos="flip-left" runat="server"></div>
                            <div id="valgrafic2" class="" data-aos="flip-left" runat="server"></div>
                            <div id="valgrafic3" class="" data-aos="flip-left" runat="server"></div>
                            <div id="valgrafic4" class="" data-aos="flip-left" runat="server"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="section-5" class="bg-alto">
            <div class="container">
                <div class="row justify-content-between">
                    <div id="col-img-section-5" class="d-flex flex-column" data-aos="fade-right">
                        <div id="cab-section-5" class="cab-rigt-sections d-flex w-100 position-relative border-color-corn" data-aos="flip-left">
                            <div id="BoletinesDePrensaIco" class="icon-cab-sections bg-jungle-green d-flex justify-content-center align-items-center" runat="server"></div>
                            <div id="BoletinesDePrensaTitle" class="box-cab-tittle-sections bg-corn h-100 d-flex justify-content-center align-items-center" runat="server"></div>
                        </div>
                        <div class="slider-img-section-5 bg-white" data-aos="flip-left">
                            <div id="panneBolentin" runat="server"></div>
                        </div>
                        <div id="panneBolentin3" runat="server"></div>
                    </div>
                    <div id="col-enlaces-section-5" class="d-flex flex-column" data-aos="fade-left">
                        <div id="EnlacesDeInteresTitle" class="cab-enlace bg-duo-horizonta-green-trans" data-aos="flip-right" runat="server"></div>
                        <div id="EnlacesDeInteresContenido" class="box-items" runat="server"></div>
                    </div>
                </div>
            </div>
        </section>
        <section id="section-6" style="display: none;">
            <div class="container">
                <div class="row justify-content-center">
                    <div id="FilialesGrupoMinerosTitle" class="section-6-tittle-box d-block w-100 text-center" data-aos="fade-up" runat="server"></div>
                    <div class="section-6-content-box d-flex flex-wrap w-100 justify-content-between" data-aos="fade-up">
                        <div class="col-img-gall" data-aos="flip-right">
                            <div id="FilialesGrupoMinerosIMG1" class="box-img" runat="server"></div>
                        </div>
                        <div class="col-img-gall" data-aos="flip-right">
                            <div id="FilialesGrupoMinerosIMG2" class="box-img" runat=server></div>
                        </div>
                        <div class="col-img-gall" data-aos="flip-right">
                            <div id="FilialesGrupoMinerosIMG3" class="box-img" runat="server"></div>
                        </div>
                        <div class="col-img-gall" data-aos="flip-right">
                            <div id="FilialesGrupoMinerosIMG4" class="box-img" runat="server"></div>
                        </div>
                        <div class="col-img-gall" data-aos="flip-right">
                            <div id="FilialesGrupoMinerosIMG5" class="box-img" runat="server"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <footer class="bg-mineral-green" data-aos="fade-down">
    	<div id="panneFooter" runat="server"></div>
    </footer>
</section>

<div id="suscriberImage" runat="server"></div>



<!-- Modal -->
<div class="modal fade page_construct" id="inconstruction" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="inconstructionLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="inconstructionLabel"><span>Pop up para</span> sección en inglés</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="card-text mb-0">En construcción....</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary px-4 rounded-0" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<!-- JS Includes -->
<dnn:DnnJsInclude runat="server" ID="jsPopper" FilePath="js/popper.min.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsBootstrap" FilePath="js/bootstrap.min.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsAos" FilePath="js/aos.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsEcharts" FilePath="libs/incubator-echarts-4.2.1/js/echarts.min.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsSlider" FilePath="js/lightslider.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsMain" FilePath="js/main.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsAppHomee" FilePath="js/app-home.js" PathNameAlias="SkinPath" />
<dnn:DnnJsInclude runat="server" ID="jsFiliales" FilePath="js/filiales_slider.js" PathNameAlias="SkinPath" />
<script>
/*function chargeGraf() {
    var pest2 = document.getElementById('action-social');
    pest2.classList.add('d-block');
    pest2.style.width = '100%';
    pest2.style.height = '100%';
    var donacion = echarts.init(document.getElementById('action-social'));
    var colors = ['#676767', '#000', '#675bba'];
    var option = {
        media: [ // each rule of media query is defined here
            {
                query: {
                    minWidth: 551
                }, // write rule here
                option: {
                    dataZoom: [{
                        type: 'inside',
                        xAxisIndex: [0],
                        realtime: false,
                    }],
                    xAxis: {
                        type: 'category',
                        name: 'Cifras en millones de dolares',
                        nameLocation: 'center',
                        nameTextStyle: {
                            color: colors[0],
                            fontFamily: 'LibreFranklin',
                            fontSize: 14,
                            align: 'center',
                            verticalAlign: 'bottom',
                            lineHeight: 19.2,
                            padding: [10, 0, 0, 0],
                            fontWeight: 700
                        },
                        maxInterval: 1,
                        splitNumber: 1,
                        data: ['2016', '2017', '2018'],
                        min: '2015',
                        axisLine: {
                            show: false,
                        },
                        axisLabel: {
                            show: 'true',
                            textStyle: {
                                color: colors[0],
                                fontSize: 10.6,
                                fontFamily: 'LibreFranklin',
                            },
                            verticalAlign: 'center',
                            padding: [10, 0, 0, 0]
                        },
                    },
                    yAxis: {
                        type: 'value',
                        position: 'left',
                        maxInterval: 800,
                        splitNumber: 1,
                        boundaryGap: false,
                        data: [0, 675, 1350, 2025, 2700],
                        offset: -30,
                        axisLine: {
                            show: false,
                        },
                        axisLabel: {
                            show: 'true',
                            textStyle: {
                                color: colors[0],
                                fontSize: 10.6,
                                fontFamily: 'LibreFranklin',
                            },
                            formatter: '${value}',
                            verticalAlign: 'bottom',
                            padding: [0, 0, 2, 0]
                        },
                        splitLine: {
                            interval: 1,
                            lineStyle: {
                                color: '#c2c2c2'
                            }
                        },
                    },
                    grid: {
                        right: 0,
                        // bottom: 90
                    },
                    series: [{
                        name: ' ',
                        type: 'bar',
                        smooth: true,
                        symbol: 'none',
                        barWidth: 80,
                        barGap: '-100%',
                        barCategoryGap: '10%',
                        data: [2025, 2600, 2300],

                    }],
                    animationEasing: 'elasticOut',
                    color: '#22a571',
                }
            },
            {
                query: {
                    maxWidth: 550
                }, // write rule here
                option: {
                    dataZoom: [{
                        type: 'inside',
                        xAxisIndex: [0],
                        realtime: false,
                    }],
                    xAxis: {
                        type: 'category',
                        name: 'Cifras en millones de pesos',
                        nameLocation: 'center',
                        nameTextStyle: {
                            color: colors[0],
                            fontFamily: 'LibreFranklin',
                            fontSize: 14,
                            align: 'center',
                            verticalAlign: 'bottom',
                            lineHeight: 19.2,
                            padding: [10, 0, 0, 0],
                            fontWeight: 600
                        },
                        maxInterval: 1,
                        splitNumber: 1,
                        data: ['2014', '2015', '2016'],
                        min: '2014',
                        axisLine: {
                            show: false,
                        },
                        axisLabel: {
                            show: 'true',
                            textStyle: {
                                color: colors[0],
                                fontSize: 10.6,
                                fontFamily: 'LibreFranklin',
                            },
                            verticalAlign: 'center',
                            padding: [10, 0, 0, 0]
                        },
                    },
                    yAxis: {
                        type: 'value',
                        position: 'left',
                        maxInterval: 800,
                        splitNumber: 1,
                        boundaryGap: false,
                        data: [0, 675, 1350, 2025, 2700],
                        offset: -30,
                        axisLine: {
                            show: false,
                        },
                        axisLabel: {
                            show: 'true',
                            textStyle: {
                                color: colors[0],
                                fontSize: 10.6,
                                fontFamily: 'LibreFranklin',
                            },
                            formatter: '${value}',
                            verticalAlign: 'bottom',
                            padding: [0, 0, 2, 0]
                        },
                        splitLine: {
                            interval: 1,
                            lineStyle: {
                                color: '#c2c2c2'
                            }
                        },
                    },
                    grid: {
                        right: 0,
                        // bottom: 90
                    },
                    series: [{
                        name: ' ',
                        type: 'bar',
                        smooth: true,
                        symbol: 'none',
                        barWidth: 40,
                        barGap: '-100%',
                        barCategoryGap: '10%',
                        data: [2025, 2600, 2300],

                    }],
                    animationEasing: 'elasticOut',
                    color: '#22a571',
                }
            },
            {
                query: {
                    maxWidth: 425
                }, // write rule here
                option: {
                    dataZoom: [{
                        type: 'inside',
                        xAxisIndex: [0],
                        realtime: false,
                    }],
                    xAxis: {
                        type: 'category',
                        name: 'Cifras en millones de pesos',
                        nameLocation: 'center',
                        nameTextStyle: {
                            color: colors[0],
                            fontFamily: 'LibreFranklin',
                            fontSize: 14,
                            align: 'center',
                            verticalAlign: 'bottom',
                            lineHeight: 19.2,
                            padding: [10, 0, 0, 0],
                            fontWeight: 600
                        },
                        maxInterval: 1,
                        splitNumber: 1,
                        data: ['2014', '2015', '2016'],
                        min: '2014',
                        axisLine: {
                            show: false,
                        },
                        axisLabel: {
                            show: 'true',
                            textStyle: {
                                color: colors[0],
                                fontSize: 10.6,
                                fontFamily: 'LibreFranklin',
                            },
                            verticalAlign: 'center',
                            padding: [10, 0, 0, 0]
                        },
                    },
                    yAxis: {
                        type: 'value',
                        position: 'left',
                        maxInterval: 800,
                        splitNumber: 1,
                        boundaryGap: false,
                        data: [0, 675, 1350, 2025, 2700],
                        offset: -30,
                        axisLine: {
                            show: false,
                        },
                        axisLabel: {
                            show: 'true',
                            textStyle: {
                                color: colors[0],
                                fontSize: 10.6,
                                fontFamily: 'LibreFranklin',
                            },
                            formatter: '${value}',
                            verticalAlign: 'bottom',
                            padding: [0, 0, 2, 0]
                        },
                        splitLine: {
                            interval: 1,
                            lineStyle: {
                                color: '#c2c2c2'
                            }
                        },
                    },
                    grid: {
                        right: 0,
                        // bottom: 90
                    },
                    series: [{
                        name: ' ',
                        type: 'bar',
                        smooth: true,
                        symbol: 'none',
                        barWidth: 25,
                        barGap: '-100%',
                        barCategoryGap: '10%',
                        data: [2025, 2600, 2300],

                    }],
                    animationEasing: 'elasticOut',
                    color: '#22a571',
                }
            }
        ]
    };
    donacion.setOption(option);
}


function noChargeGraf() {
    var pest2 = document.getElementById('action-social');
    pest2.classList.remove('d-block');
}*/
if ( document.querySelector('.language-object')) {
    document.querySelectorAll('.language-object .Language').forEach(function(a,i){
        a.innerHTML += '<span></span>'
        var languaje_name = a.getAttribute('title');
        jQuery('.Languaje').addClass('nav-item');
        jQuery('.Language a ').addClass('nav-link librefranklin-thin text-white active');
        a.querySelectorAll('a').forEach(function(c){
            c.innerHTML += languaje_name;
            c.innerHTML = c.innerHTML.split('(')[0];
        })
        
    });
}


</script>


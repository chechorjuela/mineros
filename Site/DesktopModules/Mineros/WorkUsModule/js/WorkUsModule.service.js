﻿app.factory('WorkUsModuleService', ['api_url', '$http', function (api_url, $http) {
    var service = {};

    const route_listWorks = "Workus/getListWorks";
    const route_contactWorkUs = "Workus/SendFormEmail";
    const route_count = "Workus/getCountWorksJob";

    service.GetJobsWorksUs = function (page, limit) {

        var promise = $http.get(api_url + route_listWorks + "?page=" + page + "&limit=" + limit);

        promise.catch(function (data) {

        });

        return promise;
    };

    service.GetCountJobs = function () {

        var promise = $http.get(api_url + route_count);
        promise.catch(function (data) {

        });

        return promise;
    }

    service.sendFormContact = function (workUsForm) {
        var data = new FormData();
        data.append('name', workUsForm.name);
        data.append('number_id', workUsForm.number_id);
        data.append('phone', workUsForm.phone);
        data.append('city', workUsForm.city);
        data.append('email', workUsForm.email);
        data.append('title_job', workUsForm.title_job);
        data.append('country_job', workUsForm.country_job);
        data.append('city_job', workUsForm.city_job);
        data.append('enterprise_job', workUsForm.enterprise_job);
        data.append('summary_job', workUsForm.summary_job);
        data.append('file', workUsForm.file);
        data.append('current_city', workUsForm.city);
        data.append('current_country', workUsForm.currenct_country);
        data.append('locationApplicant', workUsForm.locationApplicant);
        data.append('processApplicant', workUsForm.processApplicant);
        data.append('positionApplicant', workUsForm.positionApplicant);

        var promise = $http.post(api_url + route_contactWorkUs, data, {
            headers: { 'Content-Type': undefined }
        });

        promise.catch(function (response) {

        });

        return promise;
    };

    return service;
}]);
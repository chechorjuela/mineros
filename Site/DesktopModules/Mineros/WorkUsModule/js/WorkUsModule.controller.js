app.controller("WorkUsModuleController", function ($scope, module_resources, $q, common_resources, WorkUsModuleService, portal_id, $window, $sce, $timeout) {
    filter = "Asamblea";
    //Variables
    $scope.module_resources = JSON.parse(module_resources);
    $scope.common_resources = JSON.parse(common_resources);
    $scope.firstLoad = false;
    $scope.categories = [];
    $scope.list_news = [];
    $scope.page = 0;
    $scope.listJobs = [];
    $scope.formWorkUs = null;
    $scope.jobSelected = null;
    $scope.paginate = 0;
    $scope.paginate_selected = 0;
    $scope.pages = [];
    $scope.submited = false;
    $scope.submit_error = false;
    $scope.submit_file_error = false;
    $scope.success = false;
    $scope.applicationbutton = "Aplicar Solicitud";
    $scope.city = "Ciudad";
    $scope.country = "Pais";
    $scope.description = "Descripcion";
    $scope.attachfile = "Adjuntar hoja de vida";
    $scope.send = "Enviar";
    $scope.fieldsrequired = "Por favor verifique todos los campos.";
    $scope.acceptterms = "Debes aceptar los terminos.";
    $scope.isRequired = "es requerido";
    $scope.phone = "Telefono";
    $scope.name = "Nombre completo";
    $scope.id = "Identificacion";
    $scope.email = "Correo Electronico";
    $scope.file = "Archivo";
    $scope.sendInformation = "Se ha enviado correctamente el formulario";
    $scope.showTerms = "Ver condiciones";
    $scope.uploadFileBoolean = false;
    $scope.acceptTermsLabel = "Aceptar term&iacute;nos y condiciones. <a href='#' class='color-jungle-green accept_terms'>(" + $scope.showTerms+")</a>.";
    var limit_items = 3;
    $scope.master = {};
    //end variables
    $scope.list_asamblea = [];
    this.$onInit = function () {
         initializerPage();
    }

    function initializerPage() {
        var location_module = window.location.pathname.replace(/\//g, "");
        if (location_module.substring(0, 2) === "en") {
            $scope.applicationbutton = "Apply request";
            $scope.city = "City";
            $scope.country = "Country";
            $scope.description = "Description";
            $scope.attachfile = "Attach CV";
            $scope.send = "Send";
            $scope.acceptterms = "You must accept the terms.";
            $scope.fieldsrequired = "Please check all the fields.";
            $scope.isRequired = "is require";
            $scope.phone = "Phone";
            $scope.name = "Full name";
            $scope.id = "Id";
            $scope.file = "File";
            $scope.email = "E-mail";
            $scope.sendInformation = "The form has been sent correctly";
            $scope.showTerms = "see conditions";
            $scope.acceptTermsLabel = "Accept terms and conditions. <a href='#' class='color-jungle-green accept_terms'>(" + $scope.showTerms + ")</a>.";
        }
        WorkUsModuleService.GetJobsWorksUs($scope.page, limit_items).then((response) => {
            if (response.status == 200) {
                $scope.listJobs = response.data.Data;
            }
        });
        WorkUsModuleService.GetCountJobs().then((response) => {
            if (response.status == 200) {
                $scope.paginate = Math.ceil(response.data.Data / limit_items);
                for (var i = 0; i < $scope.paginate; i++) {
                    $scope.pages.push(i);
                }
            }
        });

    }

    $scope.selectedJob = function (job) {
        $scope.jobSelected = job;
    }

    $scope.formatDatePublish = function (date) {
        var new_date = new Date(date);
        var mounth_name = "";
        switch (new_date.getMonth()) {
            case 0:
                mounth_name = "Ene.";
                break;
            case 1:
                mounth_name = "Feb.";
                break;
            case 2:
                mounth_name = "Mar.";
                break;
            case 3:
                mounth_name = "Abr.";
                break;
            case 4:
                mounth_name = "May.";
                break;
            case 5:
                mounth_name = "Jun.";
                break;
            case 6:
                mounth_name = "Jul.";
                break;
            case 7:
                mounth_name = "Ago.";
                break;
            case 8:
                mounth_name = "Sep.";
                break;
            case 9:
                mounth_name = "Oct.";
                break;
            case 10:
                mounth_name = "Nov.";
                break;
            case 11:
                mounth_name = "Dic.";
                break;
        }
        return new_date.getDate() + " " + mounth_name + " " + new_date.getFullYear();
    }
    $scope.sendForm = function (e, formWorkUs, form_workus) {
        e.preventDefault();
        if (form_workus.$valid) {
            $(".LoaderBalls").fadeIn();
            if (formWorkUs.file != null) {
                $scope.submited = true;
                $scope.submit_file_error = false;
                $scope.submit_error = false;
                var formated_addres = $("#locationContact").val().split(",");
                country = formated_addres[formated_addres.length - 1];
                document.getElementById("country_field").value = country.trim();
                var city = formated_addres[0]
                if (formated_addres.length > 2) {
                    city += "," + formated_addres[1]
                }
                formWorkUs.city = city;
                formWorkUs.currenct_country = country.trim();
                document.getElementById("locationfirst").value = city;
                formWorkUs.easyNewId = $scope.jobSelected.ArticleID;
                formWorkUs.application = $scope.jobSelected.Title;
                formWorkUs.city_job = $scope.jobSelected.CityNews;
                formWorkUs.country_job = $scope.jobSelected.CountryNews;
                formWorkUs.enterprise_job = $scope.jobSelected.EnterpriseNews;
                formWorkUs.title_job = $scope.jobSelected.Title;
                formWorkUs.summary_job = $scope.jobSelected.Summary;
                formWorkUs.locationApplicant = $scope.jobSelected.Locations;
                formWorkUs.processApplicant = $scope.jobSelected.LevelPosition;
                formWorkUs.positionApplicant = $scope.jobSelected.ProccessInterest;
                WorkUsModuleService.sendFormContact(formWorkUs).then((response) => {
                    $scope.success = true;
                    $timeout(function () {
                        $(".LoaderBalls").fadeOut();
                        angular.element('#form-modal').modal('hide');
                        $scope.submited = false;
                        $scope.reset();
                        $scope.jobSelected = null;
                    }, 1500);
                });
            } else {
                $(".LoaderBalls").fadeOut();
                $scope.submit_file_error = true;
            }

        } else {
            $scope.submit_error = true;
        }

    }

    $scope.selectPage = function (e, page) {
        e.preventDefault();
        $scope.page = page;
        $scope.paginate_selected = page;
        WorkUsModuleService.GetJobsWorksUs($scope.page, limit_items).then((response) => {
            if (response.status == 200) {
                $scope.listJobs = response.data.Data;
            }
        });
    }

    $scope.SkipValidation = function (value) {
        return $sce.trustAsHtml(value);
    };

    $scope.reset = function () {
        $scope.formWorkUs = angular.copy($scope.master);
        $scope.success = false;
    };
});

app.filter('trusted', ['$sce', function ($sce) {
    var div = document.createElement('div');
    return function (text) {
        div.innerHTML = text;
        return $sce.trustAsHtml(div.textContent);
    };
}]);
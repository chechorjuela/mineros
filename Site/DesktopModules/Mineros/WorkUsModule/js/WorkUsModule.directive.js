﻿app.directive("fileModel", ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function () {
                scope.$apply(function () {
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);

app.directive("validFile",  function () {
    return {
        restrict: "A",
        require: '^form',

        link: function (scope, elem, attrs, ctrl) {

            elem.bind("change", function (e) {
                scope.$apply(function () {
                    ctrl.$valid = true;
                    ctrl.$invalid = false;
                });
            });

        }
    };
});
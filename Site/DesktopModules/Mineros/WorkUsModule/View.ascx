﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="View.ascx.cs" Inherits="DesktopModules_Mineros_WorkUsModule_View" %>

<div class="container consolidados" ng-app="itemApp<%=ModuleId%>" ng-controller="WorkUsModuleController">
    <div class="row ng-cloak" ng-cloak>
        <div class="col-xs-12 eds_news_Ozone eds_subCollection_news">
            <div class="card" ng-repeat="listJob in listJobs">
                <div class="card-header d-flex flex-wrap bg-white border-0 baja-resol aos-init aos-animate" data-aos="fade-right">
                    <div class="img-primary-section-card">
                        <div class="media flex">
                            <div class="bg-jungle-green caja-verde"><span class="text-white librefranklin-bold fuente-grande">J</span> </div>
                            <div class="media-body padding-izq ajuste-pad linea-vertical-gris">
                                <h6 class="title mt-2 mb-0 librefranklin-bold color-nevada"><a class="mt-2 mb-0 title librefranklin-bold color-nevada" href="http://mineros.com.co.dnn4less.net/es-es/Trabaje-con-Nosotros/jefe-administrativo-ventas" target="_self">{{listJob.Title}}</a></h6>
                                <h4 class="mt-0 mb-0 librefranklin-bold color-jungle-green">{{listJob.EnterpriseNews}}</h4>
                                <div class="edn_metaDetails">
                                    <img src="/portals/0/skins/mineros/images/reloj.png" class="align-self-center mr-0" alt="...">
                                    <h7 class="date mt-0 librefranklin-regular color-gray">Publicado {{formatDatePublish(listJob.PublishDate)}}</h7>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="loc-primary-section-card borde-izq padding-izq">
                        <div class="media">
                            <img src="/portals/0/skins/mineros/images/posicion-gps.png" class="align-self-center mr-2" alt="...">
                            <div class="media-body">
                                <h6 class="mt-0 mb-0 librefranklin-bold color-nevada" style="float: left;">{{country}}:&nbsp;</h6>
                                <h6 class="mt-0 mb-0 librefranklin-regular color-gray">{{listJob.CountryNews}}</h6>
                                <h6 class="mt-0 mb-0 librefranklin-bold color-nevada" style="float: left;">{{city}}:&nbsp;</h6>
                                <h6 class="mt-0 mb-0 librefranklin-regular color-gray">{{listJob.CityNews}}</h6>
                            </div>
                        </div>
                    </div>
                    <div class="btn-primary-section-card"><a href="#" ng-click="selectedJob(listJob)" class="btn butn librefranklin-regular text-white" data-toggle="modal" data-target="#form-modal" role="btn btn-primary" aria-pressed="true">{{applicationbutton}}</a> </div>
                </div>
                <div class="card-body aos-init aos-animate" data-aos="fade-right">
                    <p class="librefranklin-bold color-nevada mb-0">{{description}}</p>
                    <p class="librefranklin-regular color-gray"></p>
                    <div ng-bind-html="listJob.Summary | trusted"></div>
                </div>
            </div>
            <div class="article_pager">
                <a ng-repeat="page in pages" ng-attr-class="{{page==paginate_selected && 'page active' || 'page' }}" ng-click="selectPage($event,page)" href="">{{page+1}}</a>
            </div>
        </div>
    </div>
    <div class="modal fade" id="form-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-labelledby="exampleModalLabel" aria-hidden="true" aria-modal="true">
        <div class="modal-dialog frame-form modal-dialog-centered" role="document">
            <div class="modal-content border">
                <div class="modal-header bg-alto position-relative">
                    <div class="header-left d-flex">
                        <div class="d-flex align-items-center justify-content-center letter-a bg-jungle-green">
                            <span class="librefranklin-bold text-white">{{jobSelected.Title.charAt(0)}}</span>
                        </div>
                        <p class="librefranklin-bold color-nevada">
                            {{jobSelected.Title}}<br>
                            <span class="color-jungle-green header-mineros">{{jobSelected.EnterpriseNews}}</span>
                        </p>
                    </div>
                    <div class="header-right d-flex">
                        <div class="header-right-img d-flex align-items-center justify-content-center">
                            <img src="/Portals/0/skins/mineros/images/posicion-gps.png" alt="posicion-gps">
                        </div>
                        <div class="header-right-position  color-nevada">
                            <span class="librefranklin-bold">{{country}}:</span><span class="librefranklin-regular"> {{jobSelected.CountryNews}}</span><br>
                            <span class="librefranklin-bold">{{city}}:</span><span class="librefranklin-regular"> {{jobSelected.CityNews}}</span>
                        </div>
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="librefranklin-bold">X</span>
                    </button>
                </div>
                <!--end modal-header-->
                <div class="modal-body">
                    <div class="container-fluid container-body">
                        <div class="row">
                            <div class="col-12">
                                <p class="body-paragraph librefranklin-regular color-nevada"></p>
                                <div ng-bind-html="jobSelected.Summary | trusted"></div>
                            </div>
                        </div>
                        <!--form-->
                        <form></form>
                        <form name="form_workus" novalidate class="form_jobs">
                            <div class="form librefranklin-regular color-very-dark-gray">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group bg-focus">
                                            <label for="nombre" class="control-label sr-only">{{name}}*</label>
                                            <input type="text" name="name" autocomplete="off" ng-model="formWorkUs.name" id="nombre" class="form-control bg-very-light-gray " placeholder="{{name}} *" required="">
                                            <div ng-show="form_workus.$submitted || form_workus.name.$touched">
                                                <span class="error_forms" ng-show="form_workus.name.$error.required ">{{name}} {{isRequired}}.</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group bg-focus">
                                            <label for="mail" class="control-label sr-only">{{email}}</label>
                                            <input type="email" name="email" autocomplete="off" ng-model="formWorkUs.email"  class="form-control bg-very-light-gray " placeholder="{{email}} *" required="">
                                            <div ng-show="form_workus.$submitted || form_workus.email.$touched">
                                                <span class="error_forms" ng-show="form_workus.email.$error.required ">{{email}} {{isRequired}}.</span>
                                            </div>
                                        </div>
                                    </div>
                                     <div class="col-6">
                                        <div class="form-group bg-focus">
                                            <label for="mail" class="control-label sr-only">{{id}}</label>
                                            <input type="text" name="number_id" autocomplete="off" ng-model="formWorkUs.number_id" id="number_id" class="form-control bg-very-light-gray " placeholder="{{id}} *" required="">
                                            <div ng-show="form_workus.$submitted || form_workus.number_id.$touched">
                                                <span class="error_forms" ng-show="form_workus.number_id.$error.required ">{{id}} {{isRequired}}.</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 col-left">
                                        <div class="form-group bg-focus">
                                            <label for="telefono" autocomplete="off" class="control-label sr-only">{{phone}}</label>
                                            <input type="tel" name="phone" ng-model="formWorkUs.phone" id="telefono" ng-pattern="regex" class="form-control bg-very-light-gray" required placeholder="{{phone}}">
                                            <div ng-show="form_workus.$submitted || form_workus.phone.$touched">
                                                <span class="error_forms" ng-show="form_workus.phone.$error.required ">{{phone}} {{isRequired}}.</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 col-right">
                                        <div class="form-group bg-focus">
                                            <label for="ciudad" autocomplete="off" class="control-label sr-only">{{city}}, {{country}}</label>
                                            <input type="text"  name="city" ng-model="formWorkUs.city" id="locationContact" class="form-control bg-very-light-gray" placeholder="{{city}}, {{country}} *" required="">
                                            <div ng-show="form_workus.$submitted || form_workus.city.$touched">
                                                <span class="error_forms" ng-show="form_workus.city.$error.required ">{{city}}, {{country}} {{isRequired}}.</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 ">
                                        <!--adjuntar hoja de vida-->
                                        <div class="form-group curriculum">
                                            <input type="file" name="file" required file-model="formWorkUs.file" style="display: none" name="hojadevida" id="botonFileReal">
                                            <label for="botonFileReal" class="btn btn-secondary color-very-dark-gray bg-light-gray">{{attachfile}}</label>
                                            <label>{{formWorkUs.file.name}}</label>
                                         
                                        </div>
                                        <div ng-show="submit_file_error && formWorkUs.file.name==null">
                                            <span class="error_forms" >{{File}} {{isRequired}}.</span>
                                        </div>
                                    </div>
                                    <!--checkbox -->
                                    <div class="col-12">
                                        <div class="form-group form-check d-flex">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" ng-model="formWorkUs.checkterms" name="checkterms" value="" class="form-check-input" id="terminos" required="" />
                                                    <span class="cr border-color-pumice"><i class="cr-icon fas fa-check"></i></span>
                                                </label>
                                            </div>
                                            <label class="form-check-label librefranklin-regular color-very-dark-gray" for="terminos" > <p  ng-bind-html="acceptTermsLabel"> </p></label>
                                            <div class="col-xs-12">
                                                <div ng-show="form_workus.$submitted || form_workus.checkterms.$touched">
                                                    <span class="error_forms" ng-show="form_workus.checkterms.$error.required "{{acceptterms}}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">

                                            <div class="col-sm-6 ">
                                                <button ng-disabled="submited" ng-click="sendForm($event,formWorkUs,form_workus)" type="submit" class="btn btn-succes bg-jungle-green librefranklin-regular text-white ">{{send}}</button>
                                            </div>
                                            <div class="col-sm-6 " ng-show="submit_error">
                                                <span class="error_forms">{{fieldsrequired}}</span>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="LoaderBalls">
                                                    <div class="LoaderBalls__item"></div>
                                                    <div class="LoaderBalls__item"></div>
                                                    <div class="LoaderBalls__item"></div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12" ng-show="success">
                                                {{sendInformation}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--end modal-content-->
        </div>
    </div>
</div>
<style>
    .error_forms{
         color: darkred;
        font-weight: 600;
    }
    .form_jobs .form-group {
        height: 60px;
    }
</style>
<script>
    var data = {};
    data.moduleId = <%=ModuleId%>;
    data.module_resources = '<%=Resources%>';
    data.portalId = <%=PortalId%>;
    data.common_resources = '<%=CommonResources%>';
    data.culture = '<%=System.Threading.Thread.CurrentThread.CurrentCulture.Name %>';
    var app = angularInit(data);
</script>
<script src="/DesktopModules/Mineros/scripts/helpers/helpers.js"></script>
<script src="/DesktopModules/Mineros/WorkUsModule/js/WorkUsModule.directive.js"></script>
<script src="/DesktopModules/Mineros/WorkUsModule/js/WorkUsModule.controller.js"></script>
<script src="/DesktopModules/Mineros/WorkUsModule/js/WorkUsModule.service.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.4.0/jszip.min.js"></script>
<script src="https://kendo.cdn.telerik.com/2017.3.1026/js/messages/kendo.messages.es-ES.min.js"></script>

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="View.ascx.cs" Inherits="Mineros.PpalesAccionistas2PpalesAccionistas2.View" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Panel runat="server" ID="pnlExport">
    <%--<asp:DropDownList runat="server" ID="ddlType" AutoPostBack="true" DataValueField="CategoryID" DataTextField="CategoryName" AppendDataBoundItems="true">
        <asp:ListItem Value="">Seleccione el tipo de acción</asp:ListItem>
    </asp:DropDownList>--%>
    <style>
        .click_download:focus,.click_download:hover{
            color:#9494d2;
        }
    </style>
    <div>
        <p>Descargar el ejemplo de base de datos: <a target="_blank" class="click_download" href="<%=urlSite%>/Portals/0/documentos/example.xlsx" download="example.xlsx">Link</a></p>
        <p>Para cargar la base de datos, por favor seleccionar el archivo con la información:</p>
    </div>
    <fieldset>
        <legend>Carga de clientes deceval:</legend>
        <label runat="server" for="fileuploadExcel" cssclass="uploadExcel" id="lblUpload"></label>

        <asp:FileUpload ID="fileuploadExcel" runat="server" />

        <asp:Button ID="btnImport" runat="server" OnClick="btnImport_Click" Text="Subir" />
        <div style="padding-top: 25px; padding-bottom: 20px;">
            <p>La carga de los archivos Deceval se realiza mediante FTP</p>
        </div>
        <label class="dnnFormMessage dnnFormWarning successAlert" runat="server" id="lblSuccess" visible="false">
            <%=LocalizeString("success.Text") %>
        </label>
        <label class="dnnFormMessage dnnFormWarning errorAlert" runat="server" id="lblError" visible="false">
            <%=LocalizeString("error.Text") %>
        </label>
    </fieldset>
</asp:Panel>

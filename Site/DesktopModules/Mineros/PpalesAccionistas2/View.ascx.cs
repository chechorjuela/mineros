﻿    /*
' Copyright (c) 2018  Mineros.co
'  All rights reserved.
' 
' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
' TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
' THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
' CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
' DEALINGS IN THE SOFTWARE.
' 
*/

using System;
using DotNetNuke.Security;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Localization;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using System.Globalization;
using System.IO;
using System.Data;
using System.Data.OleDb;
using System.Web.UI;
using System.Web;
using System.Drawing;
using System.Data.SqlClient;
using ExcelDataReader;
using Mineros.Data.Models;
using Mineros.Data;
using DotNetNuke.Services.FileSystem;

namespace Mineros.PpalesAccionistas2PpalesAccionistas2
{
    /// -----------------------------------------------------------------------------
    /// <summary>
    /// The View class displays the content
    /// 
    /// Typically your view control would be used to display content or functionality in your module.
    /// 
    /// View may be the only control you have in your project depending on the complexity of your module
    /// 
    /// Because the control inherits from PpalesAccionistas2ModuleBase you have access to any custom properties
    /// defined there, as well as properties from DNN such as PortalId, ModuleId, TabId, UserId and many more.
    /// 
    /// </summary>
    /// -----------------------------------------------------------------------------
    public partial class View : PpalesAccionistas2ModuleBase, IActionable
    {
        private string CultureCountry
        {
            get
            {
                CultureInfo culture2 = CultureInfo.CurrentCulture;
                //return culture2.Parent.ToString() ?? "es";
                return culture2.Name ?? "es-ES";
            }
        }
        public string urlSite
        {
            get
            {
                return Request.Url.Scheme + "://" + Request.Url.Authority;
            }
        }
        public List<EasyDnnNewsApp> Articles
        {
            get
            {
                return ViewState["Articles"] as List<EasyDnnNewsApp>;
            }
            set { ViewState["Articles"] = value; }
        }

        public List<EasyDnnFieldsValues> FieldsValues
        {
            get
            {
                return ViewState["EasyDnnFieldsValues"] as List<EasyDnnFieldsValues>;
            }
            set { ViewState["EasyDnnFieldsValues"] = value; }
        }

        public List<ClientDeceval> accionesData
        {
            get
            {
                return ViewState["accionesData"] as List<ClientDeceval>;
            }
            set { ViewState["accionesData"] = value; }
        }

        private string filePath
        {
            get
            {
                return ViewState["filePath"] as string;
            }
            set { ViewState["filePath"] = value; }
        }

        public GridView gridView
        {
            get
            {
                return ViewState["gridView"] as GridView;
            }
            set { ViewState["gridView"] = value; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    ViewState["urlSite"] = this.urlSite+"/Portals/0/documentos/example.xlsx";
                    int countItems = 0;
                    EasyDnnNewsData easyDnnNewsData = new EasyDnnNewsData();

                    //var Categories = easyDnnNewsData.GetCategoriesArgosReportes(CultureCountry);
                    var Categories = easyDnnNewsData.GetAllCategoriesNews(CultureCountry);

                    if (Settings["Category"] == null)
                    {
                        Settings["Category"] = "Principales Accionistas";
                    }

                    var parentCategory = Categories.FirstOrDefault(s => s.CategoryName.ToLower() == Settings["Category"].ToString().ToLower());

                    Articles = easyDnnNewsData.GetArticles(parentCategory.CategoryID, null, 0, 0, ref countItems, CultureCountry).ToList();

                    FieldsValues = easyDnnNewsData.GetEasyDnnNewsFieldsValues(parentCategory.CategoryID, null, CultureCountry);

                    var categoriesItems = (from ca in Articles
                                           .Select(s => s.CategoryID).Distinct()
                                           select new EasyDnnNewsCategories()
                                           {
                                               CategoryID = ca,
                                               CategoryName = CultureCountry == "es-ES" ? Categories.First(s => s.CategoryID == ca).CategoryName : Categories.First(s => s.CategoryID == ca).CategoryNameLocalize,
                                               Description = CultureCountry == "es-ES" ? Categories.First(s => s.CategoryID == ca).Description : Categories.First(s => s.CategoryID == ca).DescriptionLocalize
                                           })
                                           .Distinct()
                                           .OrderByDescending(s => s.CategoryName)
                                           .ToList();

                    if (UserInfo.IsSuperUser == true || UserInfo.Roles.Contains("Administrators") || UserInfo.Roles.Contains("Ajustes") || UserInfo.Roles.Contains("Ajustes IE"))
                    {
                        pnlExport.Visible = true;
                    }
                    else
                    {
                        pnlExport.Visible = false;
                    }

                    btnImport.Text = LocalizeString("import.Text");
                    lblUpload.InnerText = LocalizeString("upload.Text");

                    //rptPpalesAccionistas.DataSource = categoriesItems;
                    //rptPpalesAccionistas.DataBind();

                    //ddlType.DataSource = categoriesItems;
                    //ddlType.DataBind();

                    //rptTabs2.DataSource = categoriesItems.OrderBy(s => s.CategoryID);
                    //rptTabs2.DataBind();
                }
            }
            catch (Exception exc) //Module failed to load
            {
                Exceptions.LogException(exc);
            }
        }

        public ModuleActionCollection ModuleActions
        {
            get
            {
                var actions = new ModuleActionCollection
                    {
                        {
                            GetNextActionID(), Localization.GetString("EditModule", LocalResourceFile), "", "", "",
                            EditUrl(), false, SecurityAccessLevel.Edit, true, false
                        }
                    };
                return actions;
            }
        }

        protected void rptPpalesAccionistas_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            EasyDnnNewsData data = new EasyDnnNewsData();
            try
            {
                RepeaterItem item = e.Item as RepeaterItem;
                HiddenField hdnCategoryId = item.FindControl("hdnCategoryId") as HiddenField;

                Label lblFechaCorte = item.FindControl("lblFechaCorte") as Label;

                if (hdnCategoryId != null)
                {
                    accionesData = data.GetAccionesPpales(int.Parse(hdnCategoryId.Value));

                    //var fechaCorte = accionesData.First(s => s.CategoryID == int.Parse(hdnCategoryId.Value)).FechaCorte;

                    if (CultureCountry == "es-ES")
                    {
                    //    lblFechaCorte.Text = fechaCorte.ToString("dd/MM/yyyy");
                    }
                    else
                    {
                    //    lblFechaCorte.Text = fechaCorte.ToString("MM/dd/yyyy");
                    }                    
                }
            }
            catch (Exception ex)
            {
                Exceptions.LogException(ex);
            }
        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            try
            {
                PortalModuleBase oPortalMB = new PortalModuleBase();
                

                string trailingPath = Path.GetFileName(fileuploadExcel.PostedFile.FileName);

                if (trailingPath.Contains(".xlsx"))
                {
                    lblError.Visible = false;

                    string nameFolder = "documentos";
                    FolderManager folderManager = new FolderManager();
                    var folders = folderManager.GetFolders(oPortalMB.PortalId);
                    IFolderInfo folderDocumentPut = folders.FirstOrDefault(f => f.FolderName == nameFolder);
                    if (folderDocumentPut == null)
                    {
                        folderDocumentPut = folderManager.AddFolder(oPortalMB.PortalId, nameFolder);
                    }

                    string folder_document = "~\\Portals\\" + oPortalMB.PortalId.ToString() + "\\" + nameFolder;

                    string fullPath = Path.Combine(Server.MapPath(folder_document), trailingPath);

                    if (File.Exists(Server.MapPath(Path.Combine(folder_document, trailingPath))))
                    {
                        File.Delete(Server.MapPath(Path.Combine(folder_document, trailingPath)));
                    }
                    fileuploadExcel.PostedFile.SaveAs(fullPath);

                    if (!string.IsNullOrEmpty(fullPath))
                    {
                        EasyDnnNewsData Data = new EasyDnnNewsData();

                        List<ClientDeceval> ListAccionesP = new List<ClientDeceval>();
                        

                        //filePath = Server.MapPath(fileuploadExcel.FileName);

                        using (FileStream stream = File.Open(fullPath, FileMode.Open, FileAccess.Read))
                        {
                            //Reading from a binary Excel file ('97-2003 format; *.xls)
                            //IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(stream);

                            //Reading from a OpenXml Excel file (2007 format; *.xlsx)
                            IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);

                            DataSet result = excelReader.AsDataSet();

                            DataTableCollection table = result.Tables;
                            
                            DataTable dtP = table["Hoja1"];

                            if (dtP != null)
                            {
                                int countP = 0;
                                foreach (DataRow row in dtP.Rows)
                                {
                                    countP++;
                                    if (countP > 1)
                                    {
                                        var codeDeceval = row[0];
                                        var typeCedula = row[1];
                                        var numberID = row[2];
                                        var nameClient = row[3];

                                        //var categoryId = row[5];

                                        ClientDeceval objAcciones = new ClientDeceval()
                                        {
                                            numberID = numberID.ToString(),
                                            typeNit = typeCedula.ToString(),
                                            nameClient = nameClient.ToString(),
                                            codeDeceval = codeDeceval.ToString(),
                                            
                                            //FechaCorte = Convert.ToDateTime(hdfDatePicker.Value).Date
                                        };
                                        ListAccionesP.Add(objAcciones);
                                    }
                                }

                                if (ListAccionesP != null && ListAccionesP.Count > 0)
                                {
                                    Data.DeleteAccionesPpales(55);
                                    Data.InsertAccionesPpales(ListAccionesP);
                                }
                            }

                            

                            excelReader.Close();

                            if (dtP == null )
                            {
                                lblSuccess.Visible = true;
                                lblError.Visible = true;
                            }
                            else
                            {
                                lblSuccess.Visible = true;
                                lblError.Visible = false;
                            }

                            if (ListAccionesP.Count > 0 )
                            {
                              //  Response.Redirect(Request.RawUrl);
                            }
                        }
                    }
                }
                else
                {
                    lblSuccess.Visible = false;
                    lblError.Visible = true;
                }
            }
            catch (Exception ex)
            {
                lblSuccess.Visible = false;
                lblError.Visible = true;
                Exceptions.LogException(ex);
            }
        }

        protected void rptTabs2_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            EasyDnnNewsData data = new EasyDnnNewsData();
            try
            {
                RepeaterItem item = e.Item as RepeaterItem;

                HiddenField hdnCategoryId = item.FindControl("hdnCategoryId") as HiddenField;
                HiddenField hdnDescription = item.FindControl("hdnDescription") as HiddenField;

                GridView grvExcelData = item.FindControl("grvExcelData") as GridView;

                if (hdnCategoryId != null && grvExcelData != null)
                {
                    accionesData = data.GetAccionesPpales(int.Parse(hdnCategoryId.Value));

                    grvExcelData.DataSource = accionesData;
                    grvExcelData.DataBind();
                }
            }
            catch (Exception ex)
            {
                Exceptions.LogException(ex);
            }
        }

        protected void grvExcelData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                //GridView grdItems = sender as GridView;
                ////if (e.Row.RowType == DataControlRowType.DataRow)
                ////{
                //AccionesPpales article = (AccionesPpales)e.Row.DataItem;
                //if (article != null)
                //{
                //    var lblTipo = e.Row.FindControl("lblTipo") as Label;
                //    var lblNombreAccionista = e.Row.FindControl("lblNombreAccionista") as Label;
                //    var lblAcciones = e.Row.FindControl("lblAcciones") as Label;
                //    var lblParticipacion = e.Row.FindControl("lblParticipacion") as Label;

                //    var fieldsArticle = accionesData.Where(s => s.Identificacion == article.Identificacion).ToList();

                //    if (fieldsArticle != null)
                //    {
                //        var tipo = article.Tipo;
                //        var nombreAccionista = article.NombreAccionista;
                //        var acciones = article.Acciones;
                //        var participacion = article.Participacion;

                //        if (tipo != null && lblTipo != null)
                //            lblTipo.Text = tipo;
                //        if (nombreAccionista != null && lblNombreAccionista != null)
                //            lblNombreAccionista.Text = nombreAccionista;
                //        if (acciones != null && lblAcciones != null)
                //            lblAcciones.Text = string.Format("{0:N0}", acciones);
                //        if (participacion != null && lblParticipacion != null)
                //            lblParticipacion.Text = string.Format("{0} {1}", participacion, "%");
                //    }
                //    //}
                //}
            }
            catch (Exception ex)
            {
                Exceptions.LogException(ex);
            }
        }

        protected void grvExcelData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView grvExcelData = sender as GridView;
            grvExcelData.PageIndex = e.NewPageIndex;

            HiddenField hdnCategoryId = grvExcelData.Parent.FindControl("hdnCategoryId") as HiddenField;
            grvExcelData.DataSource = accionesData.Where(s => s.numberID == hdnCategoryId.Value).ToList();
            grvExcelData.DataBind();
        }

        protected void grvExcelData_PageIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
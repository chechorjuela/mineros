﻿app.controller("providersController", function ($scope, module_resources, $q, common_resources, providersControllerService, portal_id, $window, $sce, sf, $timeout) {
    $scope.contentProd = false;
    $scope.contentService = false
    $scope.send = false;
    $scope.errorRequest = false;
    $scope.providerForm = {};
    $scope.providerForm.codeCheck = true;
    $scope.providerForm.brands = [];
    $scope.uploadFileBoolean = false;
    $scope.arrayCountry = [];
    $scope.arrayBrand = [];
    $scope.showCountry = false;
    $scope.brandInput = "";
    $scope.categories;
    $scope.listProducts = [];
    $scope.listServices = [];
    $scope.selectCountryBoolean = false;
    $scope.selectServiceBoolean = false;
    $scope.optionsListProduct = [];
    $scope.optionsListService = [];
    $scope.booleanotherCheck = true;
    $scope.file_document;
    var validation_file = true;
    var location_module = window.location.pathname.replace(/\//g, "");
    var required_file = "El archivo debe ser Pdf, Doc, Docx.";
    $scope.provider_update_error = 'Codigo incorrecto.'
    $scope.messageSubmit = 'Se ha guardado la información!';
    $scope.updateSubmit = 'Se actualizado la información!'
    $scope.acceptTermsLabel = 'Acepto términos y condiciones <a href="#" class="color-jungle-green accept_terms"> (Ver Condiciones)</a>';
    $scope.message_send = $scope.messageSubmit;
    $scope.acceptterms = "Debes aceptar los términos";
    $scope.require_url = '"Ejemplo: url.com | www.url.com"';
    $scope.ProviderButtonText = "Clic aquí si quiere postularse para ser un posible proveedor de Mineros S.A. Y sus filiales";
    $scope.message_error = "Hubo un error";
    $scope.provider_exist_txt = "Proveedor ya se encuentra registrado, si cuentas con el codigo de verificación ingreselo para actualizar esta información. Gracias!";
    $scope.fieldRequiredText = "Campos Obligatorios";
    $scope.required_field = "Este campo es requerido";
    $scope.selectRequiredCountry = "Debes seleccionar alguna opción o registrar algun país";
    $scope.canTypeCountryText = "Puedes buscar por ciudad pero se escojera el país. Selecciona el pais con la tecla enter.";
    $scope.brandTypeRequireText = "Ingresa tus marcas escribiendo en el campo de texto y enseguida con la tecla enter.";
    $scope.brandRegister = "Marcas registradas";
    $scope.categoryText = "Categoría";
    $scope.subcategoryText = "Subcategoías";
    $scope.maxSubCategoryText = "Puedes escoger maximo 5 Subcategorias";
    $scope.fileRequiredText = "Archivo es requerido";
    $scope.attachFile = "Adjuntar Brochure";
    $scope.sendText = "Enviar";
    $scope.nameEnterpriseText = "Nombre empresa";
    $scope.hadProductText = "Ofrece productos?";
    $scope.hadServiceText = "Ofrece servicios?";
    $scope.representationCountryText = "¿Tiene Representación en alguno de los siguientes países?";
    $scope.productsAndServiceText = "Productos y Servicios";
    $scope.textAddProvider = "Diligencie este formulario para pre-inscribirse como proveedor";
    $scope.dataContactText = "Datos de contacto";
    $scope.nitPlaceholder = "Identificación tributaria (NIT/CUIT/RUC/TIN/SIN)";
    $scope.descriptionLorem = "El registro a través de este formulario no representa una inscripción como proveedor de "+
    "Mineros S.A ni de ninguna de sus empresas filiales. Solamente es un pre-registro donde se relacionan los bienes y/o servicios ofrecidos por su compañía, " +
        "quedando así en nuestra base de datos para futuras solicitudes. En caso de que se requiera una oferta específica de sus productos y/o servicios será contactado " +
        "directamente por uno de nuestros compradores quien le indicará los pasos a seguir.";
    $scope.siteUrlPlaceholder = "Sitio Web";
    $scope.nameAdiserText = "Nombre asesor";
    $scope.phoneAdviserText = "Teléfono asesor";
    $scope.emailEnterpriseText = "Correo empresarial";
    $scope.emailAdviser = "Correo del asesor";
    $scope.otherText = "Otro";
    $scope.wichText = "¿Cuál?";
    $scope.otherCountries = "Representaciones en paises";
    $scope.brandText = "Marcas";
    $scope.codeTxt = "Código de verificación";
    $scope.codeCheckLabel = "Este código debío llegar a tú correo.";
    $scope.message_captcha = "Por favor, verica que eres humano!";
    $scope.listSubcategoryProd = [];
    $scope.listSubcategoryService = [];
    $scope.providerFormMulti = {};
    $scope.booleanArrayCategory = false;
    $scope.provider_exist = false;
    $scope.staticSubcatgoryProd = [];
    $scope.staticSubcatgoryService = [];
    $scope.success_send = false;
    var txt_error_update = "Código incorrecto";
    $scope.localate = "es";
    $scope.requireServiceOrProduct = "Seleccionar un producto o servicio.";
    $scope.requiredSomeOption = "Seleccionar un producto o servicio.";
    $scope.token_recaptcha = '';
    this.$onInit = function () {
        initializeFunctions();
    };

    function initializeFunctions() {
        providersControllerService.getCategoryAll().then((response) => {
            $scope.categories = response.data.Data;
            $scope.listProducts = $scope.categories.filter((l) => l.TypeActivityId == 1);
            $scope.listProducts.sort(function (a, b) {
                if (location_module.substring(0, 2) === "es") {
                    var keyA = a.Spanish,
                        keyB = b.Spanish;
                } else {
                    var keyA = a.English,
                        keyB = b.English;
                }
                // Compare the 2 dates
                if (keyA < keyB) return -1;
                if (keyA > keyB) return 1;
                return 0;
            });
            $scope.listServices = $scope.categories.filter((l) => l.TypeActivityId == 2);
            $scope.listServices.sort(function (a, b) {
                if (location_module.substring(0, 2) === "es") {
                    var keyA = a.Spanish,
                        keyB = b.Spanish;
                } else {
                    var keyA = a.English,
                        keyB = b.English;
                }
                // Compare the 2 dates
                if (keyA < keyB) return -1;
                if (keyA > keyB) return 1;
                return 0;
            });
        })
        var location_module = window.location.pathname.replace(/\//g, "");
        if (location_module.substring(0, 2) === "en") {
            $scope.provider_update_error = "Code is invalid";
            required_file = "The file must be Pdf, Doc, Docx.";
            $scope.messageSubmit = "Your information has been saved.";
            $scope.updateSubmit = 'Your information has been updated!'
            $scope.fieldRequiredText = "Require fields";
            $scope.codeTxt = "Code check";
            $scope.require_url = '"example: url.com | .url.com"';
            $scope.codeCheckLabel = "This code should have reached your email.";
            $scope.message_send = $scope.messageSubmit;
            $scope.ProviderButtonText = "Click in the button below to pre-register as a supplier for Mineros subsidiaries";
            $scope.provider_exist_txt = "This provider is ready, if you have the code please insert that code for update this information. Thanks!";
            $scope.acceptTermsLabel = "I accept terms and conditions <a class='color-jungle-green accept_terms' target='_blank' href='#'>(See conditions)</a>";
            $scope.required_field = "This field is required";
            $scope.selectRequiredCountry = "You should require register anyone countrie";
            $scope.canTypeCountryText = "Type country name and press 'Enter'";
            $scope.brandTypeRequireText = "Type brand name and press 'Enter'";
            $scope.brandRegister = "Brand Registered";
            $scope.categoryText = "Category";
            $scope.subcategoryText = "Subcategory";
            $scope.maxSubCategoryText = "You can select max 5 subcategory.";
            $scope.fileRequiredText = "File required";
            $scope.attachFile = "Attach File";
            $scope.nameEnterpriseText = "Company name"
            $scope.sendText = "Submit";
            $scope.hadProductText = "Do you offer products?";
            $scope.hadServiceText = "Do you offer services?";
            $scope.representationCountryText = "do you have representation in the below countries?";
            $scope.productsAndServiceText = "Products & Services";
            $scope.textAddProvider = " Fill in the required fields to complete the pre-registration";
            $scope.nitPlaceholder = "Tax ID number";
            $scope.descriptionLorem = "El registro a través de este formulario no representa una inscripción como proveedor de "+
            "Mineros S.A ni de ninguna de sus empresas filiales. Solamente es un pre-registro donde se relacionan los bienes y/o servicios ofrecidos por su compañía, " +
                "quedando así en nuestra base de datos para futuras solicitudes. En caso de que se requiera una oferta específica de sus productos y/o servicios será contactado " +
                "directamente por uno de nuestros compradores quien le indicará los pasos a seguir.";
            $scope.dataContactText = "Contact information";
            $scope.siteUrlPlaceholder = "Website";
            $scope.nameAdiserText = "Sales representative name";
            $scope.phoneAdviserText = "Sales representative phone number";
            $scope.emailEnterpriseText = "Company email";
            $scope.emailAdviser = "Sales representative email";
            $scope.otherText = "Other";
            $scope.wichText = "Which one?";
            $scope.otherCountries = "Other countries";
            $scope.brandText = "Brands";
            $scope.localate = "en";
            $scope.acceptterms = "Required accept terms";
            $scope.requiredSomeOption = "You should select an option";
            $scope.message_captcha = "please verify you are humann!";
        }

    }
    $scope.cancelFile = function () {
        $(".error_show").fadeOut();
        validation_file = true;
        $scope.providerForm.fileBrochure = null;
        $scope.file_document = null;
        $("#file-error").fadeOut();
        $(".toFile").html("");
    }

    $scope.changeServices = function (e, service) {
        if (service == 'product') {
            $scope.listSubcategoryProd = [];
            $scope.contentProd = !$scope.contentProd;
            if (!$scope.contentProd) {
                $scope.providerForm.subcategoryProduct = [];
                $scope.optionsListProduct = [];
            }
        } else {
            $scope.listSubcategoryService = [];
            $scope.contentService = !$scope.contentService;
            if (!$scope.contentService) {

                $scope.providerForm.subcategoryService = [];
                $scope.optionsListService = [];
            }
        }
    }
    $scope.gotoTerms = function (event) {
        event.preventDefault();
        var url_path = $("footer .container-fluid .col.box-col-2-footer ul li a.text-white.librefranklin-thin").attr("href")
        window.open(window.location.protocol + "//" + window.location.host + url_path, "_blank")
    }
    $scope.checkCountry = function (e) {
        var country = 'Colombia';
        switch (e.target.id) {
            case 'checkArgentina':
                country = "Argentina";
                break;
            case 'checkNicaragua':
                country = "Nicaragua";
                break;
            case 'checkOther':
                $scope.booleanotherCheck = !$scope.booleanotherCheck;
                break;
            default:
                country = "Colombia";
                break;
        }
        var indexCountry = $scope.arrayCountry.findIndex((c) => c.name === country)
        if (indexCountry == -1) {
            let objectCountry = {
                'show': 0,
                'name': country
            };
            $scope.arrayCountry.push(objectCountry);
        } else {
            $scope.arrayCountry.splice(indexCountry, 1);
        }
    }
    $('#form_providers').on('keyup keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });
    grecaptcha.ready(function () {
        grecaptcha.execute('6LfK2tQZAAAAADrzJYpRWCHRbJwHnmsp_WmDd3Ow', { action: 'loginpage' }).then(function (token) {
            $scope.token_recaptcha = token;
        });
    });
    $scope.saveProvider = function (e, form_provider, providerForm) {
        
        //grecaptcha.ready(function () {
        //    grecaptcha.execute('6LfK2tQZAAAAADrzJYpRWCHRbJwHnmsp_WmDd3Ow', { action: 'loginpage' }).then(function (token) {
        //        $scope.log.recaptcha_response = token;
        //        console.info(token);
        //    });
        //});
        if (typeof providerForm.id == "undefined") {
            $scope.sendform = true;
            $scope.selectCountryBoolean = $scope.arrayCountry.length > 0 ? true : false;
            $scope.errorRequest = false;
            $scope.selectServiceBoolean = $scope.providerForm.serviceChck || $scope.providerForm.productChck ? true : false;
            $scope.booleanArrayCategory = $scope.listSubcategoryProd.length > 0 || $scope.listSubcategoryService.length > 0;
            if ($scope.selectCountryBoolean && $scope.selectServiceBoolean && $scope.booleanArrayCategory && validation_file) {
                if (form_provider.$valid ) {
                    let countries_array = [];
                    $scope.arrayCountry.map((c) => {
                        countries_array.push(c.name);
                    })
                    providerForm.subcategoryProduct = $scope.listSubcategoryProd;
                    providerForm.subcategoryService = $scope.listSubcategoryService;
                    providerForm.countries = countries_array;
                    providerForm.fileBrochure = null;
                    if (typeof $scope.file_document != 'undefined') {
                        if ($scope.file_document != null) {
                            providerForm.fileBrochure = {
                                FileName: $scope.file_document.name,
                                SizeImage: $scope.file_document.size,
                                TypeFileBase64: $scope.file_document.type,
                                FileStringBase64: $scope.file_document.FileStringBase64,
                            }
                        }
                    }
                    $(".message-error-captcha").html("");
                    if ($scope.token_recaptcha.length > 0) {
                        $(".message-error-captcha").html("");
                        $scope.send = true;
                        providersControllerService.saveProvider(providerForm).then((response) => {
                            $scope.send = false;
                            if (response.data.Header.Code == 200) {
                                if (response.data.Data != null) {
                                    $("#modal_proveedor").modal("hide")

                                    validation_file = true;
                                    $scope.providerForm = {};
                                    $scope.providerForm.brands = [];
                                    $scope.success_send = true;
                                    validation_file = true;

                                    $scope.arrayCountry = [];
                                    $scope.listSubcategoryProd = [];
                                    $scope.listSubcategoryService = [];
                                    $scope.serviceSelect = null;
                                    $scope.productSelect = null;
                                    $("#colombiaLabel").prop('checked', false);
                                    $("#argentinaLabel").prop('checked', false);
                                    $("#nicaraguaLabel").prop('checked', false);
                                    $("#checkotherlabel").prop('checked', false);
                                    $("#modal_proveedor").modal("hide");

                                    setTimeout(() => {

                                        $(".toFile").html("");
                                        $(".error_show").fadeIn();
                                        $scope.file_document = null;
                                        $scope.contentProd = false;
                                        $scope.contentService = false;
                                        initializeFunctions();
                                        $scope.success_send = false;
                                        $scope.provider_exist = false;
                                        $scope.errorRequest = false;
                                        $scope.error_update = false;
                                        $(".error_show").fadeOut();
                                        $scope.providerForm.fileBrochure = null;
                                        $scope.file_document = null;
                                        $("#file-error").fadeOut();
                                        $(".toFile").html("");
                                    }, 2000)
                                } else {
                                    $scope.errorRequest = true;
                                    if (response.data.Header.Message == "provider_exist") {
                                        $scope.provider_exist = true;

                                    }
                                    $scope.errorRequest = response.data.Header.Message == "provider_exist" ? $scope.provider_exist : $scope.message_error;
                                }
                            } else {
                                $scope.errorRequest = true;
                            }
                        })
                    } else {
                        $(".message-error-captcha").html("<span class='error_forms'>" + $scope.message_captcha + "</span>");
                    }
                  
                }
            }
        }

    }
    $scope.updateProvider = function (e, form_provider, providerForm) {
        //providersControllerService.getProvderByNitAndId(providerForm.id, providerForm.nit).then((response) => {
        //    if (response.data.Data != null) {
        //        $scope.provider_exist = false;
        //    } else {
        //        $scope.errorRequest = true;
        //        $scope.error_update = true;
        //    }
        //});
        $scope.selectServiceBoolean = $scope.providerForm.serviceChck || $scope.providerForm.productChck ? true : false;
        $scope.booleanArrayCategory = $scope.listSubcategoryProd.length > 0 || $scope.listSubcategoryService.length > 0;
        if ($scope.selectCountryBoolean && $scope.selectServiceBoolean && $scope.booleanArrayCategory && validation_file) {

            if (form_provider.$valid) {
                let countries_array = [];
                $scope.arrayCountry.map((c) => {
                    countries_array.push(c.name);
                })
                providerForm.subcategoryProduct = $scope.listSubcategoryProd;
                providerForm.subcategoryService = $scope.listSubcategoryService;
                providerForm.countries = countries_array;
                providerForm.fileBrochure = null;
                if (typeof $scope.file_document != 'undefined') {
                    if ($scope.file_document != null) {
                        providerForm.fileBrochure = {
                            FileName: $scope.file_document.name,
                            SizeImage: $scope.file_document.size,
                            TypeFileBase64: $scope.file_document.type,
                            FileStringBase64: $scope.file_document.FileStringBase64,
                        }
                    }
                }
                if ($scope.token_recaptcha.length > 0) {
                    $scope.send = true;
                    providersControllerService.updateProvider(providerForm).then((response) => {
                        $scope.send = false;
                        if (response.data.Header.Code == 200) {
                            if (response.data.Data != null) {
                                if (response.data.Data.Id > 0) {
                                    $("#modal_proveedor").modal("hide")
                                    $scope.success_send = true;
                                    $scope.contentProd = false;
                                    $scope.contentService = false;
                                    initializeFunctions();
                                    setTimeout(() => {

                                        $scope.success_send = false;
                                        $scope.provider_exist = false;
                                        $scope.error_update = false;
                                        validation_file = true;
                                        $scope.message_send = $scope.updateSubmit;
                                        $scope.providerForm = {};
                                        $scope.providerForm.brands = [];
                                        $(".error_show").fadeOut();
                                        validation_file = true;
                                        $scope.providerForm.fileBrochure = null;
                                        $scope.file_document = null;
                                        $("#file-error").fadeOut();
                                        $(".toFile").html("");
                                        $scope.arrayCountry = [];
                                        $scope.listSubcategoryProd = [];
                                        $scope.listSubcategoryService = [];
                                        $scope.serviceSelect = null;
                                        $scope.productSelect = null;
                                        $(".toFile").html("");
                                        $(".error_show").fadeIn();
                                        $("#colombiaLabel").prop('checked', false);
                                        $("#argentinaLabel").prop('checked', false);
                                        $("#nicaraguaLabel").prop('checked', false);
                                        $("#checkotherlabel").prop('checked', false);
                                        $scope.errorRequest = false;
                                        $scope.file_document = null;
                                    }, 2000)
                                } else {
                                    $scope.errorRequest = true;
                                    $scope.error_update = true;
                                }

                            }
                        }
                    })
                }
                

            }
        }
    };
    $("#botonFileInterest").change(function () {
        var allowedFiles = [".doc", ".docx", ".pdf"];
        var fileUpload = $("#botonFileInterest");
        var regex = new RegExp("\.(" + allowedFiles.join('|') + ")$", "i");
        if (!regex.test(fileUpload.val().toLowerCase())) {
            validation_file = false;
            $("#file-error").fadeIn();
            $(".toFile").html("");
            $("#file-error").html("<span class='error'>" + required_file + "</span>")
            $(".error_show").fadeIn();
            $(".error_show").css("display","inline-block");
        } else {
            $(".error_show").fadeOut();
            $("#file-error").fadeOut();

            var fullPath = fileUpload.val();
            var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
            var filename = fullPath.substring(startIndex);
            if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                filename = filename.substring(1);
            }
            $(".toFile").html("<span>" + filename + "</span>");
            validation_file = true;
        }

    });
    $scope.deleteTag = function (item, service) {
        switch (service) {
            case 'country':
                let index_country = $scope.arrayCountry.findIndex((c) => c.name === item);
                $scope.arrayCountry.splice(index_country, 1);
                let country_show = $scope.arrayCountry.filter((c) => c.show == 1);
                if (country_show.length == 0) {
                    $scope.showCountry = false;
                }
                break;
            case 'product':
                var indexoption = $scope.listSubcategoryProd.findIndex((p) => p.id == item.id);
                if (indexoption >= 0) {
                    let findCategory = $scope.staticSubcatgoryProd.find((p) => p.id == $scope.listSubcategoryProd[indexoption].id)
                    $scope.optionsListProduct.push($scope.listSubcategoryProd[indexoption])
                    $scope.listSubcategoryProd.splice(indexoption, 1);
                    $scope.optionsListProduct = $scope.optionsListProduct.sort(function (a, b) {
                        var keyA = a.Spanish,
                            keyB = b.Spanish;
                        if (keyA != "Seleccione" && keyB != "Seleccione") {
                            // Compare the 2 dates
                            if (keyA < keyB) return -1;
                            if (keyA > keyB) return 1;
                        }

                        return 0;
                    });
                }
                break;
            case 'service':
                var indexoption = $scope.listSubcategoryService.findIndex((p) => p.id == item.id);
                if (indexoption >= 0) {
                    let findCategory = $scope.staticSubcatgoryService.find((p) => p.id == $scope.listSubcategoryService[indexoption].id)
                    $scope.optionsListService.push($scope.listSubcategoryService[indexoption])
                    $scope.listSubcategoryService.splice(indexoption, 1)
                    $scope.optionsListService = $scope.optionsListService.sort(function (a, b) {
                        var keyA = a.Spanish,
                            keyB = b.Spanish;
                        if (keyA != "Seleccione" && keyB != "Seleccione") {
                            // Compare the 2 dates
                            if (keyA < keyB) return -1;
                            if (keyA > keyB) return 1;
                        }
                        return 0;
                    });
                }
                break;
            default:
                let indexBrand = $scope.providerForm.brands.findIndex((b) => b == item);
                if (indexBrand >= 0) {
                    $scope.providerForm.brands.splice(indexBrand, 1);
                }
                break;
        }
    }
    $scope.selectService = function (category, service) {
        switch (service) {
            case 'product':
                $scope.optionsListProduct = [];
                $scope.providerForm.subcategoryProduct = [];
                listProduct = category.subCategories;
                listProduct.sort(function (a, b) {
                    if (location_module.substring(0, 2) === "es") {
                        var keyA = a.Spanish,
                            keyB = b.Spanish;
                    } else {
                        var keyA = a.English,
                            keyB = b.English;
                    }
                    // Compare the 2 dates
                    if (keyA < keyB) return -1;
                    if (keyA > keyB) return 1;
                    return 0;
                });
                listProduct.map((p) => {
                    let obj = {
                        id: p.Id,
                        Spanish: p.Spanish,
                        English: p.English
                    }
                    if (typeof $scope.listSubcategoryProd.find((l) => l.id == obj.id) == "undefined") {
                        $scope.optionsListProduct.push(obj);
                        $scope.staticSubcatgoryProd.push(obj);
                    }

                    
            
                });
                break;
            case 'service':
                $scope.optionsListService = [];
                $scope.providerForm.subcategoryService = [];
                listService = category.subCategories;
                listService.sort(function (a, b) {
                    if (location_module.substring(0, 2) === "es") {
                        var keyA = a.Spanish,
                            keyB = b.Spanish;
                    } else {
                        var keyA = a.English,
                            keyB = b.English;
                    }
                    // Compare the 2 dates
                    if (keyA < keyB) return -1;
                    if (keyA > keyB) return 1;
                    return 0;
                });
                listService.map((p) => {
                    let obj = {
                        id: p.Id,
                        Spanish: p.Spanish,
                        English: p.English
                    }
                    if (typeof $scope.listSubcategoryService.find((l) => l.id == obj.id) == "undefined") {
                        $scope.optionsListService.push(obj);
                        $scope.staticSubcatgoryService.push(obj);
                    }
                  
                });
                break;
        }
    }
    $scope.selectItemAfterProd = function (item) {
        $scope.providerForm.subcategoryProduct = [];
        $scope.providerFormMulti.subcategoryProduct = [];
        $scope.multiselectServi = [];
        if ($scope.listSubcategoryProd.length < 5) {
            var obj = {
                id: item.id,
                Spanish: item.Spanish,
                English: item.English,
            }
            $scope.listSubcategoryProd.push(obj);
            var indexoption = $scope.optionsListProduct.findIndex((p) => p.id == item.id);
            if (indexoption >= 0) {
                $scope.optionsListProduct.splice(indexoption, 1)
            }
        }
    }
    $scope.selectItemAfterServ = function (item) {
        $scope.providerForm.subcategoryService = [];
        $scope.providerFormMulti.subcategoryService = [];
        if ($scope.listSubcategoryService.length < 5) {
            var obj = {
                id: item.id,
                Spanish: item.Spanish,
                English: item.English,
            }
            $scope.listSubcategoryService.push(obj);
            var indexoption = $scope.optionsListService.findIndex((p) => p.id == item.id);
            if (indexoption >= 0) {
                $scope.optionsListService.splice(indexoption, 1)
            }
        }
    }
    $scope.searchhNit = function () {
            //providersControllerService.getProvderByNit($scope.providerForm.nit).then((response) => {
            //    $scope.provider_exist = response.data.Data;
            //})
    }
    $scope.brandAdd = function (event) {
        if (event.keyCode == 13) {
            if ($scope.brandInput.length>1) {
                let indexBrand = $scope.providerForm.brands.findIndex((b) => b == $scope.brandInput);
                if (indexBrand < 0) {
                    $scope.providerForm.brands.push($scope.brandInput);
                }
                $scope.brandInput = "";
                event.preventDefault();
                event.stopPropagation();

                return false;
            }
      
        }
    }
});

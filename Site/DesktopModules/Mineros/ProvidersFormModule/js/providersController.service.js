﻿app.factory('providersControllerService', ['api_url', '$http', function (api_url, $http) {
    var service = {};
    const route_save_provider = api_url + "Provider/createProvider";
    const route_category = api_url + "provider/getAllCategory";
    const route_update_provider = api_url + "provider/updateProvider";
    const route_search_nit = api_url + "provider/findProviderByNit?nit=";
    const route_search_nit_by_id = api_url + "provider/findProviderByNitAndId";
    service.saveProvider = function (provider) {
        var promise = $http.post(route_save_provider, provider);
        promise.catch(function (data) {

        });

        return promise;
    }

    service.getCategoryAll = function () {
        var promise = $http.get(route_category);
        promise.catch(function (data) {

        });

        return promise;
    }
    service.updateProvider = function (provider) {
        var promise = $http.post(route_update_provider, provider);
        promise.catch(function (data) {

        });

        return promise;
    }

    service.getProvderByNit = function (nit) {
        var promise = $http.get(route_search_nit+nit);
        promise.catch(function (data) {

        });
        return promise;
    }
    service.getProvderByNitAndId = function (id,nit) {
        var promise = $http.get(route_search_nit_by_id + "?id="+id+"&nit="+nit);
        return promise;
    }
    return service;
}]);
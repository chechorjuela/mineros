﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="View.ascx.cs" Inherits="DesktopModules_Mineros_ProvidersFormModule_View" %>

<div ng-app="itemApp<%=ModuleId%>" class="ng-cloak" ng-controller="providersController" ng-cloak>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB18VJBwo1GssQFBWnZe6t3YJxIesc8KZc&libraries=places&sensor=false"></script>
    <script src="https://www.google.com/recaptcha/api.js?render=6LfK2tQZAAAAADrzJYpRWCHRbJwHnmsp_WmDd3Ow" async defer></script>

    <div class="row">
        <div class="col-md-12">
            <a href="#" class="btn butn librefranklin-regular text-white button-provider librefranklin-bold button_provider" data-toggle="modal" style="width: 100% !important" data-target="#modal_proveedor" role="btn btn-primary" aria-pressed="true"><strong>{{ProviderButtonText}}</strong></a>
            <div ng-show="success_send" class="message_interest"><strong>{{message_send}}</strong></div>
        </div>
    </div>

    <div class="modal" tabindex="" role="dialog" id="modal_proveedor">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content modal-lg">
                <div class="modal-header modal-header bg-alto position-relative">
                    <h2 class="modal-title librefranklin-bold color-jungle-green header-mineros ng-binding title_interest_h2">{{textAddProvider}}</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form></form>
                            <form name="form_providers" id="form_providers" class="form_providers" novalidate>

                                <p class="field_required librefranklin-regular">(*) {{fieldRequiredText}}</p>
                                <fieldset>
                                    <strong class="title_interes date_contact librefranklin-bold">{{dataContactText}}:</strong>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control bg-very-light-gray librefranklin-reg border_field" required
                                                    ng-model="providerForm.nit" name="nit" ng-blur="searchhNit()" placeholder="{{nitPlaceholder}} *" />
                                                <div ng-show="form_providers.$submitted || form_providers.nit.$touched">
                                                    <span class="error_forms" ng-show="form_providers.nit.$error.required ">{{required_field}}.</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" name="name_enterprise" required ng-model="providerForm.nameEnterprise"
                                                    class="form-control bg-very-light-gray librefranklin-reg border_field" placeholder="{{nameEnterpriseText}} *" />
                                                <div ng-show="form_providers.$submitted || form_providers.name_enterprise.$touched">
                                                    <span class="error_forms" ng-show="form_providers.name_enterprise.$error.required ">{{required_field}}.</span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12" <%-- ng-show="!provider_exist"--%>>
                                            <div class="form-group">
                                                <input type="text" class="form-control bg-very-light-gray librefranklin-reg border_field"
                                                    ng-pattern="/^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/" ng-model="providerForm.siteUrl" name="site_url" placeholder="{{siteUrlPlaceholder}}" />
                                                <div ng-show="form_providers.$submitted || form_providers.site_url.$touched">
                                                    <span class="error_forms" ng-show="form_providers.site_url.$error.required || form_providers.site_url.$error.pattern">{{required_field}}, {{require_url}}.</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6" <%--ng-show="!provider_exist"--%>>
                                            <input type="text" required class="form-control bg-very-light-gray border_field" placeholder="{{nameAdiserText}}*" ng-model="providerForm.contactName" name="name_contact" />
                                            <div ng-show="form_providers.$submitted || form_providers.name_contact.$touched">
                                                <span class="error_forms" ng-show="form_providers.name_contact.$error.required ">{{required_field}}.</span>
                                            </div>
                                        </div>
                                        <div class="col-md-6" <%--ng-show="!provider_exist"--%>>
                                            <input type="text" required class="form-control bg-very-light-gray border_field" name="phone_number" ng-model="providerForm.phoneNumber" placeholder="{{phoneAdviserText}} *" />
                                            <small class="contact-paragraph-one librefranklin-regular color-very-dark-gray">+XX XXX XXXXXXX,+XX-XXX-XXX-XXXX,+XX(XXX)-XXX-XXXX </small>
                                            <div ng-show="form_providers.$submitted || form_providers.phone_number.$touched">
                                                <span class="error_forms" ng-show="form_providers.phone_number.$error.required ">{{required_field}}.</span>
                                            </div>
                                        </div>
                                        <div class="col-md-6" <%--ng-show="!provider_exist"--%>>
                                            <input required placeholder="{{emailAdviser}}*"
                                                type="email" ng-parent="/^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/" ng-model="providerForm.contactEmail" name="email_contact"
                                                class="form-control bg-very-light-gray border_field" />
                                            <div ng-show="form_providers.$submitted || form_providers.email_contact.$touched">
                                                <span class="error_forms" ng-show="form_providers.email_contact.$error.required ">{{required_field}}.</span>
                                            </div>
                                        </div>
                                        <div class="col-md-6" <%--ng-show="!provider_exist"--%>>
                                            <input type="email" ng-parent="/^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/" required class="form-control bg-very-light-gray border_field"
                                                ng-model="providerForm.companyEmail" name="company_email" placeholder="{{emailEnterpriseText}}*" />
                                            <div ng-show="form_providers.$submitted || form_providers.company_email.$touched">
                                                <span class="error_forms" ng-show="form_providers.company_email.$error.required ">{{required_field}}.</span>
                                            </div>
                                        </div>
                                        <div class="col-md-12" <%--ng-show="!provider_exist"--%>>
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <label class="contact-paragraph-one label-form librefranklin-regular color-very-dark-gray">{{representationCountryText}}</label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row" style="margin: 0;">
                                                        <div class="col-md-2 no-padding">
                                                            <input type="checkbox" class="check_country" id="argentinaLabel" />
                                                            <label ng-click="checkCountry($event)" for="argentinaLabel" id="checkArgentina" class="check-box"></label>
                                                            <label class="form-check-label label-form librefranklin-regular color-very-dark-gray">Argentina</label>
                                                        </div>
                                                        <div class="col-md-2 no-padding">
                                                            <input type="checkbox" class="check_country" id="colombiaLabel" />
                                                            <label ng-click="checkCountry($event)" for="colombiaLabel" id="checkColombia" class="check-box"></label>
                                                            <label class="form-check-label label-form librefranklin-regular color-very-dark-gray">
                                                            Colombia</labe>
                                                        </div>
                                                        <div class="col-md-2 no-padding">
                                                            <input type="checkbox" class="check_country" id="nicaraguaLabel" />
                                                            <label ng-click="checkCountry($event)" for="nicaraguaLabel" id="checkNicaragua" class="check-box"></label>
                                                            <label class="form-check-label label-form librefranklin-regular color-very-dark-gray">Nicaragua</label>
                                                        </div>
                                                        <div class="col-md-6 no-padding" style="padding-right: 15px">
                                                            <div class="row">
                                                                <div class="col-md-2 no-padding">
                                                                    <input type="checkbox" class="check_country" id="checkotherlabel" />
                                                                    <label ng-click="checkCountry($event)" for="checkotherlabel" id="checkOther" class="check-box"></label>
                                                                    <label class="form-check-label label-form librefranklin-regular color-very-dark-gray">{{otherText}}</label>
                                                                </div>
                                                                <div class="col-md-10 no-padding">
                                                                    <input class="form-control bg-very-light-gray border_field" id="country_enterprise" ng-disabled="booleanotherCheck" ng-model="providerForm.othersCountry" googleplace placeholder="{{wichText}}" />
                                                                </div>
                                                                <div class="col-md-12 no-padding">
                                                                    <small class=" librefranklin-regular color-very-dark-gray">{{canTypeCountryText}}</small>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="list-country" ng-if="objectCountry">
                                                        <p class=" librefranklin-regular color-very-dark-gray">{{otherCountries}}:</p>
                                                        <ul class="ul_country">
                                                            <li ng-repeat="c in arrayCountry" ng-if="c.show==1">{{c.name}}  <span ng-click="deleteTag(c,'country')" class="k-icon k-i-close"></span></li>
                                                        </ul>
                                                    </div>
                                                    <div ng-show="form_providers.$submitted && !selectCountryBoolean" class="">
                                                        <span class="error_forms">{{selectRequiredCountry}}.</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset <%--ng-show="!provider_exist"--%>>
                                    <legend>
                                        <strong class="title_interes librefranklin-bold contact-paragraph-one">{{productsAndServiceText}}:</strong>
                                    </legend>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label class="contact-paragraph-one label-form librefranklin-regular color-very-dark-gray ">{{hadProductText}}:</label>
                                                    <label class="switcher">
                                                        <input type="checkbox" ng-change="changeServices($event,'product')" ng-model="providerForm.productChck" name="products" />
                                                        <div class="switcher__indicator"></div>
                                                    </label>
                                                </div>
                                                <div class="col-md-9">
                                                    <div class="row content-categories">
                                                        <div class="col-md-6 pad-left-0" ng-if="contentProd">
                                                            <select ng-if="localate=='es'" ng-model="productSelect" class="form-control bg-very-light-gray border_field" required ng-change="selectService(productSelect,'product')"
                                                                data-ng-options="listProduct as listProduct.Spanish for listProduct in listProducts track by listProduct.Id" name="category_product">
                                                                <option selected value="">{{categoryText}}..</option>
                                                            </select>
                                                            <select ng-if="localate=='en'" ng-model="productSelect" class="form-control bg-very-light-gray border_field" required ng-change="selectService(productSelect,'product')"
                                                                data-ng-options="listProduct as listProduct.English for listProduct in listProducts track by listProduct.Id" name="category_product">
                                                                <option selected value="">{{categoryText}}..</option>
                                                            </select>
                                                            <div ng-show="form_providers.$submitted ">
                                                                <span class="error_forms" ng-show="productSelect.$error.required ">{{required_field}}.</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 pad-left-0" ng-if="contentProd">
                                                            <div ng-if="localate=='es'">
                                                                <multiple-autocomplete ng-model="providerFormMulti.subcategoryProduct"
                                                                    object-property="Spanish"
                                                                    after-select-item="selectItemAfterProd"
                                                                    name="subcategory_product"
                                                                    suggestions-arr="optionsListProduct"
                                                                    placeholder="Subcategorías">
                                                                </multiple-autocomplete>
                                                            </div>
                                                            <div ng-if="localate=='en'">
                                                                <multiple-autocomplete ng-model="providerFormMulti.subcategoryProduct"
                                                                    object-property="English"
                                                                    after-select-item="selectItemAfterProd"
                                                                    name="subcategory_product"
                                                                    suggestions-arr="optionsListProduct"
                                                                    placeholder="Subcategory">
                                                            </multiple-autocomplete>
                                                            </div>

                                                            <div ng-show="form_providers.$submitted && listSubcategoryProd.length==0">
                                                                <span class="error_forms" ng-show="form_providers.$submitted && listSubcategoryProd.length==0">{{maxSubCategory}}.</span>
                                                            </div>
                                                        </div>
                                                        <div class="list-country" ng-if="listSubcategoryProd.length>0">
                                                            <ul class="ul_country">
                                                                <li ng-repeat="p in listSubcategoryProd">{{localate=='es' ? p.Spanish : p.English}}<span ng-click="deleteTag(p,'product')" class="k-icon k-i-close"></span></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label class="contact-paragraph-one label-form librefranklin-regular color-very-dark-gray">{{hadServiceText}}:</label>
                                                    <label class="switcher">
                                                        <input type="checkbox" ng-change="changeServices($event,'service')" ng-model="providerForm.serviceChck" name="services" />
                                                        <div class="switcher__indicator"></div>
                                                    </label>
                                                </div>
                                                <div class="col-md-9">
                                                    <div class="row content-categories">
                                                        <div class="col-md-6 pad-left-0" ng-if="contentService">
                                                            <select ng-if="localate=='es'" ng-model="serviceSelect" class="form-control bg-very-light-gray border_field" required ng-change="selectService(serviceSelect,'service')"
                                                                data-ng-options="listService as listService.Spanish for listService in listServices" name="category_service">
                                                                <option selected value="">{{categoryText}}..</option>
                                                            </select>
                                                            <select ng-if="localate=='en'" ng-model="serviceSelect" class="form-control bg-very-light-gray border_field" required ng-change="selectService(serviceSelect,'service')"
                                                                data-ng-options="listService as listService.English for listService in listServices" name="category_service">
                                                                <option selected value="">{{categoryText}}..</option>
                                                            </select>
                                                            <div ng-show="form_providers.$submitted">
                                                                <span class="error_forms" ng-show="serviceSelect.$error.required ">{{required_field}}.</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 pad-left-0" ng-if="contentService">
                                                            <div ng-if="localate=='es'">
                                                                <multiple-autocomplete ng-model="providerFormMulti.subcategoryService"
                                                                    object-property="Spanish"
                                                                    after-select-item="selectItemAfterServ"
                                                                    suggestions-arr="optionsListService"
                                                                    placeholder="Subcategorías">
                                                            </multiple-autocomplete>
                                                            </div>
                                                            <div ng-if="localate=='en'">
                                                                <multiple-autocomplete ng-model="providerFormMulti.subcategoryService"
                                                                    object-property="English"
                                                                    after-select-item="selectItemAfterServ"
                                                                    suggestions-arr="optionsListService"
                                                                    placeholder="Subcategory">
                                                            </multiple-autocomplete>
                                                            </div>

                                                            <div ng-show="form_providers.$submitted && listSubcategoryService.length==0 && contentService">
                                                                <span class="error_forms" ng-show="form_providers.$submitted && listSubcategoryService.length==0">{{maxSubCategory}}.</span>
                                                            </div>
                                                        </div>
                                                        <div class="list-country" ng-if="listSubcategoryService.length>0">
                                                            <ul class="ul_country">
                                                                <li ng-repeat="p in listSubcategoryService">{{localate=='es' ? p.Spanish : p.English}}<span ng-click="deleteTag(p,'service')" class="k-icon k-i-close"></span></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div ng-show="form_providers.$submitted && !selectServiceBoolean" class="col-md-12">
                                            <span class="error_forms">{{requiredSomeOption}}.</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 " style="margin-top: 10px;">
                                            <div class="form-group">
                                                <input type="text" class="form-control bg-very-light-gray border_field" ng-keyup="brandAdd($event)" ng-model="brandInput" name="brand" placeholder="{{brandText}}" />
                                                <small class=" librefranklin-regular color-very-dark-gray">{{brandTypeRequireText}}.</small>
                                            </div>
                                            <div class="list-brand form-group" ng-if="providerForm.brands.length>0">
                                                <p class=" librefranklin-regular color-very-dark-gray">{{brandRegister}}:</p>
                                                <ul class="ul_country">
                                                    <li ng-repeat="b in providerForm.brands">{{b}} <span ng-click="deleteTag(b,'brand')" class="k-icon k-i-close"></span></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="curriculum">
                                                <input type="file" name="file" id="botonFileInterest" file-model="file_document">
                                                <label for="botonFileInterest" class="btn btn-secondary bg-light-gray librefranklin-reg">{{attachFile}}</label>

                                                <label class="toFile"></label>
                                                <label id="file-error" class="error error_forms"><span class="librefranklin-reg error_forms">El archivo debe ser Pdf,Doc,Docx.</span></label><span class="error_show" ng-click="cancelFile()">x</span>
                                            </div>
                                        </div>

                                        <div class="col-md-12 text-center form-group">
                                            <div class="form-group">
                                                <small class="librefranklin-regular color-very-dark-gray">{{descriptionLorem}}</small>
                                            </div>
                                        </div>
                                        <div class="col-md-6  form-group">
                                            <div class="form-check d-flex">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" ng-model="providerForm.checkterms" name="checkterms" required value="" class="form-check-input" id="terminos" required="" />
                                                        <span class="cr border-color-pumice"><i class="cr-icon fas fa-check"></i></span>
                                                    </label>
                                                </div>
                                                <label class="form-check-label librefranklin-regular color-very-dark-gray" for="terminos">
                                                    <p ng-click="gotoTerms($event)" ng-bind-html="acceptTermsLabel"></p>
                                                </label>
                                            </div>
                                            <div ng-show="form_providers.$submitted || providerForm.checkterms.$touched">
                                                <span class="error_forms" ng-show="form_providers.checkterms.$error.required ">{{acceptterms}}</span>
                                            </div>
                                        </div>
                                        <div class="col-md-6"></div>
                                        <div class="col-md-6  form-group">
                                            <button class="btn btn-succes bg-jungle-green librefranklin-regular text-white" id="submit_interest" type="submit" ng-click="saveProvider($event,form_providers,providerForm)">{{sendText}}</button>
                                            <div class="message-error-captcha"></div>

                                        </div>
                                        <div class="col-md-6  form-group">
                                            <div class="message_send" ng-show="errorRequest && !provider_exist">
                                                <p class="librefranklin-regular color-very-dark-gray">{{message_error}}.</p>
                                            </div>
                                            <div class="row" ng-show="provider_exist">
                                                <div class="message_send" ng-show="provider_exist">
                                                    <p class="librefranklin-regular color-very-dark-gray">{{provider_exist_txt}}.</p>
                                                </div>

                                                <div class="col-md-10  form-group">
                                                    <input type="text" class="form-control bg-very-light-gray" name="id" ng-model="providerForm.id" placeholder="{{codeTxt}} *" />
                                                    <small class="contact-paragraph-one librefranklin-regular color-very-dark-gray">{{codeCheckLabel}}</small>
                                                    <div ng-show="form_providers.$submitted || form_providers.id.$touched">
                                                        <span class="error_forms" ng-show="form_providers.id.$error.required ">{{required_field}}.</span>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <button class="btn btn-succes bg-jungle-green librefranklin-regular text-white" ng-click="updateProvider($event,form_providers,providerForm)">
                                                        >
                                                    </button>
                                                </div>
                                            </div>

                                            <div class="message_send" ng-show="errorRequest && error_update">
                                                <p class="librefranklin-regular color-very-dark-gray error_forms">{{provider_update_error}}.</p>
                                            </div>
                                            <div class="LoaderBalls" ng-show="send">
                                                <div class="LoaderBalls__item"></div>
                                                <div class="LoaderBalls__item"></div>
                                                <div class="LoaderBalls__item"></div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var data = {};
    data.moduleId = <%=ModuleId%>;
    data.module_resources = '<%=Resources%>';
    data.portalId = <%=PortalId%>;
    data.common_resources = '<%=CommonResources%>';
    data.culture = '<%=System.Threading.Thread.CurrentThread.CurrentCulture.Name %>';
    var app = angularInit(data);

</script>
<link rel="stylesheet" href="/DesktopModules/Mineros/ProvidersFormModule/css/styleprovider.css" />
<script src="/DesktopModules/Mineros/scripts/helpers/helpers.js"></script>
<script src="/DesktopModules/Mineros/ProvidersFormModule/js/providersController.component.js"></script>
<script src="/DesktopModules/Mineros/ProvidersFormModule/js/providersController.service.js"></script>
<script src="/DesktopModules/Mineros/ProvidersFormModule/js/providersController.directive.js"></script>

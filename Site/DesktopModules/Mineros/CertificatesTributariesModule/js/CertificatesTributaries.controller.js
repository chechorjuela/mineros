﻿app.controller("CertificatesTributariesController", function ($scope, module_resources, $q, common_resources, certificatesTributariesService, portal_id, $window, $sce) {

    //Variables
    $scope.module_resources = JSON.parse(module_resources);
    $scope.common_resources = JSON.parse(common_resources);
    $scope.firstLoad = false;
    //end variables
    $scope.formChallenges = false;
    $scope.type_certificate = [];
    $scope.customers = [];
    $scope.submit = false;

    $scope.list_certificates = [];
    this.$onInit = function () {
        initializerPage();
    }

    function initializerPage() {
        certificatesTributariesService.GetCertificateTypes().then(function (response) {

            if (response.status == 200) {
                $scope.type_certificate = response.data.Data;
            }
        });

        certificatesTributariesService.GetCustomers().then(function (response) {

            if (response.status == 200) {
                $scope.customers = response.data.Data;

            }
        });
    }

    $scope.searchCertificate = function (e, formCertificate,formModelCertificate) {
        e.preventDefault();
        if (formCertificate.$valid) {
            $scope.submit = true;
            $scope.list_certificates = [];
            certificatesTributariesService.GetCertificates(formModelCertificate).then(function (response) {
                if (response.status == 200) {
                    if (response.data.Data != null) {
                        $scope.list_certificates = response.data.Data;

                    }
                }
            });
        }
    }

    $scope.download_file = function (url_file) {
        window.open(url_file, '_blank', '');
    }
});
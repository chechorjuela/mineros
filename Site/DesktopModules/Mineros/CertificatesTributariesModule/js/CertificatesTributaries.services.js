﻿app.factory('certificatesTributariesService', ['api_url', '$http', function (api_url, $http) {
    var service = {};

    const route_certificates_type = "/Certificate/GetCertificateType";
    const route_customers = "/Certificate/GetCustomers";
    const route_getCertificates = '/Certificate/GetCerticates';

    service.GetCertificateTypes = function () {
        var promise = $http.get(api_url + route_certificates_type );

        promise.catch(function (data) {

        });

        return promise;
    };

    service.GetCustomers = function () {
        var promise = $http.get(api_url + route_customers);

        promise.catch(function (data) {

        });

        return promise;
    };

    service.GetCertificates = function (itemCertificate) {
        var promise = $http.post(api_url + route_getCertificates, itemCertificate);

        promise.catch(function (data) {

        });

        return promise;

    }


    return service;
}]);
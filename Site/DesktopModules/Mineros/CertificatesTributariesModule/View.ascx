﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="View.ascx.cs" Inherits="DesktopModules_Mineros_CertificatesTributariesModule_View" %>

<div class="container" ng-app="itemApp<%=ModuleId%>" ng-controller="CertificatesTributariesController">
    <div class="row ng-cloak" ng-cloak>
        <div class="col-md-12">
            <h1 class="uk-article-title">Certificados tributarios</h1>
            <div class="row">
                <div class="col-md-12">
                    <p>
                        Para descargar los certificados tributarios emitidos por las diferentes filiales del Grupo MINEROS, ingrese los siguientes datos:
                    </p>
                </div>
                <div class="col-md-12">
                    <form></form>
                    <form name="form_certificate" class="form_certificate" novalidate>
                        <div class="form-group">
                            <input type="text" class="form-control" name="nit" ng-model="formCertificate.nit" required placeholder="Número de identificación o N.I.T*:">
                            <small class="form-text text-muted">Sin puntos ni dígito de verificación.</small>
                            <div ng-show="form_certificate.$submitted || form_certificate.nit.$touched">
                                <span class="error_forms" ng-show="form_certificate.nit.$error.required ">Número de identificación incorrecto.</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>* Empresa:</label>
                            <select class="form-control" required ng-options="template_custumer.CustomerId as template_custumer.NameCustomer for template_custumer in customers"
                                ng-model="formCertificate.custumerID" name="custumerID">
                                <option value="" selected="selected">Seleccionar...</option>
                            </select>
                            <div ng-show="form_certificate.$submitted || form_certificate.custumerID.$touched">
                                <span class="error_forms" ng-show="form_certificate.custumerID.$error.required">Debe seleccionar una empresa.</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>* Tipo de certificado</label>
                            <select class="form-control" required ng-options="template_typecertificate.CertificateID as template_typecertificate.Name for template_typecertificate in type_certificate" ng-model="formCertificate.typeCertificate" name="typeCertificate">
                                <option value="" selected="selected">Seleccionar...</option>
                            </select>
                            <div ng-show="form_certificate.$submitted || form_certificate.typeCertificate.$touched">
                                <span class="error_forms" ng-show="form_certificate.typeCertificate.$error.required">Debe seleccionar un certificado.</span>
                            </div>
                        </div>
                        <button type="submit" ng-disabled="form_certificate.$invalid" ng-click="searchCertificate($event,form_certificate,formCertificate)" class="btn btn-primary">Ver Certificados</button>
                    </form>

                </div>
            </div>
        </div>

        <div class="row content-download" ng-show="submit">

            <div class="col-md-12">
                <ul class="download-list" ng-show="list_certificates.length > 0">
                    <li ng-repeat="l_certificate in list_certificates">
                        <img ng-src="/DesktopModules/Mineros/CertificatesTributariesModule/assets/{{l_certificate.FileExtension}}.png" />
                        <a ng-href="{{l_certificate.FilePath}}" download="{{l_certificate.FileName}}">{{l_certificate.FileName}}</a>
                    </li>
                </ul>
                <p ng-show="list_certificates.length == 0 && submit">No se encontraron resultados</p>
            </div>
        </div>
    </div>


</div>

<script>
    var data = {};
    data.moduleId = <%=ModuleId%>;
    data.module_resources = '<%=Resources%>';
    data.portalId = <%=PortalId%>;
    data.common_resources = '<%=CommonResources%>';
    data.culture = '<%=System.Threading.Thread.CurrentThread.CurrentCulture.Name %>';
    var app = angularInit(data);
</script>
<link rel="stylesheet" type="text/css" href="/DesktopModules/Mineros/CertificatesTributariesModule/css/CertificatesTributaries.css"></link>
<script src="/DesktopModules/Mineros/scripts/helpers/helpers.js"></script>
<script src="/DesktopModules/Mineros/CertificatesTributariesModule/js/CertificatesTributaries.controller.js"></script>
<script src="/DesktopModules/Mineros/CertificatesTributariesModule/js/CertificatesTributaries.services.js"></script>

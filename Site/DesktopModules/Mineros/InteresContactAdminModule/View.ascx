﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="View.ascx.cs" Inherits="DesktopModules_Mineros_InteresContactAdminModule_View" %>

<div class="container container-grid-interes" ng-app="itemApp<%=ModuleId%>" ng-controller="InteresContactAdminModuleController">

    <style>
        td.k-command-cell {
            padding-left: 0px !important;
            padding-right: 5px;
        }

        .k-grid-content-locked table tbody tr td {
            padding-top: 0;
            padding-bottom: 0;
        }

            .k-grid-content-locked table tbody tr td button {
                max-width: 35px;
            }

        .k-header.k-grid-toolbar, .k-header, .k-grid .k-header .k-button, .k-primary {
            background: #426d61;
            background-color: #426d61;
        }

        .k-button {
            border-color: #426d61 !important;
        }

        .k-header.k-grid-toolbar {
            border-color: #426d61;
        }

        .k-primary:focus {
            border-color: #426d61 !important;
            background: #426d61;
            background-color: #426d61;
        }

        .k-filter-item, .k-item {
            background: #fafafa;
        }

        .growl {
            background: #426d61;
            background-color: #426d61;
            border-radius: 8px !important;
            border-color: #426d61 !important;
            color: #ffffff;
            border: 0;
            height: 80px;
            width: 300px;
            font-weight: 600;
            -moz-border-radius: 3px;
            -webkit-border-radius: 3px;
            overflow: hidden;
            padding: 15px;
        }

            .growl h3 {
                color: #fafafa;
            }

        .k-animation-container {
            background: #426d61;
            background-color: #426d61;
            border-radius: 8px !important;
            border-color: #426d61 !important;
            border: 0px !important color: #ffffff;
            font-weight: 600;
            -moz-border-radius: 3px;
            -webkit-border-radius: 3px;
            overflow: hidden;
            padding: 0px;
            margin: 0;
            font-size: 1.25rem !important;
            line-height: 24px !important;
            font-family: 'LibreFranklin', sans-serif !important;
            font-weight: 700 !important;
        }
        .container{
            max-width: 1950px !important
        }
        .k-grid-header-wrap.k-auto-scrollable table tr th a,.k-grid-header-wrap.k-auto-scrollable table tr th {
            font-size: 12px !important;
        }
        .k-grid-content.k-auto-scrollable table tr td {
            font-size: 12px !important;
        }
        .k-grid-content-locked tr td {
            font-size: 12px;
            text-align: center;
        }
        tr td p{
            font-size: 12px !important;
            text-align: center !important;
        }
        div.k-grid-content-locked table tbody tr[role='row'] td:nth-child(5) {
            text-align: right;
        }
    </style>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB18VJBwo1GssQFBWnZe6t3YJxIesc8KZcY&libraries=places"
        defer></script>
    <div class="row ng-cloak" ng-cloak>
        <span kendo-notification="notif" k-options="opt" style="display: none;"></span>
        <div class="col-md-12" ng-if="!form">
            
            <div class="col-md-12">
                <kendo-grid id="gridMain" options="mainGridOptions">
                    <%--<div k-detail-template>
                        <kendo-tabstrip>
                            <ul>
                                <li class="k-state-active">Informacion de intereses</li>
                            </ul>
                           <div class="content_div">
                                
                           </div>
                        </kendo-tabstrip>
                    </div>--%>
                </kendo-grid>
            </div>

        </div>

        <div class="col-md-12" ng-if="form">
            <edit-interest-contact-component go-back="goBackFromForm()"  update-settings="updateItem()" edit-interest="interestContact" interest-settings="InterestSettings"></edit-interest-contact-component>
        </div>

    </div>

</div>

<script>
    var data = {};
    data.moduleId = <%=ModuleId%>;
    data.module_resources = '<%=Resources%>';
    data.portalId = <%=PortalId%>;
    data.common_resources = '<%=CommonResources%>';
    data.culture = '<%=System.Threading.Thread.CurrentThread.CurrentCulture.Name %>';
    var app = angularInit(data);

</script>
<script src="/DesktopModules/Mineros/scripts/helpers/helpers.js"></script>
<script src="/DesktopModules/Mineros/InteresContactAdminModule/js/interestContactAdmin.controller.js"></script>
<script src="/DesktopModules/Mineros/InteresContactAdminModule/js/interestContactAdmin.service.js"></script>
<script src="/DesktopModules/Mineros/InteresContactAdminModule/js/components/EdictInterestContact/editInterestContact.component.js"></script>
<script src="/DesktopModules/Mineros/InteresContactAdminModule/js/components/EdictInterestContact/editInterestContact.service.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.4.0/jszip.min.js"></script>
<script src="https://kendo.cdn.telerik.com/2017.3.1026/js/messages/kendo.messages.es-ES.min.js"></script>

﻿app.controller("InteresContactAdminModuleController", function ($scope, module_resources, $q, common_resources, InteresContactAdminModuleService, portal_id, $window, $sce, sf, $timeout,) {

    $scope.optionsInterest = [
        { "value": 0, "name": "Tipo de interés" },
        { "value": 1, "name": "Nivel de Cargo" },
        { "value": 2, "name": "Proceso / Especialidad" },
        { "value": 3, "name": "Ubicación" }];

    $scope.detailContact = {};
    $scope.InterestSettings = {};
    $scope.form = false;
    $scope.listSettings = true;
    $scope.formSettings = false;
    $scope.interestContact;
    $scope.excelList = [];
    $scope.pageSize = 10;
    $scope.url_request = "";
    $scope.interestNews = {
        name_spanish: "",
        name_english: "",
        type_interest: 0
    }
    $scope.opt = {
        position: {
            top: 30,
            right: 30
        },
        templates: [{
            type: "growl",
            template: "<div class='growl'><h3>#= title #</h3><p>#= message #</p></div>"
        }]
    };
    $scope.getDatasource = function () {
        var model = {};
        return {
            type: 'aspnetmvc-ajax',
            dataType: "json",
            transport: {
                read: {
                    url: "/DesktopModules/services/API/InterestContact/getInterestContact",
                    contentType: "application/json; charset=utf-8",
                    method: "get",
                    beforeSend: function (e, request) {
                        $scope.url_request = request.url;
                    }
                },

                //parameterMap: function (options, type) {
                //    //options.filters = typeof options.filter != "undefined" ? options.filter.filters:null;
                //    //options.sorts = [
                //    //    { "member": "number_phone","order":0}
                //    //]

                //    return JSON.stringify(options);
                //}
            },
            pageSize: $scope.pageSize,
            serverPaging: true,
            serverSorting: true,
            serverFiltering: true,
            serverGrouping: true,
            serverAggregates: true,
            schema: {
                data: "Data.Data",
                total: "Data.Total",
                errors: "Data.Errors",
                model: {
                    fields: {
                        Id: { type: "string" },
                        number_phone: { type: "string" },
                        email: { type: "string" },
                        number_id: { type: "string" },
                        country_current: { type: "string" },
                        city_current: { type: "string" },
                        location_interest_first: { type: "number" },
                        location_interest_second: { type: "number" },
                        interest_position_first: { type: "number" },
                        interest_position_second: { type: "number" },
                        process_interest_first: { type: "number" },
                        process_interest_second: { type: "number" },
                        file_dir: { type: "string" },
                        locationApplicant: { type: "string" },
                        positionApplicant: { type: "string" },
                        processApplicant: { type: "string" },
                        application: { type: "string" }
                    }
                },
            }
        }
    };
    $scope.getDatasourceSettings = function () {
        return {
            type: 'aspnetmvc-ajax',
            dataType: "json",
            transport: {
                read: {
                    url: "/DesktopModules/services/API/InterestContact/listSettingsFilter",
                    contentType: "application/json; charset=utf-8",
                    method: "get",
                    beforeSend: function (e, request) {
                        $scope.url_request = request.url;
                    }
                },

                //parameterMap: function (options, type) {
                //    //options.filters = typeof options.filter != "undefined" ? options.filter.filters:null;
                //    //options.sorts = [
                //    //    { "member": "number_phone","order":0}
                //    //]

                //    return JSON.stringify(options);
                //}
            },
            pageSize: 5,
            serverPaging: true,
            serverSorting: true,
            serverFiltering: true,
            serverGrouping: true,
            serverAggregates: true,
            schema: {
                data: "Data.Data",
                total: "Data.Total",
                errors: "Data.Errors",
                model: {
                    fields: {
                        Id: { type: "number" },
                        name_spanish: { type: "string" },
                        name_english: { type: "string" },
                        type_interest: { type: "number" },
                    }
                },
            }
        }
    };
    $scope.editItem = function (dataItem) {
        $scope.form = true;
        $scope.interestContact = dataItem;

    }
    $scope.downloadPDF = function (dataItem) {
        var a = document.createElement("a");
        a.target = "_blank";
        if (dataItem.file_dir != null) {
            a.href = window.location.protocol + "//" + window.location.host + dataItem.file_dir;
            var ext = dataItem.file_dir.substring(dataItem.file_dir.lastIndexOf(".")+1, dataItem.file_dir.length);
            a.download = dataItem.number_id + "_" + dataItem.name_contact + "." + ext;
            document.body.appendChild(a);
            a.click();
        }
      
    }
    function construct_interest_html(dataItem) {
        console.info(dataItem);
    }
    $scope.goBackFromForm = function () {
        $scope.form = false;

    }
    this.$onInit = function () {
        initializeFunctions();
        var data_contact = null;
    };
    function initializeFunctions() {
        $scope.interesPositionFirst = new kendo.data.DataSource({ data: $scope.optionsInterest });
        $scope.optionsInterestSelect = {
            dataSource: $scope.interesPositionFirst,
            dataTextField: 'name',
            dataValueField: 'value',

        };
        $scope.asyncLoader(true);
        $q.all([
            InteresContactAdminModuleService.GetInterestSettings(),
        ]).then(function (data) {
            $scope.InterestSettings = data[0].data.Data;

            $scope.asyncLoader(false);
        });
        $scope.mainGridOptions = {
            toolbar: [{
                name: "",
                text: "",
                template: function (grid) {
                    return '<button class=\'k-button k-button-icontext\' ng-click=\'exportToExcel($event)\'><span class=\'k-icon k-i-file-excel\'></span>Exportar a excel</button>'
                }
            },
            {
                name: "Zip",
                text: "Descargar zip",
                template: function (grid) {
                    return '<button data-ng-click=\'compressFile($event,dataItem)\' class=\'k-button\'><span class=\'k-icon k-i-file-zip k-i-zip\'></span>Descargar zip CV</button>'
                }
            }, {
                name: "ListInterest",
                text: "Listado de interes",
                template: function (grid) {
                    return '<button type=\'button\' data-ng-click=\'showListSettings()\' class=\'k-button pull-right\'><span class=\'k-icon k-i-grid-layout\'></span>Interés Maestro</button>'
                }
            }],
            dataSource: $scope.getDatasource(),
            navigatable: true,
            sortable: true,
            resizable: true,
            reorderable: true,
            columnMenu: true,
            pageable: {
                refresh: true,
                alwaysVisible: true,
                pageSizes: [5, 10, 20, 100]
            },
            filter: function (e) {
                $scope.filter = e.filter;
            },
            filterable: {
                width: "200px",
                operators: {
                    //filter menu for "string" type columns
                    string: {
                        eq: "Es igual a...",
                        neq: "No sea igual a...",
                        startswith: "Comience en...",
                        contains: "Contenga...",
                        endswith: "Termine en..."
                    },
                    //filter menu for "number" type columns
                    number: {
                        eq: "Es igual a...",
                        neq: "No sea igual a...",
                        gte: "Es mayor o igual a...",
                        gt: "Es mayor a...",
                        lte: "Es menor o igual a...",
                        lt: "Es menos que..."
                    },
                    //filter menu for "date" type columns
                    date: {
                        eq: "Es igual a...",
                        neq: "No sea igual a...",
                        gte: "Es mayor o igual a...",
                        gt: "Es mayor a...",
                        lte: "Es menor o igual a...",
                        lt: "Es menos que..."
                    },
                    //filter menu for foreign key values
                    enums: {
                        eq: "Es igual a...",
                        neq: "No sea igual a..."
                    }
                }
            },
            dataBound: function (e) {
            },
            columns: [
                {
                    field: "number_id",
                    width: "150px",
                    title: "ID",
                    locked: true,
                    lockable: false,
                },
                {
                    title: "Inf. Contacto",
                    locked: true,
                    lockable: false,
                    columns: [{
                        field: "name_contact",
                        title: "Nombre de contacto",
                        width: "150px",

                    }, {
                        field: "number_phone",
                        title: "Teléfono",
                        width: "150px",

                    }, {
                        field: "email",
                        title: "Correo",
                        width: "150px",
                    }
                    ],

                }, {
                    title: "Origenes",
                    columns: [{
                        field: "city_current",
                        title: "Ciudad",
                        width: "150px",

                    },
                    {
                        field: "country_current",
                        title: "País",
                        width: "150px",

                    }],

                }, {
                    title: "Interés de cargo",
                    columns: [{
                        field: "interest_position_first",
                        title: "Principal",
                        width: "100px",
                        template: function (dataItem) {
                            if (dataItem.interest_position_first != null) {
                                return "<p>" + $scope.InterestSettings.find(i => i.Id == parseInt(dataItem.interest_position_first)).name_spanish + "</p>";
                            }
                            return "";
                        },
                        filterable: {
                            multi: true,
                            itemTemplate: function (e) {
                                var interestSetting = $scope.InterestSettings;
                                return "<p><label><input type='checkbox' name='" + e.field + "' value='#= data.interest_position_first#'/><span>#= getSettings(data.interest_position_first||data.all)#</span></label></p>"
                            }
                        }
                    }, {
                        field: "interest_position_second",
                        title: "Secundario",
                        width: "100px",
                        template: function (dataItem) {
                            if (dataItem.interest_position_second != null) {
                                return "<p>" + $scope.InterestSettings.find(i => i.Id == parseInt(dataItem.interest_position_second)).name_spanish + "</p>";
                            }
                            return "";
                        },
                        filterable: {
                            multi: true,
                            itemTemplate: function (e) {
                                var interestSetting = $scope.InterestSettings;
                                return "<p><label><input type='checkbox' name='" + e.field + "' value='#= data.interest_position_first#'/><span>#= getSettings(data.interest_position_first||data.all)#</span></label></p>"
                            }
                        }
                    }],
                    locked: false,

                }, {
                    title: "Interés de Proceso",
                    columns: [{
                        field: "process_interest_first",
                        title: "Principal",
                        width: "100px",
                        template: function (dataItem) {
                            if (dataItem.process_interest_first != null) {
                                return "<p>" + $scope.InterestSettings.find(i => i.Id == parseInt(dataItem.process_interest_first)).name_spanish + "</p>";
                            }
                            return "";
                        },
                        filterable: {
                            multi: true,
                            itemTemplate: function (e) {
                                var interestSetting = $scope.InterestSettings;
                                return "<p><label><input type='checkbox' name='" + e.field + "' value='#= data.process_interest_first#'/><span>#= getSettings(data.process_interest_first||data.all)#</span></label></p>"
                            }
                        }

                    }, {
                        field: "process_interest_second",
                        title: "Secundario",
                        width: "100px",
                        template: function (dataItem) {
                            if (dataItem.process_interest_second != null) {
                                return "<p>" + $scope.InterestSettings.find(i => i.Id == parseInt(dataItem.process_interest_second)).name_spanish + "</p>";

                            }
                            return "";
                        },
                        filterable: {
                            multi: true,
                            itemTemplate: function (e) {

                                var interestSetting = $scope.InterestSettings;
                                return "<p><label><input type='checkbox' name='" + e.field + "' value='#= data.process_interest_second#'/><span>#= getSettings(data.process_interest_second||data.all)#</span></label></p>"
                            }
                        }
                    }],

                    locked: false,
                }, {
                    title: "Interés de Localización",
                    columns: [{
                        field: "location_interest_first",
                        title: "Principal",
                        width: "100px",
                        template: function (dataItem) {
                            if (dataItem.location_interest_first != null) {
                                return "<p>" + $scope.InterestSettings.find(i => i.Id == parseInt(dataItem.location_interest_first)).name_spanish + "</p>";

                            }
                            return "";
                        },
                        filterable: {
                            multi: true,
                            itemTemplate: function (e) {
                                console.info($scope.InterestSettings);
                                var interestSetting = $scope.InterestSettings;
                                return "<p><label><input type='checkbox' name='" + e.field + "' value='#= data.location_interest_first#'/><span>#= getSettings(data.location_interest_first||data.all)#</span></label></p>"
                            }
                        }

                    }, {
                        field: "location_interest_second",
                        title: "Secundario",
                        width: "100px",
                        template: function (dataItem) {
                            if (dataItem.location_interest_second != null) {
                                return "<p>" + $scope.InterestSettings.find(i => i.Id == parseInt(dataItem.location_interest_second)).name_spanish + "</p>";
                            }
                            return "";
                        },
                        filterable: {
                            multi: true,
                            itemTemplate: function (e) {
                                console.info($scope.InterestSettings);
                                var interestSetting = $scope.InterestSettings;
                                return "<p><label><input type='checkbox' name='" + e.field + "' value='#= data.location_interest_second#'/><span>#= getSettings(data.location_interest_second||data.all)#</span></label></p>"
                            }
                        }
                    }]
                }, {
                    title: "Solicitud",
                    columns: [{
                        field: "positionApplicant",
                        title: "Posición",
                        width: "100px",

                    },
                    {
                        field: "processApplicant",
                        title: "Proceso",
                        width: "100px",

                    },
                     {
                            field: "locationApplicant",
                            title: "ubicación",
                            width: "100px",
                        },
                    {

                        field: "application",
                        title: "Vacante",
                        width: "100px",
                        }
                    ],

                    locked: false,
                }, {
                    title: "Acciones",
                    width: "150px",
                    locked: true,
                    lockable: false,
                    template: function (dataItem) {
                     
                        var dir_exist = dataItem.file_dir != null ? "<kendo-button type='button' class='k-primary btn-iconInnovacion' icon=\"'k-icon k-i-pdf'\" ng-click='downloadPDF(dataItem)' ></kendo-button> " : "";
                        return dir_exist +
                            "<kendo-button type='button' class='k-primary btn-iconInnovacion' icon=\"'k-icon k-i-edit'\" ng-click='editItem(dataItem)' > " +
                            "</kendo-button> " +
                            "<kendo-button type='button' class='btn-iconInnovacion' icon=\"'k-icon k-i-delete'\" ng-click='deleteItem(dataItem)' > " +
                            "</kendo-button>";
                    }
                   
                }]
            //selectable: "row",
            //change: function (e) {
            //    var gview = $("#mainGrid").data("kendoGrid");
            //    var selectedItem = gview.dataItem(gview.select());
            //    //SEND PREVIEW
            //    //alert("Selected value " + selectedItem.Id);
            //}
        };
        // Parallel loads

        $scope.exportToExcel = function (e) {
            event.preventDefault();
            var params_index = $scope.url_request.indexOf("?");
            var params_send = $scope.url_request.substring(params_index, $scope.url_request.length);
            let index_chart = [];
            for (i = 0; i <= params_send.length; i++) {
                if (params_send[i] == "&") {
                    index_chart.push(i)
                }
            }
            var params_page = params_send.substring(index_chart[0] + 1, index_chart[1]);
            var params_pageSize = params_send.substring(index_chart[1] + 1, index_chart[2]);

            params_send = params_send.replace(params_pageSize, "pageSize=1000000000");
            params_send = params_send.replace(params_page, "page=1");
            InteresContactAdminModuleService.getListInterestFilter(params_send).then(response => {
                $scope.excelList = response.data.Data.Data;
                if ($scope.excelList.length === 0) {
                    $scope.excelList = $scope.listData.slice();
                }
                var rows_Transactions = [{
                    cells: [
                        { value: "Número de Identificación", background: "#426d61", bold: true, color: "#FFF" },
                        { value: "Nombre", background: "#426d61", bold: true, color: "#FFF" },
                        { value: "Email", background: "#426d61", bold: true, color: "#FFF" },
                        { value: "Número Telefónico", background: "#426d61", bold: true, color: "#FFF" },
                        { value: "Ciudad", background: "#426d61", bold: true, color: "#FFF" },
                        { value: "País", background: "#426d61", bold: true, color: "#FFF" },
                        { value: "Interés de cargo principal", background: "#426d61", bold: true, color: "#FFF" },
                        { value: "Interés de cargo segundario", background: "#426d61", bold: true, color: "#FFF" },
                        { value: "Interés de proceso Principal", background: "#426d61", bold: true, color: "#FFF" },
                        { value: "Interés de proceso segundario", background: "#426d61", bold: true, color: "#FFF" },
                        { value: "Interés de locación Principal", background: "#426d61", bold: true, color: "#FFF" },
                        { value: "Interés de locación Segundario", background: "#426d61", bold: true, color: "#FFF" },
                        { value: "Posiciòn", background: "#426d61", bold: true, color: "#FFF" },
                        { value: "Proceso", background: "#426d61", bold: true, color: "#FFF" },
                        { value: "Ubicaciòn", background: "#426d61", bold: true, color: "#FFF" },
                        { value: "Vacante", background: "#426d61", bold: true, color: "#FFF" }
                    ]
                }];
                if ($scope.excelList) {
                    for (var i = 0; i < $scope.excelList.length; i++) {
                        rows_Transactions.push({
                            cells: [
                                { value: $scope.excelList[i].number_id },
                                { value: $scope.excelList[i].name_contact },
                                { value: $scope.excelList[i].email },
                                { value: $scope.excelList[i].number_phone },
                                { value: $scope.excelList[i].city_current },
                                { value: $scope.excelList[i].country_current },
                                { value: $scope.getInterestContact($scope.excelList[i].interest_position_first) },
                                { value: $scope.getInterestContact($scope.excelList[i].interest_position_second) },
                                { value: $scope.getInterestContact($scope.excelList[i].process_interest_first) },
                                { value: $scope.getInterestContact($scope.excelList[i].process_interest_second) },
                                { value: $scope.getInterestContact($scope.excelList[i].location_interest_first) },
                                { value: $scope.getInterestContact($scope.excelList[i].location_interest_second) },
                                { value: $scope.excelList[i].locationApplicant },
                                { value: $scope.excelList[i].positionApplicant },
                                { value: $scope.excelList[i].processApplicant },
                                { value: $scope.excelList[i].application }
                            ]
                        });
                    }
            
                    // Create workbook
                    var workbook = new kendo.ooxml.Workbook({
                        sheets: [
                            {
                                columns: [
                                    { autoWidth: true },
                                    { autoWidth: true },
                                    { autoWidth: true },
                                    { autoWidth: true },
                                    { autoWidth: true },
                                    { autoWidth: true },
                                    { autoWidth: true },
                                    { autoWidth: true },
                                    { autoWidth: true },
                                    { autoWidth: true },
                                    { autoWidth: true },
                                    { autoWidth: true },
                                    { autoWidth: true },
                                    { autoWidth: true },
                                    { autoWidth: true },
                                    { autoWidth: true },
                                    { autoWidth: true },
                                    { autoWidth: true },
                                    { autoWidth: true }
                                ],
                                title: "Title",
                                rows: rows_Transactions
                            }
                        ]
                    });
                    var date = new Date();
                    kendo.saveAs({ dataURI: workbook.toDataURL(), fileName: "InterestContact_" + date.format('dd/MM/yyyy') + ".xlsx" });

                }
            }, function (response) {
                //$scope.not_service.error("Error: " + response.status + " " + response.statusText);

            })

        }

    }
    window.getSettings = function (id) {
        if (id != null) {
            if (typeof $scope.InterestSettings.find(i => i.Id == id) != "undefined") {
                return $scope.InterestSettings.find(i => i.Id == id).name_spanish;
            }
        }

        return "Selecciona Todo"
    }

    $scope.compressFile = function (event, items) {
        var grid = $("#gridMain").data('kendoGrid');

        event.preventDefault();
        var params_index = $scope.url_request.indexOf("?");
        var params_send = $scope.url_request.substring(params_index, $scope.url_request.length)
        InteresContactAdminModuleService.getDownloadZip(params_send).then(function (response) {
            var a = document.createElement("a");
            a.target = "_blank";
            a.href = window.location.protocol + "//" + window.location.host + "/" + response.data.Data;
            a.download = "cv_mineros.zip";
            document.body.appendChild(a);
            a.click();
        }, function (response) {
            //$scope.not_service.error("Error: " + response.status + " " + response.statusText);

        });
    };

    function detailInit(dataItem) {
        var detailRow = dataItem.detailRow;
        InteresContactAdminModuleService.GetInterestContactUserById(dataItem.data.Id).then(function (response) {
            var interest_contact = response.data.Data;
            var html = "<div class='row'><div class='col-md-3'><ul>" +
                "<li><label><strong>Interés de nivel de cargo:</strong></label>" +
                "<ul>" +
                "<li ng-model='" + interest_contact.interest_position_first + "'>" + $scope.getInterestContact(interest_contact.interest_position_first) + "</li>";
            if (interest_contact.interest_position_second != null && interest_contact.interest_position_second > 0) {
                html += "<li ng-model='" + interest_contact.interest_position_second + "'>" + $scope.getInterestContact(interest_contact.interest_position_second) + "</li>";
            }
            html += "</ul>" +
                "</li></ul></div><div class='col-md-4'><ul>" +
                "<li><label><strong>Interés de Proceso / Especialidad</strong></label>" +
                "<ul>" +
                "<li ng-model='" + interest_contact.process_interest_first + "' >" + $scope.getInterestContact(interest_contact.process_interest_first) + "</li>";

            if (interest_contact.process_interest_second != null && interest_contact.process_interest_second > 0) {
                html += "<li ng-model='" + interest_contact.process_interest_second + "'>" + $scope.getInterestContact(interest_contact.process_interest_second) + "</li>";
            }

            html += "</ul>" +
                "</li></ul></div><div class='col-md-3'><ul>" +
                "<li><label><strong>Interés de localizacion:</strong></label>" +
                "<ul>" +
                "<li ng-model='" + interest_contact.location_interest_first + "'>" + $scope.getInterestContact(interest_contact.location_interest_first) + "</li>";

            if (interest_contact.location_interest_second != null && interest_contact.location_interest_second > 0) {
                html += "<li ng-model='" + interest_contact.location_interest_second + "'>" + $scope.getInterestContact(interest_contact.location_interest_second) + "</li>";
            }
            html += "</ul>" +
                "</li></ul></div></div>";


            detailRow.find(".content_div").html(html);
        }, function (response) {
            //$scope.not_service.error("Error: " + response.status + " " + response.statusText);
            kendo.ui.progress($("#mainGrid"), false);
            $scope.asyncLoader(false);
        });

        //detailRow.find(".tabstrip").kendoTabStrip({
        //    animation: {
        //        open: { effects: "fadeIn" }
        //    }
        //});
        //InteresContactAdminModuleService.GetInterestContactUserById(dataItem.data.Id).then(function (response) {
        //    $scope.detailContact = response.data.Data;
        //}, function (response) {
        //    //$scope.not_service.error("Error: " + response.status + " " + response.statusText);
        //    console.log("Error");
        //    kendo.ui.progress($("#mainGrid"), false);
        //    $scope.asyncLoader(false);

        //});

    }
    $scope.updateItem = function () {
        $scope.notif.show({
            title: "Realizado",
            message: "Recurso Actualizado."
        }, "growl");
    }
    $scope.deleteItem = function (item) {
        $scope.getDatasource();
        $("<div id='confirmPopupWindow'></div>")
            .appendTo("body")
            .kendoConfirm({
                title: "Eliminar",
                content: "Desea eliminar el recurso?",
                messages: {
                    okText: "Sí",
                    cancel: "Cancelar"
                }
            }).data("kendoConfirm").open()
            .result.done(function () {
                $scope.asyncLoader(true);
                kendo.ui.progress($("#mainGrid"), true);

                InteresContactAdminModuleService.DeleteInterestContact(item.Id).then(function (response) {
                    if (response.data.Header.Code == 200) {
                        initializeFunctions();
                        $scope.getDatasource();
                        $scope.notif.show({
                            title: "Realizado",
                            message: "Operación realizada."
                        }, "growl");
                        initializeFunctions();
                        $("a[title='Actualizar']").trigger("click");
                        $scope.asyncLoader(false);
                    } else {
                        switch (response.data.Message) {
                            case "EC999":
                                $scope.not_service.warning($scope.common_resources.ConstrainGeneralError_Text);
                                console.log("Error");
                                kendo.ui.progress($("#mainGrid"), false);
                                break;
                            default:
                                kendo.ui.progress($("#mainGrid"), false);
                                $scope.not_service.error(response.data.Message);
                        }
                    }
                }, function (response) {
                    //$scope.not_service.error("Error: " + response.status + " " + response.statusText);
                    console.log("Error");
                    kendo.ui.progress($("#mainGrid"), false);
                    $scope.asyncLoader(false);

                });

            });
    }

    $scope.getInterestContact = function (id) {
        if (typeof id != "undefined" && id != null) {
            var name_settings = $scope.InterestSettings.find(i => i.Id == id);
            return name_settings.name_spanish;

        }
        return "";
    }

    $scope.showListSettings = function () {
        $scope.listSettings = true;
        const page = "/Maestro-Interes";
        window.location.href = window.location.protocol + "//" + window.location.host + page;

    }
    $scope.editItemSettigs = function (dataItem) {
        $scope.formSettings = true;
        $scope.settingInterestSeleted = dataItem;

    }
    $scope.clickCancelarSettings = function () {
        $scope.formSettings = false;
        $scope.settingInterestSeleted = null;
    }
    $scope.goBackFromForm = function () {
        $scope.form = false;
        $scope.interestContact = null;
        $window.scrollTo(0, 0);
    }

});

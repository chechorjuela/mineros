﻿app.component("editInterestContactComponent", {
    templateUrl: "/DesktopModules/Mineros/InteresContactAdminModule/js/components/EdictInterestContact/editInterestContact.component.html",
    bindings: {
        goBack: '&?',
        editInterest: '=',
        interestSettings: '=',
        updateSettings: '&?'
    },
    controllerAs: 'vm',
    controller: function ($scope, $http, api_url, $q, editInterestContactService,portal_id, $window, $timeout, $sce) {
        var vm = this;
        vm.contactEdit = null;
       
        vm.$onInit = function () {
            vm.contactEdit = vm.editInterest;
            item_first_position = [{ "Id": null, "name_spanish": "Seleccione", "type_interest": 1 }];
            item_second_position = [{ "Id": null, "name_spanish": "Seleccione", "type_interest": 1 }];

            vm.interestSettings.filter(function (i) {
                if (i.type_interest === 1) {
                  return  item_first_position.push({ "Id": i.Id, "name_spanish": i.name_spanish, "type_interest": i.type_interest });
                }
            });
            vm.interestSettings.filter(function (i) {
                if (i.type_interest === 1) {
                    return item_second_position.push({ "Id": i.Id, "name_spanish": i.name_spanish, "type_interest": i.type_interest });
                }
            });
            vm.interesPositionFirst = new kendo.data.DataSource({
                data: item_first_position.sort(function (a, b) {

                    var keyA = a.name_spanish,
                        keyB = b.name_spanish;
                    if (keyA != "Seleccione" && keyB != "Seleccione") {
                        // Compare the 2 dates
                        if (keyA < keyB) return -1;
                        if (keyA > keyB) return 1;
                    }

                    return 0;
                }) });
            vm.interesPositionSecond = new kendo.data.DataSource({
                data: item_second_position.sort(function (a, b) {

                    var keyA = a.name_spanish,
                        keyB = b.name_spanish;
                    if (keyA != "Seleccione" && keyB != "Seleccione") {
                        // Compare the 2 dates
                        if (keyA < keyB) return -1;
                        if (keyA > keyB) return 1;
                    }

                    return 0;
                }) });
           
            vm.activeOptionsPositionFirst = {
                dataSource: vm.interesPositionFirst,
                dataTextField: 'name_spanish',
                dataValueField: 'Id',
            };
            vm.activeOptionsPositionSecond = {
                dataSource: vm.interesPositionSecond,
                dataTextField: 'name_spanish',
                dataValueField: 'Id',
            };
            item_first_proccess = [{ "Id": null, "name_spanish": "Seleccione", "type_interest": 2 }];
            item_second_proccess = [{ "Id": null, "name_spanish": "Seleccione", "type_interest": 2 }];

            vm.interestSettings.filter(function (i) {
                if (i.type_interest === 2) {
                    return item_first_proccess.push({ "Id": i.Id, "name_spanish": i.name_spanish, "type_interest": i.type_interest });
                }
            });
            vm.interestSettings.filter(function (i) {
                if (i.type_interest === 2) {
                    return item_second_proccess.push({ "Id": i.Id, "name_spanish": i.name_spanish, "type_interest": i.type_interest });
                }
            });
            vm.interesProcessFirst = new kendo.data.DataSource({
                data: item_first_proccess.sort(function (a, b) {

                    var keyA = a.name_spanish,
                        keyB = b.name_spanish;
                    if (keyA != "Seleccione" && keyB != "Seleccione") {
                        // Compare the 2 dates
                        if (keyA < keyB) return -1;
                        if (keyA > keyB) return 1;
                    }

                    return 0;
                }) });
            vm.interesProcessSecond = new kendo.data.DataSource({
                data: item_second_proccess.sort(function (a, b) {

                    var keyA = a.name_spanish,
                        keyB = b.name_spanish;
                    if (keyA != "Seleccione" && keyB != "Seleccione") {
                        // Compare the 2 dates
                        if (keyA < keyB) return -1;
                        if (keyA > keyB) return 1;
                    }

                    return 0;
                }) });
            vm.activeOptionsProcessFirst = {
                dataSource: vm.interesProcessFirst,
                dataTextField: 'name_spanish',
                dataValueField: 'Id',
            };
            vm.activeOptionsProcessSecond = {
                dataSource: vm.interesProcessSecond,
                dataTextField: 'name_spanish',
                dataValueField: 'Id',
            };
            item_first_location = [{ "Id": null, "name_spanish": "Seleccione", "type_interest": 3 }];
            item_second_location = [{ "Id": null, "name_spanish": "Seleccione", "type_interest": 3 }];

            vm.interestSettings.filter(function (i) {
                if (i.type_interest === 3) {
                    return item_first_location.push({ "Id": i.Id, "name_spanish": i.name_spanish, "type_interest": i.type_interest });
                }
            });
            vm.interestSettings.filter(function (i) {
                if (i.type_interest === 3) {
                    return item_second_location.push({ "Id": i.Id, "name_spanish": i.name_spanish, "type_interest": i.type_interest });
                }
            });
            vm.interesLocationFirst = new kendo.data.DataSource({
                data: item_first_location.sort(function (a, b) {

                    var keyA = a.name_spanish,
                        keyB = b.name_spanish;
                    if (keyA != "Seleccione" && keyB != "Seleccione") {
                        // Compare the 2 dates
                        if (keyA < keyB) return -1;
                        if (keyA > keyB) return 1;
                    }
                    
                    return 0;
                }) });
            vm.interesLocationSecond = new kendo.data.DataSource({
                data: item_second_location.sort(function (a, b) {
                    var keyA = a.name_spanish,
                        keyB = b.name_spanish;
                    if (keyA != "Seleccione" && keyB!="Seleccione") {
                        // Compare the 2 dates
                        if (keyA < keyB) return -1;
                        if (keyA > keyB) return 1;
                    }

                    return 0;
                })});

            vm.activeOptionsLocationFirst = {
                dataSource: vm.interesLocationFirst,
                dataTextField: 'name_spanish',
                dataValueField: 'Id',
            };
            vm.activeOptionsLocationSecond = {
                dataSource: vm.interesLocationSecond,
                dataTextField: 'name_spanish',
                dataValueField: 'Id',
            };
        }
        vm.clickCancelar = function () {
            vm.goBack && vm.goBack();
            $(".nameField").text("");
       //     vm.cleanControls();
        }
        vm.onSubmit = function (e,contactEdit, formUpdate) {
            if (formUpdate.$valid) {
         
                    editInterestContactService.updateItem(contactEdit).then(function (response) {
                        if (response.data.Header.Code == 200) {
                            vm.goBack && vm.goBack();
                            vm.updateSettings();
                            //   vm.cleanControls();
                        }
                    }, function (response) {
                        //$scope.not_service.error("Error: " + response.status + " " + response.statusText);
                        kendo.ui.progress($("#mainGrid"), false);
                    })
                }
              
        }
     
        $scope.fileNameChanged = function () {
         
            $scope.filePrincipal = null;
            var files = event.target.files;
            angular.forEach(files, function (value, key) {
                var fileReader = new FileReader();
                vm.contactEdit.uploadfile = value;
                var allowedFiles = [".doc", ".docx", ".pdf"];
                var fileUpload = $("#botonFileInterest");
                var regex = new RegExp("\.(" + allowedFiles.join('|') + ")$", "i");
                if (!regex.test(vm.contactEdit.uploadfile.name.toLowerCase())) {
                    vm.contactEdit.uploadfile = null;
                    $(".nameField").html('<label id="file-error" class="error"><span class="librefranklin-reg">El archivo debe ser Pdf,Doc,Docx.</span></label>');
                } else {
                    $(".nameField").text(value.name);
                }
              
            })
        }
    }
    
});

app.directive('googleplace', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, model) {
            var options = {
                types: ['(cities)'],
            };
            scope.gPlace = new google.maps.places.Autocomplete(element[0], options);
            
            google.maps.event.addListener(scope.gPlace, 'place_changed', function () {
                scope.$apply(function () {
                    formated_addres = element.val().split(",");
                    country = formated_addres[formated_addres.length - 1];
                    document.getElementById("country_field").value = country.trim();
                    var city = formated_addres[0]
                    if (formated_addres.length > 2) {
                        city += "," + formated_addres[1]
                    }
                    scope.vm.contactEdit.city_current = city;
                    scope.vm.contactEdit.country_current = country;
                    document.getElementById("locationfirst").value = city;
                });
            });
        }
    };
});
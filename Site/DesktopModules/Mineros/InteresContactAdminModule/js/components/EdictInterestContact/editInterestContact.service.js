﻿app.factory('editInterestContactService', function (api_url, $http) {
    var s = {};
    const app_url_interest_put = api_url + "InterestContact/updateInterestContact";
    s.updateItem = function (item) {
        debugger;
        var data = new FormData();
        data.append('id', item.Id);
        data.append('name_contact', item.name_contact);
        data.append('number_phone', item.number_phone);
        data.append('number_id', item.number_id);
        data.append('email', item.email);
        data.append('city_current', item.city_current);
        data.append('country_current', item.country_current);
        data.append('interest_position_first', item.interest_position_first);
        data.append('interest_position_second', item.interest_position_second);
        data.append('process_interest_first', item.process_interest_first);
        data.append('process_interest_second', item.process_interest_second);
        data.append('location_interest_first', item.location_interest_first);
        data.append('location_interest_second', item.location_interest_second);
        data.append('application', item.application);
        data.append('file', item.uploadfile);
        var promise = $http.post(app_url_interest_put, data, {
            headers: { 'Content-Type': undefined }
        });

        promise.catch(function (response) {
            console.log("hubo un error");
        });

        return promise;
    }


    return s;
});
﻿app.factory('InteresContactAdminModuleService', ['api_url', '$http', function (api_url, $http) {
    var service = {};

    const app_url_interest = api_url + "InterestContact/getInterestContact";
    const route_delete_interest = api_url + "InterestContact/deleteInterestContactById?id="
    const route_get_interest = api_url + "InterestContact/getInterestContactById?id="
    const route_get_interes_settings = api_url + "InterestContact/index";
    const route_downlad_zip = api_url + "InterestContact/downloadZip";
    const app_url_interest_save = api_url + "InterestContact/store";
    service.GetInterestSettings = function () {
        var promise = $http.get(route_get_interes_settings);
        promise.catch(function (data) {

        });

        return promise;
    }
    service.GetInteresContactUser = function () {
        var promise = $http.get(app_url_interest);

        promise.catch(function (data) {

        });

        return promise;
    };
    service.DeleteInterestContact = function (id) {
        var promise = $http.get(route_delete_interest + id);
        promise.catch(function (data) {

        })
        return promise;
    }
    service.GetInterestContactUserById = function (id) {
        var promise = $http.get(route_get_interest + id);
        promise.catch(function (data) {

        })
        return promise;
    }

    service.getDownloadZip = function (filter) {
        var promise = $http.get(route_downlad_zip+filter);

        promise.catch(function (data) {

        })
        return promise;
    }
    service.getListInterestFilter = function (filter) {
        var promise = $http.get(app_url_interest+filter);

        promise.catch(function (data) {

        });

        return promise;
    }

    service.storeSettings = function (settingSave) {
        var promise = $http.post(app_url_interest_save, settingSave);

        promise.catch(function (data) {

        });

        return promise;
    }
    return service;
}]);
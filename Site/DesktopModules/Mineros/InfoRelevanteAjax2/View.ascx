﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="View.ascx.cs" Inherits="Mineros.InfoRelevanteAjax2InfoRelevanteAjax2.View" %>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    var currentCategoryID = "0";
    var categoriesItems = [];
    var items = [];
    var templateYear = "<%=LocalizeString("templateYear.html")%>";
    var templateItem = "<%=LocalizeString("templateItem.html")%>";

    var listInfoRelevente = [];

    $(document).ready(function () {
        $('.urlMoreInfoRelevant').attr("href", $('.urlmoreRelevant').text());
        loadData();
    });
    var location_module = window.location.pathname.replace(/\//g, "");
    if (location_module.substring(0, 2) === "en") {
        var buscarTema = "Search";
        var fechaTransmisionTitle = 'Date';
        var temaTitle = 'Subject';
        var resumenTitle = 'Summary';
        var anexoTitle = 'Attachment';
        var hourTitle = 'Time';

    } else {
        var buscarTema = "<%=LocalizeString("buscarTema.Text")%>";
        var fechaTransmisionTitle = '<%=LocalizeString("fechaTransmision.Text") %>';
        var temaTitle = '<%=LocalizeString("tema.Text") %>';
        var resumenTitle = '<%=LocalizeString("resumen.Text") %>';
        var anexoTitle = '<%=LocalizeString("anexoTitle.Text") %>';
        var hourTitle = '<%=LocalizeString("HourTitle.Text") %>';

    }
    $(window).on("load", function () {
        if (currentCategoryID == "0") {
           currentCategoryID = $(".divYears > div a").first().attr("data-categorid");
        } else {
            $(".panel-default-relevante" + currentCategoryID + " a").first().click();

        }
    });

    function getTemplateYear(categoryID, categoryName, items,itemsByCategory) {

        var items_filter_category = itemsByCategory.filter(x => x.CategoryID == categoryID);
        var document_items = items_filter_category.filter(f => f.Document.FilePath != null && f.Document.UrlView != "" && f.Document.UrlView!=null);
        var htmlYear = "";

        if (document_items.length > 0) {

            htmlYear = templateYear.replace("[[ITEMS]]", items);
            var indexCategoryID, indexCategoryName, indexBuscar;
            while (indexCategoryID != -1) {
                indexCategoryID = htmlYear.indexOf("[[CATEGORYID]]");
                if (indexCategoryID != -1) {
                    htmlYear = htmlYear.replace("[[CATEGORYID]]", categoryID);
                }
            }
            while (indexCategoryName != -1) {
                indexCategoryName = htmlYear.indexOf("[[CATEGORYNAME]]");
                if (indexCategoryName != -1) {
                    htmlYear = htmlYear.replace("[[CATEGORYNAME]]", categoryName);
                }
            }
        }
       
        return htmlYear;
    }

    function getItemsHtml(categoryId, itemsByCategory, pageIndex, pagesCount) {
        var itemsHtml = "";

        if (itemsByCategory && itemsByCategory.length > 0) {
            itemsHtml += "<table class='table table-bordered'><thead class='bg-pumice'><tr><th class='color-scorpion librefranklin-semibold' >[[FECHATRANSMISIONTITLE]]</th><th class='color-scorpion librefranklin-semibold' >[[HOURTRANSMISIONTITLE]]</th><th class='color-scorpion librefranklin-semibold'>[[TEMATITLE]]</th><th class='color-scorpion librefranklin-semibold'>[[RESUMENTITLE]]</th><th class='color-scorpion librefranklin-semibold'>[[ANEXOTITLE]]</th></tr></thead><tbody class='border-color-alto'>";
            itemsHtml = itemsHtml.replace("[[FECHATRANSMISIONTITLE]]", fechaTransmisionTitle);
            itemsHtml = itemsHtml.replace("[[HOURTRANSMISIONTITLE]]", hourTitle);
            itemsHtml = itemsHtml.replace("[[TEMATITLE]]", temaTitle);
            itemsHtml = itemsHtml.replace("[[RESUMENTITLE]]", resumenTitle);
            itemsHtml = itemsHtml.replace("[[ANEXOTITLE]]", anexoTitle);

            for (var j = pageIndex * 10; j < pageIndex * 10 + 10; j++) {
                if (j < itemsByCategory.length) {
                    var item = itemsByCategory[j];
                    if (item.Document.FilePath != null && item.Document.UrlView != "" && item.Document.UrlView != null) {
                        var articleID = item.Article.ArticleID;
                        var title = item.Article.Title;
                        var summary = htmlDecode(item.Article.Summary);
                        var publishDate = new Date(item.Article.PublishDate).format("yyyy-MM-dd");
                        var publishHouyrDate = new Date(item.Article.PublishDate).format("hh:mm");
                        var document = item.Document;
                        var portalId = 0, moduleId = 0, documentId = 0;
                        var styleItemDownload = "", styleItemView = "";

                        if (title != "" && title.length > 0) {

                            if (document != null && document.DocEntryID != 0) {
                                portalId = document.PortalId;
                                moduleId = item.ModuleID;
                                documentId = document.DocEntryID;

                                if (document.UrlView == "" || document.UrlView == null) {
                                    styleItemDownload = "style='display:none;'";
                                    styleItemView = "style='display:none;'";
                                }
                            }
                            else {
                                styleItemDownload = "style='display:none;'";
                                styleItemView = styleItemDownload;
                            }

                            var itemHtml = templateItem;
                            itemHtml = itemHtml.replace("[[TITLE]]", title);
                            itemHtml = itemHtml.replace("[[SUMMARY]]", summary);
                            itemHtml = itemHtml.replace("[[PUBLISHDATE]]", publishDate);
                            itemHtml = itemHtml.replace("[[PUBLISHHOURSDATE]]", publishHouyrDate);
                            itemHtml = itemHtml.replace("[[STYLEITEMDOWNLOAD]]", styleItemDownload);
                            itemHtml = itemHtml.replace("[[STYLEITEMVIEW]]", styleItemView);


                            var indDocId, indModId, indPortalId, indArticleId;
                            //while (indDocId != -1) {
                            //    indDocId = itemHtml.indexOf("[[DOCUMENTID]]");

                            //    if (indDocId != -1) {
                            itemHtml = itemHtml.replace("[[DOCUMENTID]]", documentId);
                            //    }
                            //}
                            //while (indModId != -1) {
                            //    indModId = itemHtml.indexOf("[[MODULEID]]");
                            //    if (indModId != -1) {
                            itemHtml = itemHtml.replace("[[MODULEID]]", moduleId);
                            //    }
                            //}
                            //while (indPortalId != -1) {
                            //    indPortalId = itemHtml.indexOf("[[PORTALID]]");
                            //    if (indPortalId != -1) {
                            itemHtml = itemHtml.replace("[[PORTALID]]", portalId);
                            //    }
                            //}
                            //while (indArticleId != -1) {
                            //    indArticleId = itemHtml.indexOf("[[ARTICLEID]]");
                            //    if (indArticleId != -1) {
                            itemHtml = itemHtml.replace("[[ARTICLEID]]", articleID);
                            //    }
                            //}
                            itemHtml = itemHtml.replace("[[URLVIEW]]", document.UrlView);

                            itemsHtml += itemHtml;
                        }
                    }
                    
                }
            }

            if (pagesCount > 1) {
                var paginador = "<tr class='col-md-12 paginador'><td colspan='3'><table><tbody><tr>";
                for (var p = 0; p < pagesCount; p++) {
                    if (p != pageIndex) {
                        paginador += "<td><a style='cursor:pointer;' onclick='searchData(" + categoryId + "," + p + ")'>" + (p + 1) + "</a></td>";
                    }
                    else {
                        paginador += "<td><span>" + (p + 1) + "</span></td>";
                    }
                }
                paginador += "</tr></tbody></table></td></tr>";
                itemsHtml += paginador;
            }

            itemsHtml += " </tbody></table>";
        }
        return itemsHtml;
    }

    function searchData(filter, pageIndex) {

        $("#items" + filter).empty();

        var itemsFilter = $.grep(items, function (v) {
            return v.Article.CategoryID === parseInt(filter);
        });

        var countItems = itemsFilter.filter(f => f.Document.FilePath != null && f.Document.UrlView != "" && f.Document.UrlView != null);

        var pages = Math.ceil(countItems.length / 10);

        var itemsHtml = getItemsHtml(filter, itemsFilter, pageIndex, pages);
        $("#items" + filter).append(itemsHtml);
        $("#items" + filter).append();
    }

    function clickAccordion(e) {
        var hash = e.hash;
        if (!hash) return;
        for (var i = 0; i < $('.panel-collapse').length; i++) {
            var item = $('.panel-collapse')[i];

            if ($("#" + item.id).attr("data-relevante") != "relevante" + currentCategoryID) {
                $("#" + item.id).removeClass("show");
            }
        }

        $(".panel.panel-default").removeClass("opcion-activa-informacion");

        if (!$("[data-relevante='relevante" + currentCategoryID + "']").hasClass("show")) {
            $(".panel-default-relevante" + currentCategoryID).addClass("opcion-activa-informacion");
        }
    }

    function htmlEncode(value) {
        return $('<div/>').text(value).html();
    }

    function htmlDecode(value) {
        return $('<div/>').html(value).text();
    }

    function loadData() {
        $(".divYears").empty();

        var data = <%=InfoRelevanteArray%>;
        

      <%--  $.ajax({

            type: "GET",
            url: "/DesktopModules/InfoRelevanteAjax2/GetInfoRelevante?cultureCountry=<%=CultureCountry%>&category=Info Relevante",
            contentType: "application/json;charset=utf-8",
            data: {},
            dataType: "json",
            success: function (data) {--%>

                if (data) {

                    if (data.Items && data.Items.length > 0) {
                        items = data.Items;
                    }

                    if (data.CategoriesItems && data.CategoriesItems.length > 0) {

                        categoriesItems = data.CategoriesItems;

                        for (var i = 0; i < categoriesItems.length; i++) {
                            var category = categoriesItems[i];

                            if (items && items.length > 0) {

                                var itemsByCategory = $.grep(items, function (v) {
                                    return v.Article.CategoryID === category.CategoryID;
                                });

                                var items_filter_category = itemsByCategory.filter(x => x.CategoryID == category.CategoryID);
                                var countItems =items_filter_category.filter(f => f.Document.FilePath != null && f.Document.UrlView != "" && f.Document.UrlView != null);

                                var pages = Math.ceil(countItems.length / 10);

                                var itemsHtml = getItemsHtml(category.CategoryID, countItems, 0, pages);
                            }

                            var categoryHtml = getTemplateYear(category.CategoryID, category.CategoryName, itemsHtml, countItems);
                            $(".divYears").append(categoryHtml);
                        }
                    }
                }
        //    },
        //    error: function (result) {
        //        console.log("error", result);
        //    }
        //});
    }    
</script>
<div id="accordion" class="divYears collapse-conatiner mb-3">
</div>
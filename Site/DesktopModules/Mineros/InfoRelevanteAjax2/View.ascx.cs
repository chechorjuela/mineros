﻿/*
' Copyright (c) 2018  Mineros.co
'  All rights reserved.
' 
' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
' TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
' THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
' CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
' DEALINGS IN THE SOFTWARE.
' 
*/

using System;
using DotNetNuke.Security;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Localization;
using Mineros.Data.Models;
using System.Collections.Generic;
using Mineros.Data;
using System.Linq;
using DotNetNuke.Services.Log.EventLog;
using DotNetNuke.Entities.Portals;
using DotNetNuke.Entities.Users;
using System.Globalization;

using DotNetNuke.Common.Utilities;
using Mineros.API.App.Controllers;

namespace Mineros.InfoRelevanteAjax2InfoRelevanteAjax2
{
    /// -----------------------------------------------------------------------------
    /// <summary>
    /// The View class displays the content
    /// 
    /// Typically your view control would be used to display content or functionality in your module.
    /// 
    /// View may be the only control you have in your project depending on the complexity of your module
    /// 
    /// Because the control inherits from InfoRelevanteAjax2ModuleBase you have access to any custom properties
    /// defined there, as well as properties from DNN such as PortalId, ModuleId, TabId, UserId and many more.
    /// 
    /// </summary>
    /// -----------------------------------------------------------------------------
    public partial class View : InfoRelevanteAjax2ModuleBase, IActionable
    {
        public string CultureCountry
        {
            get
            {
                CultureInfo culture2 = CultureInfo.CurrentCulture;
                return culture2.Name ?? "es-ES";
            }
        }

        public string InfoRelevanteArray
        {
            get
            {
                return (string)ViewState["InfoRelevanteArray"];
            }
            set
            {
                ViewState["InfoRelevanteArray"] = value;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            TraceController service = new TraceController();
            try
            {
                if (Settings["Category"] == null)
                {
                    Settings["Category"] = "Info Relevante";
                }

                var infoRelevante = service.GetInfoRelevante(CultureCountry, Settings["Category"].ToString().ToLower());

                var infoJson = Json.Serialize(infoRelevante);

                InfoRelevanteArray = infoJson;
            }
            catch (Exception exc) //Module failed to load
            {                Exceptions.ProcessModuleLoadException(this, exc);
            }
        }

        public ModuleActionCollection ModuleActions
        {
            get
            {
                var actions = new ModuleActionCollection
                    {
                        {
                            GetNextActionID(), Localization.GetString("EditModule", LocalResourceFile), "", "", "",
                            EditUrl(), false, SecurityAccessLevel.Edit, true, false
                        }
                    };
                return actions;
            }
        }

        public InfoRelevanteResult GetInfoRelevante(string cultureCountry, string category)
        {
            InfoRelevanteResult result = new InfoRelevanteResult();
            int countItems = 0;

            EasyDnnNewsData easyDnnNewsData = new EasyDnnNewsData();
            List<InfoRelevanteViewItem> Items = new List<InfoRelevanteViewItem>();
            try
            {
                string tabName = "Información relevante";
                var Categories = easyDnnNewsData.GetAllCategoriesNews(cultureCountry);
                var parentCategory = Categories.FirstOrDefault(s => s.CategoryName.ToLower() == category.ToLower());
                if (parentCategory != null)
                {
                    var Articles = easyDnnNewsData.GetArticles(parentCategory.CategoryID, null, 0, 0, ref countItems, cultureCountry).OrderByDescending(s => s.PublishDate);

                    foreach (var article in Articles)
                    {
                        var documents = easyDnnNewsData.GetALLEasyDnnNewsDocuments(article.ArticleID, cultureCountry, tabName);
                        Articles articleView = ConvertArticleToArticleView(article);

                        var document = documents.FirstOrDefault(x => x.ArticleID == article.ArticleID && x.LocaleCode.ToLower() == cultureCountry.ToLower());

                        Documents documentView = ConvertDocumentToDocumentView(document);

                        InfoRelevanteViewItem item = new InfoRelevanteViewItem()
                        {
                            ArticleID = article.ArticleID,
                            CategoryID = article.CategoryID,
                            Title = article.Title,
                            PublishDate = article.PublishDate,
                            Article = articleView,
                            Document = documentView
                        };
                        Items.Add(item);
                    }

                    var categoriesItems = (from ca in Articles.Select(s => s.CategoryID).Distinct() select new EasyDnnNewsCategories() { CategoryID = ca, CategoryName = Categories.First(s => s.CategoryID == ca).CategoryName }).Distinct().OrderByDescending(s => s.CategoryName).ToList();

                    List<Categories> categoriesList = new List<Categories>();
                    foreach (var item in categoriesItems)
                    {
                        Categories ca = ConvertCategoryToCategoryView(item);
                        categoriesList.Add(ca);
                    }
                    result.CategoriesItems = categoriesList;
                    result.Items = Items;
                }
                return result;
            }
            catch (Exception ex)
            {
                SendLog("Error Service GetInfoRelevante " + ex.ToString());
                return new InfoRelevanteResult();
            }
        }

        public class InfoRelevanteResult
        {
            public List<Categories> CategoriesItems { get; set; }
            public List<InfoRelevanteViewItem> Items { get; set; }
        }

        public class InfoRelevanteViewItem
        {
            public int ArticleID { get; set; }
            public int CategoryID { get; set; }
            public string Title { get; set; }
            public DateTime PublishDate { get; set; }
            public Articles Article { get; set; }
            public Documents Document { get; set; }
        }

        private void SendLog(string msn)
        {
            EventLogController eventLog = new EventLogController();
            LogInfo logInfo = new LogInfo();
            PortalSettings ps = PortalController.Instance.GetCurrentPortalSettings();
            UserInfo userInfo = UserController.Instance.GetCurrentUserInfo();
            logInfo.LogUserID = userInfo.UserID;
            logInfo.LogPortalID = ps.PortalId;
            logInfo.LogTypeKey = EventLogController.EventLogType.ADMIN_ALERT.ToString();
            logInfo.AddProperty("Mineros Inversionistas", msn);
            eventLog.AddLog(logInfo);
        }

        private Articles ConvertArticleToArticleView(EasyDnnNewsApp article)
        {
            Articles articleView = new Articles();
            articleView.Article = article.Article;
            articleView.ArticleID = article.ArticleID;
            articleView.ArticleImage = article.ArticleImage;
            articleView.Category = article.Category;
            articleView.CategoryID = article.CategoryID;
            articleView.CategoryName = article.CategoryName;
            articleView.DateAdded = article.DateAdded;
            articleView.Featured = article.Featured;
            articleView.LangArticle = article.LangArticle;
            articleView.LangLocaleCode = article.LangLocaleCode;
            articleView.LangLocaleString = article.LangLocaleString;
            articleView.LangSubTitle = article.LangSubTitle;
            articleView.LangSummary = article.LangSummary;
            articleView.LangTitle = article.LangTitle;
            articleView.PortalID = article.PortalID;
            articleView.PublishDate = article.PublishDate;
            articleView.SubTitle = article.SubTitle;
            articleView.Summary = article.Summary;
            articleView.Title = article.Title;
            return articleView;
        }
        private Documents ConvertDocumentToDocumentView(EasyDnnNewsDocuments document)
        {
            Documents documentView = new Documents();
            if (document != null)
            {
                documentView.ArticleID = document.ArticleID;
                documentView.DateUploaded = document.DateUploaded;
                documentView.Description = document.Description;
                documentView.DocEntryID = document.DocEntryID;
                documentView.FileExtension = document.FileExtension;
                documentView.FileName = document.FileName;
                documentView.FilePath = document.FilePath;
                documentView.LocaleCode = document.LocaleCode;
                documentView.ModuleId = document.ModuleId;
                documentView.PortalId = document.PortalId;
                documentView.Title = document.Title;
                documentView.UrlView = document.UrlView;
                documentView.UserID = document.UserID;
                documentView.Visible = document.Visible;
            }
            return documentView;
        }
        private Categories ConvertCategoryToCategoryView(EasyDnnNewsCategories item)
        {
            Categories ca = new Categories();
            ca.CategoryID = item.CategoryID;
            ca.CategoryName = item.CategoryName;
            ca.Description = item.Description;
            ca.Level = item.Level;
            ca.ParentCategory = item.ParentCategory;
            ca.PortalID = item.PortalID;
            ca.Position = item.Position;
            return ca;
        }
    }

    public class Categories
    {
        public int CategoryID { get; set; }
        public int PortalID { get; set; }
        public string CategoryName { get; set; }
        public int Position { get; set; }
        public int? ParentCategory { get; set; }
        public int Level { get; set; }

        public string Description { get; set; }


    }

    public class Articles
    {
        public int ArticleID { get; set; }
        public int PortalID { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string Summary { get; set; }
        public string Article { get; set; }
        public string ArticleImage { get; set; }
        public DateTime DateAdded { get; set; }
        public DateTime PublishDate { get; set; }
        public string Category { get; set; }
        public int CategoryID { get; set; }
        public string CategoryName { get; set; }
        public bool Featured { get; set; }

        // English
        public string LangTitle { get; internal set; }
        public string LangSubTitle { get; internal set; }
        public string LangSummary { get; internal set; }
        public string LangArticle { get; internal set; }
        public string LangLocaleCode { get; internal set; }
        public string LangLocaleString { get; internal set; }
    }

    public class Documents
    {
        public int ArticleID { get; set; }
        public int DocEntryID { get; set; }
        public int UserID { get; set; }
        public DateTime DateUploaded { get; set; }
        public string FilePath { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string FileName { get; set; }
        public bool Visible { get; set; }
        public string FileExtension { get; set; }
        public int? ModuleId { get; set; }
        public int PortalId { get; set; }
        public string LocaleCode { get; set; }
        public string UrlView { get; set; }
    }
}
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="View.ascx.cs" Inherits="DesktopModules_Mineros_CertficateAdminDECEVALModule_View" %>


<!-- Wrapper -->
<div class="wrapper">

    <section class="container-fluid inner-page">

        <div class="row">

            <div class="col-xl-6 offset-xl-3 col-lg-6 offset-lg-3 col-md-12 full-dark-bg">
                <form></form>
                <!-- Files section -->
                <h4 class="section-sub-title"><span>Subir</span> tus documentos</h4>

                <form class="fields">
                 <%--   <div class="form-group">
                        <input class="form-control" type="text" name="nit" placeholder="*Número de identificación o NIT"/>
                    </div>
                     <div class="form-group">
                         <input class="form-control" type="text" name="codeDeceval" placeholder="*Número cuenta DECEVAL"/>
                    </div>
                     <div class="form-group">
                    </div>--%>
                </form>
                <form action="" method="post" enctype="multipart/form-data" class="dropzone files-container dz-clickable">
                    <div class="fallback">
                        <input name="file" type="file" multiple />
                    </div>
                </form>
                <!-- Notes -->
                <span>Solamente PDF tipos de archivos son soportados.</span>
                <!-- Uploaded files section -->
                <h4 class="section-sub-title"><span>Archivos</span> seleccionados(<span class="uploaded-files-count">0</span>)</h4>
                <span class="no-files-uploaded">No hay archivos cargados todavía.</span>
                <!-- Preview collection of uploaded documents -->
                <div class="preview-container dz-preview uploaded-files">
                    <div id="previews">
                        <div id="onyx-dropzone-template">
                            <div class="onyx-dropzone-info">
                                <div class="thumb-container">
                                    <img data-dz-thumbnail />
                                </div>
                                <div class="details">
                                    <div>
                                        <span data-dz-name></span><span data-dz-size></span>
                                    </div>
                                    <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
                                    <div class="dz-error-message"><span data-dz-errormessage></span></div>
                                    <div class="actions">
                                        <a href="#!" data-dz-remove>x</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary pull-left send-files">Subir</button>
                <!-- Warnings -->
                <div id="warnings">
                    <span>Warnings will go here!</span>
                </div>
            </div>
        </div>
        <!-- /End row -->
    </section>
</div>
<style>
    .error{
        color:#f96969;
    }
    .dz-error-message span{
        color:#f96969 !important;
        font-weight:600;
    }
</style>
<!-- /Wrapper -->
<link rel="stylesheet" type="text/css" href="/DesktopModules/Mineros/CertficateAdminDECEVALModule/css/style.css" />
<script src="/DesktopModules/Mineros/scripts/libs/jquery-1.10.2.min.js"></script>
<script src="/DesktopModules/Mineros/scripts/libs/jquery.validate.min.js"></script>
<script src="/DesktopModules/Mineros/scripts/libs/dropzone.min.js"></script>
<script src="/DesktopModules/Mineros/CertficateAdminDECEVALModule/js/CertficateAdminDECEVALModule.js"></script>
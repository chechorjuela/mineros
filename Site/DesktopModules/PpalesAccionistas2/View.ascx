﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="View.ascx.cs" Inherits="Mineros.PpalesAccionistas2PpalesAccionistas2.View" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Panel runat="server" ID="pnlExport">
    <%--<asp:DropDownList runat="server" ID="ddlType" AutoPostBack="true" DataValueField="CategoryID" DataTextField="CategoryName" AppendDataBoundItems="true">
        <asp:ListItem Value="">Seleccione el tipo de acción</asp:ListItem>
    </asp:DropDownList>--%>
    <label runat="server" for="fileuploadExcel" cssclass="uploadExcel" id="lblUpload"></label>

    <asp:FileUpload ID="fileuploadExcel" runat="server" />


    <asp:Button ID="btnImport" runat="server" OnClick="btnImport_Click" />

    <label class="dnnFormMessage dnnFormWarning errorAlert" runat="server" id="lblError" visible="false">
        <%=LocalizeString("error.Text") %>
    </label>
</asp:Panel>